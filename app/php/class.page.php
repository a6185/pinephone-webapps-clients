<?php

define('ROOT',dirname(__FILE__,2));

/**
 * Global class called to create the output HTML page
 */

class Page {
	/**
	 * Construction
	 * @param  {string} path Module path
	 */
	public function __construct ($module_abspath) {
		$this->path = $module_abspath; // Module absolute path
		$this->css = []; // array of CSS links
		$this->js = []; // array of JS scripts
		// get all core css
		foreach (glob(ROOT.'/core/css/*.css') as $file) {
			$this->css[] = '		<link href="..'.str_replace(ROOT,'',$file).'" rel="stylesheet" type="text/css" />';
		}
		// get all core js
		$paths = [
			'/core/js/prototypes/*.js',
			'/core/js/fn/*.js',
			'/core/js/tpl/*.js',
			'/core/js/tpl/settings/*.js',
			'/core/js/gui/*.js',
			'/core/js/*.js',
			'/core/autoload/*/*.js'
		];
		foreach($paths as &$js_path) {
			foreach (glob(ROOT.$js_path) as $file) {
				$this->js[] = '		<script src="..'.str_replace(ROOT,'',$file).'" type="text/javascript"></script>';
			}
		}
	}	
	/**
	 * Output the HTML page
	 */
	public function output () {
		die('<!DOCTYPE html>
<html lang="fr" xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title class="manifest" data-key="title">Title</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no">
		<meta name="author" class="manifest" data-key="author_name" content=""/>
		<link rel="icon" class="manifest" data-key="icons_256" href="" />
		<!-- core css/js first -->
'.join("\n",$this->css).'		
'.join("\n",$this->js).'		
		<!-- local css/js after -->
'.file_get_contents($this->path.'/head.html').'
	</head>
	<body>
		<!-- TAB LOG -->
		<div id="tab_log" class="tab fixed" style="display:none">
			<div class="header">
				<span class="rounded" id="button_log_close">Close log</span>
			</div>
			<div class="body overflow" id="tab_log_events"></div>
		</div>
		<!-- TAB SPINNER -->
		<div id="tab_spinner_bg" class="tab fixed" style="display:none"></div>
		<div id="tab_spinner" class="tab fixed" style="display:none">
			<div class="spinner_container">
				<div class="spinner_circle"></div>
			</div>
			<div class="center" style="margin:90px auto 30px">
				<div id="spinner_welcome" class="manifest" data-key="spinner">
					/* cf. manifest.js */
				</div>
			</div>
			<div id="spinner_txt"></div>
		</div>
		<!-- TAB SELECTOR -->
		<div id="tab_selector" class="tab fixed">
			<div class="header">
				<div class="absleft">
					<span class="rounded menu icon_backward" id="button_selector_backward"></span>
				</div>
				<p id="label_selector">Selector</p>
			</div>
			<div id="research">
				<input type="text" name="selector_kws" id="selector_kws" placeholder="?" />
				<button type="button" id="selector_clear_kws">&#9003;</button>
			</div>
			<div class="body overflow rounded" style="top:91px">
				<input type="hidden" name="selected_target_id" id="selected_target_id" value="" />
				<ul id="selector" class="horiz overflow"></ul>
			</div>
		</div>
		<div class="manifest" data-key="internal" id="">
		</div>
		<!-- TAB SETTINGS -->
		<div id="tab_settings" class="tab fixed">
			<div class="header">
				<div class="absleft">
					<span class="rounded menu icon_backward" id="button_settings_close"></span>
				</div>
				<div class="absright">
					<span class="rounded menu icon_db_plus" id="button_settings_submit"></span>
				</div>
				<p id="label_settings">Settings</p>
			</div>
			<div class="body overflow with_padding">
				<div id="module_settings">
				</div>
				<div class="center manifest" data-key="infos" style="margin-top:30px">
					/* cf. manifest.js */
				</div>
			</div>
		</div>
'.file_get_contents($this->path.'/body.html').'
	</body>
</html>');		
	}	
}



