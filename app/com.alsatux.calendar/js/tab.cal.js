// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Calendar tab
 */

_Tab.cal = {
	debug: false,
	force_redraw_flag: false,
	counter: 0,
	current_year: 2015,
	/**
	 * Initialization
	 */
	init: () => {
		// fix current year by default
		let now = new Date();
		_Tab.cal.current_year = now.getFullYear();
		_GUI._DOM.listen('#cal', 'click', _Tab.cal.slot.click_on_day);
	},
	/**
	 * Slots (DOM events handlers)
	 */
	slot: {
		/**
		 * Handler on user click on a day
		 * @param  {event} e DOM click
		 */
		click_on_day: (e) => {
			return _GUI._Lock.action(e, (e) => {
				let o = e.target;
				while (o && o.classList && !o.classList.contains('day') && !o.classList.contains('event')) {
					o = o.parentNode;
				}
				if (o.nodeName && o.nodeName=='DIV') {
					if (o.classList.contains('day')) {
						// create a new event
						let day = o.id.replace('day_','');
						let new_event = new Event_Object();
						new_event.obj.date = day;
						_Tab.event.edit(new_event.obj);
					}
					if (o.classList.contains('event')) {
						// edit existing event
						let uid = o.getAttribute('data-event');
						_Tab.event.edit(_Module.tbl.events.obj[uid].obj);
					}
				}
			});
		}
	},
	/**
	 * Redraw current year without events
	 */
	redraw_current_year: () => {
		let y = _Tab.cal.current_year;
		_GUI._DOM.id('#main_current_year').innerHTML = y;
		let months = tr1('January,February,March,April,May,June,July,August,September,October,November,December').split(',');
		let days = tr1('S,M,T,W,T,F,S').split(',');
		let d = new Date(y,0,1); // 1st january
		let dn = d.getDay();
		let oldm = -1;
		let cal = '';
		while (d.getFullYear()==y) {
			if (d.getMonth()!=oldm) {
				cal += '<h3 class="month">' + months[d.getMonth()]+ '</h3>';
				oldm = d.getMonth();
			}
			let day = d.getDate();
			let yyyymmdd = d.to_yyyymmdd('-');
			let now = new Date();
			let today = now.to_yyyymmdd('-');
			cal += '<div class="day' + (dn==0 ? ' sunday' : '') + (today==yyyymmdd ? ' today' : '') + '" id="day_' + yyyymmdd + '">' + (day<10 ? '0' : '') + day + ' ' + days[dn] + '</div>';
			// <div class="events"></div>
			dn = ++dn%7;
			d = new Date(d.getFullYear(),d.getMonth(),day + 1);
		}
		let div_cal = _GUI._DOM.id('#cal');
		div_cal.innerHTML = cal;
	},
	/**
	 * Build the HTML div for a single event
	 * @param  {object} event Event as JS object
	 * @return {string}       HTML code
	 */
	get_html_event: (event) => {
		let line1 = '';
		let infos = [];
		let cat = _Module.tbl.cats.obj[event.cat_uid].obj;
		let title = ('title' in event ? event.title : ''); // optionnal
		if (event.time=='00:00' && event.duration=='24:00') {
			// force spacers to avoid empty cells (cell height will be too small)
			line1 = '<span class="allday">-&nbsp;' + _GUI._HTML.escape(title) + '&nbsp;-</span>';
		} else {
			line1 = '<span class="time">' + event.time + '</span> <span class="title">' + _GUI._HTML.escape(title) + '</span>';
			infos.push('<span class="duration">' + event.duration + '</span>');
		}
		if (cat.name.length && cat.uid!='00') {
			infos.push('<span class="cat">' + _GUI._HTML.escape(cat.name) +'</span>');
		}
		if (_Tab.cal.debug) {
			line1 += ' {' + event.rrule.freq + ',' + event.rrule.count + ',' + event.rrule.interval + ',' + event.rrule.until + '}';
		}

		let order = event.time.split(':');
		let hour = parseInt(order[0],10);
		let min = parseInt(order[1],10);
		// do not remove () in the following or you will experience problems !
		return '\
		<div class="event cat_' + cat.uid + '" id="cal_event_' + event.uid + '" data-event="' + event.uid + '" data-order="' + (hour*60+min) + '" data-hour="' + hour + '" data-min="' + min + '" >\
			 <p>' + line1 + '</p>\
			<p class="infos">' + (infos.length ? '[' + infos.join('/') + ']' : '') + '</p>\
		</div>';
	},
	/**
	 * Add a new event
	 * @param  {object} event   Event as JS object
	 * @param  {bool} reorder Force to reorder the events for the given day
	 */
	add: (event, reorder) => {
		// future repeat events should be filtered here
		// and should create new events for the current year only
		let repeat_events_dates = _RRule.filter(_Tab.cal.current_year, event);
		//let single_event = JSON.parse(JSON.stringify(event));
		for (let i=0;i<repeat_events_dates.length;i++) {
			let start_date = repeat_events_dates[i];
			let div_day = _GUI._DOM.id('#day_' + start_date);
			if (div_day!=null) {
				let html = _Tab.cal.get_html_event(event);
				div_day.insertAdjacentHTML('beforeend',html);
				if (reorder) {
					_Tab.cal.reorder_day(div_day);
				}
			}
		}
	},
	/**
	 * Delete an event with repeat events
	 * @param  {string} uid Event UID
	 */
	del: (uid) => {
		let divs = document.querySelectorAll('div[data-event="'+uid+'"]');
		for (let i=divs.length-1;i>=0;i--) {
			let div = divs[i];
			div.parentNode.removeChild(div);
		}
	},
	/**
	 * Reorder events for a day DIV
	 * @param  {DOM div} div_day DIV to reorder
	 */
	reorder_day: (div_day) => {
		// reorder the events
		let events = [...div_day.querySelectorAll('div.event[data-order]')];
		if (events.length>1) {
			events.sort( (a,b) => {
				return a.getAttribute('data-order')-b.getAttribute('data-order');
			}).forEach(event => {
				div_day.appendChild(event);
			})
		}
	},
	/**
	 * Reorder events for all events
	 */
	reorder_events: () => {
		[..._GUI._DOM.id('#cal').querySelectorAll('div.day')].forEach(div_day => {
			_Tab.cal.reorder_day(div_day);			
		});
	},
	/**
	 * Redraw the current year with events
	 */
	redraw: () => {
		let scrollTop = _GUI._DOM.id('#cal').parentNode.scrollTop;
		_Tab.cal.redraw_current_year();
		_Tab.cal.refresh_events();
		_GUI._DOM.id('#cal').parentNode.scrollTop = scrollTop;
		_Tab.cal.force_redraw_flag = false;
		_Tab.main.filter_events();
	},
	/**
	 * Refresh all events for the current year
	 */
	refresh_events: () => {
		_GUI._DOM.hide('#cal');
		_Module.tbl.cats.upd_style();
		for (let uid in _Module.tbl.events.obj) {
			_Tab.cal.add(_Module.tbl.events.obj[uid].obj, 0);
		}
		let nb_events_total = [..._GUI._DOM.id('#cal').querySelectorAll('div.event')].length;
		_GUI._Log.info(_Tab.cal.current_year + ': ' + nb_events_total + ' ' + tr1('events viewed') + '.');
		_Tab.cal.reorder_events();
		_GUI._DOM.show('#cal');
	}
};

