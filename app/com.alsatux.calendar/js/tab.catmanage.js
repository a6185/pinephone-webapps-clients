// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Categories manager tab
 */

_Tab.catmanage = {
	/**
	 * Initialization
	 */
	init: () => {
		_GUI._DOM.listen('#button_cat_manage_open', 'click', _Tab.catmanage.slot.open);
		_GUI._DOM.listen('#button_cat_manage_close', 'click', _Tab.catmanage.slot.close);
		_GUI._DOM.listen('#cat_manage_radio', 'click', _Tab.catmanage.slot.click_cat);
	},
	/**
	 * Slots (DOM events handlers)
	 */
	slot: {
		/**
		 * Open the tab
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		open: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tabs.open('tab_cat_manage');
				_Tab.catmanage.upd();
			});
		},
		/**
		 * Close the tab
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		close: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tabs.open('tab_cat');
			});
		},
		/**
		 * Edit a category
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		click_cat: (e) => {
			return _GUI._Lock.action(e, (e) => {
				let o = e.target;
				if (o.nodeName && o.nodeName=='DIV') {
					let uid = o.id.replace('cat_','');
					_GUI._Log.info(tr1('Editing category') + ' : ' + _Module.tbl.cats.obj[uid].obj.name);
					_Tab.cateditor.edit(_Module.tbl.cats.obj[uid].obj)
				}
			});
		}
	},
	/**
	 * When returning from category editor, redraw all categories
	 */
	upd: () => {
		_GUI._DOM.id('#cat_manage_radio').innerHTML = '';
		let cat_manage_radio = _GUI._DOM.id('#cat_manage_radio');
		for (let uid in _Module.tbl.cats.obj) {
			let cat = _Module.tbl.cats.obj[uid].obj;
			let div = document.createElement('div');
			div.id = 'cat_' + uid;
			div.style.color = cat.color;
			div.style.background = _Tab.cat.get_css_background(cat);
			div.className = cat.styles;
			div.innerHTML = cat.name;
			cat_manage_radio.appendChild(div);
		}
	}
};
