// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Main tab
 */

_Tab.main = {
	/**
	 * Initialization
	 */
	init: () => {
		_GUI._DOM.listen('#main_prev_year', 'click', _Tab.main.slot.click_prev_year);
		_GUI._DOM.listen('#main_next_year', 'click', _Tab.main.slot.click_next_year);
		_GUI._DOM.listen('#main_goto_today', 'click', _Tab.main.slot.click_today);
		_GUI._DOM.listen('#main_goto_month', 'click', _Tab.month.slot.open);
		_GUI._DOM.listen('#button_settings_open', 'click', _Tab.settings.slot.open);
		// no onchange() on input_keywords_filter please (loop) ...
		_GUI._DOM.listen('#button_filter_submit', 'click', _Tab.main.slot.filter_submit);
		_GUI._DOM.listen('#button_filter_reset', 'click', _Tab.main.slot.filter_reset);
		_GUI._DOM.listen('#button_filter_open', 'click', _Tab.filter.slot.open);
	},
	/**
	 * Slots (DOM events handlers)
	 */
	slot: {
		/**
		 * Go to previous year
		 * @param  {event} e DOM click
		 * @return {function}   GUI lock action
		 */
		click_prev_year: (e) => {
			return _GUI._Lock.action_with_spinner(e, (e) => {
				_Tab.cal.current_year--;
				_Tab.cal.redraw();
			});
		},
		/**
		 * Go to next year
		 * @param  {event} e DOM click
		 * @return {function}   GUI lock action
		 */
		click_next_year: (e) => {
			return _GUI._Lock.action_with_spinner(e, (e) => {
				_Tab.cal.current_year++;
				_Tab.cal.redraw();
			});
		},
		/**
		 * Go to current date
		 * @param  {event} e DOM click
		 * @return {function}   GUI lock action
		 */
		click_today: (e) => {
			let now = new Date();
			if (_Tab.cal.current_year!=now.getFullYear()) {
				return _GUI._Lock.action_with_spinner(e, (e) => {
					_Tab.main.scroll_today();
				});
			} else {
				return _GUI._Lock.action(e, (e) => {
					_Tab.main.scroll_today();
				});
			}
		},
		/**
		 * Apply filter
		 * @param  {event} e DOM click
		 */
		filter_submit: (e) => {
			_Tab.main.filter_events(1);
		},
		/**
		 * Reset filter
		 * @param  {event} e DOM click
		 */
		filter_reset: (e) => {
			_GUI._DOM.id('#input_keywords_filter').value = '';
			_Tab.main.remove_events_filtered();
		}
	},
	/**
	 * Clean keywords
	 * @param  {string} keywords Keywords to use
	 * @return {array}          Array of cleaned keywords
	 */
	filter_clean_keywords: (keywords) => {
		let res = [];
		let re = /\s+/;
		let kws = keywords.split(re);
		let kws2 = [];
		for (let i=0;i<kws.length;i++) {
			let kw = kws[i].trim();
			if (kw.length) {
				kws2.push(kw);
			}
		}
		return kws2;
	},
	/**
	 * Read and apply filter keywords
	 * @param  {bool} add_keywords_as_new_filter If true, record filter to the server
	 */
	filter_events: (add_keywords_as_new_filter) => {
		let keywords = _GUI._DOM.id('#input_keywords_filter').value;
		let kws2 = _Tab.main.filter_clean_keywords(keywords);
		if (kws2.length) {
			if (add_keywords_as_new_filter) {
				let filter = {
					'uid': '',
					'value': kws2.join(' ')
				};
				// send new filter to the server
				_Signals.trigger('filters', 'add', {
					'filter': filter
				});
			}
			_Tab.main.add_events_filtered(kws2);
		} else {
			_Tab.main.remove_events_filtered();
		}
	},
	/**
	 * Filter events using keywords
	 * @param  {array} kws2 Array of keywords
	 */
	add_events_filtered: (kws2) => {
		_Tab.main.remove_events_filtered();
		let pattern = '(' + kws2.join('|') + ')';
		let re2 = new RegExp('.*' + pattern + '.*','gi');
		let days = _GUI._DOM.id('#cal').querySelectorAll('div.day');
		for (let i=0;i<days.length;i++) {
			let day = days[i];
			let events = day.querySelectorAll('div.event');
			if (events.length==0) {
				day.classList.add('filtered');
			} else {
				let nb_filtered = 0;
				for (let j=0;j<events.length;j++) {
					let event = events[j];
					let found = false;
					/* search into title */
					let title = event.querySelector('span.title');
					if (title!=null) {
						if (title.innerHTML.match(re2)) {
							found = true;
						}
					}
					/* search into category */
					let cat = event.querySelector('span.cat');
					if (cat!=null) {
						if (cat.innerHTML.match(re2)) {
							found = true;
						}
					}
					/* search into description */
					let uid = event.getAttribute('data-event');
					let data = _Module.tbl.events.obj[uid].obj;
					if (data.description) {
						if (data.description.match(re2)) {
							found = true;
						}
					}
					/* not found -> add class filtered */
					if (!found) {
						event.classList.add('filtered');
						nb_filtered++;
					}
				}
				/* all event filtered for the current day -> add class filtered */
				if (nb_filtered==events.length) {
					day.classList.add('filtered');
				}
			}
		}
	},
	/**
	 * Remove filter events (show all events)
	 */
	remove_events_filtered: () => {
		let objs = _GUI._DOM.id('#cal').querySelectorAll('.filtered');
		for (let i=0;i<objs.length;i++) {
			objs[i].classList.remove('filtered');
		}
	},
	/**
	 * Scroll calendar to today
	 */
	scroll_today: () => {
		let now = new Date();
		if (_Tab.cal.current_year!=now.getFullYear()) {
			_Tab.cal.current_year = now.getFullYear();
			_Tab.cal.redraw();
		} else {
			let div_today = document.querySelectorAll('div.today')[0];
			if (div_today.id!='day_' + now.to_yyyymmdd('-')) {
				div_today.classList.remove('today');
			}
		}
		let div_now = _GUI._DOM.id('#day_' + now.to_yyyymmdd('-'));
		_GUI._DOM.id('#cal').parentNode.scrollTop = div_now.offsetTop;
		if (!div_now.classList.contains('today')) {
			div_now.classList.add('today');
		}
	}
};

