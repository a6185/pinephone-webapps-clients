// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Catégory editor tab
 */

_Tab.cateditor = {
	mouse: {
		drag: false
	},
	/**
	 * Initialization
	 */
	init: () => {
		// opener
		_GUI._DOM.listen('#button_cat_editor_add', 'click', _Tab.cateditor.slot.add);
		// GUI objects
		_GUI._DOM.listen('#button_cat_editor_delete', 'click', _Tab.cateditor.slot.del);
		_GUI._DOM.listen('#button_cat_editor_submit', 'click', _Tab.cateditor.slot.submit);
		_GUI._DOM.listen('#cat_editor_styles', 'click', _Tab.cateditor.slot.click_style);
		_GUI._DOM.listen('#cat_editor_bg_type_radio', 'click', _Tab.cateditor.slot.click_bg_type);
		_GUI._DOM.listen('#cat_editor_color', 'click', _Tab.cateditor.slot.set_color);
		_GUI._DOM.listen('#cat_editor_bg_from', 'click', _Tab.cateditor.slot.set_color);
		_GUI._DOM.listen('#cat_editor_bg_to', 'click', _Tab.cateditor.slot.set_color);
		// touch events for angle
		_GUI._DOM.listen('#cat_editor_circle_black','touchstart', _Tab.cateditor.slot.angle_down);
		_GUI._DOM.listen('#cat_editor_circle_black','touchend', _Tab.cateditor.slot.angle_up);
		_GUI._DOM.listen('#cat_editor_circle_black','touchmove', _Tab.cateditor.slot.angle_move);
		// mouse events for angle
		_GUI._DOM.listen('#cat_editor_circle_black','mousedown', _Tab.cateditor.slot.angle_down);
		_GUI._DOM.listen('#cat_editor_circle_black','mouseup', _Tab.cateditor.slot.angle_up);
		_GUI._DOM.listen('#cat_editor_circle_black','mousemove', _Tab.cateditor.slot.angle_move);
	},
	/**
	 * Slots (DOM events handlers)
	 */
	slot: {
		/**
		 * Add a new category
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		add: (e) => {
			return _GUI._Lock.action(e, (e) => {
				let cat = new Cat_Object();
				_Tab.cateditor.edit(cat.obj);
			});
		},
		/**
		 * Delete a category (except default one)
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		del: (e) => {
			return _GUI._Lock.action(e, (e) => {
				let cat_uid = _GUI._DOM.id('#cat_editor_uid').value;
				if (cat_uid=='00') {
					alert(tr1('Unable to delete default category') + tr1('!'));
				} else {
					if (confirm(tr1('Delete this category ?'))) {
						if (_Module.tbl.cats.obj.hasOwnProperty(cat_uid)) {
							_Signals.trigger('cats', 'del', {
								'cat': _Module.tbl.cats.obj[cat_uid].obj
							});	
						}
						_Tab.cateditor.hide();
					}
				}
			});
		},
		/**
		 * Add or update a category
		 * @param  {event} e DOM event submit
		 * @return {function}   GUI lock action
		 */
		submit: (e) => {
			return _GUI._Lock.action(e, (e) => {
				let cat_uid = _GUI._DOM.id('#cat_editor_uid').value;
				if (cat_uid=='00') {
					alert(tr1('Unable to change default category') + tr('!'));
					_Tab.cateditor.hide();
				} else {
					let styles = [];
					'italic,bold,underline'.split(',').forEach(style => {
						if (_GUI._DOM.id('#cat_editor_' + style).classList.contains('selected')) {
							styles.push(style);	
						}
					})
					let cat = {
						'uid': _GUI._DOM.id('#cat_editor_uid').value,
						'name': _GUI._DOM.id('#cat_editor_name').value,
						'color': _GUI._DOM.id('#cat_editor_color').innerHTML,
						'bg_type': _GUI._DOM.id('#cat_editor_bg_type').value,
						'bg_from': _GUI._DOM.id('#cat_editor_bg_from').innerHTML,
						'bg_to': _GUI._DOM.id('#cat_editor_bg_to').innerHTML,
						'bg_angle': _GUI._DOM.id('#cat_editor_bg_angle').innerHTML,
						'styles': styles.join(' ')
					}
					// send new category to the server
					if (cat.uid=='') {
						_Signals.trigger('cats', 'add', {
							'cat': cat
						});
					} else {
						_Signals.trigger('cats', 'upd', {
							'cat': cat
						});
					}
				}
			});
		},
		/**
		 * Flip flop button style
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		click_style: (e) => {
			return _GUI._Lock.action(e, (e) => {
				let o = e.target;
				if (o.nodeName && o.nodeName=='SPAN') {
					// flip flop button style
					let cn = o.getAttribute('data-value');
					_Tab.cateditor.set_button(cn, !o.classList.contains('selected'));
					_Tab.cateditor.upd_preview();
				}
			});
		},
		/**
		 * Set background
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		click_bg_type: (e) => {
			return _GUI._Lock.action(e, (e) => {
				let o = e.target;
				if (o.nodeName && o.nodeName=='SPAN') {
					_Tab.cateditor.set_bg_type(o.getAttribute('data-value').toLowerCase());
					_Tab.cateditor.upd_preview();
				}
			});
		},
		/**
		 * Set color
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		set_color: (e) => {
			return _GUI._Lock.action(e, (e) => {
				let o = e.target;
				_Tab.colors.open(o.innerHTML, (value) => {
					o.innerHTML = value;
					_Tab.cateditor.upd_preview();
				});
			});
		},
		/**
		 * Detect mouse down on angular selector
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		angle_down: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tab.cateditor.mouse.drag = true;
			});
		},
		/**
		 * Detect mouse up on angular selector
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		angle_up: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tab.cateditor.mouse.drag = false;
			});
		},
		/**
		 * Detect mouse move on angular selector
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		angle_move: (e) => {
			return _GUI._Lock.action(e, (e) => {
				let mousex, mousey;
				let theta = 0;
				let cb = _GUI._DOM.id('#cat_editor_circle_black');
				let pos = _Tab.cateditor.angle_position(cb);
				//console.log(e.touches[0].clientX+','+e.touches[0].clientY);
				//console.log('e.clientX='+e.touches[0].clientX+' e.clientY='+e.touches[0].clientY+' pos.x='+pos.x+'pos.y='+pos.y);
				if (e.touches !== undefined) {
					mousex = e.touches[0].clientX - pos.x;
					mousey = e.touches[0].clientY - pos.y;
				} else {
					mousex = e.clientX - pos.x;
					mousey = e.clientY - pos.y;
				}
				//console.log('mousex='+mousex+' mousey='+mousey);
				if (_Tab.cateditor.mouse.drag) {
					let xc = cb.offsetWidth/2;
					let yc = cb.offsetHeight/2;
					let mx = mousex - xc;
					let my = yc - mousey;
					//console.log('xc='+xc+' yc='+yc+' mx='+mx+' my='+my);
					if (mx==0) {
						if (my>0)
							theta = Math.PI/2;
						if (my<0)
							theta = -Math.PI/2;
					} else {
						if (mx>0)
							theta = Math.atan(my/mx);
						if (mx<0) {
							if (my>0)
								theta = Math.PI+Math.atan(my/mx);
							if (my<0)
								theta = -Math.PI+Math.atan(my/mx);
						}
					}
					_Tab.cateditor.angle_set(theta);
				}
			});
		}
	},
	/**
	 * Detect position offset on angular selector
	 * @param  {element} el DOM element
	 * @return {object}    Position offset
	 */
	angle_position: (el) => {
		let x = 0;
		let y = 0;
		while (el) {
			x += (el.offsetLeft - el.scrollLeft + el.clientLeft);
			y += (el.offsetTop - el.scrollTop + el.clientTop);
			el = el.offsetParent;
		}
		return { x: x, y: y };
	},
	/**
	 * Place the circle of the angular selector
	 * @param  {number} theta Angle
	 */
	angle_set: (theta) => {
		let cb = _GUI._DOM.id('#cat_editor_circle_black');
		let cw = _GUI._DOM.id('#cat_editor_circle_white');
		let d = cb.offsetWidth/2-cw.offsetWidth/2-3;
		let xc = cb.offsetWidth/2;
		let yc = cb.offsetHeight/2;
		let deg = 5*Math.round((theta*180/Math.PI)/5);
		_GUI._DOM.id('#cat_editor_bg_angle').innerHTML = deg;
		theta = deg*Math.PI/180;
		cw.style.left = parseInt(xc + d*Math.cos(theta) - cw.offsetWidth/2,10)+'px';
		cw.style.top = parseInt(yc - d*Math.sin(theta) - cw.offsetHeight/2,10)+'px';
		_Tab.cateditor.upd_preview();
	},
	/**
	 * Return to tab category management
	 */
	hide: () => {
		_Tab.cateditor.mouse.drag = false;
		_Tabs.open('tab_cat_manage');
		_Tab.catmanage.upd();
	},
	/**
	 * Edit a category
	 * @param  {object} cat Category as JS object
	 */
	edit: (cat) => {
		_Tabs.open('tab_cat_editor');
		_GUI._DOM.id('#cat_editor_uid').value = cat.uid;
		_GUI._DOM.id('#cat_editor_name').value = cat.name;
		_GUI._DOM.id('#cat_editor_color').innerHTML = cat.color;
		_GUI._DOM.id('#cat_editor_bg_from').innerHTML = cat.bg_from;
		_GUI._DOM.id('#cat_editor_bg_to').innerHTML = cat.bg_to;
		_GUI._DOM.id('#cat_editor_bg_angle').innerHTML = cat.bg_angle;
		_Tab.cateditor.angle_set(parseInt(cat.bg_angle,10)*Math.PI/180);
		// bold, italic, underline
		_Tab.cateditor.set_button('italic', cat.styles.indexOf('italic')!=-1);
		_Tab.cateditor.set_button('bold', cat.styles.indexOf('bold')!=-1);
		_Tab.cateditor.set_button('underline', cat.styles.indexOf('underline')!=-1);
		// background type
		_Tab.cateditor.set_bg_type(cat.bg_type);
		// fg after bg...
		_Tab.cateditor.upd_preview();
	},
	/**
	 * Flip flop a checkbox button
	 * @param  {string} id       Category UID
	 * @param  {bool} selected True if button must be selected
	 */
	set_button: (id, selected) => {
		let o = _GUI._DOM.id('#cat_editor_' + id);
		if (selected) {
			if (!o.classList.contains('selected'))
				o.classList.add('selected');
		} else {
			if (o.classList.contains('selected'))
				o.classList.remove('selected');
		}
	},
	/**
	 * Update the HTML for the category currently edited
	 */
	upd_preview: () => {
		// update name field
		let f = _GUI._DOM.id('#cat_editor_name');
		f.style.color = _GUI._DOM.id('#cat_editor_color').innerHTML;
		let bg =  _Tab.cat.get_css_background({
			'bg_type': _GUI._DOM.id('#cat_editor_bg_type').value,
			'bg_from': _GUI._DOM.id('#cat_editor_bg_from').innerHTML,
			'bg_to': _GUI._DOM.id('#cat_editor_bg_to').innerHTML,
			'bg_angle': _GUI._DOM.id('#cat_editor_bg_angle').innerHTML
		});
		f.style.background = bg;
		// set styles
		let cstyle = document.querySelectorAll('span.cstyle');
		for (let i=0;i<cstyle.length;i++) {
			let cn = cstyle[i].getAttribute('data-value');
			if (_GUI._DOM.id('#cat_editor_' + cn).classList.contains('selected')) {
				if (!f.classList.contains(cn))
					f.classList.add(cn);
			} else {
				if (f.classList.contains(cn))
					f.classList.remove(cn);
			}
		}
	},
	/**
	 * Display or hide the background CSS options
	 * @param  {string} value Category background type
	 */
	set_bg_type: (value) => {
		let ul = _GUI._DOM.id('#cat_editor_bg_type_radio');
		let lis = ul.querySelectorAll('li');
		// unselect all
		_GUI._CSS.resetClassName(lis);
		// select by value
		for (let i=0;i<lis.length;i++) {
			if (lis[i].getAttribute('data-value').toLowerCase()==value.toLowerCase())
				lis[i].className = 'selected';
		}
		// update colors and angle
		_GUI._DOM.id('#cat_editor_bg_type').value = value.toLowerCase();
		if (value.toLowerCase()=='transparent') {
			_GUI._DOM.hide('#cat_editor_block_from');
			_GUI._DOM.hide('#cat_editor_block_to');
			_GUI._DOM.hide('#cat_editor_block_angle');
		}
		else if (value.toLowerCase()=='color') {
			_GUI._DOM.show('#cat_editor_block_from');
			_GUI._DOM.hide('#cat_editor_block_to');
			_GUI._DOM.hide('#cat_editor_block_angle');
		}
		else { // gradient
			_GUI._DOM.show('#cat_editor_block_from');
			_GUI._DOM.show('#cat_editor_block_to');
			_GUI._DOM.show('#cat_editor_block_angle');
		}
	}
};
