// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Filter delete tab
 */

_Tab.filterdelete = {
	/**
	 * Initialization
	 */
	init: () => {
		_GUI._DOM.listen('#button_filter_manage_close', 'click', _Tab.filterdelete.slot.close);
		_GUI._DOM.listen('#div_filters_list_delete', 'click', _Tab.filterdelete.slot.del);
	},
	/**
	 * Slots (DOM events handlers)
	 */
	slot: {
		/**
		 * Open the tab
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		open: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tab.filterdelete.ls();
				_Tabs.open('tab_filter_delete');
			});
		},
		/**
		 * Close the tab
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		close: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tabs.open('tab_filter');
			});
		},
		/**
		 * Delete a filter
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		del: (e) => {
			return _GUI._Lock.action_with_spinner(e, (e) => {
				let obj = e.target.closest('span');
				if (obj!=null) {
					let uid = obj.getAttribute('data-uid');
					if (uid!='' && _Module.tbl.filters.obj.hasOwnProperty(uid)) {
						_Signals.trigger('filters', 'del', {
							'filter': _Module.tbl.filters.obj[uid].obj
						});		
					}
				}
			});
		}
	},
	/**
	 * Build filters list
	 */
	ls: () => {
		_GUI._DOM.id('#div_filters_list_delete').innerHTML = '';
		for (let uid in _Module.tbl.filters.obj) {
			let filter = _Module.tbl.filters.obj[uid].obj;
			_GUI._DOM.id('#div_filters_list_delete').innerHTML += '\
			<span class="rounded" data-uid="' + uid + '">' + _GUI._HTML.escape(filter.value) + '</span>';
		}
	}
};
