// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Categories table
 */

class Cats_Tbl extends PP_Tbl {
	/**
	 * Constructor
	 */
	constructor () {
		super();		
	}
	/**
	 * Reset all categories except the default one (None)
	 */
	reset () {
		this.clear();
		this.set('00',(new Cat_Object()).upd({
			'uid': '00',
			'name': tr1('None')
		}));
	}
	/**
	 * Load categories
	 * @param  {object} cats Categories objects
	 */
	fill (cats) {
		for (let uid in cats) {
			let obj = cats[uid];
			this.obj[obj.uid] = (new Cat_Object()).upd(obj);
		}
	}
	/**
	 * Add a new category
	 * @param {object} obj Properties as JS object
	 */
	add (obj) {
		this.set(obj.uid,(new Cat_Object()).upd(obj));
		this.upd_style();
		this.refresh_names();
		_Tab.cateditor.hide();
		_Tab.cat.upd();
	}
	/**
	 * Delete an existing category
	 * @param  {object} obj Category to delete (as JS object)
	 */
	del (obj) {
		this.unset(obj.uid);
		this.upd_style();
		this.refresh_names();
		_Tab.cateditor.hide();
		_Tab.cat.upd();
	}
	/**
	 * Create dynamically the CSS styles for each existing category
	 */
	upd_style () {
		let s = _GUI._DOM.id('#cats_styles'); // div outside tabs
		let rules = '';
		for (let uid in this.obj) {
			let obj = this.obj[uid].obj;
			rules += '.cat_' + obj.uid + '{ color: ' + obj.color + '; background: ' + _Tab.cat.get_css_background(obj) + '; ';
			if (obj.styles.indexOf('italic')!=-1) {
				rules += ' font-style: italic;';
			}
			if (obj.styles.indexOf('bold')!=-1) {
				rules += ' font-weight: bold;';
			}
			if (obj.styles.indexOf('underline')!=-1) {
				rules += ' text-decoration: underline;';
			}
			rules += ' } ';
		}
		s.innerHTML = rules;
	}
	/**
	 * Called after a change, update all categorie names
	 */
	refresh_names () {
		for (let uid in this.obj) {
			let obj = this.obj[uid].obj;
			// update all objects name
			[...document.querySelectorAll('div.event.cat_' + obj.uid + ' span.cat')].forEach(obj => {
				obj.innerHTML = obj.name;
			})
		}
	}
}

