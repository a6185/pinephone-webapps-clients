// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Repeat rules object
 */

var _RRule = {
	y_cal: 2014,
	cal_year_events: [],
	/**
	 * Get full year as string from a JS date
	 * @param  {date} date JS date
	 * @return {string}      Year
	 */
	YYYY: (date) => {
		return String(date).substr(0,4);
	},
	/**
	 * Get full year as interger from a JS date
	 * @param  {date} date JS date
	 * @return {integer}      Year
	 */
	Y: (date) => {
		return parseInt(_RRule.YYYY(date),10);
	},
	/**
	 * Get full month as two chars string from a JS date
	 * @param  {date} date JS date
	 * @return {string}      Month (two chars)
	 */
	MM: (date) => {
		return String(date).substr(5,2);
	},
	/**
	 * Get full month as integer from a JS date
	 * @param  {date} date JS date
	 * @return {number}      Month
	 */
	M: (date) => {
		return parseInt(_RRule.MM(date),10);
	},
	/**
	 * Get full day as two chars string from a JS date
	 * @param  {date} date JS date
	 * @return {string}      Day (two chars)
	 */
	DD: (date) => {
		return String(date).substr(8,2);
	},
	/**
	 * Get full day as integer from a JS date
	 * @param  {date} date JS date
	 * @return {number}      Day
	 */
	D: (date) => {
		return parseInt(_RRule.DD(date),10);
	},
	/**
	 * Add a new repeat rule for a date
	 * @param  {date} date  JS date
	 * @param  {object} rrule JS object
	 * @return {bool}       Success
	 */
	add: (date, rrule) => {
		if (_RRule.YYYY(date)!=_RRule.y_cal) {
			return false;
		}
		if (rrule.hasOwnProperty('until') && rrule.until.constructor === String && rrule.until.length) {
			if (date>rrule.until) {
				return false;
			}
		}
		_RRule.cal_year_events.push(date);
		return true;
	},
	/**
	 * Get all events for a year
	 * @param  {number} y_cal Year as integer
	 * @param  {object} event Event object to filter
	 * @return {array}       Array of repeat events linked to the current given in argument
	 */
	filter: (y_cal, event) => {

		let start_date = event.date;
		let y_start_date = _RRule.Y(start_date); // int

		if (y_start_date>y_cal) // ignore futur events
			return [];

		_RRule.cal_year_events = [];
		_RRule.y_cal = y_cal;

		if (!event.hasOwnProperty('rrule')) {
			if (_RRule.YYYY(start_date)!=_RRule.y_cal) {
				return [];
			}
			_RRule.cal_year_events.push(start_date);
			return _RRule.cal_year_events;
		}

		let rrule = event.rrule;
		if (rrule.hasOwnProperty('until') && rrule.until.constructor === String && rrule.until.length) {
			if (_RRule.Y(rrule.until)<y_cal) { // ignore old events
				return [];
			}
		}

		/*
		let now = new Date();
		let today = now.to_yyyymmdd('-');
		let yyyy_now = _RRule.YYYY(today); // string 4 chars
		let y_now = _RRule.Y(today); // int
		let mm_now = _RRule.MM(today); // string 2 chars
		let m_now = _RRule.M(today); // int
		let dd_now = _RRule.DD(today); // string 2 chars
		let d_now = _RRule.D(today); // int
		*/

		// frequency first
		if (rrule.hasOwnProperty('freq') && rrule.freq.constructor === String && rrule.freq.length) {

			let yyyy_start_date = _RRule.YYYY(start_date); // string 4 chars
			let mm_start_date = _RRule.MM(start_date); // string 2 chars
			let m_start_date = _RRule.M(start_date); // int
			let dd_start_date = _RRule.DD(start_date); // string 2 chars
			let d_start_date = _RRule.D(start_date); // int

			// yearly
			if (rrule.freq=='yearly') {
				let interval = 'interval' in rrule && rrule.interval>0 ? rrule.interval : 1;
				if ((y_cal-y_start_date)%interval==0) {
					let step = (y_cal-y_start_date)/interval;
					if (step<0) // start_date in the future
						return [];
					if (rrule.hasOwnProperty('count') && rrule.count>0) {
						if (step>rrule.count)
							return [];
					}
					let date = y_cal + '-' + mm_start_date + '-' + dd_start_date;
					_RRule.add(date, rrule);
				}
			}
			// monthly
			if (rrule.freq=='monthly') {
				let interval = 'interval' in rrule && rrule.interval>0 ? rrule.interval : 1;
				for (let m=1;m<13;m++) {
					if ((y_cal*12+m-(y_start_date*12+m_start_date))%interval==0) {
						let step = (y_cal*12+m-(y_start_date*12+m_start_date))/interval;
						if (step<0) // start_date in the future
							continue;
						if (rrule.hasOwnProperty('count') && rrule.count>0) {
							if (step>rrule.count)
								continue;
						}
						let date = sprintf('%04d-%02d-%02d', y_cal, m, dd_start_date);
						_RRule.add(date, rrule);
					}
				}
			}
			// daily
			if (rrule.freq=='daily') {
				let interval = 'interval' in rrule && rrule.interval>0 ? rrule.interval : 1;
				_RRule.add(start_date, rrule);
				if (rrule.hasOwnProperty('count') && rrule.count>0) {
					for (let i=1;i<=rrule.count;i++) {
						let day = new Date(y_start_date, m_start_date-1, d_start_date+i*interval);
						let real_date = sprintf('%04d-%02d-%02d', day.getFullYear(), day.getMonth()+1, day.getDate());
						_RRule.add(real_date, rrule);
					}
				} else { // no count
					if (y_start_date==y_cal) {
						let i = 0;
						let y = y_start_date;
						while (y==y_cal) {
							i++;
							let day = new Date(y_start_date, m_start_date-1, d_start_date+i*rrule.interval);
							let real_date = sprintf('%04d-%02d-%02d', day.getFullYear(), day.getMonth()+1, day.getDate());
							_RRule.add(real_date, rrule);
							y = day.getFullYear();
						}
					} else { // event starts before calendar year
						let event_time = (new Date(y_start_date,m_start_date-1,d_start_date)).getTime();
						let step_time = rrule.interval*86400000;
						let step_min = Math.round(((new Date(y_cal,0,1)).getTime()-event_time)/step_time);
						let step_max = Math.round(((new Date(y_cal+1,0,1)).getTime()-event_time)/step_time);
						let y = y_start_date;
						for (let i=step_min;i<=step_max;i++) {
							let day = new Date(y_start_date, m_start_date-1, d_start_date+i*interval);
							let real_date = sprintf('%04d-%02d-%02d', day.getFullYear(), day.getMonth()+1, day.getDate());
							_RRule.add(real_date, rrule);
							y = day.getFullYear();
						}
					}
				}
			}
			// weekly
			if (rrule.freq=='weekly') {
				let interval = 'interval' in rrule && rrule.interval>0 ? rrule.interval*7 : 7;
				_RRule.add(start_date, rrule);
				if (rrule.hasOwnProperty('count') && rrule.count>0) {
					for (let i=1;i<=rrule.count;i++) {
						let day = new Date(y_start_date, m_start_date-1, d_start_date+i*interval);
						let real_date = sprintf('%04d-%02d-%02d', day.getFullYear(), day.getMonth()+1, day.getDate());
						_RRule.add(real_date, rrule);
					}
				} else { // no count
					if (y_start_date==y_cal) {
						let i = 0;
						let y = y_start_date;
						while (y==y_cal) {
							i++;
							let day = new Date(y_start_date, m_start_date-1, d_start_date+i*interval);
							let real_date = sprintf('%04d-%02d-%02d', day.getFullYear(), day.getMonth()+1, day.getDate());
							_RRule.add(real_date, rrule);
							y = day.getFullYear();
						}
					} else { // event starts before calendar year
						let event_time = (new Date(y_start_date,m_start_date-1,d_start_date)).getTime();
						let step_time = rrule.interval*7*86400000;
						let step_min = Math.round(((new Date(y_cal,0,1)).getTime()-event_time)/step_time);
						let step_max = Math.round(((new Date(y_cal+1,0,1)).getTime()-event_time)/step_time);
						let y = y_start_date;
						for (let i=step_min;i<=step_max;i++) {
							let day = new Date(y_start_date, m_start_date-1, d_start_date+i*interval);
							let real_date = sprintf('%04d-%02d-%02d', day.getFullYear(), day.getMonth()+1, day.getDate());
							_RRule.add(real_date, rrule);
							y = day.getFullYear();
						}
					}
				}
			}
		} else {
			//let date = y_cal + '-' + _RRule.MM(start_date) + '-' + _RRule.DD(start_date);
			_RRule.add(start_date, rrule);
		}
		return _RRule.cal_year_events;
	}
};
