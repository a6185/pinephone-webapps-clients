// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * GUI auto template translations
 */

_GUI._Tpl.set([
// tab_main
'input_keywords_filter','Text to filter ?','Texte à filtrer',
//'settings_weeks',['Monday,Sunday'],['Lundi,Dimanche'],
// tab_import
'label_import','Import','Importer',
// tab_import
'label_export','Export','Exporter',
'label_import_help1','Drag/drop your XML backup file here','Glissez/déposez votre fichier XML dans cette zone',
'label_import_help2','or download it manually :','ou téléchargez-le manuellement :',
// tab_export
'label_export_calendar','Export your calendar :','Exporter le calendrier :',
'button_export_as_xml','as XML','en XML',
'button_export_as_json','as JSON','en JSON',
// tab_goto_month
'label_go_to_month','Go to month ?','Aller au mois ?',
'ul_months_abrv',['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],['Jan','Fév','Mar','Avr','Mai','Jui','Jul','Aoû','Sep','Oct','Nov','Déc'],
'label_event_start_hour','Hour ?','Heure ?',
'label_event_start_minute','Minute ?','Minute ?',
'label_event_start_minute_ok','OK','OK',
'label_event_duration_hour','Hour ?','Heure ?',
'label_event_duration_hour_ok','OK','OK',
'label_event_duration_minute','Minute ?','Minute ?',
'label_event_duration_minute_ok','OK','OK',
'event_date','Start date','Date de début',
'label_event_start_time','Start time:','Début :',
'button_event_allday','Allday','Jour entier',
'label_event_duration','Duration:','Durée : ',
'button_event_repeat','Repeat','Répéter',
'event_title','Title ?','Titre ?',
'event_description','Description ?','Description ?',
'label_event_cat','Category:','Catégorie :',
'button_event_cat','None','Aucune',
// tab_cat
'label_cat_category','Category ?','Catégorie ?',
// tab_cat_manage
'label_cat_manage','Manage categories','Gestion des catégories',
// tab_cat_editor
'label_cat_editor','Edit category','Édition catégorie',
'cat_editor_name','Category name ?','Nom catégorie ?',
'cat_editor_fg_label','Text','Texte',
'label_cat_editor_color','Color:','Couleur : ',
'cat_editor_italic','I','I',
'cat_editor_bold','B','G',
'cat_editor_underline','U','S',
'cat_editor_bg_label','Background','Fond',
'label_cat_editor_bg_from','Color 1: ','Couleur 1 : ',
'label_cat_editor_bg_to','Color 2: ','Couleur 2 : ',
'label_cat_editor_bg_angle','Angle: ','Angle : ',
'cat_editor_bg_transparent','Transparent','Transparent',
'cat_editor_bg_color','Color','Aplat',
'cat_editor_bg_gradient','Gradient','Dégradé',
// tab_log
'button_log_close','Close log','Fermer le journal',
// tab_menu
// tab_repeat
'button_repeat_close','Close','Fermer',
//'event_rrule_freq_radio',['Yearly,Monthly,Weekly,Daily'],['An,Mois,Semaine,Jour'],
'event_rrule_freq_yearly','Yearly','An',
'event_rrule_freq_monthly','Monthly','Mois',
'event_rrule_freq_weekly','Weekly','Semaine',
'event_rrule_freq_daily','Daily','Jour',
'event_rrule_label_interval','Interval ?','Intervalle ?',
'event_rrule_label_count','Count','Nombre',
'event_rrule_label_count_no_limit','(0=no limit)','(0=sans fin)',
'event_rrule_until','End date ?','Date de fin ?',
'event_rrule_until_reset','Reset','Effacer',
// tab_filter
'label_filter','Available filters','Filtres disponibles',
// tab_filter_delete
'label_filter_delete','Delete filters','Effacer des filtres',
/* tab settings */
'label_settings','Settings','Réglages',
'label_setting_user_locale','Language','Langue',
'label_setting_server_websocket','Websocket','Websocket',
'label_setting_calendar_wkst','Week start','Début de la semaine',
'websocket_host','Host ?','Hôte ?',
'websocket_port','Port ?','Port ?'
]);
