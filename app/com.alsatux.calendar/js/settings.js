// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Module settings
 */

class Settings extends PP_Settings {
	/**
	 * Constructor
	 */
	constructor () {
		super();
		this.add_setting(this.setting_week_start);
		this.add_override(this.override_user_locale);
	}
	/**
	 * Add week start setting
	 */
	setting_week_start () {
		let _Setting_Object = this.tbl.settings.add({
			label: 'Week start',
			cat: 'calendar',
			key: 'wkst',
			type: 'select',
			nb_options: '1,1', // min,max
			val: {
				'monday': 'Monday',
				'sunday': 'Sunday'
			},
			cur: ['monday'],
			def: ['monday']
		});
		_Setting_Object.tpl = new PP_Settings_Tpl_Option(_Setting_Object);
		_Setting_Object.tpl.post_HTML = () => {
			_Tab.cal.redraw_current_year();
		};
		_Setting_Object.upd_cur = cur => {
			let wkst = cur[0];
			if (wkst.match(/^(monday|sunday)$/)!=null) {
				_Setting_Object.obj.cur = [wkst];
			}
		}
		_Setting_Object.callback = cur => { // from HTML event
			_Setting_Object.upd_cur(cur);
			_GUI._Log.info(tr1('Week start is now') + ' : ' + tr1(_Setting_Object.obj.cur[0]));
			_Module._Settings.have_changed();
		};
	}
	/**
	 * On user locale change, reload GUI template with new dictionnary
	 */
	override_user_locale () {
		let _Setting_Object = this.tbl.settings.find_by_cat_key('user','locale');
		_Setting_Object.callback = cur => { // from HTML event
			let locale = _Lang.locale;
			_Setting_Object.upd_cur(cur);
			_Module._Settings.have_changed();
			_GUI._Tpl.load(_Lang.locale);
			_Tab.cal.redraw();
		};
	}
}
