// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Dictionnary use in js code, according to manifest.json
 */

function tr1 (txt) {
	const dict = [
'!',' !',
'English','Anglais',
'French','Français',
// pp_db.class.js
'Wrong UID','Mauvaise UID',
'Unknown UID','UID inconnue',
// cactus.js
'None','Aucune',
'Week start is now','Le premier jour de la semaine est maintenant',
'Reading settings failed','La lecture des préférences a échoué',
'Recording settings failed','L\'enregistrement des préférences a échoué',
'monday','lundi',
'sunday','dimanche',
// catdb.js
'A category object is missing','Un objet catégorie est manquant',
'The field','Le champ',
'is missing','est manquant',
'is bad formatted','est mal formaté',
'Unable to change default category','Impossible de modifier la catégorie par défaut',
'name','nom',
'is empty','est vide',
'text color','couleur du texte',
'background type','type de fond',
'gradient color 1','couleur de dégradé 1',
'gradient color 2','couleur de dégradé 2',
'gradient angle','angle du dégradé',
'Unknown style','Style inconnu',
'No sequence given','Pas de séquence donnée',
'Unknown sequence','Séquence inconnue',
// eventdb.js
'An event object is missing','Un objet événement est manquant',
'Invalid start date','Date de départ invalide',
'Invalid rrule frequency','Fréquence de répétition invalide',
'event count','nombre de répétitions',
'event interval','intervalle de répétition',
'Invalid until date','Date de fin de répétition invalide',
// filterdb.js
'Filter','Le filtre',
'already exists','existe déjà',
'A filter object is missing','Un objet Filtre est manquant',
'sequence','séquence',
// main.js
'Please report any bug to','Veuillez signaler tout bug à',
'Searching for the last backup file','Recherche de la dernière sauvegarde',
// tab_cal.js
'January,February,March,April,May,June,July,August,September,October,November,December','Janvier,Février,Mars,Avril,Mai,Juin,Juillet,Août,Septembre,Octobre,Novembre,Décembre',
'S,M,T,W,T,F,S','D,L,M,M,J,V,S',
'events viewed','événements affichés',
// tab.cateditor.js
'Unable to delete default category','Impossible d\'enlever la catégorie par défaut',
'Delete this category ?','Effacer cette catégorie ?',
'Category not found','Catégorie introuvable',
// tab.cat.js
'Category choosed','Catégorie choisie : ',
// tab.catmanage.js
'Editing category','Edition de la catégorie',
// tab.event.js
'Recording','Enregistrement en cours',
'Nothing to paste','Rien à coller',
'Category has been reset','La catégorie a été réinitialisée',
// tab.import.js
'File uploaded','Fichier téléchargé',
'Error while uploading file','Erreur durant le chargement du fichier',
// tab.export.js
'Your browser doesn\'t support file creation on the fly ! Please use Mozilla Firefox !','Votre navigateur ne supporte pas la création de fichier à la volée ! Merci d\'utiliser Mozilla Firefox !',
// tab.settings.js
'Monday','Lundi',
'Sunday','Dimanche',
// xml.data.js
'Parsing XML failed','L\'analyse XML a échoué',
'Reading category failed','La lecture de la catégorie a échoué',
'Reading filter failed','La lecture du filtre a échoué',
'Bad start date value','La date de l\'événement est erronée',
'Creating new event','Création d\'un nouvel événement',
'Bad frequency value','La valeur de la fréquence de répétition est erronée',
'Unknown category','Catégorie inconnue',
'Reading event failed','La lecture de l\'événement a échoué',
'Records found','Engistrements trouvés',
'Recording category failed','L\'enregistrement  d\'une catégorie a échoué',
'Recording filter failed','L\'enregistrement  d\'un filtre a échoué',
'Recording event failed','L\'enregistrement de l\'événement a échoué',
	];
	let offset = Object.keys(_Module._Manifest.obj.lang).indexOf(_Lang.locale);	
	let pos = dict.indexOf(txt);	
	if (pos!=-1) {
		return dict[pos + (offset<0 ? 0 : offset)];	
	} else {
		_Lang.object_not_found(txt);
		return txt;
	}
}