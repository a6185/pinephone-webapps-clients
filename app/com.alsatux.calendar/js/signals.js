// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Module signals
 */

var _Signals = {
	/**
	 * Load all events from server side
	 * @return {[type]} [description]
	 */
	read: () => {
		_WS.send({
			route: 'com.alsatux.calendar:/events/ls',
			args: {
			},
			callback: 'com.alsatux.calendar:/events/reset'
		});		
	},
	/**
	 * Trigger an action to the server
	 * @param  {string} slot   Server slot
	 * @param  {string} action Server action
	 * @param  {object} args   Arguments as JS object
	 */
	trigger: (slot, action, args) => {
		_WS.send({
			route: `com.alsatux.calendar:/${slot}/${action}`,
			args: args
		});
	}
};