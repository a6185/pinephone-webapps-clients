// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Repeat tab
 */

_Tab.repeat = {
	/**
	 * Initialization
	 */
	init: () => {
		// opener
		_GUI._DOM.listen('#button_event_repeat', 'click', _Tab.repeat.slot.open);
		// GUI objects
		_GUI._DOM.listen('#button_repeat_close', 'click', _Tab.repeat.slot.close);
		_GUI._DOM.listen('#event_rrule_freq_radio', 'click', _Tab.repeat.slot.click_freq);
		_GUI._DOM.listen('#event_rrule_until_reset', 'click', _Tab.repeat.slot.click_reset_until);
	},
	/**
	 * Slots (DOM events handlers)
	 */
	slot: {
		/**
		 * Open the tab
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		open: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tabs.open('tab_repeat');
			});
		},
		/**
		 * Close the tab
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		close: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tabs.open('tab_edit_event');
				_Tab.event.upd_repeat_button();
			});
		},
		/**
		 * Click on a frequency
		 * @param  {event} e DOM click
		 * @return {function}   GUI lock action
		 */
		click_freq: (e) => {
			return _GUI._Lock.action(e, (e) => {
				var o = e.target;
				if (o.nodeName && o.nodeName=='SPAN') {
					if (o.classList.contains('selected')) {
						// unselect
						_Tab.repeat.select_freq('');
					} else {
						// select
						_Tab.repeat.select_freq(o.getAttribute('data-value'));
					}
				}
			});
		},
		/**
		 * Click on reset until
		 * @param  {event} e DOM click
		 * @return {function}   GUI lock action
		 */
		click_reset_until: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_GUI._DOM.id('#event_rrule_until').value = '';
			});
		}
	},
	/**
	 * Select a frequency
	 * @param  {string} value Frequency
	 */
	select_freq: (value) => {
		[...document.querySelectorAll('#event_rrule_freq_radio span')].forEach(span => {
			(span.getAttribute('data-value').toLowerCase()==value.toLowerCase()) ? span.classList.add('selected') : span.classList.remove('selected');
		});
		_GUI._DOM.id('#event_rrule_freq').value = value.toLowerCase();
	}
};
