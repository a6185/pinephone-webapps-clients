// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Category tab
 */

_Tab.cat = {
	/**
	 * Initialization
	 */
	init: () => {
		_GUI._DOM.listen('#button_event_cat', 'click', _Tab.cat.slot.open);
		_GUI._DOM.listen('#div_event_cat_radio', 'click', _Tab.cat.slot.click_cat);
	},
	/**
	 * Slots (DOM events handlers)
	 */
	slot: {
		/**
		 * Open the tab
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		open: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tabs.open('tab_cat');
				_Tab.cat.upd();
			});
		},
		/**
		 * Choose an event category
		 * @param  {event} e DOM click
		 * @return {function}   GUI lock action
		 */
		click_cat: (e) => {
			return _GUI._Lock.action(e, (e) => {
				let o = e.target;
				if (o.nodeName && o.nodeName=='DIV') {
					let divs = o.parentNode.querySelectorAll('div');
					_GUI._CSS.resetClassName(divs);
					o.classList.add('selected');
					_GUI._DOM.id('#event_cat_uid').value = o.id;
					_GUI._Log.info(tr1('Category choosed') + ' : ' + _Module.tbl.cats.obj[o.id].obj.name);
				}
				_Tab.cat.hide();
				_Module.tbl.cats.upd_style();
			});
		}
	},
	/**
	 * Update all categories rebuilding HTML buttons/divs
	 */
	upd: () => {
		_GUI._DOM.id('#div_event_cat_radio').innerHTML = '';
		let cat_uid =	_GUI._DOM.id('#event_cat_uid').value;
		let div_event_cat_radio = _GUI._DOM.id('#div_event_cat_radio');
		for (let uid in _Module.tbl.cats.obj) {
			let cat = _Module.tbl.cats.obj[uid].obj;
			let div = document.createElement('div');
			div.id = uid;
			div.style.color = cat.color;
			div.style.background = _Tab.cat.get_css_background(cat);
			div.className = cat.styles;
			if (uid==cat_uid) {
				div.classList.add('selected');
			}
			div.innerHTML = cat.name;
			div_event_cat_radio.appendChild(div)
		}
	},
	/**
	 * Return to event tab and update category
	 */
	hide: () => {
		_Tabs.open('tab_edit_event');
		_Tab.event.upd_cat_button();
	},
	/**
	 * Build the CSS code to handle CSS background
	 * @param  {object} cat Category as JS object
	 * @return {string}     CSS code
	 */
	get_css_background: (cat) => {
		if (cat.bg_type.toLowerCase()=='transparent') {
			return 'transparent';
		}
		if (cat.bg_type.toLowerCase()=='color') {
			return cat.bg_from;
		}
		return 'linear-gradient(' + cat.bg_angle +'deg, ' + cat.bg_from + ' 0%, ' + cat.bg_to + ' 100%)';
	}
};
