// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Event object
 */

class Event_Object extends PP_Object {
	/**
	 * Constructor
	 */
	constructor () {
		let now = new Date();
		super('event_object',{
			'uid': '',
			'cat_uid': '00',
			'date': now.to_yyyymmdd('-'),
			'time': sprintf('%02d:%02d',now.getHours(),now.getMinutes()),
			'duration': sprintf('%02d:%02d',1,0),
			'title': '',
			'description': '',
			'rrule': {
				'freq': '',
				'count': 0,
				'interval': 1,
				'until': ''
			}
		});
	}
}