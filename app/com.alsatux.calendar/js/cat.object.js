// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Category object
 */

class Cat_Object extends PP_Object {
	/**
	 * Constructor
	 */
	constructor () {
		super('cat_object',{
			'uid': '',
			'name': '',
			'color': '#000000',
			'bg_type': 'transparent', // or color or gradient
			'bg_from': '#68EE66',
			'bg_to': '#00AA2E',
			'bg_angle': '90', // -180 to 180 (deg) - top is 0
			'styles': '' // space separated values : "italic bold underline"
		});
	}
}