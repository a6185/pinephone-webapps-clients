// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Import tab
 */

_Tab.import = {
	/**
	 * Initialization
	 */
	init: () => {
		_GUI._DOM.listen('#button_import_close', 'click', _Tab.import.slot.close);
		_GUI._DOM.listen('#import_file', 'change', _Tab.import.slot.import);
		// add uploader
		let _Uploader1 = new Uploader();
		_Uploader1.init('uploader1', (file) => {
			_Tab.import.read(file);
		});
	},
	/**
	 * Slots (DOM events handlers)
	 */
	slot: {
		/**
		 * Open the tab
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		open: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tabs.open('tab_import');
			});
		},
		/**
		 * Close the tab
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		close: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tabs.open('tab_main');
			});
		},
		/**
		 * Import an external file from outside
		 * @param  {object} e DOM file object
		 */
		import: (e) => {
			_Tab.import.read(e.target.files[0]);
		}		
	},
	/**
	 * Read file and update events
	 * @param  {object} file DOM file object
	 */
	read: (file) => {
		let reader = new FileReader();
		reader.onload = (e) => {
			try {
				let succes = false;
				// no checks on import !
				if (file.name.substr(-3)=='xml') {
					let parser = new DOMParser();
					// sanitize XML using two ways...
					let xml_doc = parser.parseFromString(e.target.result, "text/xml");
					xml_doc.normalize();
					let blobdata = (new XMLSerializer()).serializeToString(xml_doc);
					_XML_Data.read(blobdata);
					success = true;
				}
				if (file.name.substr(-4)=='json') {
					let data = JSON.parse(e.target.result);
					for (let uid in data.cats) {
						let obj = data.cats[uid];
						_Module.tbl.cats.obj[obj.uid] = (new Cat_Object()).upd(obj);
					}
					for (let uid in data.filters) {
						let obj = data.filters[uid];
						_Module.tbl.filters.obj[obj.uid] = (new Filter_Object()).upd(obj);
					}
					for (let uid in data.events) {
						let obj = data.events[uid];
						_Module.tbl.events.obj[obj.uid] = (new Event_Object()).upd(obj);
					}
					success = true;
				}
				if (success) {
					alert(tr1('File uploaded') + tr1('!'));
					_Tab.import.slot.hide(e);
					_Tab.settings.slot.hide(e);					
				}
				_Tab.cal.redraw_current_year();
				// no auto save on import !
			} catch (err) {
				alert(err.message);
			}
		}
		reader.onerror = (e) => {
			alert(tr1('Error while uploading file') + tr1('!'));
		}
		reader.readAsText(file, "UTF-8");		
	}
}
