// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Agenda main object
 */

var _Agenda = {
	/**
	 * Prepare the view with the current year by default
	 */
	start: () => {
		_Agenda.reset();
		_Tab.cal.redraw_current_year();						
		_Tab.main.scroll_today();
		// read events from server
		_Signals.read();
	},
	/**
	 * On import, reset all current events and load new ones
	 */
	reset: () => { // for import
		_Module.tbl.cats.reset();
		_Module.tbl.filters.obj = {};
		_Module.tbl.events.obj = {};
	}
}