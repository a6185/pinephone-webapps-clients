// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Export tab
 */

_Tab.export = {
	/**
	 * Initialization
	 */
	init: () => {
		_GUI._DOM.listen('#button_export_close', 'click', _Tab.export.slot.close);
		_GUI._DOM.listen('#button_export_as_json', 'click', _Tab.export.slot.export_as_json);
		_GUI._DOM.listen('#button_export_as_xml', 'click', _Tab.export.slot.export_as_xml);
	},
	/**
	 * Slots (DOM events handlers)
	 */
	slot: {
		/**
		 * Open the tab
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		open: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tabs.open('tab_export');
			});
		},
		/**
		 * Close the tab
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		close: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tabs.open('tab_main');
			});
		},
		/**
		 * Export full calendar datas as JSON
		 * @param  {event} e DOM click
		 */
		export_as_json: (e) => {
			if (_Tab.export.test_navigator) {
				let data = JSON.stringify({
						cats: _Module.tbl.cats.obj,
						filters: _Module.tbl.filters.obj,
						events: _Module.tbl.events.obj
					});
				_Tab.export.export({
					name: (new Date()).to_yyyymmddhhiiss() + '.json',
					type: 'application/json',
					data: data
				});
			}
		},
		/**
		 * Export full calendar datas as XML
		 * @param  {event} e DOM click
		 */
		export_as_xml: (e) => {
			if (_Tab.export.test_navigator) {
				let data = _XML_Data.write();
				_Tab.export.export({
					name: (new Date()).to_yyyymmddhhiiss() + '.xml',
					type: 'text/plain',
					data: data
				});
			}				
		}
	},
	/**
	 * Test if client navigator supports createObjectURL
	 * @return {bool} Success
	 */
	test_navigator: () => {
		if (!window.URL || !window.URL.createObjectURL) {
			alert(tr1('Your browser doesn\'t support file creation on the fly ! Please use Mozilla Firefox !'));
			return false;
		}
		return true;
	},
	/**
	 * Method to create an upload event
	 * @param  {file} file DOM file to export
	 */
	export: (file) => {
		let blob = new Blob([file.data], {type:file.type+';charset=utf-8'});
		let a = document.createElementNS("http://www.w3.org/1999/xhtml", "a");
		a.href = window.URL.createObjectURL(blob);
		a.download = file.name;
		let event = document.createEvent("MouseEvents");
		event.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
		a.dispatchEvent(event);
	}
}