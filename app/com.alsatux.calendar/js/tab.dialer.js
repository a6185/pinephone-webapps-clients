// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Dialer tab
 */

_Tab.dialer = {
	input_target: null,
	/**
	 * Initialization
	 */
	init: () => {
		// show dialer
		_GUI._DOM.listen('#event_start_hour', 'click', _Tab.dialer.slot.open);
		_GUI._DOM.listen('#event_start_minute', 'click', _Tab.dialer.slot.open);
		_GUI._DOM.listen('#event_duration_hour', 'click', _Tab.dialer.slot.open);
		_GUI._DOM.listen('#event_duration_minute', 'click', _Tab.dialer.slot.open);
		// hide dialer
		_GUI._DOM.listen('#tab_event_start_hour', 'click', _Tab.dialer.slot.close);
		_GUI._DOM.listen('#tab_event_start_minute', 'click', _Tab.dialer.slot.close);
		_GUI._DOM.listen('#tab_event_duration_hour', 'click', _Tab.dialer.slot.close);
		_GUI._DOM.listen('#tab_event_duration_minute', 'click', _Tab.dialer.slot.close);
	},
	/**
	 * Slots (DOM events handlers)
	 */
	slot: {
		/**
		 * Open the tab
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		open: (e) => {
			return _GUI._Lock.action(e, (e) => {
				let o = e.target;
				let div = _GUI._DOM.id('#tab_' + o.id);
				div.style.display = 'block';
				// search for a text field inside the fix
				if (div.getElementsByTagName('input').length) {
					div.getElementsByTagName('input')[0].value = o.innerHTML;
				}
				// select current li
				let lis = div.querySelectorAll('li');
				for (let i=0;i<lis.length;i++) {
					if (lis[i].innerHTML==o.innerHTML) {
						lis[i].className = 'selected';
						break;
					}
				}
				_Tab.dialer.input_target = o;
				e.stopPropagation();
			});
		},
		/**
		 * Close the tab
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		close: (e) => {
			return _GUI._Lock.action(e, (e) => {
				let o = e.target;
				let close_panel = false;
				let div = _GUI._DOM.id('#tab_' + _Tab.dialer.input_target.id);
				if (o.nodeName && o.nodeName=='LI') {
					// user choose a value
					_Tab.dialer.input_target.innerHTML = sprintf('%02d', o.innerHTML);
					close_panel = true;
				}
				if (o.nodeName && o.nodeName=='BUTTON') {
					// user enter a value then press OK
					let value = div.getElementsByTagName('input')[0].value.to_uint();
					if (_Tab.dialer.input_target.classList.contains('hour')) {
						// we allow durations from 0h to 99h for compatibility
						// don't forget that 24h is allowed by default for allday events
						_Tab.dialer.input_target.innerHTML = sprintf('%02d', value%100);
					} else { // minute
						_Tab.dialer.input_target.innerHTML = sprintf('%02d', value%60);
					}
					close_panel = true;
				}
				// unselect selected li
				_GUI._CSS.resetClassName(div.querySelectorAll('li'));
				if (close_panel) {
					// user press OK or a value
					o = o.closest('.fixed');
					//while (o && o.className.match(/fixed/)==null)
						// search for the parent "card" div
					//	o = o.parentNode;
					if (o!=null) {
						o.style.display = 'none';
					}
				}
				_Tab.event.upd_allday_button();
				e.stopPropagation();
			});
		}
	}
};

