// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Main
 */

window.onload = () => {	
	_Module = new PP_Module('com.alsatux.calendar');
	_Module.init();
	_Module.tbl = {
		'cats': new Cats_Tbl(),
		'events': new Events_Tbl(),
		'filters': new Filters_Tbl()
	};
	_Module._Manifest.read().then(result => {
		_Module.settings();
		// add event handlers
		_Tab.cal.init();
		_Tab.cat.init();
		_Tab.cateditor.init();
		_Tab.catmanage.init();
		_Tab.colors.init();
		_Tab.dialer.init();
		_Tab.event.init();
		_Tab.filter.init();
		_Tab.filterdelete.init();
		_Tab.log.init();
		_Tab.import.init();
		_Tab.export.init();
		_Tab.main.init();
		_Tab.month.init();
		_Tab.repeat.init();
		_Tab.selector.init();
		_Tab.settings.init();
		_Tab.spinner.init();
		_Tabs.init('tab_main');
		_Module.run(_Agenda.start);
	}).catch(err => {
		alert(err);
	});
}
