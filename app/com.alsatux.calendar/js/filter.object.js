// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Filter object
 */

class Filter_Object extends PP_Object {
	/**
	 * Constructor
	 */
	constructor () {
		super('filter_object',{
			'uid': '',
			'value': ''
		});
	}
}