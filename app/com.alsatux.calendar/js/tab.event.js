// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Event tab
 */

_Tab.event = {
	event_buffer: null,
	/**
	 * Initialization
	 */
	init: () => {
		// all day button
		_GUI._DOM.listen('#button_event_allday', 'click', _Tab.event.slot.allday);
		// event buttons
		_GUI._DOM.listen('#button_event_submit', 'click', _Tab.event.slot.submit);
		_GUI._DOM.listen('#button_event_paste', 'click', _Tab.event.slot.paste);
		_GUI._DOM.listen('#button_event_delete', 'click', _Tab.event.slot.del);
	},
	/**
	 * Slots (DOM events handlers)
	 */
	slot: {
		/**
		 * Make an event all day
		 * @param  {event} e DOM click
		 * @return {function}   GUI lock action
		 */
		allday: (e) => {
			return _GUI._Lock.action(e, (e) => {
			_GUI._DOM.id('#event_start_hour').innerHTML = '00';
			_GUI._DOM.id('#event_start_minute').innerHTML = '00';
			_GUI._DOM.id('#event_duration_hour').innerHTML = '24';
			_GUI._DOM.id('#event_duration_minute').innerHTML = '00';
			_Tab.event.upd_allday_button();
			});
		},		
		/**
		 * Add or update an event with spinner
		 * @param  {event} e DOM click
		 * @return {function}   GUI lock action
		 */
		submit: (e) => {
			return _GUI._Lock.action_with_spinner(e, (e) => {
			_Tab.spinner.log(tr1('Recording') + '...');
				let event = _Tab.event.form_datas();
				// send new event to the server side
				if (event.uid=='') {
					_Signals.trigger('events', 'add', {
						'event': event
					});	
				} else {
					_Signals.trigger('events', 'upd', {
						'event': event
					});	
				}
				// copy event to buffer for paste()
				_Tab.event.event_buffer = event;
				_Tab.event.hide();
			});
		},
		/**
		 * Paste last event to the current one
		 * @param  {event} e DOM click
		 * @return {function}   GUI lock action
		 */
		paste: (e) => {
			return _GUI._Lock.action(e, (e) => {
				if (_Tab.event.event_buffer==null) {
					alert(tr1('Nothing to paste') + tr1('!'));
				} else {
					_Tab.event.event_buffer['uid'] = _GUI._DOM.id('#event_uid').value;
					_Tab.event.event_buffer['date'] = _GUI._DOM.id('#event_date').value;
					let cat_uid = _Tab.event.event_buffer['cat_uid'];
					if (!_Module.tbl.cats.obj.hasOwnProperty(_Tab.event.event_buffer['cat_uid'])) {
						_Tab.event.event_buffer['cat_uid'] = 'cat_uid_0';
						alert(tr1('Category has been reset') + tr1('!'));
					}
					_Tab.event.edit(_Tab.event.event_buffer);
				}
			});
		},
		/**
		 * Delete an event
		 * @param  {event} e DOM click
		 * @return {function}   GUI lock action
		 */
		del: (e) => {
			return _GUI._Lock.action(e, (e) => {
				//let uid = _GUI._DOM.id('#event_uid').value;
				let event = _Tab.event.form_datas();
				if (event.uid!='') {
					// send to the server side
					_Signals.trigger('events', 'del', {
						'event': event
					});		
				}
				_Tab.event.hide();
			});
		}
	},
	/**
	 * Collect form datas to build the new/updated event
	 * @return {object} Event as JS object
	 */
	form_datas: () => {
		let event = {
			'uid': _GUI._DOM.id('#event_uid').value,
			'cat_uid': _GUI._DOM.id('#event_cat_uid').value,
			'date': _GUI._DOM.id('#event_date').value,
			'time': sprintf('%02d:%02d',_GUI._DOM.id('#event_start_hour').innerHTML,_GUI._DOM.id('#event_start_minute').innerHTML),
			'duration': sprintf('%02d:%02d',_GUI._DOM.id('#event_duration_hour').innerHTML,_GUI._DOM.id('#event_duration_minute').innerHTML),
			'title': _GUI._DOM.id('#event_title').value,
			'description': _GUI._DOM.id('#event_description').value
		};
		if (_GUI._DOM.id('#event_rrule_freq').value.length) {
			// add rrule
			event.rrule = {
				'freq': _GUI._DOM.id('#event_rrule_freq').value,
				'until': _GUI._DOM.id('#event_rrule_until').value,
				'count': _GUI._DOM.id('#event_rrule_count').value,
				'interval': _GUI._DOM.id('#event_rrule_interval').value
			};
		}
		return event;
	},
	/**
	 * Edit an event
	 * @param  {object} event Event as JS object
	 */
	edit: (event) => {
		_Tabs.open('tab_edit_event');
		_GUI._DOM.id('#event_uid').value = event.uid;
		_GUI._DOM.id('#event_date').value = event.date;
		let [start_hour,start_minute] = event.time.split(':');
		_GUI._DOM.id('#event_start_hour').innerHTML = start_hour;
		_GUI._DOM.id('#event_start_minute').innerHTML = start_minute;
		let [duration_hour,duration_minute] = event.duration.split(':');
		_GUI._DOM.id('#event_duration_hour').innerHTML = duration_hour;
		_GUI._DOM.id('#event_duration_minute').innerHTML = duration_minute;
		_GUI._DOM.id('#event_title').value = ('title' in event ? event.title : '');
		_GUI._DOM.id('#event_description').value = ('description' in event ? event.description : '');
		// rrule
		if (event.hasOwnProperty('rrule')) {
			_GUI._DOM.id('#event_rrule_freq').innerHTML = event.rrule.freq;
			_GUI._DOM.id('#event_rrule_count').value = event.rrule.count;
			_GUI._DOM.id('#event_rrule_interval').value = event.rrule.interval;
			_GUI._DOM.id('#event_rrule_until').value = event.rrule.until;
			_Tab.repeat.select_freq(event.rrule.freq);
		}
		// cat uid
		_GUI._DOM.id('#event_cat_uid').value = event.cat_uid;
		// update all buttons
		_Tab.event.upd_repeat_button();
		_Tab.event.upd_allday_button();
		_Tab.event.upd_cat_button();
	},
	/**
	 * Hide event tab
	 */
	hide: () => {
		_Tabs.open('tab_main');
	},
	/**
	 * Update repeat buttons
	 */
	upd_repeat_button: () => {
		// update repeat button according to parameters
		let button = _GUI._DOM.id('#button_event_repeat');
		let repeated = _GUI._DOM.id('#event_rrule_until').value.length || _GUI._DOM.id('#event_rrule_freq_radio').querySelectorAll('span.selected').length;
		if (repeated) {
			if (!button.classList.contains('selected')) {
				button.classList.add('selected');
			}
		} else {
			if (button.classList.contains('selected')) {
				button.classList.remove('selected');
			}
		}
	},
	/**
	 * Update allday button
	 */
	upd_allday_button: () => {
		// update allday button
		let allday = _GUI._DOM.id('#event_start_hour').innerHTML=='00' && _GUI._DOM.id('#event_start_minute').innerHTML=='00' && _GUI._DOM.id('#event_duration_hour').innerHTML=='24' && _GUI._DOM.id('#event_duration_minute').innerHTML=='00';
		let button = _GUI._DOM.id('#button_event_allday');
		if (allday) {
			if (!button.classList.contains('selected')) {
				button.classList.add('selected');
			}
		} else {
			if (button.classList.contains('selected')) {
				button.classList.remove('selected');
			}
		}
	},
	/**
	 * Update category button
	 */
	upd_cat_button: () => {
		let uid = _GUI._DOM.id('#event_cat_uid').value;
		let obj = _GUI._DOM.id('#button_event_cat');
		if (uid.length) {
			let cat = _Module.tbl.cats.obj[uid].obj;
			obj.style.color = cat.color;
			obj.style.background = _Tab.cat.get_css_background(cat);
			obj.className = 'rounded ' + cat.styles;
			obj.innerHTML = cat.name;
		} else {
			obj.style.color = '';
			obj.style.background = '';
			obj.className = 'rounded';
			obj.innerHTML = tr1('None');
		}
	}
};

