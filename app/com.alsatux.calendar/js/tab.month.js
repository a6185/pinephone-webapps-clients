// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Month tab
 */

_Tab.month = {
	/**
	 * Initialization
	 */
	init: () => {
		_GUI._DOM.listen('.ul_months_abrv li', 'click', _Tab.month.slot.close);
	},
	/**
	 * Slots (DOM events handlers)
	 */
	slot: {
		/**
		 * Open the tab
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		open: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tabs.open('tab_goto_month');
			});
		},
		/**
		 * Close the tab
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		close: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tabs.open('tab_main');
				let o = e.target;
				if (o.hasAttribute('data-value')) {
					let months = [...document.getElementsByClassName('month')];
					let i = o.getAttribute('data-value').to_uint();
					_GUI._DOM.id('#cal').parentNode.scrollTop = months[i].offsetTop;
				}
			});
		}
	}
};
