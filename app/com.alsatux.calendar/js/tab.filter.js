// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Filter tab
 */

_Tab.filter = {
	/**
	 * Initialization
	 */
	init: () => {
		_GUI._DOM.listen('#button_filter_close', 'click', _Tab.filter.slot.close);
		_GUI._DOM.listen('#button_filter_del', 'click', _Tab.filterdelete.slot.open);
		_GUI._DOM.listen('#div_filters_list', 'click', _Tab.filter.slot.research);
	},
	/**
	 * Slots (DOM events handlers)
	 */
	slot: {
		/**
		 * Open the tab
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		open: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tab.filter.ls();
				_Tabs.open('tab_filter');
			});
		},
		/**
		 * Close the tab
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		close: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tabs.open('tab_main');
			});
		},
		/**
		 * Research events
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		research: (e) => {
			return _GUI._Lock.action(e, (e) => {
				let obj = e.target.closest('span');
				if (obj!=null) {
					_Tab.filter.select(obj.innerHTML);
				}
			});
		}
	},
	/**
	 * Build filters list
	 */
	ls: () => {
		_GUI._DOM.id('#div_filters_list').innerHTML = '';
		for (let uid in _Module.tbl.filters.obj) {
			let filter = _Module.tbl.filters.obj[uid].obj;
			_GUI._DOM.id('#div_filters_list').innerHTML += '\
			<span class="rounded" data-uid="' + uid + '">' + filter.value + '</span>';
		}
	},
	/**
	 * Filter events
	 * @param  {string} kws Keywords to use
	 */
	select: (kws) => {
		_GUI._DOM.id('#input_keywords_filter').value = kws;
		_Tabs.show('tab_main');
		_Tab.main.filter_events(0);
	}
};
