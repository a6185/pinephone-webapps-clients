// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Events table
 */

class Events_Tbl extends PP_Tbl {
	/**
	 * Constructor
	 */
	constructor () {
		super();		
	}
	/**
	 * Load events
	 * @param  {object} events Events as JS object
	 */
	fill (events) {
		for (let uid in events) {
			let obj = events[uid];
			this.set(obj.uid,(new Event_Object()).upd(obj));
		}		
	}
	/**
	 * Add a new event to table
	 */
	add (obj) {
		this.set(obj.uid,(new Event_Object()).upd(obj));
		_Tab.cal.add(obj,true);
	}
	/**
	 * Update an event
	 * @param  {object} obj New event as JS object
	 */
	upd (obj) {
		this.set(obj.uid,(new Event_Object()).upd(obj));
		_Tab.cal.del(obj.uid);
		_Tab.cal.add(obj,true);
	}
	/**
	 * Delete an event
	 * @param  {object} obj Event as JS object
	 */
	del (obj) {
		this.unset(obj.uid);
		_Tab.cal.del(obj.uid);
	}
}