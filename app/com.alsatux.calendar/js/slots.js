// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Module slots
 */

var _Slots = {
	/**
	 * Register available routes
	 */
	init: () => {
		_Route.add('com.alsatux.calendar:/events/reset',_Slots.reset);
		_Route.add('com.alsatux.calendar:/filter/added',_Slots.filter.added);
		_Route.add('com.alsatux.calendar:/filter/updated',_Slots.filter.updated);
		_Route.add('com.alsatux.calendar:/filter/deleted',_Slots.filter.deleted);
		_Route.add('com.alsatux.calendar:/cat/added',_Slots.cat.added);
		_Route.add('com.alsatux.calendar:/cat/updated',_Slots.cat.updated);
		_Route.add('com.alsatux.calendar:/cat/deleted',_Slots.cat.deleted);
		_Route.add('com.alsatux.calendar:/event/added',_Slots.event.added);
		_Route.add('com.alsatux.calendar:/event/updated',_Slots.event.updated);
		_Route.add('com.alsatux.calendar:/event/deleted',_Slots.event.deleted);
	},
	/**
	 * Reset calendar using incoming datas from server
	 * @param  {object} data All datas (events, cats, filters)
	 */
	reset: (data) => {
		_Module.tbl.cats.fill(data.args.cats);
		_Module.tbl.filters.fill(data.args.filters);
		_Module.tbl.events.fill(data.args.events);
		// we need to redraw all because the lang and others settings can changed
		_Tab.cal.redraw_current_year();
		_Tab.cal.refresh_events();
		_Tab.main.scroll_today();
		_GUI._Log.info(tr1('Records found') + ': ' + Object.keys(_Module.tbl.events.obj).length);		
		_Module.loaded();
	},
	/**
	 * Categories callbacks
	 */
	cat: {
		/**
		 * A new category was added
		 * @param  {object} data Category as JS object
		 */
		added: (data) => {
			_Module.tbl.cats.add(data.args.cat);
		},
		/**
		 * A category was updated
		 * @param  {object} data Category as JS object
		 */
		updated: (data) => {
			_Module.tbl.cats.add(data.args.cat);
		},
		/**
		 * A category was deleted
		 * @param  {object} data Category as JS object
		 */
		deleted: (data) => {
			_Module.tbl.cats.del(data.args.cat);
		}
	},
	/**
	 * Filters callbacks
	 */
	filter: {
		/**
		 * A new filter was added
		 * @param  {object} data Filter as JS object
		 */
		added: (data) => {
			_Module.tbl.filters.add(data.args.filter);
		},
		/**
		 * A filter was updated
		 * @param  {object} data Filter as JS object
		 */
		updated: (data) => {
			_Module.tbl.filters.add(data.args.filter);
		},
		/**
		 * A filter was deleted
		 * @param  {object} data Filter as JS object
		 */
		deleted: (data) => {
			_Module.tbl.filters.del(data.args.filter);
		}
	},
	/**
	 * Events callbacks
	 */
	event: {
		/**
		 * A new event was added
		 * @param  {object} data Event as JS object
		 */
		added: (data) => {
			_Module.tbl.events.add(data.args.event);
		},
		/**
		 * A event was updated
		 * @param  {object} data Event as JS object
		 */
		updated: (data) => {
			_Module.tbl.events.upd(data.args.event);
		},
		/**
		 * A event was deleted
		 * @param  {object} data Event as JS object
		 */
		deleted: (data) => {
			_Module.tbl.events.del(data.args.event);
		}
	}
};