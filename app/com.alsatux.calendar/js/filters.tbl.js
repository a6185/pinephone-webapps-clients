// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Filters table
 */

class Filters_Tbl extends PP_Tbl {
	/**
	 * Constructor
	 */
	constructor () {
		super();		
	}
	/**
	 * Load filters
	 * @param  {object} filters Filters as JS object
	 */
	fill (filters) {
		for (let uid in filters) {
			let obj = filters[uid];
			this.set(obj.uid,(new Filter_Object()).upd(obj));
		}		
	}
	/**
	 * Add a new filter to table
	 */
	add (obj) {
		this.set(obj.uid,(new Filter_Object()).upd(obj));
		_Tab.filter.ls();
		_Tab.filterdelete.ls();
	}
	/**
	 * Delete an filter
	 * @param  {object} obj Filter as JS object
	 */
	del (obj) {
		this.unset(obj.uid);
		_Tab.filter.ls();
		_Tab.filterdelete.ls();
	}
}