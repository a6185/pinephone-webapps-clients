// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Try to clean phone/sms numbers before sending them to the modem
 */

class Phone_Cleaner {
	/**
	 * Constructor
	 * @param  {string} number Phone/SMS number
	 */
	constructor (number) {
		this.number = number;
	}
	/**
	 * Clean phone number
	 * @return {string} Phone number cleaned
	 */
	clean () {
		let number = this.number;
		number = number.replace(/[^0-9a-z\+]+/ig,'');
		if (/^00/.test(number)) { // International call
			number = number.replace(/^00/,'+');
		}
		let _Pref_Object = _Prefs_Tbl.find_by_cat_key('default','phoneprefix');
		switch (_Pref_Object.cur) {
			case 'FR':
				// 0389768566 -> +33389768566
				if (/^0/.test(number)) { // Call inside France
					number = number.replace(/^0/,'+33');
				}
				break;
			// add your own country here !
		}
		return number;
	}
}