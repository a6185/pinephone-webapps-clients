// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Global preferences
 */

var _Prefs_Slots = {
	/**
	 * Register preferences routes
	 */
	init: () => {
		_Route.add('com.alsatux.prefs:/list', _Prefs_Slots.handler);
		_Route.add('com.alsatux.prefs:/updated', _Prefs_Slots.handler);
	},
	/**
	 * Handle preferences returned by server
	 * @param  {object} data Hash of datas
	 */
	handler: (data) => {
		if (hasKey(data, 'args', Object)) {
			if (hasKey(data.args, 'prefs', Object)) {
				_Prefs_Tbl.refresh(data.args.prefs);
			}
		}
	}
};