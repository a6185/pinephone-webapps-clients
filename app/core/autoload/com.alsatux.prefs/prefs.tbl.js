// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Global preferences
 */

class Prefs_Tbl extends PP_Tbl {
	/**
	 * Constructor
	 */
	constructor () {
		super();
	}
	/**
	 * Refresh current object
	 * @param  {object} obj Hash of preferences
	 */
	refresh (obj) {
		this.obj = obj;	
		// app program is started here
		_Module.callback();
	}
	/**
	 * Find a preference object using its cat/key
	 * @param  {string} cat Category
	 * @param  {string} key Uniq key
	 * @return {object|null}     Preference or null
	 */
	find_by_cat_key (cat,key) {
		for (let uid in this.obj) {
			if (this.obj[uid].cat==cat && this.obj[uid].key==key) {
				return this.obj[uid];
			}
		}
		return null;
	}
};

var _Prefs_Tbl = new Prefs_Tbl();