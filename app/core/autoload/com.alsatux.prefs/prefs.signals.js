// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Global preferences
 */

var _Prefs_Signals = {
	/**
	 * On startup, get all preferences stored on server side
	 */
	start: () => {
		_WS.send({
			route: 'com.alsatux.prefs:/read',
			args: {
			},
			callback: 'com.alsatux.prefs:/list'
		});
	}
};