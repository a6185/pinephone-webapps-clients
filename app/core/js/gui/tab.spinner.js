// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Tab spinner is used to lock the GUI and to display a waiting spinner to the user,
 * as long an action is currently running.
 */


_Tab.spinner = {
	/**
	 * Init the GUI
	 */
	init: () => {
		_GUI._DOM.listen('#tab_spinner_bg', 'click', _Tab.spinner.slot.stop_propagation);
		_GUI._DOM.listen('#tab_spinner_bg', 'touchstart', _Tab.spinner.slot.stop_propagation);
		_GUI._DOM.listen('#tab_spinner_bg', 'touchend', _Tab.spinner.slot.stop_propagation);
		_GUI._DOM.listen('#tab_spinner_bg', 'touchmove', _Tab.spinner.slot.stop_propagation);
		_GUI._DOM.listen('#tab_spinner', 'click', _Tab.spinner.slot.stop_propagation);
		_GUI._DOM.listen('#tab_spinner', 'touchstart', _Tab.spinner.slot.stop_propagation);
		_GUI._DOM.listen('#tab_spinner', 'touchend', _Tab.spinner.slot.stop_propagation);
		_GUI._DOM.listen('#tab_spinner', 'touchmove', _Tab.spinner.slot.stop_propagation);
	},
	slot: {
		/**
		 * Stop DOM bubbling
		 * @param  {event} e DOM event
		 * @return {bool}   False
		 */
		stop_propagation: (e) => {
			e.preventDefault();
			e.stopPropagation();
			return false;
		}
	},
	/**
	 * Display the spinner animation
	 */
	show: () => {
		_GUI._DOM.show('#tab_spinner_bg');
		_GUI._DOM.show('#tab_spinner');
	},
	/**
	 * Clear the spinner then hide the spinner animation
	 */
	hide: () => {
		_Tab.spinner.clear();
		_GUI._DOM.hide('#tab_spinner_bg');
		_GUI._DOM.hide('#tab_spinner');
	},
	/**
	 * Hide welcome message the clear the spinner text
	 */
	clear: () => {
		_GUI._DOM.hide('#spinner_welcome');
		_GUI._DOM.id('#spinner_txt').innerHTML = '';
	},
	/**
	 * Add a new message to the spinner text
	 * @param  {string} mess Message to add
	 */
	log: (mess) => {
		let p = document.createElement('P');
		p.innerHTML = _GUI._HTML.escape(mess);
		_GUI._DOM.id('#spinner_txt').appendChild(p);
	}
};
