// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Global log, associated to log tab
 */
class GUI_Log {
	/**
	 * Add an info message to tab log
	 * @param  {string} mess Message to add
	 * @return {boll}      Always true
	 */
	info (mess) {
		_Tab.log.push(mess, 'info');
		return true;
	}
	/**
	 * Add a warning message to tab log
	 * @param  {string} mess Message to add
	 * @return {boll}      Always true
	 */
	warn (mess) {
		_Tab.log.push(mess, 'warn');
		return true;
	}
	/**
	 * Add an error message to tab log
	 * @param  {string} mess Message to add
	 * @return {boll}      Always false
	 */
	error (mess) {
		_Tab.log.push(mess, 'error');
		return false;
	}
}

