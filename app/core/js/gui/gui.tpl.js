// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Global GUI templace
 */
class GUI_Tpl {
	/**
	 * Constructor
	 */
	constructor () {
		this.dict = []; //
	}
	/**
	 * Set the dictionnary containing all available locales
	 * @param {hash} dict Dictionnary
	 */
	set (dict) {
		this.dict = dict;
	}
	/**
	 * Find and replace all GUI elements labels according to current locale
	 * @param  {string} locale Current locale
	 */
	load (locale) {
		let re = new RegExp('^[a-z0-9_]+$','i');
		let langs = Object.keys(_Module._Manifest.obj.lang);
		let offset = langs.length + 1;
		let pos = (langs.indexOf(locale)!=-1) ? langs.indexOf(locale) : 0;
		for (let i=0;i<this.dict.length;i+=offset) {
			let key = this.dict[i];
			/*if (key=='ul_months_abrv') {
				debugger;
			}*/
			let value = this.dict[i+1+pos];
			let obj = _GUI._DOM.id('#' + key);
			if (obj!=null) {
				if (obj.hasAttribute('placeholder')) {
					obj.setAttribute('placeholder',value);
					continue;
				}
				obj.innerHTML = value;
			} else {				
				let elements = null;
				if (value.constructor === Array) {
					if (re.test(key)) {					
						elements = document.querySelectorAll('.' + key + ' span');
						if (elements.length) {
							elements.forEach(el => {
								el.innerHTML = value.shift();
							});
							continue;
						}
						elements = document.querySelectorAll('.' + key + ' li');
						if (elements.length) {
							elements.forEach(el => {
								el.innerHTML = value.shift();
							});
							continue;
						}
					}
				}
				if (value.constructor === String) {
					elements = document.querySelectorAll('*[data-value="' + key + '"]');
					if (elements.length) {
						elements.forEach(el => {
							el.innerHTML = value;
						});
						continue;
					}
					if (re.test(key)) {					
						elements = document.querySelectorAll('.' + key);
						if (elements.length) {
							elements.forEach(el => {
								el.innerHTML = value;
							});
							continue;
						}
					}
				}
				_Lang.object_not_found(this.dict[i]);
			}
		}		
	}
}