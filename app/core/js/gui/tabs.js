// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Tabs main controller
 */

var _Tabs = {
	cur_id: null, // DOM if of the current tab displayed to user
	divs: [], // Array of tabs (DOM div)
	/**
	 * Research all existing tabs then open tab id
	 * @param  {string} id Tab id to open
	 */
	init: (id) => {
		_Tabs.divs = [...document.getElementsByClassName('tab')];
		_Tabs.open(id);
	},
	/**
	 * Check if tab exists and display an error message if not
	 * @param  {string} id Tab id
	 * @return {bool}    True is tab exists
	 */
	chk: (id) => {
		if (!_Tabs.exists(id)) {
			alert(`Tab ${id} not found !`);
			return false;
		}
		return true;
	},
	/**
	 * Check if a tab existes
	 * @param  {string} id Tab id
	 * @return {bool}    True if tab existes
	 */
	exists: (id) => {
		return _Tabs.divs.map(x => x.id).indexOf(id)!=-1;
	},
	/**
	 * Update the tab id, hiding all others tab
	 * @param  {string} id Tab id
	 */
	open: (id) => {
		if (_Tabs.chk(id) && _Tabs.cur_id!=id) {
			_Tabs.divs.forEach(tab => {
				tab.style.display = tab.id==id ? 'block' : 'none';
			});
			_Tabs.cur_id = id;
		}
	}
};

