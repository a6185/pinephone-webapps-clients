// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Global CSS functions
 */
class GUI_CSS {
	/**
	 * Remove all classes of a DOM object
	 * @param  {array} objs DOM objects
	 */
	resetClassName (objs) {
		for (let i=0;i<objs.length;i++) {
			objs[i].className = '';
		}
	}
}
