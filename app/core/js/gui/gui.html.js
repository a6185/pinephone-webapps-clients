// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Global HTML methods
 */
class GUI_HTML {
	/**
	 * Set several attributes to a DOM object
	 * @param {string} id   DOM object id
	 * @param {hash} hash Attributes to add
	 */
	setAttribute (id, hash) {
		let obj = _GUI._DOM.id('#' + id);
		for (let key in hash) {
			obj.setAttribute(key, escape(hash[key]));
		}
	}
	/**
	 * Get an attribute for an object
	 * @param  {string} id  DOM object id
	 * @param  {string} key Attribute name
	 * @return {object}     Attribute value
	 */
	getAttribute (id, key) {
		let obj = _GUI._DOM.id('#' + id);
		return unescape(obj.getAttribute(key));
	}
	/**
	 * Autofill DOM elements using an hash of keys/values
	 * @param  {hash} hash Keys/Values
	 */
	fill (hash) {
		for (let fieldname in hash) {
			let value = hash[fieldname];
			if (value === undefined) {
				console.log(_Lang0.tr('UNDEFINED value for field') + ' ' + fieldname);
				value = '';
			}
			if (value === null) {
				console.log(_Lang0.tr('NULL value for field') + ' ' + fieldname);
				value = '';
			}
			let obj = _GUI._DOM.id('#' + fieldname);
			if (!obj) {
				console.log(fieldname + ' ' + _Lang0.tr('not found in document') + '.');
			} else {
				if (obj.type) { // form element
					// do not use _GUI._HTML.escape here !
					if (obj.type=='checkbox') {
						obj.checked = ((value=='true' || value=='1') ? true : false);
					}
					if (obj.type=='text' || obj.type=='hidden') {
						obj.value = value;
					}
					if (obj.type=='textarea') {
						obj.value = value;
					}
					if (obj.type=='select-one') {
						obj.value = value;
					}
				} else { // span
					obj.innerHTML = value;
				}
			}
		}
	}
	/**
	 * Escape XML chars
	 * @param  {string} str Source string
	 * @return {string}     String escaped
	 */
	escape (str) {
		return String(str)
		.replace(/&/g, "&amp;")
		.replace(/</g, "&lt;")
		.replace(/>/g, "&gt;")
		.replace(/"/g, "&quot;")
		.replace(/'/g, "&#039;");
	}
}

