// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Global selector
 * Use <span data-value="value">text</span> for a single value
 * or <input type="text" data-value="value1,value2" value="text1,text2"> for multiple values
 */

_Tab.selector = {
	tab_parent: 'tab_main', // default (to override)
	target: null, // DOM target object- span or input
	callback: null, // callback function or null (default)
	nboptions: { // default : radio mode (one option mandatory)
		min: 1,
		max: 1
	},
	/**
	 * Init the GUI
	 */
	init: () => {
		_GUI._DOM.listen('#button_selector_backward', 'click', _Tab.selector.slot.upd);
		_GUI._DOM.listen('#selector', 'click', _Tab.selector.slot.toogle);
		_GUI._DOM.listen('#selector_kws', 'keyup', _Tab.selector.slot.research);
		_GUI._DOM.listen('#selector_clear_kws', 'click', _Tab.selector.slot.reset);
	},
	slot: {
		/**
		 * Open and fill the selector
		 * @param  {string} tab_parent Parent tab
		 * @param  {object} target     Target object
		 * @param  {hash|null} options    Selector properties or null (in this case, options should be retrieve using HTML ul)
		 */
		open: (tab_parent,target,data,callback) => {
			_Tab.selector.tab_parent = tab_parent;
			_Tab.selector.target = target;
			_Tab.selector.callback = callback;
			_Tabs.open('tab_selector');
			_Tab.selector.build(data);
			_Tab.selector.select(target);
		},
		/**
		 * Close current selector and return to parent tabulation
		 * @param  {event} e HTML event
		 * @return {function}   GUI lock action
		 */
		close: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tabs.open(_Tab.selector.tab_parent);
			});
		},
		/**
		 * Update the target field with new values and return to parent tab
		 * @param  {event} e Click
		 */
		upd: (e) => {
			let keys = [];
			let values = [];
			[...document.querySelectorAll('#selector span.selected')].forEach(span => {
				let key = span.getAttribute('data-value');				
				keys.push(key);
				values.push(span.textContent);
			});
			// update parent field
			let target = _Tab.selector.target;
			target.setAttribute('data-value',keys.join(','));
			let text = _GUI._HTML.escape(values.join(','));
			if (target.nodeName == 'SPAN') {
				target.textContent = text;
			}
			if (target.nodeName == 'INPUT') {
				target.value = text;
			}
			// close and reset selector
			_Tab.selector.slot.close(e);
			_Tab.selector.slot.reset(e);
			if (_Tab.selector.callback !== null) {
				_Tab.selector.callback(keys,values);
			}
		},
		/**
		 * Toogle options in the selector
		 * @param  {event} e User's click on a span
		 */
		toogle: (e) => {
			let target = e.target.closest('span');
			if (target!=null) {
				let selection = document.querySelectorAll('#selector span.selected');					
				let nb_selected = [...selection].length;
				if (target.classList.contains('selected')) { // selected
					if ((nb_selected-1)<_Tab.selector.nboptions.min) {
						return alert(`Too few options not allowed (min is ${_Tab.selector.nboptions.min})`);
					} else {
						target.classList.toggle('selected');						
					}
				} else { // unselected
					if (_Tab.selector.nboptions.max>1) { // multiple
						if ((nb_selected+1)>_Tab.selector.nboptions.max) {
							return alert(`Too many options not allowed (max is ${_Tab.selector.nboptions.max})`);
						} else {
							target.classList.toggle('selected');
						}
					} else { // max is 1 (single)
						selection.forEach(el => {
							el.classList.remove('selected');
						});
						target.classList.toggle('selected');
					}
				}
			}
		},
		/**
		 * Use the research field to hide some options
		 * @param  {event} e Keydown
		 */
		research: (e) => {
			let kws = e.target.value.trim();
			if (kws.length) {
				[...document.querySelectorAll('#selector li')].forEach(li => {
					let span = li.querySelector('span');
					if (span.textContent.toUpperCase().indexOf(kws.toUpperCase())!=-1) {
						li.style.display = 'inline-block';
					} else {
						li.style.display = 'none';
					}
				});
			} else {
				_Tab.selector.slot.reset(e);
			}
		},
		/**
		 * Reset the research field
		 * @param  {event} e Click
		 */
		reset: (e) => {
			_GUI._DOM.id('#selector_kws').value = '';
			[...document.querySelectorAll('#selector li')].forEach(li => {
				li.style.display = 'inline-block';
			});
		}
	},
	/**
	 * Build list options
	 * @param  {hash|null} data Selector properties or null (in this case, options should be retrieve using HTML ul)
	 */
	build: (data) => {
		let target = _Tab.selector.target;
		_Tab.selector.nboptions = { // reset to defaults
			min: 1,
			max: 1
		};
		let selector = _GUI._DOM.id('#selector');
		selector.innerHTML = '';
		if (data !== null) {
			if (hasKey(data, 'nboptions', Array)) {
				let nbo = data.nboptions.split(',');
				_Tab.selector.nboptions = { // override defaults
					min: nbo[0],
					max: nbo[1]
				};
			}
			if (hasKey(data, 'options', Array)) { // array of objects with format {key: abc, value: 123}
				data.options.forEach(option => {
					let li = document.createElement('li');
					let span = document.createElement('span');
					span.innerHTML = option.value;
					span.setAttribute('data-value',option.key);
					li.appendChild(span);
					selector.appendChild(li);
				});
			}
		}
		// HTML settings ovveride JS code
		// retrieve min/max options from the parentNode
		if (target.parentNode.hasAttribute('data-nboptions')) { // min,max
			let nbo = target.parentNode.getAttribute('data-nboptions').split(',');
			_Tab.selector.nboptions = { // override defaults
				min: nbo[0],
				max: nbo[1]
			};
		}
		// retrieve options from the parentNode + linked ul
		if (target.parentNode.hasAttribute('data-options')) { // static options
			let options = target.parentNode.getAttribute('data-options');
			selector.innerHTML = _GUI._DOM.id('#' + options).innerHTML;
		}
	},
	/**
	 * Select/highlight current values
	 * @param  {object} target Target object
	 */
	select: (target) => {
		if (target==null) {
			alert('TabSelector : null target given...');
		}
		// retieve current values
		let values = [];
		// a span MUST always get a data-value attribute !
		if (target.nodeName == 'SPAN') {			
			if (!target.hasAttribute('data-value')) { // static options
				alert('Missing data-value for target SPAN !');
			} else {
				values = target.getAttribute('data-value').split(',');
			}
		}
		if (target.nodeName == 'INPUT') {
			values = target.value.split(',');
		}
		// highlight current active values
		values.forEach(value => {
			if (value.length) {
				let option = document.querySelector(`#selector li span[data-value="${value}"]`);
				if (option !== null) {
					option.classList.add('selected');
				} else {
					alert(`Selector value "${value}" not found - so ignored...`);
				}
			}
		});
	}		
}