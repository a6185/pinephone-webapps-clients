// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Global Lock
 */

class GUI_Lock {
	constructor () {
		this.flag = false;
	}
	/**
	 * Lock GUI
	 * @return {bool} Success
	 */
	on () {
		if (this.flag==false) {
			this.flag = true;
			return true;
		} else {
			alert(_Lang0.tr('Pending action') + '...');
			return false;
		}
	}
	/**
	 * Unlock GUI
	 */
	off () {
		this.flag = false;
	}
	/**
	 * Stop DOM bubbling then make the action with no spinner
	 * @param  {event} e DOM event
	 * @param  {string} f Callback function
	 * @return {bool}   Success
	 */
	action (e, f) {
		if (e!=null) {
			e.preventDefault();
			e.stopPropagation();
			if (this.on()) {
				try {
					f.apply(f,[e]);
					this.off();
				} catch (err) {
					alert(err);
					this.off();
				}
			}
		}
		return false;
	}
	/**
	 * Stop DOM bubbling then make the action with spinner
	 * @param  {event} e DOM event
	 * @param  {string} f Callback function
	 * @return {bool}   Success
	 */
	action_with_spinner (e, f) {
		if (e!=null) {
			e.preventDefault();
			e.stopPropagation();
			if (this.on()) {
				_Tab.spinner.show();
				setTimeout( () => {
					try {
						f.apply(f,[e]);
						this.off();
						_Tab.spinner.hide();
					} catch (err) {
						alert(err);
						this.off();
						_Tab.spinner.hide();
					}
				}, 100);
			}
		}
		return false;
	}
}
