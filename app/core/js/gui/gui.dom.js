// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Global DOM methods
 */
class GUI_DOM {
	/**
	 * Shortcut to return a single DOM element
	 * @param  {string} selector CSS selector
	 * @return {object|null}          DOM object found or null
	 */
	id (selector) {
		return document.querySelector(selector);
	}
	/**
	 * Display DOM elements
	 * @param  {string} selector CSS selector
	 * @return {objects|null}          DOM objects found
	 */
	show (selector) {
		[...document.querySelectorAll(selector)].forEach(el => {
			el.style.display = 'block';
		});
	}
	/**
	 * Hide DOM elements
	 * @param  {string} selector CSS selector
	 */
	hide (selector) {
		[...document.querySelectorAll(selector)].forEach(el => {
			el.style.display = 'none';
		});
	}
	/**
	 * Add an event listener to several DOM objects
	 * @param  {string}   selector CSS selector
	 * @param  {event}   event    DOM event
	 * @param  {function} callback Function callback
	 */
	listen (selector, event, callback) {
		[...document.querySelectorAll(selector)].forEach(el => {
			el.addEventListener(event,callback);
		});
	}
	/**
	 * Remove an event lister to several DOM objects
	 * @param  {string}   selector CSS selector
	 * @param  {event}   event    DOM event
	 * @param  {function} callback Function callback
	 */
	unlisten (selector, event, callback) {
		[...document.querySelectorAll(selector)].forEach(el => {
			el.removeEventListener(event,callback);
		});
	}
	/**
	 * Get DOM objects with same class names
	 * @param  {string} names CSS class names
	 * @return {array}       Array of DOM objects
	 */
	byClassName (names) {
		return [...document.getElementsByClassName(names)];
	}
}
