// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Tab log is mandatory for all application.
 * It's used as hidden div to concentrate all debug messages usefull to devs.
 */

_Tab.log = {
	counter: 1, // message counter
	/**
	 * Init the GUI
	 */
	init: () => {
		_GUI._DOM.listen('#button_log_open', 'click', _Tab.log.slot.show);
		_GUI._DOM.listen('#button_log_close', 'click', _Tab.log.slot.hide);
	},
	slot: {
		/**
		 * Display tab log
		 * @param  {event} e DOM click
		 * @return {function}   GUI lock action
		 */
		show: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_GUI._DOM.show('#tab_log');
			});
		},
		/**
		 * Hide tab log
		 * @param  {event} e DOM click
		 * @return {function}   GUI lock action
		 */
		hide: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_GUI._DOM.hide('#tab_log');
			});
		}
	},
	/**
	 * Clear tab content
	 */
	clear: () => {
		_GUI._DOM.id('#tab_log_events').innerHTML = '';
	},
	/**
	 * Add a new message to the tab content
	 * @param  {string} mess      Message to add
	 * @param  {string} className Message type (err,info,warn,dbg)
	 */
	push: (mess, className) => {
		let index = sprintf('%04d : ',_Tab.log.counter++ % 10000);
		let p = document.createElement('p');
		p.className = 'log_' + className;
		p.innerHTML = _GUI._HTML.escape(index + mess);
		let parent = _GUI._DOM.id('#tab_log_events');
		parent.insertBefore(p, parent.firstChild);
	}
};

