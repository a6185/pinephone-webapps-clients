// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Tab settings is mandatory for all applications.
 * It contains at least the module languages and the ip/port for the websocket to connect to.
 */

_Tab.settings = {
	tab_on_close: 'tab_main', // tab to call when closing settings - default is main tab
	/**
	 * Init the GUI
	 */
	init: () => {
		_GUI._DOM.listen('#button_settings_close', 'click', _Tab.settings.slot.close);
		_GUI._DOM.listen('#button_settings_submit', 'click', _Tab.settings.slot.click_submit);
	},
	slot: {
		/**
		 * Open the tab settings for all applications
		 * @param  {event} e DOM click
		 * @return {function}   GUI lock action
		 */
		open: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tabs.open('tab_settings');
			});
		},
		/**
		 * Close the tab settings, record the changes then return to tab_on_close
		 * @param  {event} e DOM click
		 * @return {function}   GUI lock action
		 */
		close: (e) => {
			return _GUI._Lock.action(e, (e) => {
				// check if changes occured and have not been recorded
				if (_Module._Settings.get_flag('settings_changed')) {
					if (confirm(_Lang0.tr('Record unsaved new settings') + _Lang0.tr('?'))) {
						_Module._Settings.rec();
					}
				}
				_Tabs.open(_Tab.settings.tab_on_close);
			});
		},
		/**
		 * Record new settings without closing tab
		 * @param  {event} e DOM click
		 * @return {function}   GUI lock action
		 */
		click_submit: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Module._Settings.rec();
			});
		}
	},
}

