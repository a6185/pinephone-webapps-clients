// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Try to control webapps windows.
 * Sometimes, a webapp needs to open another webapp (example: the diary webapp to the phone or sms webapp).
 * We use here the namespace of the webapp to be sure that only one tab will be opened, but in fact,
 * if the user already has manually opened a webapp, it will be displayed twice...
 * So this point should be improved !
 */
class GUI_Window {
	constructor () {
		this.tab = {}; // references to windows containing webapps
	}
	/**
	 * Open a webapp to a new navigator tab, using namespace as reference
	 * @param  {string} namespace Tab namespace
	 * @return {element}           DOM window
	 */
	open (namespace) {
		if (this.tab[namespace] === undefined) {
				this.tab[namespace] = window.open(`https://mobian/app/${namespace}`,namespace);
			} else {
				if (this.tab[namespace].closed) {
					this.tab[namespace] = window.open(`https://mobian/app/${namespace}`,namespace);
				}
			}
			this.tab[namespace].focus();
			return this.tab[namespace];
	}
}
