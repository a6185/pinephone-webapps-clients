// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Global Manifest object
 */

class Manifest {
	/**
	 * Constructor
	 */
	constructor () {
		this.obj = {};
	}
	/**
	 * Read the manifest for the current module
	 * @return {promise}
	 */
	read () {
		return new Promise((resolve,reject) => {
			fetch('manifest.json').then(response => {
				if (response.status !== 200) {
					reject('Unable to read manifest.json. Status Code: ' + response.status);
				}
				response.json().then(data => {
					this.obj = data;
					_Lang.register(this.obj.lang);
					_GUI._Log.info(_Lang0.tr('Please report any bug to') + ' ' + this.obj.author.email + ' ' + _Lang0.tr('!'));
					this.autofill();
					resolve();
				});
			}).catch(err => {
				reject(err);
			});
		});
	}
	/**
	 * Use manifest informations to autofill some HTML fields
	 */
	autofill () {
		[...document.querySelectorAll('.manifest')].forEach(el => {
			let key = el.getAttribute('data-key');
			switch (key) {
				case 'internal':
					el.id = this.obj.internal;
					break;
				case 'author_name':
					el.content = _GUI._HTML.escape(this.obj.author.name);
					break;
				case 'icons_256':
					el.href = _GUI._HTML.escape(this.obj.icons['256']);
					break;
				case 'spinner':
					el.innerHTML = `
						<p><span>${this.obj.title}</span> - v.<span>${this.obj.version}</span></p>
						<p><span><img class="icon manifest" src="${this.obj.icons['60']}" alt="Logo"/></span></p>`;
					break;
				case 'infos':
					el.innerHTML = `
						<p><img class="icon manifest" src="${this.obj.icons['128']}" alt="Logo"/></p>
						<p>
							<span>${this.obj.title}</span> - v.<span>${this.obj.version}</span><br/>
							<span>${this.obj.author.name}</span><br/>
							<a href="mailto:${this.obj.author.email}"><span>${this.obj.author.email}</span></a><br/>
							<a href="${this.obj.author.url}" target="_blank">${this.obj.author.url}</a><br/>
							<span>${this.obj.author.company}</span> - <span>${this.obj.year}</span>
						</p>`;
					break;
				default: // span or title
					el.innerHTML = _GUI._HTML.escape(this.obj[key]);
			}
		});
	}
}