// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Default debug function
 * @param  {string} msg Message to display
 */
function debug (msg) {
	console.log(msg);
}