// Jean Luc Biellmann - contact@alsatux.com

"use strict";

var _Lang_Backoffice = {
	available: {
		'en-GB': 'English', 
		'fr-FR': 'Français'
	},
	dict: [
// file.js
'Websocket not available','Websocket non disponible',
// html.js
'No value defined for field','Pas de valeur définir pour le champ',
'not found in document','introuvable dans le document',
// local.storage.js
'"Local storage" API not available','API "Stockage local" non disponible',
// lock.js
'Pending action','Action en cours',
// route.js
'Callback','Procédure de retour',
'Unknown route','Route inconnue',
'!',' !',
// sd.js
'Storage media is now','Le media de stockage est maintenant',
'Storage path is now','Le dossier de stockage est maintenant',
'Last backup','Dernière sauvegarde',
'No backup available','Aucun sauvegarde disponible',
'File','Fichier',
'succesfully readed','lu avec succès',
'Unable to read the last backup','Imopssible de lire la dernière sauvegarde',
'Writing backup has failed','La sauvegarde a échoué',
'File saved','Fichier enregistré',
'Lang is now','Langue courante',
// lang.js
'Lang is now','Langue courante',
'Object','L\'objet',
'has no translation','n\'a pas été traduit',
// uploader.js
'Drag and drop your XML backup on this button','Glissez/déposez votre sauvegarde XML sur ce bouton',
// websocket.js
'Unable to open websocket','Impossible d\'ouvrir la websocket',
'Websocket available','Websocket disponible',
'Websocket not available','Websocket non disponible',
'Websocket host is now','L\'hôte Websocket est maintenant',
'Websocket port is now','Le port Websocket est maintenant',
'Trying to connect to','Tentative de connexion à',
'Socket error','Erreur dans la socket',
'Lost connection','Connexion perdue',
'Incoming datas','Données entrantes',
'Status code','Code de retour',
'Socket open','Socket ouverte',
// xml.settings.js
'Parsing XML failed','L\'analyse XML a échoué',
'Reading settings failed','La lecture des préférences a échoué',
'Recording settings failed','L\'enregistrement des préférences a échoué',
	]
};