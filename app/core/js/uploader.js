// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Global file uploader using drag'n drop mouse
 */

function Uploader () {
}
Uploader.prototype = {
	/**
	 * Transform a DIV object to an uploader
	 * @param  {string}   id       DOM id of the HTML object
	 * @param  {function} callback Method to call on upload
	 */
	init: (id, callback) => {
		var obj = document.getElementById(id);
		obj.addEventListener('click', (e) => {
			alert(_Lang0.tr('Drag and drop your XML backup on this button') + _Lang0.tr('!'));
		});
		obj.addEventListener('dragleave', (e) => {
			e.preventDefault();
			e.stopPropagation();
			return false;
		});
		obj.addEventListener('dragover', (e) => {
			e.preventDefault();
			e.stopPropagation();
			return false;
		});
		obj.addEventListener('dragenter', () => {
			return false;
		});
		obj.addEventListener('drop', (e) => {
			e.preventDefault();
			e.stopPropagation();
			var event = e.originalEvent || e;
			if (!event || !event.dataTransfer || !event.dataTransfer.files || !event.dataTransfer.files.length)
				return false;
			for (var i=0;i<event.dataTransfer.files.length;i++)
				callback(event.dataTransfer.files[i]);
			return false;
		});
	}
};

