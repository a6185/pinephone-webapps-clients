// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Global LocalStorage object
 */

var _LocalStorage = {
	/**
	 * Check if local storage is available
	 * @return {bool} Success
	 */
	allowed: () => {
		return localStorage!==undefined;
	},
	/**
	 * Retrieve a value from the local storage using its key
	 * @param  {string} key Key in local storage
	 * @return {string|null}     Value or null if none
	 */
	read: (key) => {
		if (!_LocalStorage.allowed()) {
			_GUI._Log.warn(_Lang0.tr('Local storage API not available') + '.');
			return null;
		}
		return localStorage.getItem(key); // return null if not found
	},
	/**
	 * Store/Update a new element in the local storage
	 * @param  {string} key Key
	 * @param  {string} data Value
	 * @return {bool}      Success
	 */
	write: (key,data) => {
		if (!_LocalStorage.allowed()) {
			_GUI._Log.warn(_Lang0.tr('Local storage API not available') + '.');
			return false;
		}
		try {
			// backup last file
			let bak = localStorage.getItem(key);
			if (bak !== null) {
				localStorage.setItem(key + '_bak', bak);
			}
			// record the new one
			localStorage.setItem(key,data);
			return true;
		} catch (err) {
			alert(sprintf('%s%s',_Lang0.tr('Disk full'),_Lang0.tr('!')));
			return false;
		}
	}
};

