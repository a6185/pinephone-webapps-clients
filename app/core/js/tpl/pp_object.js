// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class PP_Object {
	constructor (classname,obj) {
		this.classname = classname;
		this.obj = obj;
	}
	debug (level, msg) {
		debug(level,`<module_${this.classname}>${msg}</module_${this.classname}>`);
	}
	set (key, value) {
		this.obj[key] = value;
	}
	get () {
		return this.obj;
	}
	upd (obj) {
		for (let key in obj) {
			this.set(key,obj[key]);
		}
		return this;
	}
	import (obj) {
		this.obj = obj;
	}
}