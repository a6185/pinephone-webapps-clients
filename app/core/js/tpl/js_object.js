// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class JS_Object {
	constructor () {
		this.obj = {};
	}
	clear () {
		this.obj = {};
	}
	set (key, value) {
		this.obj[key] = value;
	}
	get (key) {
		return this.has(key) ? this.obj[key] : null;
	}
	unset (key) {
		if (this.has(key)) {
			delete this.obj[key];
		}
	}
	has (key) {
		return this.obj.hasOwnProperty(key);
	}
	/**
	 * Get current table length
	 * @return {number} Table length
	 */
	length () {
		 return Object.keys(this.obj).length;
	}
	import (obj) {
		this.obj = obj;
	}
	export () {
		return this.obj;
	}
}