// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class PP_Module extends JS_Object {
	constructor (namespace){
		super();
		this.namespace = namespace;
		this.callback = null;
		this._Manifest = null;
		this._Settings = null;
		this._IPC = new PP_IPC(this);
		this._Dict = new PP_Dict(this);
		this.const = {};
		this.tbl = {};
		this.core = {};
		this.slots = {};
		this.signals = {};
		this.set('loaded',false);
	}
	init () {
		this._Manifest = new Manifest();
		this._Settings = new Settings();
	}
	settings () {
		// fix default settings
		this._Settings.init();
		// override current values from localStorage if any
		this._Settings.read();
		// override upd_cur and/or callback
		this._Settings.override();
		// add HTML
		this._Settings.render();
		// save settings for future sessions
		this._Settings.write('silently');
	}
	run (callback) {
		this.callback = callback;
		/* set websocket host and port */
		//_WS.init(this._Settings.tbl.settings.obj['server.websocket'].obj.cur);
		/* add prefs slots */
		_Prefs_Slots.init();
		/* add local slots */
		_Slots.init();
		// Oh No ! More Lemmings
		_WS.watchdog(this, this.start);
	}
	start () {
		// send routes to server
		_Route.register();
		// ask for prefs
		_Prefs_Signals.start();
	}
	loaded () {
		this.set('loaded',true);
		let message = this._IPC.get();
		if (message !== null) {
			this.ipc(JSON.parse(message));
		}
	}
	ipc (message) {
		/* 
		per module - but don't forget to indicate _Module.loaded(); when the module is ready to handle IPC call ! */
	}


}

var _Module; // reserved !
var _Tab = {}; // reserved !