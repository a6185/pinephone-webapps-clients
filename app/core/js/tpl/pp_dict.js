// Jean Luc Biellmann - contact@alsatux.com

"use strict";

class PP_Dict {
	constructor () {
		this.dict1 = [
			'!',' !',
			'?',' ?',			
		];
		this.dict2 = [
			'label_settings','Settings','Réglages',
			'label_user_locale','Language','Langue',
			'label_server_websocket','Websocket','Websocket',
			'websocket_host','Host ?','Hôte ?',
			'websocket_port','Port ?','Port ?',
		];
		this.offset = 0;
	}
	add (level,data) {
		switch (level) {
			case 2:
				this.dict1.push(...data);
				break;
			case 3:
				this.dict2.push(...data);
				break;
		}
	}
	upd_offset () {
		this.offset = Object.keys(_Module._Manifest.obj.lang).indexOf(_Lang.locale);	
	}
	tr1 (txt) {
		this.upd_offset();
		let pos = this.dict1.indexOf(txt);	
		if (pos!=-1) {
			return this.dict1[pos + (this.offset<0 ? 0 : this.offset)];	
		} else {
			_Lang.object_not_found(txt);
			return txt;
		}
	}
	tr2 (txt) {
		this.upd_offset();
		let pos = this.dict2.indexOf(txt);	
		if (pos!=-1) {
			return this.dict2[pos + this.offset + 1];	
		} else {
			_Lang.object_not_found(txt);
			return txt;
		}
	}
}