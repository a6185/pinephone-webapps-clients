// Jean Luc Biellmann - contact@alsatux.com

"use strict";

class PP_Setting_Object extends PP_Object {
	constructor () {
		super('setting_object',{
			'uid': '',
			//'label': '',
			'cat': '', // category
			'key': '', 
			'type': '', // select, websocket, ...
			'nb_options': '1,1', // min,max for select
			'val': {}, 
			'cur': [], 
			'def': [],
		});
		this.tpl = null;
		this.upd_cur = null;
		this.callback = null;
		this.override = null;
	}
}
