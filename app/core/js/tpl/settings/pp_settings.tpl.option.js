// Jean Luc Biellmann - contact@alsatux.com

"use strict";

class PP_Settings_Tpl_Option {
	constructor (_Setting_Object) {
		this._Setting_Object = _Setting_Object;		
		/*this.translate = {
			options: false
		}*/
		this.span = null;
		/*this.fn = {
			pre_HTML: null,
			post_HTML: null,
			tr: null
		};*/
	}
	pre_HTML () {
		// to override
	}
	/*tr () {
		if (this.fn.tr !== null) {
			let obj = this._Setting_Object.obj;
			debugger;
			document.querySelector(`#label_${obj.key}`).textContent = tr1(`label_${obj.key}`);
			document.querySelectorAll(`#settings_${obj.key}s span`).forEach(span => {
				if (span.hasAttribute('data-value')) {
					let keys = span.getAttribute('data-value').split(',');
					let values = [];
					while (keys.length) {
						let key = keys.pop();
						let val = this._Setting_Object.obj.val[key];
						values.push(this.fn.tr(val));
					}
					span.textContent = values.join(',');
				}
			});
		}
	}*/
	to_HTML () {
		let obj = this._Setting_Object.obj;
		let opts = '';
		for (let key in obj.val) {
			//let val = this.translate.options ? tr1(obj.val[key]) : obj.val[key];
			let val = obj.val[key];
			opts += `
						<li><span data-value="${key}">${val}</span></li>`;
		}
		_Module._Settings.add_div(`
				<label id="label_setting_${obj.cat}_${obj.key}">${obj.label}</label>
				<div class="rounded" data-cat="${obj.cat}" data-key="${obj.key}" data-type="${obj.type}" data-nboptions="${obj.nb_options}" data-options="settings_${obj.key}s">
					<span data-value=""></span>
					<ul id="settings_${obj.key}s" style="display: none;">
${opts}
					</ul>
				</div>
		`);
		// set values
		let v = [];
		let t = [];
		obj.cur.forEach(key => {
			if (Object.keys(obj.val).indexOf(key) != -1) {
				v.push(key);
				t.push(obj.val[key]);
			}
		});
		this.span = _GUI._DOM.id(`#module_settings div[data-cat="${obj.cat}"][data-key="${obj.key}"] span`);
		this.span.setAttribute('data-value',v.join(','));
		this.span.textContent = t.join(', ');
		// add event handlers
		this.span.addEventListener('click', this.slot_selector.bind(this));		
	}
	post_HTML () {
		// to override
	}
	slot_selector (e) {
		let o = e.target;
		if (o.nodeName && o.nodeName=='SPAN' && o==this.span) {
			let p = o.parentNode;
			if (p.hasAttribute('data-type') && p.getAttribute('data-type')=='select') {
				_Tab.selector.slot.open('tab_settings', o, null, this.rec.bind(this));
			}
		}
	}
	rec () {
		this._Setting_Object.callback(this.span.getAttribute('data-value').split(','));
	}
}
