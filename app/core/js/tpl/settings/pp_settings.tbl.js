// Jean Luc Biellmann - contact@alsatux.com

"use strict";

class PP_Settings_Tbl extends PP_Tbl {
	constructor () {
		super();		
	}
	add (obj) {
		let o = (new PP_Setting_Object()).upd(obj);
		o.obj.uid = o.obj.cat + '.' + o.obj.key; // uid
		this.obj[o.obj.uid] = o;
		return o;
	}
	find_by_cat_key (cat,key) {
		let uid = cat + '.' + key
		return this.has(uid) ? this.obj[uid] : null;
	}
	/*del (obj) {
		delete this.obj[obj.uid];
	}*/
}
