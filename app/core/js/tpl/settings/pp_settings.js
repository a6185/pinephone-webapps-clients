// Jean Luc Biellmann - contact@alsatux.com

"use strict";

class PP_Settings {
	constructor () {
		this.tbl = {
			'settings': new PP_Settings_Tbl()
		}
		this.flag = {
			reload_location: false,
			settings_changed: false // true if some settings have been changed
		};
		this.settings = [];
		this.overriden = [];
		this.add_setting(this.default_locale);
		this.add_setting(this.default_websocket);
	}
	add_setting (fn) {
		this.settings.push(fn.bind(this));
	}
	add_override (fn) {
		this.overriden.push(fn.bind(this));
	}
	init () {
		this.settings.forEach(fn => {
			fn();
		});
		/*for (let i=0;i<this.settings.length;i++) {
			this.settings[i]();
		}*/
	}
	override () {
		this.overriden.forEach(fn => {
			fn();
		});
	}
	get_flag (name) {
		return this.flag[name];
	}
	set_flag (name, bool) {
		this.flag[name] = bool;
	}
	have_changed() {
		this.set_flag('settings_changed', true);
	}
	reset_changed() {
		this.set_flag('settings_changed', false);
	}
	default_locale () {
		let _Setting_Object = this.tbl.settings.add({
			cat: 'user',
			key: 'locale', // add label_locale to lang.js
			type: 'select',
			nb_options: '1,1', // min,max
			val: _Module._Manifest.obj.lang, 
			cur: [_Lang.locale],
			def: [_Lang.locale]
		});
		_Setting_Object.tpl = new PP_Settings_Tpl_Option(_Setting_Object);
		_Setting_Object.upd_cur = cur => {
			let locale = cur[0];			
			if (_Setting_Object.obj.val.hasOwnProperty(locale)) {
				_Setting_Object.obj.cur = [locale];
				if (_Lang.locale != locale) {
					_Lang.set(locale);
				}
			}
		}
		_Setting_Object.callback = cur => { // from HTML event
			_Setting_Object.upd_cur(cur);
			_Module._Settings.have_changed();
			_GUI._Tpl.load(_Lang.locale);
		};
	}
	default_websocket () {
		let _Setting_Object = this.tbl.settings.add({
			cat: 'server',
			key: 'websocket', // add label_websocket to lang.js
			type: 'websocket',
			nb_options: '2,2', // min,max
			cur: {
				'host': _WS.db.host,
				'port': _WS.db.port
			},
			def: _WS.db
		});
		_Setting_Object.tpl = new PP_Settings_Tpl_Websocket(_Setting_Object);
		_Setting_Object.upd_cur = cur => {
			if (cur.hasOwnProperty('host') && cur.host.constructor === String
				&& cur.hasOwnProperty('port') && cur.port.constructor === String) {
				_Setting_Object.obj.cur = {
					'host': cur.host,
					'port': cur.port
				}
				_WS.set_host(cur.host);
				_WS.set_port(cur.port);
			}
		}
		_Setting_Object.callback = cur => { // from HTML event
			let old_host = _Setting_Object.obj.cur.host;
			let old_port = _Setting_Object.obj.cur.port;
			_Setting_Object.upd_cur(cur);
			_Module._Settings.have_changed();
			/*if (_Setting_Object.obj.cur.host!=old_host || _Setting_Object.obj.cur.port!=old_port) {
				_Module._Settings.flag.reload_location = true;
			}*/
		};
	}
	add_div (html) {
		// never use innerHTML += 'code' : it destroys events recorded with addEventListener !
		let div = document.createElement('DIV');
		div.classList.add('setting');
		div.innerHTML = html;
		_GUI._DOM.id('#module_settings')?.appendChild(div);
	}
	render () {
		for (let uid in this.tbl.settings.obj) {
			let _Setting_Object = this.tbl.settings.obj[uid];
			if (_Setting_Object.tpl.pre_HTML !== null) {
				_Setting_Object.tpl.pre_HTML();
			}
		}
		for (let uid in this.tbl.settings.obj) {
			let _Setting_Object = this.tbl.settings.obj[uid];
			if (_Setting_Object.tpl.to_HTML !== null) {
				_Setting_Object.tpl.to_HTML();
			}
		}
		for (let uid in this.tbl.settings.obj) {
			let _Setting_Object = this.tbl.settings.obj[uid];
			if (_Setting_Object.tpl.post_HTML !== null) {
				_Setting_Object.tpl.post_HTML();
			}
		}
		_GUI._Tpl.load(_Lang.locale);
	}
	rec () {
		for (let uid in this.tbl.settings.obj) {
			let _Setting_Object = this.tbl.settings.obj[uid];
			_Setting_Object.tpl.rec();
		}
		this.write();			
		if (this.get_flag('reload_location')) {
			document.location.reload();
		}
	}
	read () {
		let data = _LocalStorage.read(_Module._Manifest.obj.namespace + '.settings');
		if (data !== null && data.constructor === String && data.length) {
			try {
				let settings = JSON.parse(data);
				if (settings.constructor === Array && settings.length) {
					settings.forEach(obj => {
						if (obj.hasOwnProperty('cat') && obj.hasOwnProperty('key') && obj.hasOwnProperty('cur')) {
							let _Setting_Object = this.tbl.settings.find_by_cat_key(obj.cat,obj.key);
							if (_Setting_Object !== null) {
								// check missing for array or object (TODO)
								_Setting_Object.upd_cur(obj.cur);
							}
						}
					});
				}
			} catch (err) {
				alert(err);
			}
		}
	}
	write () {
		let obj = []
		for (let uid in this.tbl.settings.obj) {
			let _Setting_Object = this.tbl.settings.obj[uid];
			obj.push({
				'cat': _Setting_Object.obj.cat, 
				'key': _Setting_Object.obj.key, 
				'cur': _Setting_Object.obj.cur, 
			});
		}
		_LocalStorage.write(_Module._Manifest.obj.namespace + '.settings',JSON.stringify(obj));
		if (!arguments.length) {
			alert(_Lang0.tr('Settings have been saved') + _Lang0.tr('!'));
		}
		this.reset_changed();
	}
}
