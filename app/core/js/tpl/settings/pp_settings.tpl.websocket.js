// Jean Luc Biellmann - contact@alsatux.com

"use strict";

class PP_Settings_Tpl_Websocket {
	constructor (_Setting_Object) {
		this._Setting_Object = _Setting_Object;
		this.span = null;
	}
	pre_HTML () {
	}
	to_HTML () {
		let obj = this._Setting_Object.obj;
		_Module._Settings.add_div(`
					<label id="label_setting_${obj.cat}_${obj.key}">${obj.label}</label>
					<div class="center">
						<p><input type="text" name="websocket_host" id="websocket_host" value="${obj.cur.host}" placeholder="Host ?"/></p>
						<p><input type="text" name="websocket_port" id="websocket_port" value="${obj.cur.port}" placeholder="Port ?"/></p>
					</div>
				</div>`);
	}
	post_HTML () {
	}
	rec () {
		this._Setting_Object.callback({
			'host': _GUI._DOM.id('#websocket_host').value,
			'port': _GUI._DOM.id('#websocket_port').value
		});
	}
}