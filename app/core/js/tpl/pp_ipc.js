// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/*
	Inter tabs communication messages using localStorage

	Model of messages:
	let message = {
		method: 'dial',
		args: {
			number: number
		}
	};

	To send a message:
	_PP_IPC.trigger('com.alsatux.phone', message);
*/

class PP_IPC {
	constructor (_Module) {
		this._Module = _Module;
		window.addEventListener("storage", this.receive.bind(this));
	}
	receive (e) {
		if (e.key == this._Module.namespace) {
			let message = JSON.parse(e.newValue);
			// take care : this == window here !
			this.loop(message, 40); // 20s to open the application...
		}
	}
	trigger (namespace, message) {
		localStorage.setItem(namespace, JSON.stringify(message));
	}
	get () {
		let message = localStorage.getItem(this._Module.namespace);
		// autoremove old message
		localStorage.removeItem(this._Module.namespace);
		return message; // null if none
	}
	loop (message, n) {
		if (_Module == 'undefined' || _Module.obj == 'undefined' || !_Module.get('loaded')) {
			if (n) {
				setTimeout(() => {
					this.loop(message, n-1);
				},500);
			} else {
				localStorage.removeItem(this._Module.namespace);
			}
		} else {
			localStorage.removeItem(this._Module.namespace);
			_Module.ipc(message);
		}
	}
}

