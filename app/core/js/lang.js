// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Global language to use
 */

var _Lang = {
	locale: null, // current locale
	all: {
		'en-GB': 'English' // always by default
	},
	/**
	 * Register all locales available
	 * @param  {hash} all Hash of locales
	 */
	register: (all) => {
		_Lang.all = all;
		let locales = Object.keys(_Lang.all);
		_Lang.chg_to(locales[0]);
	},
	/**
	 * Set current locale
	 * @param  {string} data Locale to use
	 */
	set: (data) => {
		_Lang.chg_to(data.toString().trim());
	},
	/**
	 * Error message displayed when an HTML static element has no translation in the current locale
	 * @param  {string} key DOM id of the element
	 */
	object_not_found: (key) => {
		alert(_Lang0.tr('Object') + ' "' + key + '" ' + _Lang0.tr('has no translation') + _Lang0.tr('!'));
		debugger;
	},
	/**
	 * Change current locale
	 * @param  {string} locale New locale to use
	 */
	chg_to: (locale) => {
		if (_Lang.all.hasOwnProperty(locale)) {
			_Lang.locale = locale;
			_GUI._Log.info(_Lang0.tr('Lang is now') + ' : ' + _Lang.all[locale]);
		}
	}
};

