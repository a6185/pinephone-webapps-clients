// Jean Luc Biellmann - contact@alsatux.com

"use strict";

var _Lang0 = {
	locales: ['en-GB','fr-FR'],
	dict: [
'!',' !',
'?',' ?',
// file.js
'Websocket not available','Websocket non disponible',
// html.js
'UNDEFINED value for field','Valeur indéfinie pour le champ',
'NULL value for field','Valeur nulle pour le champ',
'not found in document','introuvable dans le document',
// local.storage.js
'"Local storage" API not available','API "Stockage local" non disponible',
'Disk full','Disque plein',
// lock.js
'Pending action','Action en cours',
// route.js
'Callback','Procédure de retour',
'Unknown route','Route inconnue',
// sd.js
'Storage media is now','Le media de stockage est maintenant',
'Storage path is now','Le dossier de stockage est maintenant',
'Last backup','Dernière sauvegarde',
'No backup available','Aucun sauvegarde disponible',
'File','Fichier',
'succesfully readed','lu avec succès',
'Unable to read the last backup','Imopssible de lire la dernière sauvegarde',
'Writing backup has failed','La sauvegarde a échoué',
'File saved','Fichier enregistré',
'Lang is now','Langue courante',
// lang.js
'Lang is now','Langue courante',
'Object','L\'objet',
'has no translation','n\'a pas été traduit',
// manifest.js
'Please report any bug to','Merci de signaler tout bug à',
// pict.js
'Picture size < %dx%d !','Taile de l\'image < %dx%d pixels !',
'Picture size > %dx%d !','Taile de l\'image > %dx%d pixels !',
'Error while uploading image: %s','Erreur de chargement image : %s',
'Bad picture filename !','Nom de fichier incorrect !',
'Bad picture extension !','Mauvaise extension image !',
'Wrong picture type (shoud be %s) !','Type de l\image erroné (devrait être %s) !',
'Wrong picture size (max: %d) !','Taille de l\'image erronée (max: %d) !',
// uploader.js
'Drag and drop your XML backup on this button','Glissez/déposez votre sauvegarde XML sur ce bouton',
// websocket.js
'Unable to open websocket','Impossible d\'ouvrir la websocket',
'Websocket available','Websocket disponible',
'Websocket not available','Websocket non disponible',
'Websocket host is now','L\'hôte Websocket est maintenant',
'Websocket port is now','Le port Websocket est maintenant',
'Trying to connect to','Tentative de connexion à',
'Socket error','Erreur dans la socket',
'Lost connection','Connexion perdue',
'Incoming datas','Données entrantes',
'Status code','Code de retour',
'Socket open','Socket ouverte',
// xml.settings.js
'Parsing XML failed','L\'analyse XML a échoué',
'Reading settings failed','La lecture des préférences a échoué',
'Recording settings failed','L\'enregistrement des préférences a échoué',
'Settings have been saved','Paramètres enregistrés',
// tab.settings.js
'Record unsaved new settings','Enregistrer les réglages non sauvegardés'
	],	
	tr: (txt) => {
		let offset = _Lang0.locales.indexOf(_Lang.locale);	
		let pos = _Lang0.dict.indexOf(txt);		
		if (pos!=-1) {
			return _Lang0.dict[pos + (offset<0 ? 0 : offset)];
		} else {
			_Lang.object_not_found(txt);
			return txt;
		}
	}
}
