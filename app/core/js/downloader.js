// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Dowloader for all files
 */

class Downloader {
	/**
	 * Constructor
	 * @param  {file} file DOM file
	 * @param  {hash} opt  File options
	 */
	constructor (file, opt) {
		this.file = file;
		this.opt = opt;
	}
	/**
	 * Check a dataurl string according to its type
	 * @param  {string} data String in DataURL format
	 */
	check (data) {
		var _this = this;
		switch (this.file.type) {
			case 'image/jpeg':
			case 'image/jpg':
				if (data.match(/^data:image\/jpe?g;base64,([0-9a-zA-Z+\/]{4})*(([0-9a-zA-Z+\/]{2}==)|([0-9a-zA-Z+\/]{3}=))?$/)==null) {
					throw 'Picture should be in JPG format !';
				}
				var img = new Image();
				img.onload = function () {
					if (img.width<_this.opt.min.width && img.height<_this.opt.min.height) {
						throw `The image size MUST be > ${_this.opt.min.width}x${_this.opt.min.height}...`;
					}
					if (img.width>_this.opt.max.width && img.height>_this.opt.max.height) {
						throw `The image size MUST be < ${_this.opt.max.width}x${_this.opt.max.height}...`;
					}
				}
				img.src = data;
				break;
			case 'image/png':
				if (data.match(/^data:image\/png;base64,([0-9a-zA-Z+\/]{4})*(([0-9a-zA-Z+\/]{2}==)|([0-9a-zA-Z+\/]{3}=))?$/)==null) {
					throw 'Picture should be in PNG format !';
				}
				var img = new Image();
				img.onload = function () {
					if (img.width<_this.opt.min.width && img.height<_this.opt.min.height) {
						throw `The image size MUST be > ${_this.opt.min.width}x${_this.opt.min.height}...`;
					}
					if (img.width>_this.opt.max.width && img.height>_this.opt.max.height) {
						throw `The image size MUST be < ${_this.opt.max.width}x${_this.opt.max.height}...`;
					}
				}
				img.src = data;
				break;
			case 'application/pdf':
				if (data.match(/^data:application\/pdf;base64,([0-9a-zA-Z+\/]{4})*(([0-9a-zA-Z+\/]{2}==)|([0-9a-zA-Z+\/]{3}=))?$/)==null) {
					throw 'File is not a valid PDF !';
				}
				break;
			case 'text/plain':
				if (data.match(/^data:text\/plain;charset=utf-8;base64,([0-9a-zA-Z+\/]{4})*(([0-9a-zA-Z+\/]{2}==)|([0-9a-zA-Z+\/]{3}=))?$/)==null) {
					throw 'File is not a valid UT8 text !';
				}
				break;
			default:
				throw 'File type not supported';
		}
	}
	/**
	 * Read a file as DataURL
	 * @return {promise} File readed on resolve, error message on reject
	 */
	readAsDataURL () {
		return new Promise((resolve,reject) => {
			const reader = new FileReader();
			var _this = this;
			var data = null;
			reader.onload = function (e) {			
				data = e.target.result;
				_this.check(data);
				resolve(data);
			}
			reader.onerror = function (e) {
				reader.abort();
				throw 'Error while uploading file !';
			}
			reader.readAsDataURL(this.file);	
		}).catch(err => {
			reject(err);
		});
	}
}