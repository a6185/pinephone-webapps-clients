// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Global websocket object
 */

var _WS = {
	uid: null,
	socket: null, // websocket object
	db: { // default server host/port
		host: 'mobian',
		port: '2020'
	},
	/**
	 * Change server hostname/IP
	 * @param  {string} data Hostname or IP
	 */
	set_host: (data) => {
		let hostname = data.toString().trim().toLowerCase();
		_WS.db.host = hostname;
		_GUI._Log.info(_Lang0.tr('Websocket host is now') + ' : ' + hostname);
	},
	/**
	 * Change server port
	 * @param  {string} data Port number
	 */
	set_port: (data) => {
		let port = parseInt(data,10).toString();
		_WS.db.port = port;
		_GUI._Log.info(_Lang0.tr('Websocket port is now') + ' : ' + port);
	},
	/**
	 * Try to open the websocket to the server
	 * @return {promise}
	 */
	open: () => {
		return new Promise((resolve, reject) => {
			// Get last backup
			_Tab.spinner.show();
			var timeout = null;
			Promise.race([
				new Promise((resolve, reject) => {
					_WS.connect(resolve, reject);
				}),
				new Promise((resolve, reject) => {
					timeout = setTimeout(() => {
						_GUI._Log.info(_Lang0.tr('Unable to open websocket') + '... ');
						reject();
					},3000);
				})
			]).then(() => {
				clearTimeout(timeout);
				_GUI._Log.info(_Lang0.tr('Websocket available') + '... ');
				_Tab.spinner.hide();
				resolve();
			}).catch(() => {
				clearTimeout(timeout);
				_GUI._Log.error(_Lang0.tr('Websocket not available') + ' !');
				_Tab.spinner.hide();
				reject();
			});			
		});
	},
	/**
	 * Every 20s, check if the websocket is always open
	 * @param  {object}   _Module  Module to use
	 * @param  {function} callback Method to call when the socket is open
	 */
	watchdog: (_Module, callback) => {
		if (_WS.socket === null) {
			// call socket for datas
			_WS.open().then(() => {
				callback();
			}).catch(err => {
				_GUI._Log.error(err);
			});;
		}
		setTimeout(() => {
			_WS.watchdog(_Module, callback);
		}, 20000);
	},
	/**
	 * Connect to the server
	 * @param  {function} resolve Method called on resolve
	 * @param  {function} reject  Method called on reject
	 */
	connect: (resolve,reject) => {
		try {
			if (_WS.socket!==null) {
				_WS.socket.close();
			}
			let url = 'wss://' + _WS.db.host + ':' + _WS.db.port;
			_GUI._Log.warn(_Lang0.tr('Trying to connect to') + ' ' + url + '.'); 
			_WS.socket = new WebSocket(url);
			_WS.socket.onerror = (error) => {
				_GUI._Log.info(_Lang0.tr('Socket error') + _Lang0.tr('!'));
				console.debug(error);
			};

			_WS.socket.onclose = (e) => {
				_GUI._Log.info(_Lang0.tr('Lost connection') + '...');
				_WS.socket = null;
			};

			_WS.socket.onmessage = (e) => {
				_GUI._Log.info(_Lang0.tr('Incoming datas') + '...');
				let data = JSON.parse(e.data);
				let status = data.status;
				if (hasKey(status, 'code', Number)) {
					_GUI._Log.info(_Lang0.tr('Status code') + ' = ' + status.code);
				}
				if (hasKey(status, 'messages', Array)) {
					status.messages.forEach(row => {
						switch (row.type) {
							case 'err':
								alert(row.txt);
								break;
							case 'info':
								_GUI._Log.info(row.txt);
								break;
							case 'warn':
								alert(row.txt);
								break;
						}
					});
				}
				_Route.run(data);
			};

			_WS.socket.onopen = (e) => {
				_GUI._Log.info(_Lang0.tr('Socket open') + _Lang0.tr('!'));
				resolve();
			};

		} catch (exception) {
			alert(exception);
			reject();
		}
	},
	/**
	 * Send data trough websocket
	 * @param  {object} data Hash of datas
	 */
	send: (data) => {
		_WS.socket.send(JSON.stringify(data));
	}
};


