// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Global routes handler object
 */

var _Route = {
	db: {}, // hash of available routes
	/**
	 * Add a new route and store the associated handle
	 * @param  {string} route   Route as /somepath/something format
	 * @param  {function} handler Route handler
	 */
	add: (route, handler) => {
		_Route.db[route] = handler;
	},
	/**
	 * Call the route from the page URL
	 * @param  {string} data Route called
	 */
	run: (data) => {
		if (data.hasOwnProperty('callback') && data.callback.constructor === String) {
			_GUI._Log.info(_Lang0.tr('Callback') + ' = ' + data.callback);
			if (_Route.db.hasOwnProperty(data.callback)) {
				_Route.db[data.callback](data);
			} else {
				_GUI._Log.error(_Lang0.tr('Unknown route') + _Lang0.tr('!'));
			}
		}
	},
	/**
	 * Once all the routes have been collected, send them through the websocket
	 * to the server using /register/slots
	 */
	register: () => {
		_WS.send({
			route: '/register/slots',
			args: {
				slots: Object.keys(_Route.db)
			}
		});
	}
};
