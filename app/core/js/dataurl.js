// Jean Luc Biellmann - contact@alsatux.com

"use strict";

var _DataURL = {
	decode2Uint8Array: function (data) {
		// decode a base64 string
		var binary = atob(data);
		// create an array of byte values where each element will be the value of the byte
		var buffer = [];
		for (var i=0;i<binary.length;i++)
			// charCodeAt() method returns the Unicode of the character at the specified index in a string. (H -> 72)
			buffer.push(binary.charCodeAt(i));
		// convert the array of byte values into a real typed byte array
		return new Uint8Array(buffer);
	}
};
