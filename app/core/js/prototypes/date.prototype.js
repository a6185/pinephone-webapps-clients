// Jean Luc Biellmann - contact@alsatux.com

'use strict';

/**
 * Convert a date as YearMonthday
 * @return {string} Date converted
 */
Date.prototype.to_yyyymmdd = function () {
	let spacer = arguments.length ? arguments[0] : '';
	return sprintf('%04d%s%02d%s%02d', this.getFullYear(), spacer, this.getMonth() + 1, spacer, this.getDate())
}
/**
 * Convert a date as MonthDayYear
 * @return {string} Date converted
 */
Date.prototype.to_mmddyyyy = function () {
	let spacer = arguments.length ? arguments[0] : '';
	return sprintf('%02d%s%02d%s%04d', this.getMonth() + 1, spacer, this.getDate(), spacer, this.getFullYear())
}
/**
 * Convert a date as DayMonthYear
 * @return {string} Date converted
 */
Date.prototype.to_ddmmyyyy = function () {
	let spacer = arguments.length ? arguments[0] : '';
	return sprintf('%02d%s%02d%s%04d', this.getDate(), spacer, this.getMonth() + 1, spacer, this.getFullYear())
}
/**
 * Convert the time of a date as HourMinuteSecond
 * @return {string} Time converted
 */
Date.prototype.to_hhiiss = function () {
	let spacer = arguments.length ? arguments[0] : '';
	return sprintf('%02d%s%02d%s%02d',this.getHours(), spacer, this.getMinutes(), spacer, this.getSeconds());
}
/**
 * Convert a date as YearMonthDayHourMinuteSecond
 * @return {string} Date converted
 */
Date.prototype.to_yyyymmddhhiiss = function () {
	return this.to_yyyymmdd() + this.to_hhiiss();
}
/**
 * Convert a date to locale format
 * @return {string} Date converted
 */
Date.prototype.to_locale = function () {
	let _Pref_Object = _Prefs_Tbl.find_by_cat_key('default','date_pattern');
	switch (_Pref_Object.cur[0]) {
		case 'iso': // YYYY-MM-DD
		 return this.to_yyyymmdd('-');
		 break;
		case 'usa': // MM/DD/YYYY
		 return this.to_mmddyyyy('/');
		 break;
		case 'eur': // DD/MM/YYYY
		 return this.to_ddmmyyyy('/');
		 break;
	}
}
