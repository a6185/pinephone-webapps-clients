// Jean Luc Biellmann - contact@alsatux.com

'use strict';

/**
 * Round a real
 * @param  {int} dec Number of decimals
 * @return {number}     Number rounded
 */
Number.prototype.round = function (dec) {
	return Math.round((Math.pow(10,dec)*this))/Math.pow(10,dec);
}
/**
 * Convert a number to a currency
 * @return {number} Real with 2 decimals
 */
Number.prototype.to_currency = function () {
	return this.round(2).toFixed(2).to_locale_real();
}
/**
 * Convert a number to a volume
 * @return {number} Real with 2 decimals
 */
Number.prototype.to_volume = function () {
	return this.round(2).toFixed(2).to_locale_real();
}	
