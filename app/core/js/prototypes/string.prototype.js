// Jean Luc Biellmann - contact@alsatux.com

'use strict';

/**
 * Convert a string to date format
 * @param  {string} format Format to use
 * @return {string|null}        String converted or null
 */
String.prototype.to_date_format = function (format) {
	let m, d, dd, dm, dy;
	try {
		switch (format) {
			case 'YYYY-MM-DD':
				m = this.match(/^(\d\d\d\d)-(\d\d)-(\d\d)$/);
				dy = m[1].to_uint();
				dm = (m[2]-1).to_uint();
				dd = (m[3]).to_uint();
			 break;
			case 'MM/DD/YYYY':
				m = this.match(/^(\d\d)\/(\d\d)\/(\d\d\d\d)$/);
				dy = m[3].to_uint();
				dm = (m[1]-1).to_uint();
				dd = (m[2]).to_uint();
			 break;
			case 'DD/MM/YYYY':
				m = this.match(/^(\d\d)\/(\d\d)\/(\d\d\d\d)$/);
				dy = m[3].to_uint();
				dm = (m[2]-1).to_uint();
				dd = (m[1]).to_uint();
			 break;
			default:
				return null;
		}		
		d = new Date(dy, dm, dd, 0, 0, 0, 0.);
		if (d.getFullYear()==dy && d.getMonth()==dm && d.getDate()==dd) {
			return this;
		}
		return null;
	} catch (err) {
		return null;
	}
}
/**
 * Convert a string to an integer
 * @return {number} Integer
 */
String.prototype.to_int = function () {
	return parseInt(this,10);
}
/**
 * Convert a string to an unsigned integer
 * @return {number} Unsigned integer
 */
String.prototype.to_uint = function () {
	return Math.abs(this.to_int());
}
/**
 * Convert a string to a float
 * @return {number} Floating number
 */
String.prototype.to_float = function () {
	return parseFloat(this.replace(',','.'));
}
/**
 * Convert a string to a modulo
 * @param  {number} modulo Integer
 * @return {number}        Remaining modulo
 */
String.prototype.modulo = function (modulo) {
	return this.to_uint()%modulo.to_uint();
}
/**
 * Convert a string to a float format using locale
 * @return {string} String converted
 */
String.prototype.to_locale_real = function () {
	let _Pref_Object = _Prefs_Tbl.find_by_cat_key('default','separator_real');
	if (_Pref_Object.cur[0]=='dot') {
		return this.replace(/,+/g,'.');
	} else { // comma
		return this.replace(/\.+/g,',');
	}
}
/**
 * Escape HTML entities & < > "
 * @return {string} String converted
 */
String.prototype.escape_html = function() {
  return this.replace(/&(?!amp;)/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}
/*String.prototype.to_locale = function () {
	return (new Date(this)).to_locale(arguments);
}
String.prototype.to_hhiiss = function () {
	return (new Date(this)).to_hhiiss(arguments);
}
*/
/**
 * Cut a long line to a max length using words limit
 * @param  {number} maxlength Max length integer
 * @return {string}           Line cut
 */
String.prototype.cut_to_words = function (maxlength) {
	let text = this.trim();
	if (maxlength>1 && text.length>maxlength) { // truncate words
		let a = text.substr(0,maxlength-1);
		while (a.length && a.substr(-1)!=' ') {
			a = a.slice(0,-1);
		}
		text = a + '…';
	}
	return text;
}
/**
 * Cut a long line to a max length using chars limit
 * @param  {number} maxlength Max length integer
 * @return {string}           Line cut
 */
String.prototype.cut_to_chars = function (maxlength) {
	let text = this.trim();
	if (maxlength && text.length>maxlength) {
		text = text.substr(0,maxlength-1) + '…';
	}
	return text;
}
/**
 * Convert new line to break return
 * @return {string} String converted
 */
String.prototype.nl2br = function() {
	let text = this.trim();
	text = text.replace(/(\r\n)/,'<br/>');
	text = text.replace(/\r/,'<br/>');
	text = text.replace(/\n/,'<br/>');
	return text;
}

