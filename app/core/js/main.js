// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Global GUI storage object
 */

var _GUI = {
	_Window: new GUI_Window(), // IPC
	_Tpl: new GUI_Tpl(), // dict
	_Log: new GUI_Log(), // debug
	_Lock: new GUI_Lock(), // lock GUI waiting for an action to finish
	_DOM: new GUI_DOM(), // DOM shortcuts
	_HTML: new GUI_HTML(), // HTML shortcuts
	_CSS: new GUI_CSS // CSS shortcuts
}