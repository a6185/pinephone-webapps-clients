// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Global MD5 creator
 */
class MD5 {
	/**
	 * Constructor
	 */
	constructor () {
		this.dict = '0123456789abcdefghijklmnopqrstuvwxyz';
	}
	/**
	 * Generate a MD5 of 32 chars
	 * @return {string} MD5 generated
	 */
	generate () {
		let str = '';
		while (str.length<32) {
			let j = parseInt(Math.random()*this.dict.length,10);
			str += dict[j];
		}
		return str;
	}
	/**
	 * Generate an uniq MD5 according to an existing hash index
	 * @param  {hash} index Data hash
	 * @return {string}       Uniq MD5
	 */
	uniq (index) {
		let id = '';
		while (id=='' || id in index) {
			id = this.generate();
		}
		return id;
	}
}