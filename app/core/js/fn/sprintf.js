// Jean Luc Biellmann - contact@alsatux.com

// https://www.cplusplus.com/reference/cstdio/printf/

"use strict";

const DEBUG_SPRINTF=0;

/**
 * Global sprintf
 */

/**
 * Debug messages for devs
 * @param  {string} txt Text to display
 */
function debug_sprintf (txt) {
	if (DEBUG_SPRINTF) {
		console.log(txt);
	}
}

/**
 * Partial sprintf function using current C syntax
 * @param  {string}    str  format
 * @param  {variable} args Arguments
 * @return {string}         Formatted string
 */
function sprintf(str, ...args) {
	// supported : %c %s %d %f
   	debug_sprintf('>> string: ' + str.toString());
    return str.replace(/(%c|%\-?\d*s|%\-?\+?\0?\d*d|%\-?\+?\0?\d*\.?\d*f)/g, (m) => {
    	debug_sprintf('>> format: ' + m);
    	if (!args.length) {
    		return '';
    	}
    	let value = args.shift();
    	debug_sprintf('>> value: ' + value);
    	let type = m.substr(-1);
    	let number, out, format, align, width, sign, pad;
    	switch (type) {
    		case 'c':
    			if (isNaN(value)) {
    				return value.toString();
    			} else {
    				return String.fromCharCode(parseInt(value,10));
    			}
    			break;
    		case 's':
    			out = value.toString();
    			format = m.match(/%(\-?)(\d*)s/);
    			debug_sprintf(format);
    			align = format[1] == '-' ? 'left' : 'right';
    			debug_sprintf('align = ' + align);
    			width = format[2] != undefined ? parseInt(format[2],10) : out.length;
    			debug_sprintf('width = ' + width);
    			while (out.length<width) {
    				if (align == 'left') {
    					out = out + ' ';
    				} else {
    					out = ' ' + out;
    				}
    			}
    			return out;
    			break; 
    		case 'd':
			    number = parseInt(value,10);
    			out = number.toString();
    			format = m.match(/%(\-)?(\+)?(0)?(\d*)d/);
    			align = format[1] == '-' ? 'left' : 'right';
    			debug_sprintf('align = ' + align);
    			sign = (format[2] == '+' && number > 0) ? 'yes' : 'no';
    			debug_sprintf('sign = ' + sign);
    			pad = format[3] == '0' ? '0' : ' ';
    			debug_sprintf('pad = ' + pad);
    			width = format[4] != undefined ? parseInt(format[4],10) : out.length;
    			debug_sprintf('width = ' + width);
    			if (pad == '0') {
    				if (sign == 'yes') {
    					if (align == 'left') {
    						/*
    						sprintf("%-+010d\n",-1234); // => "-1234     "
    						sprintf("%-+010d\n",0);     // => "+0        "
    						sprintf("%-+010d\n",1234);  // => "+1234     "
    						*/
	   						out = (number>=0 ? '+' : '') + out;
    						// ignore 0 pads - always pad with spaces
			    			while (out.length<width) {
			   					out = out + ' ';
			   				}
    					} else {
    						/*
    						sprintf("%+010d\n",-1234); // => "-000001234"
    						sprintf("%+010d\n",0);     // => "+000000000"
    						sprintf("%+010d\n",1234);  // => "+000001234"
    						*/
    						out = out.replace('-','');
			    			while (out.length<width-1) {
			   					out = '0' + out;
			   				}
	   						out = (number>=0 ? '+' : '-') + out;
    					}
    				} else { // no sign
    					if (align == 'left') {
    						/*
	    					sprintf("%-010d\n",-1234); // => "-1234     "
	    					sprintf("%-010d\n",0);     // => "0         "
	    					sprintf("%-010d\n",1234)   // => "1234      "
	    					*/
    						// ignore 0 pads - always pad with spaces
			    			while (out.length<width) {
			   					out = out + ' ';
			   				}
			   			} else {
			   				/*
	    					sprintf("%010d\n",-1234); // => "-000001234"
	    					sprintf("%010d\n",0);     // => "0000000000"
	    					sprintf("%010d\n",1234);  // => "0000001234"
	    					*/
	    					out = out.replace('-','');
			    			while (out.length<width-1) {
			   					out = '0' + out;
			   				}
			   				if (out.length<width) {
	   							out = (number>=0 ? '0' : '-') + out;
	   						} else {
	   							// ignore width limit
	   							if (number<0) {
	   								out = '-' + out;
	   							}
	   						}
			   			}
	   				}
	   			} else { // no padding
	   				if (sign == 'yes') {
	   					if (align == 'left') {
	   						/*
    						sprintf("%-+10d\n",-1234); // => "-1234     "
    						sprintf("%-+10d\n",0);     // => "+0        "
    						sprintf("%-+10d\n",1234);  // => "+1977     "
    						*/
	   						out = (number>=0 ? '+' : '') + out;
    						// ignore 0 pads - always pad with spaces
			    			while (out.length<width) {
			   					out = out + ' ';
			   				}
    					} else {
    						/*
    						sprintf("%+10d\n",-1234); // => "     -1234"
    						sprintf("%+10d\n",0);     // => "        +0"
    						sprintf("%+10d\n",1234);  // => "     +1234"
    						*/
			   				out = (number>=0 ? '+' : '') + out;
			    			while (out.length<width) {
			   					out = ' ' + out;
			   				}	   					
			   			}
	   				} else {
	   					if (align == 'left') {
	   						/*
	   						sprintf("%-10d\n",-1234); // => "-1234     "
	   						sprintf("%-10d\n",0);     // => "0         "
	   						sprintf("%-10d\n",1234);  // => "1234      "
	   						*/
			    			while (out.length<width) {
			   					out = out + ' ';
			   				}	   				
			   			} else {
	   						/*
	   						sprintf("%10d\n",-1234); // => "     -1234"
	   						sprintf("%10d\n",0);     // => "         0"
	   						sprintf("%10d\n",1234);  // => "      1234"
	   						*/
			    			while (out.length<width) {
			   					out = ' ' + out;
			   				}	   							   				
			   			}	
	   				}
	   			}
    			return out;
    			debug_sprintf(format);
    			break; 
    		case 'f':
			    number = parseFloat(value);
    			out = number.toString();
    			format = m.match(/%(\-)?(\+)?(0)?(\d*)(?:\.(\d+))?f/);
    			//console.log(format);
    			// precision 1st
    			let precision = format[5] != undefined ? parseInt(format[5],10) : 6; // default in C++
    			debug_sprintf('precision = ' + precision);
    			let pow = Math.pow(10,precision);
    			number = (Math.round(number*pow)/pow).toFixed(precision);
    			debug_sprintf('number = ' + number);
    			out = number.toString();
    			debug_sprintf('out = ' + out);
    			align = format[1] == '-' ? 'left' : 'right';
    			debug_sprintf('align = ' + align);
    			sign = (format[2] == '+' && number > 0.0) ? 'yes' : 'no';
    			debug_sprintf('sign = ' + sign);
    			pad = format[3] == '0' ? '0' : ' ';
    			debug_sprintf('pad = ' + pad);
    			width = format[4] != undefined ? parseInt(format[4],10) : out.length;
    			debug_sprintf('width = ' + width);
     			/* TODO ! */
    			if (pad == '0') {
    				if (sign == 'yes') {
    					if (align == 'left') {
    						/*
    						sprintf("%-+010.2f\n",-1234.5678); // => "-1234.57  "
    						sprintf("%-+010.2f\n",0.);         // => "+0.00     "
    						sprintf("%-+010.2f\n",1234.5678);  // => "+1234.57  "
    						*/
	   						out = (number>=0.0 ? '+' : '') + out;
    						// ignore 0 pads - always pad with spaces
			    			while (out.length<width) {
			   					out = out + ' ';
			   				}
    					} else {
    						/*
    						sprintf("%+010.2f\n",-1234.5678); // => "-001234.57"
    						sprintf("%+010.2f\n",0.)    ;     // => "+000000.00"
    						sprintf("%+010.2f\n",1234.5678);  // => "+001234.57"
    						*/
    						out = out.replace('-','');
			    			while (out.length<width-1) {
			   					out = '0' + out;
			   				}
	   						out = (number>=0.0 ? '+' : '-') + out;
    					}
    				} else { // no sign
    					if (align == 'left') {
    						/*
	    					sprintf("%-010.2f\n",-1234.5678); // => "-1234.57  "
	    					sprintf("%-010.2f\n",0.);         // => "0.00      "
	    					sprintf("%-010.2f\n",1234.5678);  // => "1234.57   "
	    					*/
    						// ignore 0 pads - always pad with spaces
			    			while (out.length<width) {
			   					out = out + ' ';
			   				}
			   			} else {
			   				/*
	    					sprintf("%010.2f\n",-1234.5678); // => "-001234.57"
	    					sprintf("%010.2f\n",0.);         // => "0000000.00"
	    					sprintf("%010.2f\n",1234.5678);  // => "0001234.57"
	    					*/
	    					out = out.replace('-','');
			    			while (out.length<width-1) {
			   					out = '0' + out;
			   				}
	   						out = (number>=0.0 ? '0' : '-') + out;
			   			}
	   				}
	   			} else { // no padding
	   				if (sign == 'yes') {
	   					if (align == 'left') {
	   						/*
    						sprintf("%-+10.2f\n",-1234.5678); // => "-1234.57  "
    						sprintf("%-+10.2f\n",0.);         // => "+0.00     "
    						sprintf("%-+10.2f\n",1234.5678);  // => "+1234.57  "
    						*/
	   						out = (number>=0.0 ? '+' : '') + out;
    						// ignore 0 pads - always pad with spaces
			    			while (out.length<width) {
			   					out = out + ' ';
			   				}
    					} else {
    						/*
    						sprintf("%+10.2f\n",-1234.5678); // => "  -1234.57"
    						sprintf("%+10.2f\n",0.);         // => "     +0.00"
    						sprintf("%+10.2f\n",1234.5678);  // => "  +1234.57"
    						*/
			   				out = (number>=0.0 ? '+' : '') + out;
			    			while (out.length<width) {
			   					out = ' ' + out;
			   				}	   					
			   			}
	   				} else {
	   					if (align == 'left') {
	   						/*
	   						sprintf("%-10.2f\n",-1234.5678); // => "-1234.57  "
	   						sprintf("%-10.2f\n",0.);         // => "0.00      "
	   						sprintf("%-10.2f\n",1234.5678);  // => "1234.57   "
	   						*/
			    			while (out.length<width) {
			   					out = out + ' ';
			   				}	   				
			   			} else {
	   						/*
	   						sprintf("%10.2f\n",-1234.5678); // => "  -1234.57"
	   						sprintf("%10.2f\n",0.);         // => "      0.00"
	   						sprintf("%10.2f\n",1234.5678);  // => "   1234.57"
	   						*/
			    			while (out.length<width) {
			   					out = ' ' + out;
			   				}	   							   				
			   			}	
	   				}
	   			}
    			return out;
    			break; 
    		}
    		return m;
    });
}

/**
 * Test function for dev
 */
function  test_sprintf () {
	let s;
	/* CHAR */
	s = sprintf('Char <%c> !','a');
	console.log(s);
	s = sprintf('Char <%c> !',65);
	console.log(s);
	/* STRING */
	s = sprintf('Hello <%10s> !','world');
	console.log(s);
	s = sprintf('Hello <%-10s> !','world');
	console.log(s);
	/* INTEGER */
	s = sprintf("Integer: <%-+010d>",-1234); // => "-1234     "
	console.log(s);
	s = sprintf("Integer: <%-+010d>",0);     // => "+0        "
	console.log(s);
	s = sprintf("Integer: <%-+010d>",1234);  // => "+1234     "
	console.log(s);
	s = sprintf("Integer: <%+010d>",-1234); // => "-000001234"
	console.log(s);
	s = sprintf("Integer: <%+010d>",0);     // => "+000000000"
	console.log(s);
	s = sprintf("Integer: <%+010d>",1234);  // => "+000001234"
	console.log(s);
	s = sprintf("Integer: <%-010d>",-1234); // => "-1234     "
	console.log(s);
	s = sprintf("Integer: <%-010d>",0);     // => "0         "
	console.log(s);
	s = sprintf("Integer: <%-010d>",1234)   // => "1234      "
	console.log(s);
	s = sprintf("Integer: <%010d>",-1234); // => "-000001234"
	console.log(s);
	s = sprintf("Integer: <%010d>",0);     // => "0000000000"
	console.log(s);
	s = sprintf("Integer: <%010d>",1234);  // => "0000001234"
	console.log(s);
	s = sprintf("Integer: <%-+10d>",-1234); // => "-1234     "
	console.log(s);
	s = sprintf("Integer: <%-+10d>",0);     // => "+0        "
	console.log(s);
	s = sprintf("Integer: <%-+10d>",1234);  // => "+1977     "
	console.log(s);
	s = sprintf("Integer: <%+10d>",-1234); // => "     -1234"
	console.log(s);
	s = sprintf("Integer: <%+10d>",0);     // => "        +0"
	console.log(s);
	s = sprintf("Integer: <%+10d>",1234);  // => "     +1234"
	console.log(s);
	s = sprintf("Integer: <%-10d>",-1234); // => "-1234     "
	console.log(s);
	s = sprintf("Integer: <%-10d>",0);     // => "0         "
	console.log(s);
	s = sprintf("Integer: <%-10d>",1234);  // => "1234      "
	console.log(s);
	s = sprintf("Integer: <%10d>",-1234); // => "     -1234"
	console.log(s);
	s = sprintf("Integer: <%10d>",0);     // => "         0"
	console.log(s);
	s = sprintf("Integer: <%10d>",1234);  // => "      1234"
	console.log(s);
	// ---
	s = sprintf('Integer: <%d>',123);
	console.log(s);
	s = sprintf('Integer: <%5d>',123);
	console.log(s);
	s = sprintf('Integer: <%05d>',123);
	console.log(s);
	s = sprintf('Integer: <%+5d>',123);
	console.log(s);
	s = sprintf('Integer: <%+05d>',123);
	console.log(s);
	/* FLOAT */
	s = sprintf("Float: <%-+010.2f>",-1234.5678); // => "-1234.57  "
	console.log(s);
	s = sprintf("Float: <%-+010.2f>",0.);          // => "+0.00     "
	console.log(s);
	s = sprintf("Float: <%-+010.2f>",1234.5678);  // => "+1234.57  "
	console.log(s);
	s = sprintf("Float: <%+010.2f>",-1234.5678); // => "-001234.57"
	console.log(s);
	s = sprintf("Float: <%+010.2f>",0.)    ;     // => "+000000.00"
	console.log(s);
	s = sprintf("Float: <%+010.2f>",1234.5678);  // => "+001234.57"
	console.log(s);
	s = sprintf("Float: <%-010.2f>",-1234.5678); // => "-1234.57  "
	console.log(s);
	s = sprintf("Float: <%-010.2f>",0.);         // => "0.00      "
	console.log(s);
	s = sprintf("Float: <%-010.2f>",1234.5678);  // => "1234.57   "
	console.log(s);
	s = sprintf("Float: <%010.2f>",-1234.5678); // => "-001234.57"
	console.log(s);
	s = sprintf("Float: <%010.2f>",0.);         // => "0000000.00"
	console.log(s);
	s = sprintf("Float: <%010.2f>",1234.5678);  // => "0001234.57"
	console.log(s);
	s = sprintf("Float: <%-+10.2f>",-1234.5678); // => "-1234.57  "
	console.log(s);
	s = sprintf("Float: <%-+10.2f>",0.);         // => "+0.00     "
	console.log(s);
	s = sprintf("Float: <%-+10.2f>",1234.5678);  // => "+1234.57  "
	console.log(s);
	s = sprintf("Float: <%+10.2f>",-1234.5678); // => "  -1234.57"
	console.log(s);
	s = sprintf("Float: <%+10.2f>",0.);         // => "     +0.00"
	console.log(s);
	s = sprintf("Float: <%+10.2f>",1234.5678);  // => "  +1234.57"
	console.log(s);
	s = sprintf("Float: <%-10.2f>",-1234.5678); // => "-1234.57  "
	console.log(s);
	s = sprintf("Float: <%-10.2f>",0.);         // => "0.00      "
	console.log(s);
	s = sprintf("Float: <%-10.2f>",1234.5678);  // => "1234.57   "
	console.log(s);
	s = sprintf("Float: <%10.2f>",-1234.5678); // => "  -1234.57"
	console.log(s);
	s = sprintf("Float: <%10.2f>",0.);         // => "      0.00"
	console.log(s);
	s = sprintf("Float: <%10.2f>",1234.5678);  // => "   1234.57"
	console.log(s);
	s = sprintf("Float: <%4.2f>",3.1416);  // => "3.14"
	console.log(s);
}

//test_sprintf();

//printf ("floats: %4.2f %+.0e %E \n", 3.1416, 3.1416, 3.1416);
//floats: 3.14 +3e+000 3.141600E+000