// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Improvement of hasOwnProperty
 * @param  {hash}  ob   Hash object
 * @param  {string}  key  Key to check
 * @param  {string}  type JS type (String, Number, ...)
 * @return {bool}      Success
 */
function hasKey (ob, key, type) {
	if (ob!==null && ob!==undefined && ob.constructor===Object) {
		if (key!==null && key!==undefined && key.constructor===String) {
			if (ob.hasOwnProperty(key) && ob[key].constructor === type) {
				return true;
			}
		}
	}
	return false;
}


