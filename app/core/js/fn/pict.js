// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Global picture handler
 */
class Pict {	
	/**
	 * Constructor
	 * @param  {hash} args   Picture properties/options
	 * @param  {hash} regexp Picture file extensions
	 */
	constructor (args,regexp) {
		this._img = null;
		this._default = {
			type: 'image/jpeg',
			size: {
				min: 1,
				max: 1024*1024 // 1Mo
			},
			width: {
				min: 32,
				max: 320
			},
			height: {
				min: 32,
				max: 320
			},
			resize: true
		};
		this._args = JSON.parse(JSON.stringify(this._default));
		this._regexp = {
			filename: /^[a-z0-9][a-z0-9\-\.\_]{4,255}$/i,
			extension: /.jpe?g$/i
		};
		if (args !== undefined && args !== null && args.constructor === Object) {
			for (let i in args) {
				this._args[i] = args[i]; // weak security
			}
		}
		if (regexp !== undefined && regexp !== null && regexp.constructor === Object) {
			for (let i in regexp) {
				this._regexp[i] = regexp[i]; // weak security
			}
		}
	}
	/**
	 * Resize an existing picture to a new size
	 * @param  {object} img DOM picture
	 * @return {object}     Picture resized
	 */
	resize (img) {
		let canvas = document.createElement('canvas');
		let ctx = canvas.getContext('2d');
		let scaleFactor = Math.min(this._args.width.max / img.width, this._args.height.max / img.height);
		let width = Math.floor(img.width * scaleFactor);
		let height = Math.floor(img.height * scaleFactor);
		canvas.width = width;
		canvas.height = height;
		ctx.drawImage(img, 0, 0, width, height);
		return canvas.toDataURL(this._args.type); // image/jpeg by default
	}
	/**
	 * Read an uploaded file and check its properties
	 * @param  {object} file DOM file object
	 * @return {object}      Promise object
	 */
	read (file) {
		// dont't forget to chech filename before reading picture !
		return new Promise ((resolve,reject) => {
			let reader = new FileReader();
			reader.onload = (e) => {
				let data = e.target.result;
				let img = new Image();
				img.onload = () => {
					if (img.width<this._args.width.min || img.height<this._args.height.min) {
						reject(sprintf(_Lang0.tr('Picture size < %dx%d !'),this._args.width.min,this._args.height.min));
					}
					if (img.width>this._args.width.max || img.height>this._args.height.max) {
						if (this._args.resize) {
							img = this.resize(img);
							resolve(img);
						} else {
							reject(sprintf(_Lang0.tr('Picture size > %dx%d !'),this._args.width.max,this._args.height.max));		
						}
					}
				}
				img.src = data;
			}
			reader.onerror = (e) => {
				reject(sprintf(_Lang0.tr('Error while uploading image: %s',e)));
			}
			reader.readAsDataURL(file);
		});
	}
	/**
	 * Check file properties and display errors messages on failure
	 * @param  {object} file DOM file object
	 * @return {bool}      Success
	 */
	check (file) {
		if (!file.name.match(this._regexp.filename)) {
			alert(_Lang0.tr('Bad picture filename !'));
			return false;
		}			
		if (!file.name.match(this._regexp.extension)) {
			alert(_Lang0.tr('Bad picture extension !'));				
			return false;
		} 
		if (file.type!=this._args.type) {
			alert(sprintf(_Lang0.tr('Wrong picture type (shoud be %s) !'),this._args.type));			
			return false;
		}
		if (file.size>1000000) {
			alert(sprintf(_Lang0.tr('Wrong picture size (max: %d) !'),this._args.type));						
			return false;
		}
		return true;
	}
}