StatsCars for Firefox OS - v0.5

StatsCars helps you controlling the real costs of your cars.

It uses the purchase cost, the fills, the repairs and the insurances
to compute many statistics.

You can even take a photo of your "precious" directly from your
FFOS device !

Datas are recorded to the SD-card for Firefox OS, or local storage
for web browsers, using XML format, so you can backup your datas
like any other file.

On web browsers, two optionnal buttons let you easily import/export
your datas, so you can even work with several files !

Enjoy it and... have a good trip !

Specially thanks to Rogowsky_A for the basic icon design
(https://openclipart.org/detail/21803/vw-scirocco)

Copyright (c) 2018 Jean Luc Biellmann (contact@alsatux.com)

