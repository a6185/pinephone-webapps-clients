// Jean Luc Biellmann - contact@alsatux.com

"use strict";

class Repairs_Tbl extends PP_Tbl {
	constructor () {
		super();		
	}
	add (obj) {
		this.set(obj.uid,(new Repair_Object()).upd(obj));
	}
	del (obj) {
		this.unset(obj.uid);
	}
}