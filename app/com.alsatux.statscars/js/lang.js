// Jean Luc Biellmann - contact@alsatux.com

"use strict";

function tr1 (txt) {
	const dict = [
// fmt.js
'Bad integer','Format d\'entier erroné',
'Bad unsigned','Format d\'entier non signé erroné',
'Round failed','Arrondi erroné',
//global
'Missing or wrong datas !','Données vides ou erronnées !',
// car.ctrl.js
'Delete this car and all datas related ?','Effacer ce véhicule et toutes les données rattachées ?',
'Car purchase','Achat du véhicule',
'Fills','Pleins',
'Nb','Nb',
'Without last fill','Sans le dernier plein',
'Price / km','Prix / km',
'Total','Total',
'Bad car ignored: ','Données véhicules erronnées et ignorées : ',
'Bad fill record ignored: ','Enregistrement de type "plein" erronné et ignoré : ',
'Bad repair record ignored: ','Enregistrement de type "réparation" erronné et ignoré : ',
'Bad insurance record ignored: ','Enregistrement de type "assureur" erronné et ignoré : ',
'Bad photo ignored: ','Photo du véhicule erronnée et ignorée : ',
'Car added:','Véhicule ajouté',
'No car to add ?!','Aucun véhicule à ajouter ?!',
// lang.js
'Lang is now','Langue courante',
'Object','L\'objet',
'has no translation','n\'a pas été traduit',
// lock.js
'Pending action','Action en cours',
'Select a car please !','Sélectionnez un véhicule svp !',
'Select a fill please !','Sélectionnez un plein svp !',
'Select a repair please !','Sélectionnez une réparation svp !',
'Select a photo please !','Sélectionnez une photo svp !',
// local.js
'Reading backup from local session','Lecture de la sauvegarde depuis le cache local',
'No backup found','Aucune sauvegarde n\'a été trouvée',
'Records found','Engistrements trouvés',
// local.storage.js
'Local storage API not available','API de stockage local non disponible',
// main.js
'Error while uploading file','Erreur durant le chargement du fichier',
'Local storage available','Stockage local disponible',
'Please report any bug to','Veuillez signaler tout bug à',
'Storage allowed','Stockage autorisé',
'Searching for the last backup file','Recherche de la dernière sauvegarde',
'Storage not allowed','Stockage non autorisé',
'Warning: your browser doesn\'t support XML file creation on the fly ! Please use Mozilla Firefox !','Attention : votre navigateur ne supporte pas la création de fichier XML à la volée ! Merci d\'utiliser Mozilla Firefox !',
// mdl.car.js
'Photo width must be < 320px !','La largeur de la photo doit être < 320px !',
'Photo height must be < 240px !','La hauteur de la photo doit être < 240px !',
'Bad photo format','Le format de la photo est invalide',
'Missing car name','Vous devez donner un nom de voiture',
'Missing driver name','Vous devez donner un nom pour le/la conducteur(-trice) du véhicule',
'Missing purchase date','Vous devez donner la date d\'achat du véhicule',
'Missing purchase counter','Vous devez donner le kilométrage d\'achat du véhicule',
'Missing purchase price','Vous devez donner le prix d\'achat du véhicule',
// mdl.fill.js
'Missing fill date','Vous devez donner la date du plein',
'Missing fill counter','Vous devez donner le kilométrage du véhicule à la date du plein',
'Missing price per volume','Vous devez donner le prix par unité de volume à la date du plein',
'Missing fill price','Vous devez donner le prix du plein',
// mdl.insurance.js
'Missing insurance date','Vous devez donner la date du contrat d\'assurance',
'Missing insurance name','Vous devez donner le nom de l\'assureur',
'Missing insurance price','Vous devez donner le prix de l\'assurance',
// mdl.repair.js
'Missing repair date','Vous devez donner la date de la réparation',
'Missing repair garage','Vous devez donner le nom du garage de la réparation',
'Missing repair desc','Vous devez donner la description de la réparation',
'Missing repair price','Vous devez donner le prix de la réparation',
// sd.js
' files found',' fichiers disponibles',
'Last backup','Dernière sauvegarde',
'No backup found','Aucune sauvegarde n\'a été trouvée',
'File','Fichier',
'succesfully readed','lu avec succès',
'Unable to read the file','Impossible de lire le fichier',
'Unable to read the last backup','Impossible de lire la dernière sauvegarde',
'Records found','Engistrements trouvés',
// tab settings
'Unit distance is now','L\'unité de distance est maintenant',
'kilometer','kilomètre',
'mile','mile',
'Unit volume is now','L\'unité de volume est maintenant',
'litre,','litre',
'gallon','gallon',
// tab stats
'Repairs','Réparations',
'Counter','Compteur',
'Consumption','Consommation',
'Nb repairs','Nb réparations',
'Price','Prix',
'Insurances','Assurances',
'Nb records','Nb enregistrements',
'Total insurances','Total assurances',
'Year','Année',
'Details','Détails',
'Costs','Coûts',
'No car selected','Pas de véhicule sélectionné',
'Average','Moyenne',
// storage.js
'Storage API not available','API de stockage non disponible',
'Storage device','Périphérique de stockage',
'not found','introuvable',
'Unable to write the file','Impossible d\'écrire le fichier',
'The file','Le fichier',
'has been written','a été écrit',
'Unable to write','Impossible d\'écrire',
'Unable to read the directory','Impossible de lire le répertoire',
'Reading directory','Lecture du répertoire',
'Adding a file','Ajout d\'un fichier',
'Continue','Continu',
'Finished','Terminé',
'RegExp ok','RegExp ok',
'USB not unplugged','cordon USB non débranché',
// uploader.js
'Drag and drop your XML file on this button','Glissez/déposez votre fichier XML sur ce bouton',
// xml.js
'Parsing XML failed','L\'analyse XML a échoué',
'Reading settings failed','La lecture des préférences a échoué',
'Reading car failed','La lecture de la voiture a échoué',
'Recording settings failed','L\'enregistrement des préférences a échoué',
'Recording car failed','L\'enregistrement  d\'une voiture a échoué',
	];
	let offset = Object.keys(_Module._Manifest.obj.lang).indexOf(_Lang.locale);	
	let pos = dict.indexOf(txt);	
	if (pos!=-1) {
		return dict[pos + (offset<0 ? 0 : offset)];	
	} else {
		_Lang.object_not_found(txt);
		return txt;
	}
}

