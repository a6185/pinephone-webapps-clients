// Jean Luc Biellmann - contact@alsatux.com

"use strict";

var _Slots = {
	init: () => {
		_Route.add('com.alsatux.statscars:/cars/list', _Slots.handler);
		'car,fill,repair,insurance,photo'.split(',').forEach(type => {			
			_Route.add(`com.alsatux.statscars:/${type}/added`, _Slots.handler);
			_Route.add(`com.alsatux.statscars:/${type}/updated`, _Slots.handler);
			_Route.add(`com.alsatux.statscars:/${type}/deleted`, _Slots.handler);
		});
	},	
	handler: (data) => {
		_Tab.spinner.hide();
		if (hasKey(data, 'args', Object) && hasKey(data, 'callback', String)) {
			let [ns, route] = data.callback.split(':');
			let [, type, action] = route.split('/'); // com.alsatux.statscars:/type/action
			if (action == 'list') {
				'cars,fills,repairs,insurances,photos'.split(',').forEach(type => {
					if (hasKey(data.args, type, Object))	{
						for (let uid in data.args[type]) {
							_Module.tbl[type].add(data.args[type][uid]);
						}
					}
				});
				_Tab.car.reset();
				_Tab.car.list();
				_Module.loaded();
			} else {
				if (type == 'car') {
					switch (action) {
						case 'added':
						case 'updated':
							_Module.tbl[type +'s'].add(data.args[type]);
							break;
						case 'deleted':
							_Module.tbl[type +'s'].del(data.args[type]);
							break;
					}
				} else {
					if (hasKey(data.args, 'car_uid', String) && hasKey(data.args, type, Object)) {						
						switch (action) {
							case 'added':
								_Module.tbl[type +'s'].add(data.args[type]);
								break;
							case 'updated':
								_Module.tbl[type +'s'].add(data.args[type]);
								break;
							case 'deleted':
								_Module.tbl[type +'s'].del(data.args[type]);
								break;
						}
					}
				}
				_Tab[type].list();
			}
		}
	}
}
