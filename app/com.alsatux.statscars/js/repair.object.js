// Jean Luc Biellmann - contact@alsatux.com

"use strict";

class Repair_Object extends PP_Object {
	constructor () {
		super('repair_object',{
			'uid': '',
			'car_uid': '',
			'date': '', // timestamp
			'garage': 0, // garage
			'desc': '', // description
			'price': 0. // float
		});
	}
	to_HTML () {
		let html = '';
		let tr1 = '', tr2 = '';
		tr1 += '<td>' + (new Date(this.obj.date)).to_locale() + '</td>';
		tr1 += '<td>' + String(this.obj.garage).escape_html() + '</td>';
		tr1 += '<td class="currency">' + this.obj.price.to_currency() + '</td>';
		tr2 += '<td colspan="3"><div class="description">' + String(this.obj.desc).escape_html() + '</div></td>';
		return '<tr data-uid="' + this.obj.uid + '">' + tr1 + '</tr><tr data-uid="' + this.obj.uid + '">' + tr2 + '</tr>';
	}
}
