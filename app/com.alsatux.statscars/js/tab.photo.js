// Jean Luc Biellmann - contact@alsatux.com

"use strict";

_Tab.photo = {
	selected: null,
	dataurl: null,
	init: () => {
		_GUI._DOM.listen('#button_photos_add', 'click', _Tab.photo.slot.add);
		_GUI._DOM.listen('#button_photos_return', 'click', _Tab.car.slot.sel_back);
		_GUI._DOM.listen('#button_photos_del', 'click', _Tab.photo.slot.del);
		_GUI._DOM.listen('#button_photos_rec', 'click', _Tab.photo.slot.submit);
		_GUI._DOM.listen('#button_photos_back', 'click', _Tab.photo.slot.ls);
		_GUI._DOM.listen('#button_photos_change', 'click', _Tab.photo.slot.change);
		_GUI._DOM.listen('#photos_list', 'click', _Tab.photo.slot.sel);
	},
	reset: () => {
		_Tab.photo.selected = null;
		_Tab.photo.dataurl = null;
	},
	slot: {
		add: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tab.photo.editor(null);
			});
		},
		edit: (e) => {
			return _GUI._Lock.action_need_photo_selected(e, (e) => {
				_Tab.photo.editor(_Tab.photo.selected);
			});
		},
		sel: (e) => {
			return _GUI._Lock.action(e, (e) => {
				let el = e.target.closest('div[data-uid]');
				if (el!=null) {
					_Tab.photo.editor(el.getAttribute('data-uid'));
				}
			});
		},
		del: (e) => {
			return _GUI._Lock.action_need_photo_selected(e, (e) => {
				if (confirm('Delete this record ?')) {
					let row = _Module.tbl.photos.obj[_Tab.photo.selected].obj;
					_Signals.trigger('photos', 'del', {
						photo: row
					});
				}
			});
		},
		change: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tab.photo.dataurl = null;
				let pick = new MozActivity({
					name: 'pick',
					data: {
						type: ['image/jpg', 'image/jpeg']
					}
				});
				pick.onsuccess = () => {
					let img = new Image();
					img.src = window.URL.createObjectURL(this.result.blob);
					img.onload = function() {
						let MAX_WIDTH = 320;
						let MAX_HEIGHT = 240;
						let width = img.width;
						let height = img.height;
						if (width > MAX_WIDTH) {
							height *= MAX_WIDTH / width;
							width = MAX_WIDTH;
						}
						if (height > MAX_HEIGHT) {
							width *= MAX_HEIGHT / height;
							height = MAX_HEIGHT;
						}
						let canvas = document.createElement('canvas');
						canvas.width = width;
						canvas.height = height;
						let ctx = canvas.getContext('2d');
						ctx.drawImage(this, 0, 0, width, height);
						_Tab.photo.dataurl = canvas.toDataURL('image/jpeg');
						_GUI._DOM.id('#photo_car_edit').src = _Tab.photo.dataurl;
					}
				};
				pick.onerror = () => {
				};
			});
		},
		submit: (e) => {
			return _GUI._Lock.action(e, (e) => {
				let row = {
					'car_uid': _Tab.car.selected
				};
				try {					
					if (_Tab.photo.dataurl!=null) { // true is user cancel activity
						if (_Tab.photo.selected!=null) {
							row.uid = _Tab.photo.selected;
							_Signals.trigger('photos', 'upd', {
								photo: row
							});
						} else {
							_Signals.trigger('photos', 'add', {
								photo: row
							});
						}
					}
				} catch (e) {
					_GUI._Log.error(e);
				}
			});
		},
		ls: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tab.photo.list();
			});
		}
	},
	list: () => {
		_Tabs.open('tab_photos_list');
		_Tab.photo.selected = null;
		_Tab.photo.dataurl = null;
		//let rows = _Module.tbl.cars.obj[_Tab.car.selected].obj.photo;
		let rows = _Module.func.select('photos');
		_GUI._DOM.id('#nb_of_photos').innerHTML = rows.length;
		if (!rows.length) {
			_GUI._DOM.hide('#photos_list');
			_GUI._DOM.show('#no_photos_list');
		} else {
			let html = '';
			for (let i=0;i<rows.length;i++) {
				let uid = rows[i];
				html += '<div class="row" data-uid="' + uid + '"><img class="pict s160x120" src="' + _Module.tbl.photos.obj[uid].obj.dataurl + '"/></div>';
			}
			_GUI._DOM.id('#photos_rows').innerHTML = html;
			_GUI._DOM.hide('#no_photos_list');
			_GUI._DOM.show('#photos_list');
		}
	},
	editor: (uid) => {
		_Tabs.open('tab_photo_editor');
		_Tab.photo.selected = uid;
		if (uid!=null) {
			_Tab.photo.dataurl = _Module.tbl.photos.obj[uid].obj.dataurl;
			_GUI._DOM.id('#photo_car_edit').src = _Tab.photo.dataurl;
		} else {
			_Tab.photo.dataurl = null;
			_GUI._DOM.id('#photo_car_edit').src = DEFAULT_CAR;
		}
	}
};
