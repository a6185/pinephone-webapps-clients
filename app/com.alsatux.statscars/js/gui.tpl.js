// Jean Luc Biellmann - contact@alsatux.com

"use strict";

_GUI._Tpl.set([
// tab_cars_list
'welcome','Welcome !','Bienvenue !',
'welcome_resume','Cars manager with financial statistics.','Gestionnaire de voitures avec statistiques financières.',
'welcome_start','Press the + button to begin.','Utilisez le bouton + pour commencer.',
// tab_car_sel
'car_sel_selected','Car selected','Voiture sélectionnée',
'button_car_sel_gas_label','Gas','Pleins',
'button_car_sel_repair_label','Repair','Réparations',
'button_car_sel_insurance_label','Insurance','Assurance',
'button_car_sel_stats_label','Stats','Stats',
// tab_car_editor
'car_name','Name','Nom',
'car_driver','Driver','Conducteur',
'car_purchase_date','Purchase date','Date d\'achat',
'car_purchase_counter','Purchase counter','Compteur à l\'achat',
'car_purchase_price','Purchase price','Prix à l\'achat',
'title_car_editor','Car editor','Edition voiture',
// tab_car_list
'nb_of_cars_label','Cars available','Voitures disponibles',
// tab_fills_list
'title_fill_up','Fill up','Pleins',
'fills_list_col_date','Date','Date',
'no_fills_list_warning','No fills yet for this car.','Il n\'y a pas encore eu de plein pour ce véhicule.',
// tab_fill_editor
'fill_date','Date','Date',
'fill_counter','Counter','Compteur',
'fill_price_per_vol','Price per volume','Prix par vol',
'fill_price','Price','Prix',
// tab_insurances_list
'title_insurance','Insurance','Asssurance',
'insurances_list_col_date','Date','Date',
'insurances_list_col_insurer','Insurer','Assureur',
'no_insurances_list_warning','No insurances yet for this car.','Il n\'y a pas encore d\'assurances pour ce véhicule.',
// tab_insurance_editor
'insurance_date','From','Du',
'insurance_name','Insurer','Assureur',
'insurance_price','Price','Prix',
// tab_repairs_list
'title_repair','Repair','Réparations',
'repairs_list_col_date','Date','Date',
'repairs_list_col_garage','Garage','Garage',
'repairs_list_col_desc','Description','Description',
'no_repairs_list_warning','No repairs yet for this car.','Il n\'y a pas encore de réparations pour ce véhicule.',
// tab_repair_editor
'repair_date','Date','Date',
'repair_garage','Garage','Garage',
'repair_desc','Description','Description',
'repair_price','Price','Prix',
// tab_photos_list
'title_car_photos','Car photos','Photos du véhicule',
'nb_of_photos_label','Photos available','Photos disponibles',
'no_photos_list_warning','No photos yet for this car.','Pas de photos pour ce véhicule.',
// tab_photo_editor
'button_photos_change_label','Change photo','Changer la photo',
// tab_stats
'title_stats','Stats','Stats',
/* tab settings */
'label_settings','Settings','Réglages',
'label_setting_user_locale','Language','Langue',
'label_setting_server_websocket','Websocket','Websocket',
'label_setting_unit_distance','Distance','Distance',
'label_setting_unit_volume','Volume','Volume',
'websocket_host','Host ?','Hôte ?',
'websocket_port','Port ?','Port ?',
/* tab log */
'button_log_close','Close log','Fermer le journal'
]);
/*
_GUI._Tpl = {
	data: [
	],
	load: (locale) => {
		let pos = (Object.keys(_Module._Manifest.obj.lang).indexOf(locale)!=-1) ? Object.keys(_Module._Manifest.obj.lang).indexOf(locale) : 0;
		// by id
		for (let i=0;i<_GUI._Tpl.data.length;i+=3) {
			let key = _GUI._Tpl.data[i];
			let value = _GUI._Tpl.data[i+1+pos];
			let obj = _GUI._DOM.id('#' + key);
			if (obj==null) {
				alert(_Lang0.tr('Object') + ' "' + _GUI._Tpl.data[i] + '" has no translation' + ' !');
			} else {
				// object is a placeholder
				if (obj.hasAttribute('placeholder')) {
					obj.setAttribute('placeholder',value);
					continue;
				}
				// value is an array
				if (typeof value == 'object') {
					let nodes;
					let v = String(value[0]).split(',');
					// obj is an ul
					nodes = obj.querySelectorAll('li');
					if (nodes.length) {
						for (let j=0;j<v.length;j++) {
							nodes[j].innerHTML = v[j];
						}
						continue;
					}
					// obj is a span
					nodes = obj.querySelectorAll('span');
					if (nodes.length) {
						for (let j=0;j<v.length;j++) {
							nodes[j].innerHTML = v[j];
						}
						continue;
					}
				}
				// value is a node
				obj.innerHTML = value;
			}
		}
	}
}*/