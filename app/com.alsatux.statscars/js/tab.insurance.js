// Jean Luc Biellmann - contact@alsatux.com

"use strict";

_Tab.insurance = {
	selected: null,
	init: () => {
		_GUI._DOM.listen('#button_insurances_add', 'click', _Tab.insurance.slot.add);
		_GUI._DOM.listen('#button_insurances_return', 'click', _Tab.car.slot.sel_back);
		_GUI._DOM.listen('#button_insurances_del', 'click', _Tab.insurance.slot.del);
		_GUI._DOM.listen('#button_insurances_rec', 'click', _Tab.insurance.slot.submit);
		_GUI._DOM.listen('#button_insurances_back', 'click', _Tab.insurance.slot.ls);
		_GUI._DOM.listen('#insurances_list', 'click', _Tab.insurance.slot.sel);
	},
	reset: () => {
		_Tab.insurance.selected = null;
	},
	slot: {
		add: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tab.insurance.editor(null);
			});
		},
		edit: (e) => {
			return _GUI._Lock.action_need_insurance_selected(e, (e) => {
				_Tab.insurance.editor(_Tab.insurance.selected);
			});
		},
		sel: (e) => {
			return _GUI._Lock.action(e, (e) => {				
				let el = e.target.closest('tr');
				if (el!=null) {
					_Tab.insurance.editor(el.getAttribute('data-uid'));
				}
			});
		},
		del: (e) => {
			return _GUI._Lock.action_need_insurance_selected(e, (e) => {
				if (confirm('Delete this record ?')) {
					let row = _Module.tbl.insurances.obj[_Tab.insurance.selected].obj;
					_Signals.trigger('insurances', 'del', {
						insurance: row
					});
				}
			});
		},
		submit: (e) => {
			return _GUI._Lock.action_with_spinner(e, (e) => {
				let row = {
					'car_uid': _Tab.car.selected,
					'date': _GUI._DOM.id('#insurance_date').value,
					'name': _GUI._DOM.id('#insurance_name').value,
					'price': _GUI._DOM.id('#insurance_price').value.to_float()
				};
				try {
					//_DateString.pattern = 'YYYY-MM-DD';
					if (_Tab.insurance.selected!=null) {
						row.uid = _Tab.insurance.selected;
						_Signals.trigger('insurances', 'upd', {
							insurance: row
						});
					} else {
						_Signals.trigger('insurances', 'add', {
							insurance: row
						});
					}
				} catch(e) {
					_GUI._Log.error(e);
				}
			});
		},
		ls: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tab.insurance.list();
			});
		}
	},
	list: () => {
		_Tabs.open('tab_insurances_list');
		_Tab.insurance.selected = null;
		//let rows = _Module.tbl.cars.obj[_Tab.car.selected].sort_by_date_desc('insurance');
		let rows = _Module.func.sort_by_date_desc('insurances', _Tab.car.selected);
		if (!rows.length) {
			_GUI._DOM.hide('#insurances_list');
			_GUI._DOM.show('#no_insurances_list');
		} else {
			let html = '';
			rows.forEach(row => {
				html += row.to_HTML();
			});
			_GUI._DOM.id('#insurances_rows').innerHTML = html;
			_GUI._DOM.hide('#no_insurances_list');
			_GUI._DOM.show('#insurances_list');
		}
	},
	editor: (uid) => {
		_Tabs.open('tab_insurance_editor');
		_Tab.insurance.selected = uid;
		if (uid!=null) {
			let row = _Module.tbl.insurances.obj[uid].obj;
			_GUI._DOM.id('#insurance_date').value = (new Date(row.date)).to_yyyymmdd('-');
			_GUI._DOM.id('#insurance_name').value = String(row.name);
			_GUI._DOM.id('#insurance_price').value = Number(row.price).to_currency();
		} else {
			_GUI._DOM.id('#insurance_date').value = (new Date()).to_yyyymmdd('-');
			_GUI._DOM.id('#insurance_name').value = '';
			_GUI._DOM.id('#insurance_price').value = '';
		}
	}
};
