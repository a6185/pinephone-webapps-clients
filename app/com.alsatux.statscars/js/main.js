// Jean Luc Biellmann - contact@alsatux.com

"use strict";

const DEFAULT_CAR = 'img/default.png';

window.onload = () => {	
	_Module = new PP_Module('com.alsatux.statscars');
	_Module.init();
	_Module.tbl = {
		'cars': new Cars_Tbl(),
		'fills': new Fills_Tbl(),
		'insurances': new Insurances_Tbl(),
		'photos': new Photos_Tbl(),
		'repairs': new Repairs_Tbl()
	};
	_Module.func = {};
	_Module.func.select = (type, car_uid) => {
		let uid, rows = [];
		for (uid in _Module.tbl[type].obj) {
			let _Object = _Module.tbl[type].obj[uid];
			if (_Object.obj.car_uid==car_uid) {
				rows.push(_Object);
			}
		}
		return rows;
	};
	_Module.func.sort_by_date_desc = (type, car_uid) => {
		let rows = _Module.func.select(type, car_uid);
		rows.sort((a,b) => {
			return (new Date(b.obj.date))-(new Date(a.obj.date));
		});
		return rows;
	};
	_Module._Manifest.read().then(result => {
		_Tab.settings.tab_on_close = 'tab_cars_list';
		_Module.settings();
		_Tab.car.init();
		_Tab.fill.init();
		_Tab.insurance.init();
		_Tab.repair.init();
		_Tab.photo.init();
		_Tab.selector.init();
		_Tab.settings.init();
		_Tabs.init('tab_cars_list');
		_Module.run(_Signals.start);
	}).catch(err => {
		alert(err);
	});
}
