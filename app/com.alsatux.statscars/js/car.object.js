// Jean Luc Biellmann - contact@alsatux.com

"use strict";

class Car_Object extends PP_Object {
	constructor () {
		super('car_object',{
			'uid': '',
			'name': '', // mandatory
			'driver': '', // mandatory
			'purchase': {
				'date': '', // timestamp
				'counter': 0., // float
				'price': 0. // float
			}
		});
	}
	to_html_list () {
		let photo = DEFAULT_CAR;
		let rows = _Module.func.select('photos',this.obj.uid);
		if (rows.length) {
			photo = rows[0].obj.dataurl;
		}
		return '<div class="row" data-uid="' + this.obj.uid + '"><p><img class="pict" src="' + photo + '" />' + '</p>' + this.to_html_editor() + '</div>';
	}
	to_html_editor () {
		return '<p>' + this.obj.name.escape_html() + '&nbsp;- ' + this.obj.driver.escape_html() + '&nbsp;- ' + this.obj.purchase.date.to_locale() + '</p>';
	}
	to_string () {
		return 'Name: ' + this.obj.name.escape_html() + '<br/>' + 'Driver: ' + this.obj.driver.escape_html() + '<br/>' + 'Purchase date: ' + this.obj.purchase.date.to_locale() + '<br/>' + 'Purchase counter: ' + String(this.obj.purchase.counter).to_locale_real() + '<br/>' + 'Purchase price: ' + String(this.obj.purchase.price).to_locale_real() + '<br/>';
	}
	// photos
	/*chk_photo (dataurl) {
		let m = dataurl.match(/^(data:image\/jpe?g;base64,([0-9a-zA-Z+\/]{4})*(([0-9a-zA-Z+\/]{2}==)|([0-9a-zA-Z+\/]{3}=))?)$/);
		if (m==null) {
			throw tr('Bad photo format');
		}
		let img = document.createElement('img');
		img.src = dataurl;
		if (img.width>320) {
			throw tr('Photo width must be < 320px !');
		}
		if (img.height>240) {
			throw tr('Photo height must be < 240px !');
		}
	}*/
}
