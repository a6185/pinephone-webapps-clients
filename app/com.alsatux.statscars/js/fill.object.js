// Jean Luc Biellmann - contact@alsatux.com

"use strict";

class Fill_Object extends PP_Object {
	constructor () {
		super('fill_object',{
			'uid': '',
			'car_uid': '',
			'date': '', // timestamp
			'counter': 0, // uint
			'price_per_vol': 0, // float
			'price': 0 // float
		});
	}
	to_HTML () {
		let html = '';
		let vol=0.;
		let td = '';
		td += '<td>' + this.obj.date.to_locale() + '</td>';
		td += '<td>' + String(this.obj.counter).to_locale_real() + '</td>';
		if (this.obj.price>0. && this.obj.price_per_vol>0.) {
			vol = this.obj.price/this.obj.price_per_vol.round(2);
		} else {
			vol = '-';
		}
		td += '<td class="volume">' + vol.to_volume() + '</td>';
		// we need 3 digits precision for price per vol !
		td += '<td class="currency">' + this.obj.price_per_vol.round(3).toFixed(3).to_locale_real() + '</td>';
		td += '<td class="currency">' + this.obj.price.to_currency() + '</td>';
		return '<tr data-uid="' + this.obj.uid + '">' + td + '</tr>';
	}
}
