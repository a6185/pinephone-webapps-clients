// Jean Luc Biellmann - contact@alsatux.com

"use strict";

class Insurances_Tbl extends PP_Tbl {
	constructor () {
		super();		
	}
	add (obj) {
		this.set(obj.uid,(new Insurance_Object()).upd(obj));
	}
	del (obj) {
		this.unset(obj.uid);
	}
}