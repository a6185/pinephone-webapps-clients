// Jean Luc Biellmann - contact@alsatux.com

"use strict";

_Tab.stats = {
	init: () => {
	},
	slot: {
		stats: (event) => {
			_Tabs.open('tab_stats');
			let html = '', html2 = '';
			if (_Tab.car.selected == null) {
				html = tr1('No car selected');
			} else {
				// main stats
				let i,y,uid;
				let total = 0.;
				let total_gas_price = 0.;
				let total_gas_vol = 0.;
				let obj;
				let currency = '&nbsp;' + _GUI._HTML.escape(_Prefs_Tbl.find_by_cat_key('default','currency').cur);
				let v = _Module._Settings.tbl.settings.find_by_cat_key('unit','volume');
				let volume = '&nbsp;' + _GUI._HTML.escape(v.obj.cur[0]);
				let u = _Module._Settings.tbl.settings.find_by_cat_key('unit','distance');
				let distance = '&nbsp;' + _GUI._HTML.escape(u.obj.cur[0]);
				/*
				let currency = '&nbsp;' + _Tab.settings.currencies[_Tab.settings.currency];
				let volume = '&nbsp;' + _Tab.settings.volume;
				let distance = '&nbsp;' + _Tab.settings.distance;
				*/
				let car = _Module.tbl.cars.obj[_Tab.car.selected];
				total += parseFloat(car.obj.purchase.price);
				let counter_min = Number(car.obj.purchase.counter);
				let counter_max = Number(car.obj.purchase.counter);
				let fills = null;
				let fills_obj = _Module.func.sort_by_date_desc('fills', _Tab.car.selected);
				if (fills_obj.length) {
					fills = {};
					fills_obj.forEach(fill => {
						obj = fill.obj;
						y = (new Date(obj.date)).getFullYear();
						if (!(y in fills)) {
							fills[y] = {nb: 0, gas_vol: 0., gas_price: 0.};
						}
						fills[y].nb++;
						if (obj.price_per_vol!=0) {
							fills[y].gas_vol += obj.price/obj.price_per_vol;
						}
						fills[y].gas_price += obj.price;
						total += obj.price;
						total_gas_price += obj.price;
						if (obj.price_per_vol!=0) {
							total_gas_vol += obj.price/obj.price_per_vol;
						}
						if (obj.counter>counter_max) {
							counter_max = obj.counter;
						}
					});
				}
				let counter_delta = counter_max-counter_min;
				let total_repairs = 0;
				let repairs = null;
				let repairs_obj = _Module.func.sort_by_date_desc('repairs', _Tab.car.selected);
				if (repairs_obj.length) {
					repairs = {};
					repairs_obj.forEach(repair => {
						obj = repair.obj;
						y = (new Date(obj.date)).getFullYear();
						if (!(y in repairs)) {
							repairs[y] = {nb: 0, total: 0};
						}
						repairs[y].nb++;
						repairs[y].total += obj.price;
						total += obj.price;
						total_repairs += obj.price;
					});
				}
				let total_insurances = 0.;
				let insurances = null;
				let insurances_obj = _Module.func.sort_by_date_desc('insurances', _Tab.car.selected);
				if (insurances_obj.length) {
					insurances = {};
					insurances_obj.forEach(insurance => {
						obj = insurance.obj;
						y = (new Date(obj.date)).getFullYear();
						if (!(y in insurances)) {
							insurances[y] = {nb: 0, total: 0};
						}
						insurances[y].nb++;
						insurances[y].total += obj.price;
						total += obj.price;
						total_insurances += obj.price;
					});
				}
				/* COUNTER */
				html2 = '<table class="list"><thead><tr><th>' + tr1('Counter') + '</th><th>' + distance + '</th><tbody>';
				html2 += '<tr><td>min</td><td>' + String(counter_min.round(2)).to_locale_real() +  '</td></tr>';
				html2 += '<tr><td>max</td><td>' + String(counter_max.round(2)).to_locale_real() + '</td></tr>';
				if (counter_delta!=0) {
					html2 += '<tr><td>&Delta;' + tr1('Counter') + '</td><td>' + String(counter_delta.round(2)).to_locale_real() + '</td></tr>';
				} else {
					html2 += '<tr><td>&Delta;' + tr1('Counter') + '</td><td>0' + distance + '</td></tr>';
				}
				html2 += '</tbody></table>';
				html += '<div>' + html2 + '</div>';
				/* TOTAL */
				html2 = '<table class="list"><thead><tr><th>' + tr1('Costs') + '</th><th>' + tr1('Nb') + '</th><th>' + currency + '</th><tbody>';
				html2 += '<tr><td>' + tr1('Car purchase') + '</td><td>&nbsp;</td><td class="currency">' + car.obj.purchase.price.to_currency() + '</td></tr>';
				if (fills_obj.length) {
					html2 += '<tr><td>' + tr1('Fills') + '</td><td>' + fills_obj.length + '</td><td class="currency">' + total_gas_price.to_currency() + '</td></tr>';
				}
				if (repairs_obj.length) {
					html2 += '<tr><td>' + tr1('Repairs') + '</td><td>' + repairs_obj.length + '</td><td class="currency">' + total_repairs.to_currency() + '</td></tr>';
				}
				if (insurances_obj.length) {
					html2 += '<tr><td>' + tr1('Insurances') + '</td><td>' + insurances_obj.length + '</td><td class="currency">' + total_insurances.to_currency() + '</td></tr>';
				}
				html2 += '<tr><td>' + tr1('Total') + '</td><td>&nbsp;</td><td class="currency">' + total.to_currency() + '</td></tr>';
				html2 += '</tbody></table>';
				html += '<div>' + html2 + '</div>';
				/* STATS */
				html2 = '<table class="list"><tbody>';
				if (counter_delta!=0) {
					html2 += '<tr><td>' + tr1('Total') + '&nbsp;/&nbsp;&Delta;' + tr1('Counter') + '</td><td>' + String((total/counter_delta).round(2)).to_locale_real() + currency + ' /' + distance + '</td></tr>';
				}
				html2 += '</tbody></table>';
				html += '<div>' + html2 + '</div>';
				html2 = '<table class="list"><thead>';
				if (counter_delta!=0 && fills_obj.length>1) {
					let last_fill = fills_obj[0];
					//let last_fill = car.obj.fill[car.obj.fill.length-1];
					html2 += '<tr><th colspan="2">' + tr1('Consumption') + '</th></tr></thead><tbody>';
					html2 += '<tr><td>' + tr1('Without last fill') + '</td><td>' + String(((total_gas_price-last_fill.obj.price)/counter_delta).round(4)).to_locale_real() + currency + ' /' + distance + '</td></tr>';
					if (last_fill.obj.price_per_vol!=0) {
						html2 += '<tr><td>' + tr1('Average') + '</td><td>' + ((100*(total_gas_vol-(last_fill.obj.price/last_fill.obj.price_per_vol)))/counter_delta).to_volume() + volume + ' / 100' + distance + '</td></tr>';
					}
				}
				html2 += '</tbody></table>';
				html += '<div>' + html2 + '</div>';
				/* FILLS */
				if (fills_obj.length) {
					html2 = '<table class="list"><thead><tr><th>' + tr1('Fills') + '</th><th>' + tr1('Nb') + '</th><th>' + volume + '</th><th>' + currency + '</th></thead><tbody>';
					if (fills!=null) {
						Object.keys(fills).sort().reverse().forEach(y => {
							html2 += '<tr><td>' + y + '</td><td>' + fills[y].nb + '</td><td class="volume">' + fills[y].gas_vol.to_volume() + '</td><td class="currency">' + fills[y].gas_price.to_currency() + '</td></tr>';
						});
					}
					html2 += '<tr><td>' + tr1('Total') + '</td><td>' + fills_obj.length + '</td><td class="volume">' + total_gas_vol.to_volume() + '</td><td class="currency">' + total_gas_price.to_currency() + '</td></tr>';
					html2 += '</tbody></table>';
					html += '<div>' + html2 + '</div>';
				}
				/* REPAIRS */
				if (repairs_obj.length) {
					html2 = '<table class="list"><thead><tr><th>' + tr1('Repairs') + '</th><th>' + tr1('Nb') + '</th><th>' + currency + '</th></tr></thead><tbody>';
					if (repairs!=null) {
						Object.keys(repairs).sort().reverse().forEach(y => {
							html2 += '<tr><td>' + y + '</td><td>' + repairs[y].nb + '</td><td class="currency">' + repairs[y].total.to_currency() + '</td></tr>';
						});
					}
					html2 += '<tr><td>' + tr1('Total') + '</td><td>' + repairs_obj.length + '</td><td class="currency">' + total_repairs.to_currency() + '</td></tr>';
					html2 += '</tbody></table>';
					html += '<div>' + html2 + '</div>';
				}
				/* INSURANCES */
				if (insurances_obj.length) {
					html2 = '<table class="list"><thead><tr><th>' + tr1('Insurances') + '</th><th>' + tr1('Nb') + '</th><th>' + currency + '</th></tr></thead><tbody>';
					if (insurances!=null) {
						Object.keys(insurances).sort().reverse().forEach(y => {
							html2 += '<tr><td>' + y + '</td><td>' + insurances[y].nb + '</td><td class="currency">' + insurances[y].total.to_currency() + '</td></tr>';
						});
					}
					html2 += '<tr><td>' + tr1('Total') + '</td><td>' + insurances_obj.length + '</td><td class="currency">' + total_insurances.to_currency() + '</td></tr>';
					html2 += '</tbody></table>';
					html += '<div>' + html2 + '</div>';
				}
			}
			_GUI._DOM.id('#stats_list').innerHTML = html;
		}
	}
};
