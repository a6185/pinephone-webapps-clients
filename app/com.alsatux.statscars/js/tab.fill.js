// Jean Luc Biellmann - contact@alsatux.com

"use strict";

_Tab.fill = {
	selected: null,
	init: () => {
		_GUI._DOM.listen('#button_fills_add', 'click', _Tab.fill.slot.add);
		_GUI._DOM.listen('#button_fills_return', 'click', _Tab.car.slot.sel_back);
		_GUI._DOM.listen('#button_fills_del', 'click', _Tab.fill.slot.del);
		_GUI._DOM.listen('#button_fills_rec', 'click', _Tab.fill.slot.submit);
		_GUI._DOM.listen('#button_fills_back', 'click', _Tab.fill.slot.ls);
		_GUI._DOM.listen('#fills_list', 'click', _Tab.fill.slot.sel);
	},
	reset: () => {
		_Tab.fill.selected = null;
	},
	slot: {
		add: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tab.fill.editor(null);
			});
		},
		sel: (e) => {
			return _GUI._Lock.action(e, (e) => {
				let el = e.target.closest('tr');
				if (el!=null) {
					_Tab.fill.editor(el.getAttribute('data-uid'));
				}
			});
		},
		del: (e) => {
			return _GUI._Lock.action(e, (e) => {
				if (confirm('Delete this record ?')) {
					let row = _Module.tbl.fills.obj[_Tab.fill.selected].obj;
					_Signals.trigger('fills', 'del', {
						fill: row
					});
				}
			});
		},
		submit: (e) => {
			return _GUI._Lock.action_with_spinner(e, (e) => {
				let row = {
					'car_uid': _Tab.car.selected,
					'date': _GUI._DOM.id('#fill_date').value,
					'counter': _GUI._DOM.id('#fill_counter').value.to_uint(),
					'price_per_vol': _GUI._DOM.id('#fill_price_per_vol').value.to_float(),
					'price': _GUI._DOM.id('#fill_price').value.to_float()
				};
				try {
					if (_Tab.fill.selected!=null) {
						row.uid = _Tab.fill.selected;
						_Signals.trigger('fills', 'upd', {
							fill: row
						});
					} else {
						_Signals.trigger('fills', 'add', {
							fill: row
						});
					}
				} catch(e) {
					_GUI._Log.error(e);
				}
			});
		},
		ls: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tab.fill.list();
			});
		}
	},
	list: () => {
		//_Tabs.hide('tab_fill_editor');
		_Tabs.open('tab_fills_list');		
		_Tab.fill.selected = null;
		let rows = _Module.func.sort_by_date_desc('fills', _Tab.car.selected);
		if (!rows.length) {
			_GUI._DOM.hide('#fills_list');
			_GUI._DOM.show('#no_fills_list');
		} else {
			let html = '';
			rows.forEach(row => {
				html += row.to_HTML();
			});
			_GUI._DOM.id('#fills_rows').innerHTML = html;
			_GUI._DOM.hide('#no_fills_list');
			_GUI._DOM.show('#fills_list');
		}
	},
	editor: (uid) => {
		_Tabs.open('tab_fill_editor');
		_Tab.fill.selected = uid;
		if (uid!=null) {
			let row = _Module.tbl.fills.obj[uid].obj;
			_GUI._DOM.id('#fill_date').value = (new Date(row.date)).to_yyyymmdd('-');
			_GUI._DOM.id('#fill_counter').value = String(row.counter).to_locale_real();
			_GUI._DOM.id('#fill_price_per_vol').value = Number(row.price_per_vol).round(3).toFixed(3).to_locale_real();
			_GUI._DOM.id('#fill_price').value = Number(row.price).to_currency();
		} else {
			_GUI._DOM.id('#fill_date').value = (new Date()).to_yyyymmdd('-');
			_GUI._DOM.id('#fill_counter').value = '';
			_GUI._DOM.id('#fill_price_per_vol').value = '';
			_GUI._DOM.id('#fill_price').value = '';
		}
	}
};
