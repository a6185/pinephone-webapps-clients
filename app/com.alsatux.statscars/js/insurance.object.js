// Jean Luc Biellmann - contact@alsatux.com

"use strict";

class Insurance_Object extends PP_Object {
	constructor () {
		super('insurance_object',{
			'uid': '',
			'car_uid': '',
			'date': '', // timestamp
			'name': '', // insurer name
			'price': 0 // float
		});
	}
	to_HTML () {
		let td = '';
		td += '<td>' + (new Date(this.obj.date)).to_locale() + '</td>';
		td += '<td>' + String(this.obj.name).escape_html() + '</td>';
		td += '<td class="currency">' + Number(this.obj.price).to_currency() + '</td>';
		return '<tr data-uid="' + this.obj.uid + '">' + td + '</tr>';
	}
}
