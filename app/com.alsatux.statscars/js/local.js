// Jean Luc Biellmann - contact@alsatux.com

"use strict";

var _Local = {
	recall_last_backup: function () {
		var xml_string = _LocalStorage.read();
		if (xml_string==null) {
			_GUI._Log.info(tr('No backup found') + '.');
			_Tab.spinner.clear();
			_Tab.spinner.hide();
		} else {
			_GUI._Log.info(tr('Reading backup from local session') + '...');
			_XML.read(xml_string);
			_Tab.car.list();
			_GUI._Log.info(tr('Records found') + ': ' + Object.keys(_Tab.car.db).length);
			_Tab.spinner.clear();
			_Tab.spinner.hide();
		}
	}
};

