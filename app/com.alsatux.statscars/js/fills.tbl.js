// Jean Luc Biellmann - contact@alsatux.com

"use strict";

class Fills_Tbl extends PP_Tbl {
	constructor () {
		super();		
	}
	add (obj) {
		this.set(obj.uid,(new Fill_Object()).upd(obj));
	}
	del (obj) {
		this.unset(obj.uid);
	}
}