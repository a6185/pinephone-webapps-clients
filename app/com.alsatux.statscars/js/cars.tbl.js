// Jean Luc Biellmann - contact@alsatux.com

"use strict";

class Cars_Tbl extends PP_Tbl {
	constructor () {
		super();		
	}
	add (obj) {
		this.set(obj.uid,(new Car_Object()).upd(obj));
	}
	del (obj) {
		this.unset(obj.uid);
	}
}