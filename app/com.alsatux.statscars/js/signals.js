// Jean Luc Biellmann - contact@alsatux.com

"use strict";

var _Signals = {
	start: () => {
		_GUI._Log.info(tr1('Searching for the last backup file') + '... ');
		_WS.send({
			route: 'com.alsatux.statscars:/cars/list',
			args: {},
			callback: 'com.alsatux.statscars:/cars/list'
		});
	},
	trigger: (slot, action, args) => {
		args['car_uid'] = _Tab.car.selected;
		_WS.send({
			route: `com.alsatux.statscars:/${slot}/${action}`,
			args: args
		});		
	}
};
