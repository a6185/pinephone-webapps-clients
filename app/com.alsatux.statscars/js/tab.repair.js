// Jean Luc Biellmann - contact@alsatux.com

"use strict";

_Tab.repair = {
	selected: null,
	init: () => {
		_GUI._DOM.listen('#button_repairs_add', 'click', _Tab.repair.slot.add);
		_GUI._DOM.listen('#button_repairs_return', 'click', _Tab.car.slot.sel_back);
		_GUI._DOM.listen('#button_repairs_del', 'click', _Tab.repair.slot.del);
		_GUI._DOM.listen('#button_repairs_rec', 'click', _Tab.repair.slot.submit);
		_GUI._DOM.listen('#button_repairs_back', 'click', _Tab.repair.slot.ls);
		_GUI._DOM.listen('#repairs_list', 'click', _Tab.repair.slot.sel);
	},
	reset: () => {
		_Tab.repair.selected = null;
	},
	slot: {
		add: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tab.repair.editor(null);
			});
		},
		edit: (e) => {
			return _GUI._Lock.action_need_repair_selected(e, (e) => {
				_Tab.repair.editor(_Tab.repair.selected);
			});
		},
		sel: (e) => {
			return _GUI._Lock.action(e, (e) => {
				let el = e.target.closest('tr');
				if (el!=null) {
					_Tab.repair.editor(el.getAttribute('data-uid'));
				}
			});
		},
		del: (e) => {
			return _GUI._Lock.action_need_repair_selected(e, (e) => {
				if (confirm('Delete this record ?')) {
					let row = _Module.tbl.repairs.obj[_Tab.repair.selected].obj;
					_Signals.trigger('repairs', 'del', {
						repair: row
					});
				}
			});
		},
		submit: (e) => {
			return _GUI._Lock.action_with_spinner(e, (e) => {
				let row = {
					'car_uid': _Tab.car.selected,
					'date': _GUI._DOM.id('#repair_date').value,
					'garage': _GUI._DOM.id('#repair_garage').value,
					'desc': _GUI._DOM.id('#repair_desc').value,
					'price': _GUI._DOM.id('#repair_price').value.to_float()
				};
				try {
					//_DateString.pattern = 'YYYY-MM-DD';
					if (_Tab.repair.selected!=null) {
						row.uid = _Tab.repair.selected
						_Signals.trigger('repairs', 'upd', {
							repair: row
						});
					} else {
						_Signals.trigger('repairs', 'add', {
							repair: row
						});
					}
				} catch(e) {
					_GUI._Log.error(e);
				}
			});
		},
		ls: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tab.repair.list();
			});
		}
	},
	list: () => {
		_Tabs.open('tab_repairs_list');
		_Tab.repair.selected = null;
		//let rows = _Module.tbl.cars.obj[_Tab.car.selected].sort_by_date_desc('repair');
		let rows = _Module.func.sort_by_date_desc('repairs', _Tab.car.selected);
		if (!rows.length) {
			_GUI._DOM.hide('#repairs_list');
			_GUI._DOM.show('#no_repairs_list');
		} else {
			let html = '';
			rows.forEach(row => {
				html += row.to_HTML();
			});
			_GUI._DOM.id('#repairs_rows').innerHTML = html;
			_GUI._DOM.hide('#no_repairs_list');
			_GUI._DOM.show('#repairs_list');
		}
	},
	editor: (uid) => {
		_Tabs.open('tab_repair_editor');
		_Tab.repair.selected = uid;
		if (uid!=null) {
			let row = _Module.tbl.repairs.obj[uid].obj;
			_GUI._DOM.id('#repair_date').value =  (new Date(row.date)).to_yyyymmdd('-');
			_GUI._DOM.id('#repair_garage').value = String(row.garage);
			_GUI._DOM.id('#repair_desc').value = String(row.desc);
			_GUI._DOM.id('#repair_price').value = Number(row.price).to_currency();
		} else {
			_GUI._DOM.id('#repair_date').value = (new Date()).to_yyyymmdd('-');
			_GUI._DOM.id('#repair_garage').value = '';
			_GUI._DOM.id('#repair_desc').value = '';
			_GUI._DOM.id('#repair_price').value = '';
		}
	}
};
