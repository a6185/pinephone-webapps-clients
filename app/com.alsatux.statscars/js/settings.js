// Jean Luc Biellmann - contact@alsatux.com

"use strict";

class Settings extends PP_Settings {
	constructor () {
		super();
		this.add_setting(this.setting_distance);
		this.add_setting(this.setting_volume);
	}
	setting_distance () {
		let _Setting_Object = this.tbl.settings.add({
			cat: 'unit',
			key: 'distance', // add label_locale to lang.js
			type: 'select',
			nb_options: '1,1', // min,max
			val: {
				km: 'kilometer',
				mi: 'mile'
			}, 
			cur: ['km'],
			def: ['km']
		});
		_Setting_Object.tpl = new PP_Settings_Tpl_Option(_Setting_Object);
		_Setting_Object.upd_cur = cur => {
			let distance = cur[0];			
			if (_Setting_Object.obj.val.hasOwnProperty(distance)) {
				_Setting_Object.obj.cur = [distance];
			}
		}
		_Setting_Object.callback = cur => { // from HTML event
			_Setting_Object.upd_cur(cur);
			_Module._Settings.have_changed();
			_GUI._Log.info(tr1('Unit volume is now') + ' : ' + _Setting_Object.obj.cur[0]);
			_GUI._Tpl.load(_Lang.locale);
		};
	}
	setting_volume () {
		let _Setting_Object = this.tbl.settings.add({
			cat: 'unit',
			key: 'volume', // add label_locale to lang.js
			type: 'select',
			nb_options: '1,1', // min,max
			val: {
				l: 'liter',
				gal: 'gallon'
			}, 
			cur: ['l'],
			def: ['l']
		});
		_Setting_Object.tpl = new PP_Settings_Tpl_Option(_Setting_Object);
		_Setting_Object.upd_cur = cur => {
			let volume = cur[0];			
			if (_Setting_Object.obj.val.hasOwnProperty(volume)) {
				_Setting_Object.obj.cur = [volume];
			}
		}
		_Setting_Object.callback = cur => { // from HTML event
			_Setting_Object.upd_cur(cur);
			_Module._Settings.have_changed();
			_GUI._Log.info(tr1('Unit distance is now') + ' : ' + _Setting_Object.obj.cur[0]);
			_GUI._Tpl.load(_Lang.locale);
		};
	}
}