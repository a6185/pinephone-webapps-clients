// Jean Luc Biellmann - contact@alsatux.com

"use strict";

_Tab.car = {
	selected: null,
	init: () => {
		// list of cars
		_GUI._DOM.listen('#button_settings_open', 'click', _Tab.settings.slot.open);
		_GUI._DOM.listen('#button_car_list_add', 'click', _Tab.car.slot.add);
		_GUI._DOM.listen('#cars_rows', 'click', _Tab.car.slot.sel);
		// car selected
		_GUI._DOM.listen('#button_car_sel_back', 'click', _Tab.car.slot.ls);
		_GUI._DOM.listen('#button_car_sel_edit', 'click', _Tab.car.slot.edit);
		_GUI._DOM.listen('#button_car_sel_del', 'click', _Tab.car.slot.del);
		_GUI._DOM.listen('#button_car_sel_fill', 'click', _Tab.car.slot.fill);
		_GUI._DOM.listen('#button_car_sel_repair', 'click', _Tab.car.slot.repair);
		_GUI._DOM.listen('#button_car_sel_insurance', 'click', _Tab.car.slot.insurance);
		_GUI._DOM.listen('#button_car_sel_stats', 'click', _Tab.stats.slot.stats);
		// car editor
		_GUI._DOM.listen('#button_car_edit_rec', 'click', _Tab.car.slot.submit);
		_GUI._DOM.listen('#button_car_edit_back', 'click', _Tab.car.slot.sel_back);
		// car photos
		_GUI._DOM.listen('#button_car_sel_photos', 'click', _Tab.photo.slot.ls);
		// stats
		_GUI._DOM.listen('#button_car_stats_back', 'click', _Tab.car.slot.sel_back);
		// export
		_GUI._DOM.listen('#main_export', 'click', _Tab.car.slot.click_export);
	},
	reset: () => {
		_Tab.car.selected = null;
	},
	slot: {
		add: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tab.car.editor(null);
			});
		},
		fill: (e) => {
			if (_Tab.car.selected !== null) {
				return _GUI._Lock.action(e, (e) => {
					_Tab.fill.list();
				});				
			}
		},
		repair: (e) => {
			if (_Tab.car.selected !== null) {
				return _GUI._Lock.action(e, (e) => {
					_Tab.repair.list();
				});				
			}
		},
		insurance: (e) => {
			if (_Tab.car.selected !== null) {
				return _GUI._Lock.action(e, (e) => {
					_Tab.insurance.list();
				});				
			}
		},
		edit: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tab.car.editor(_Tab.car.selected);
			});
		},
		sel: (e) => {
			return _GUI._Lock.action(e, (e) => {
				let el = e.target.closest('.row');
				if (el!=null) {
					_Tab.car.selector(el.getAttribute('data-uid'));
				}
			});
		},
		sel_back: (e) => {
			return _GUI._Lock.action(e, (e) => {
				if (_Tab.car.selected!=null) {
					_Tab.car.selector(_Tab.car.selected);
				} else {
					_Tab.car.list();
				}
			});
		},
		ls: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tab.car.list();
			});
		},
		del: (e) => {
			return _GUI._Lock.action_need_car_selected(e, (e) => {
				if (confirm(tr1('Delete this car and all datas related ?'))) {
					let row = _Module.tbl.cars.obj[_Tab.car.selected].obj;
					_Signals.del('car',row);
				}
			});
		},
		submit: (e) => {
			return _GUI._Lock.action_with_spinner(e, (e) => {
				let row = {
					'name': _GUI._DOM.id('#car_name').value,
					'driver': _GUI._DOM.id('#car_driver').value,
					'purchase': {
						'date': _GUI._DOM.id('#car_purchase_date').value,
						'counter': _GUI._DOM.id('#car_purchase_counter').value,
						'price': _GUI._DOM.id('#car_purchase_price').value,
					}
				};
				try {
					//_DateString.pattern = 'YYYY-MM-DD';
					if (_Tab.car.selected!=null) {
						row.uid = _Tab.car.selected;
						_Signals.upd('car',row);
					} else {
						_Signals.add('car',row);
					}
					_Tab.car.selector(_Tab.car.selected);
				} catch (e) {
					alert(e);
				}
			});
		},
		click_export: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tab.car.export();
			});
		}
	},
	list: () => {
		_Tabs.open('tab_cars_list');
		_Tab.car.selected = null;
		let n = Object.keys(_Module.tbl.cars.obj).length;
		_GUI._DOM.id('#nb_of_cars').innerHTML = n;
		if (!n) {
			_GUI._DOM.show('#no_cars_list');
			_GUI._DOM.hide('#cars_list');
		} else {
			let html = '';
			for (let uid in _Module.tbl.cars.obj) {
				html += _Module.tbl.cars.obj[uid].to_html_list();
			}
			_GUI._DOM.id('#cars_rows').innerHTML = html;
			_GUI._DOM.hide('#no_cars_list');
			_GUI._DOM.show('#cars_list');
		}
	},
	editor: (uid) => {
		_Tabs.open('tab_car_editor');
		_Tab.car.selected = uid;
		if (uid!=null) {
			let obj = _Module.tbl.cars.obj[uid].obj;
			_GUI._DOM.id('#car_name').value = String(obj.name);
			_GUI._DOM.id('#car_driver').value = String(obj.driver);
			_GUI._DOM.id('#car_purchase_date').value = (new Date(obj.purchase.date)).to_yyyymmdd('-');
			_GUI._DOM.id('#car_purchase_counter').value = String(obj.purchase.counter).to_locale_real();
			_GUI._DOM.id('#car_purchase_price').value = Number(obj.purchase.price).to_currency();
		} else {
			_GUI._DOM.id('#car_name').value = '';
			_GUI._DOM.id('#car_driver').value =  '';
			_GUI._DOM.id('#car_purchase_date').value = (new Date()).to_yyyymmdd('-');
			_GUI._DOM.id('#car_purchase_counter').value = '';
			_GUI._DOM.id('#car_purchase_price').value = '';
		}
	},
	selector: (uid) => {
		_Tab.car.selected = uid;
		let _Car_Object = _Module.tbl.cars.obj[uid];
		_Tabs.open('tab_car_sel');
		let photo = DEFAULT_CAR;
		/*if (_Car_Object.obj.photo.length) {
			let photo_uid = _Car_Object.obj.photo[0];
			photo = _Module.tbl.photos.obj[photo_uid].obj.dataurl;
		}*/
		let rows = _Module.func.select('photos',uid);
		if (rows.length) {
			photo = rows[0].obj.dataurl;
		}
		_GUI._DOM.id('#car_sel_photo_src').src = photo;
		_GUI._DOM.id('#car_sel_text').innerHTML = _Car_Object.to_html_editor();
	},
	export: () => {
		let xml_string = _XML.write();
		if (!window.URL || !window.URL.createObjectURL)
			return alert(tr1('Warning: your browser doesn\'t support XML file creation on the fly ! Please use Mozilla Firefox !'));
		let blob = new Blob([xml_string], {type:'text/plain;charset=utf-8'});
		let a = document.createElementNS("http://www.w3.org/1999/xhtml", "a");
		a.href = window.URL.createObjectURL(blob);
		a.download = (new Date()).to_yyyymmddhhiiss() + '.xml';
		let event = document.createEvent("MouseEvents");
		event.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
		a.dispatchEvent(event);
	}
};
