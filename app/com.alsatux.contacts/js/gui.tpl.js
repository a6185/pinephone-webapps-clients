// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * GUI auto template translations
 */

_GUI._Tpl.set([
'contacts_number','Contacts','Contacts',
'kws','?','?',
'label_fields','Visible fields','Champs à afficher',
'preselect_all','All','Tous',
'preselect_usual','Most currents','Les plus courants',
'activated_part_contact','Contact','Contact',
'tr_cont_name','Name','Nom',
'tr_cont_honorificPrefix','Honorific prefix','Préfixe honorifique',
'tr_cont_givenName','Given name','Prénom',
'tr_cont_additionalName','Additional name','Nom additionnel',
'tr_cont_familyName','Family name','Nom de famille',
'tr_cont_honorificSuffix','Honorific suffix','Suffixe honorifique',
'tr_cont_nickname','Nickname','Surnom',
'tr_cont_sex','Sex','Sexe',
'tr_cont_genderIdentity','Gender identity','Genre',
'tr_cont_category','Category','Catégorie',
'tr_cont_org','Org','Organisation',
'tr_cont_jobTitle','Job title','Emploi',
'tr_cont_bday','Birthday','Date de naissance',
'tr_cont_anniversary','Anniversary','Anniversaire',
'tr_cont_note','Note','Note',
'tr_cont_key','Key','Clé',
'activated_part_adr','Addresses','Adresses',
'tr_adr_type','Type','Type',
'tr_adr_streetAddress','Street address','Adresse',
'tr_adr_postalCode','Postal code','Code postal',
'tr_adr_locality','Locality','Ville',
'tr_adr_region','Region','Région',
'tr_adr_countryName','Country','Pays',
'activated_part_email','Emails','Emails',
'tr_email_type','Type','Type',
'tr_email_value','Value','Email',
'activated_part_phone','Phones','Téléphones',
'tr_tel_value','Value','Téléphone',
'tr_tel_carrier','Carrier','Opérateur',
'activated_part_url','URLs','Site web',
'tr_url_type','Type','Type',
'tr_url_value','Value','Adresse',
'activated_part_impp','IMPP','Messagerie instantanée',
'tr_impp_type','Type','Type',
'tr_impp_value','Value','Adresse',
'label_sort','Sort contacts by','Trier les contacts par',
'name','Name','Nom',
'givenName','Given name','Prénom',
'familyName','Family name','Nom de famille',
'org','Org','Organisation',
'category','Category','Catégorie',
'label_import','Import','Importer',
'label_import_help1','Drag/drop your backup file here','Déposez votre fichier de sauvegarder ici',
'label_import_help2','or download it manually','ou téléchargez le manuellement',
'label_export_calendar','Export your contacts','Exportez les contacts',
//'button_export_as_xml','as XML','en XML',
'label_go_to_letter','Go to','Aller à',
'cont_name','Name (String[])','Nom (Chaîne[])',
'cont_honorificPrefix','Honorific prefix (String[])','Préfixe honorifique (Chaîne[])',
'cont_givenName','Given name (String[])','Prénom (Chaîne[])',
'cont_additionalName','Additional name (String[])','Nom additionnel (Chaîne[])',
'cont_familyName','Family name (String[])','Nom de famille (Chaîne[])',
'cont_honorificSuffix','Honorific suffix (String[])','Suffixe honorifique (Chaîne[])',
'cont_nickname','Nickname (String[])','Surnom (Chaîne[])',
'cont_sex','Sex (String)','Sexe (Chaîne)',
'cont_genderIdentity','Gender identity (String)','Genre (Chaîne)',
'cont_category','Category (String[])','Catégorie (Chaîne[])',
'cont_org','Org (String[])','Organisation (Chaîne[])',
'cont_jobTitle','Job title (String[])','Emploi (Chaîne[])',
'cont_bday','Bday (YYYY-MM-DD)','Date de naissance (JJ/MM/AAAA)',
'cont_anniversary','Anniversary (YYYY-MM-DD)','Anniversaire (JJ/MM/AAAA)',
'cont_note','Note (String[])','Note (Chaîne[])',
'cont_key','Key (String[])','Clé (Chaîne[])',
'label_edit_adr','Addresses','Adresses',
'adr_type','Type: (String) [home, work, ...]','Type: (Chaîne) [domicile, bureau, ...]',
'adr_streetAddress','Street address (String)','Adresse (Chaîne)',
'adr_postalCode','Postal code (String)','Code postal (Chaîne)',
'adr_locality','Locality (String)','Ville (Chaîne)',
'adr_region','Region (String)','Région (Chaîne)',
'adr_countryName','Country (String)','Pays (Chaîne)',
'label_edit_email','Emails','Emails',
'email_type','Type: (String) [home, work, ...]','Type: (Chaîne) [domicile, bureau, ...]',
'email_value','Value (String)','Valeur (Chaîne)',
'label_edit_phone','Phones','Téléphones',
'tel_type','Type: (String) [home, work, ...]','Type: (Chaîne) [domicile, bureau, ...]',
'tel_value','Value (String)','Valeur (Chaîne)',
'tel_carrier','Carrier (String)','Opérateur (Chaîne)',
'label_edit_url','URLs','URLs',
'url_type','Type: (String) [home, work, ...]','Type: (Chaîne) [domicile, bureau, ...]',
'url_value','Value (String)','Valeur (Chaîne)',
'label_edit_impp','IMPP','Messagerie instantanée',
'impp_type','Type: (String) [home, work, ...]','Type: (Chaîne) [domicile, bureau, ...]',
'impp_value','Value (String)','Valeur (Chaîne)',
'label_edit_photo','Photos','Photos',
'uploader2','Drag and drop your jpeg photos here !','Déposez vos photos JPG ici !',
'label_import_pict','or download them manually :','ou télécharger-les manuellement !',
'label_edit_info','Infos','Infos',
'label_edit_info_id','ID','ID',
'label_edit_info_published','published','publié le',
'label_edit_info_updated','updated','mis à jour le',
'label_edit_help','Help','Aide',
'edit_help1','- use comma as separator in the text fields -','- utilisez la virgule comme séparateur dans les champs texte -',
'edit_help2','- use --- (3 chars) as separator in the textarea fields -','utilisez --- (3 caractères) comme séparateur dans les champs multilignes',
'button_log_close','Close log','Fermer le journal',
'button_delete_contact','Delete','Effacer',
// classes
'part_submit','Submit','Envoyer',
'part_reset','Reset','Réinitialiser',
/* tab settings */
'label_settings','Settings','Réglages',
'label_setting_user_locale','Language','Langue',
'label_setting_server_websocket','Websocket','Websocket',
'websocket_host','Host ?','Hôte ?',
'websocket_port','Port ?','Port ?',
/* tab log */
'button_log_close','Close log','Fermer le journal'
]);
/*_GUI._Tpl = {
	data: [ // for index.html
	],
	load: (locale) => {
		let pos = (Object.keys(_Module._Manifest.obj.lang).indexOf(locale)!=-1) ? Object.keys(_Module._Manifest.obj.lang).indexOf(locale) : 0;
		let re = new RegExp('^[a-z0-9_]+$','i');
		for (let i=0;i<_GUI._Tpl.data.length;i+=3) {
			let key = _GUI._Tpl.data[i];
			//if (key=='tr_cont_givenName')
			//	debugger;
			let value = _GUI._Tpl.data[i+1+pos];
			let obj = _GUI._DOM.id('#' + key);
			if (obj!=null) {
				// object has a placeholder
				if (obj.hasAttribute('placeholder')) {
					obj.setAttribute('placeholder',value);
					continue;
				}
				obj.innerHTML = value;
			} else {				
				let elements = document.querySelectorAll('*[data-value="' + key + '"]');
				if (elements.length) {
					elements.forEach(el => {
						el.innerHTML = value;
					});
					continue;
				}
				if (re.test(key)) {					
					elements = document.querySelectorAll('.' + key);
					if (elements.length) {
						elements.forEach(el => {
							el.innerHTML = value;
						});
						continue;
					}
				}
				_Lang.object_not_found(_GUI._Tpl.data[i]);
			}
		}
	}
};
*/