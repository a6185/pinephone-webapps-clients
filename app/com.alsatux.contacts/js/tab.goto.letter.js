// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Go to letter tab
 */

_Tab.gotoletter = {
	/**
	 * Initialization
	 */
	init: () => {
		_GUI._DOM.listen('#main_goto_letter', 'click', _Tab.gotoletter.slot.open);		
	},
	/**
	 * Slots (DOM events handlers)
	 */
	slot: {
		/**
		 * Open the tab
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		open: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tabs.open('tab_goto_letter');
			});
		},
		/**
		 * Returns to tab main and scroll to the choosen letter
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		goto_letter: (e) => {
			return _GUI._Lock.action(e, (e) => {
				let el = e.target.closest('li');
				if (el!=null) {
					let i = el.getAttribute('data-value');
					_Tabs.open('tab_main');
					let y = _GUI._DOM.id('#letter_'+i).offsetTop;
					_GUI._DOM.id('#contacts').scrollTo({top: y});
				}
			});
		}
	},
	/**
	 * Build the letters found in contacts list
	 * @param  {array} chars Array of chars
	 */
	update: (chars) => {
		let ul_letters_abrv = _GUI._DOM.id('#ul_letters_abrv');
		_GUI._DOM.unlisten('#ul_letters_abrv','click',_Tab.gotoletter.slot.goto_letter);
		ul_letters_abrv.innerHTML = '';		
		let i = 0;	
		while (chars.length) {
			let c = chars.shift();
			ul_letters_abrv.innerHTML += '<li data-value="' + i++ + '">&nbsp;' + _GUI._HTML.escape(c) + '&nbsp;</li>';
		}
		_GUI._DOM.listen('#ul_letters_abrv','click',_Tab.gotoletter.slot.goto_letter);
	}
}
