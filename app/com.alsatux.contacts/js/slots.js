// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Module slots
 */

var _Slots = {
	/**
	 * Register available routes
	 */
	init: () => {
		_Route.add('com.alsatux.contacts:/read', _Slots.read);
		_Route.add('com.alsatux.contacts:/added', _Slots.added);
		_Route.add('com.alsatux.contacts:/updated', _Slots.updated);
		_Route.add('com.alsatux.contacts:/deleted', _Slots.deleted);
	},
	/**
	 * Refresh the contacts using datas from server side
	 * @param  {object} data Datas from server
	 */
	read: (data) => {
		_Module.tbl.contacts.refresh(data);
		_Module.loaded();
	},
	/**
	 * Handler when a new contact has been created on server sire
	 * @param  {object} data Datas from server
	 */
	added: (data) => {
		_Module.tbl.contacts.add(data.args.contact);
	},
	/**
	 * Handler when a contact has been updated on server sire
	 * @param  {object} data Datas from server
	 */
	updated: (data) => {
		_Module.tbl.contacts.add(data.args.contact);
	},
	/**
	 * Handler when a contact has been deleted on server sire
	 * @param  {object} data Datas from server
	 */
	deleted: (data) => {
		_Module.tbl.contacts.del(data.args.contact);
	}
};
