// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Dictionnary use in js code, according to manifest.json
 */

function tr1 (txt) {
	const dict = [
'Please report any bug to','Merci de signaler tout bug à',	
'Searching for the last backup file','Recherche de la dernière sauvegarde',
//	
'!',' !',
'?',' ?',
// contact
'Favorites','Favoris',
'Delete the selected contact ?','Effacer le contact sélectionné ?',
'Field "%s" is empty !','Le champ "%s" est vide !',
'Wrong format for field "%s" !','Le format du champ "%s" est erroné !',
// lang.js
'Lang is now','Langue courante',
'Object','L\'objet',
'has no translation','n\'a pas été traduit',
// import.js
'Error while uploading file','Erreur durant le chargement du fichier',
'Add to favorites','Ajouter aux favoris',
'Remove from favorites','Retirer des favoris',
'File uploaded','Fichier téléchargé',
'Remove existing contacts','Effacer les contacts existants',
'Override existing contacts','Écraser les contacts existants',
// tab.resume.js
'Delete this contact','Effacer ce contact'
	];
	let offset = Object.keys(_Module._Manifest.obj.lang).indexOf(_Lang.locale);	
	let pos = dict.indexOf(txt);	
	if (pos!=-1) {
		return dict[pos + (offset<0 ? 0 : offset)];	
	} else {
		_Lang.object_not_found(txt);
		return txt;
	}
}

function tr2 (txt) {
	const dict = [
// contact.js
'mobile','Mobile','Portable',
'home','Home','Domicile',
'work','work','Bureau',
'personal','Personal','Personnel',
'faxHome','Fax home','Fax domicile',
'faxOffice','Fax office','Fax bureau',
'faxOther','Fax other','Autre fax',
'another','Another','Autre',
	];
	let offset = Object.keys(_Module._Manifest.obj.lang).indexOf(_Lang.locale);	
	let pos = dict.indexOf(txt);	
	if (pos!=-1) {
		return dict[pos + offset+1];	
	} else {
		_Lang.object_not_found(txt);
		return txt;
	}
}
