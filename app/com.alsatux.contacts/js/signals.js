// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Module signals
 */

var _Signals = {
	/**
	 * Load contacts from server
	 */
	start: () => {
		_GUI._Log.info(tr1('Searching for the last backup file') + '... ');
		_WS.send({
			route: 'com.alsatux.contacts:/ls',
			args: {},
			callback: 'com.alsatux.contacts:/read'
		});
	},
	/**
	 * Add ou update a contact
	 */
	submit: () => {
		let action = _Module.core.editor.cur_uid !== null ? 'upd' : 'add';
		_WS.send({
			route: 'com.alsatux.contacts:/' + action,
			args: {
				contact: _Module.core.editor.cur_cont
			}
		});
		_Module.core.list.refresh();
	},
	/**
	 * Remove a contact
	 */
	remove: () => {
		if (_Module.core.editor.cur_uid !== null) {
			_WS.send({
				route: 'com.alsatux.contacts:/del',
				args: {
					contact: _Module.core.editor.cur_cont
				}
			});								
		}
	},
	/**
	 * Import contact from an external backup file
	 * @param  {object} db       Database of contacts (JS object)
	 * @param  {bool} reset    If true, remove existing contact, else add them
	 * @param  {bool} override If true, remove existing contact, else add them
	 */
	import: (db,reset,override) => {
		_WS.send({
			route: 'com.alsatux.contacts:/import',
			args: {
				db: db,
				reset: reset,
				override: override
			}
		});			
	},
	/**
	 * Modem extra actions
	 */
	modem: {
		/**
		 * Call the phone module
		 * @param  {string} number Number to call
		 */
		call: (number) => {
			_GUI._Window.open('com.alsatux.phone');
			_Module._IPC.trigger('com.alsatux.phone', {
				method: 'dial',
				args: {
					number: number
				}
			});
		},
		/**
		 * Call the SMS module
		 * @param  {string} number Number to dial
		 */
		sms: (number) => {
			_GUI._Window.open('com.alsatux.smms');
			_Module._IPC.trigger('com.alsatux.smms', {
				method: 'create',
				args: {
					number: number
				}
			});
		}
	}
};