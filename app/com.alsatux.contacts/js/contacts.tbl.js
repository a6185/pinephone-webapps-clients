// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Contacts table
 */

class Contacts_Tbl extends PP_Tbl {
	/**
	 * Constructor
	 */
	constructor () {
		super();		
	}
	/**
	 * Refresh the contacts using datas from server side
	 * @param  {object} data Datas returned by the server
	 */
	refresh (data) {
		if (hasKey(data, 'args', Object)) {
			if (hasKey(data.args, 'date_pattern', String)) {
				_Module.const.date_pattern = data.args.date_pattern // for dates !
			}
			if (hasKey(data.args, 'db', Object)) {
				for (let uid in data.args.db) {
					this.obj[uid] = (new Contact_Object()).upd(data.args.db[uid]);
				}
				//_Contact.obj = data.args.db;				
				_Module.core.list.refresh();				
			}
		}
	}
	/**
	 * Add an existing Contact object to the table
	 * @param {object} obj JS object
	 */
	add (obj) {
		this.set(obj.uid,(new Contact_Object()).upd(obj));
		if (_Tabs.cur_id=='tab_edit' || _Tabs.cur_id=='tab_resume') {
			_Tab.resume.contact(obj.uid);
			_Tabs.open('tab_resume');
		}
	}
	/**
	 * Delete an existing entry
	 * @param  {object} obj JS object
	 */
	del (obj) {
		this.unset(obj.uid);
		_Module.core.editor.reset_cur();
		_Module.core.list.refresh();
		_Tabs.open('tab_main');
	}
	/**
	 * Extract distinct values for a given field
	 * @param  {array} rows Result array with single datas found
	 * @param  {object} obj  JS object to scan
	 */
	extract_single_values (rows, obj) {
		if (obj !== null && obj !== undefined) {
			if (obj.constructor==Array) {
				if (obj.length) {
					if (obj[0].constructor === String) {
						for (let j=0; j<obj.length; j++) {
							if (rows.indexOf(obj[j])==-1) {
								rows.push(obj[j]);
							}
						}
					}
				}
			} else if (obj.constructor === String) {
				if (obj.length) {
					if (rows.indexOf(obj)==-1) {
						rows.push(obj);
					}
				}
			}
		}			
	}
	/**
	 * Select distinct values for a given part_name/field_name
	 * @param  {array} rows       Result array with single datas found
	 * @param  {string} part_name  Part name
	 * @param  {string} field_name Field name
	 */
	select_distinct_values (rows, part_name, field_name) {
		let uid1,uid2,obj;
		for (uid1 in this.obj) {
			let contact = this.obj[uid1].obj;
			if (part_name=='cont') {
				obj = contact[field_name];
				this.extract_single_values(rows, obj);
			} else {
				// part
				if (contact[part_name]) {
					for (uid2 in contact[part_name]) {
						obj = contact[part_name][uid2][field_name];
						this.extract_single_values(rows, obj);
					}
				}
			}
		}
	}	
	/**
	 * For each selector, get count for each value
	 * @param  {object} hash Result object
	 * @param  {string} key1 Part name
	 * @param  {string} key2 Part type
	 */
	db_select_count (hash, key1, key2) {
		for (let id in this.obj) {
			let contact = this.obj[id].obj;
			if (key1=='cont') {
				if (contact[key2])
					if (contact[key2].constructor==Array) {
						if (contact[key2].length) {
							if (contact[key2][0].constructor==String) {
								for (let j=0; j<contact[key2].length; j++) {
									let value = contact[key2][j];
									if (!hash.hasOwnProperty(value)) {
										hash[value] = 1;
									} else {
										hash[value] += 1;
									}
								}
							}	
						}
					} else if (contact[key2].constructor==String) {
						if (contact[key2].length) {
							let value = contact[key2];
							if (!hash.hasOwnProperty(value)) {
								hash[value] = 1;
							} else {
								hash[value] += 1;
							}
						}
					}
			} else {
				// part
				if (contact[key1]) {
					for (let uid in contact[key1]) {
						if (contact[key1][uid][key2]) {
							if (contact[key1][uid][key2].constructor==Array) {
								if (contact[key1][uid][key2].length) {
									if (contact[key1][uid][key2][0].constructor==String) {
										for (let k=0; k<contact[key1][uid][key2].length; k++) {
											let value = contact[key1][uid][key2][k];
											if (!hash.hasOwnProperty(value)) {
												hash[value] = 1;
											} else {
												hash[value] += 1;
											}
										}
									}	
								}
							} else {
								if (contact[key1][uid][key2].constructor==String) {
									if (contact[key1][uid][key2].length) {
										let value = contact[key1][uid][key2];
										if (!hash.hasOwnProperty(value)) {
											hash[value] = 1;
										} else {
											hash[value] += 1;
										}
									}
								}	
							}
						}
					}
				}
			}
		}
	}
}
