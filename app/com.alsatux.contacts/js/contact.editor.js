// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Contact editor
 */

class Contact_Editor {
	/**
	 * Constructor
	 */
	constructor () {
		this.reset_cur();
		this.record = {
			arrays: 'name,honorificPrefix,givenName,additionalName,familyName,honorificSuffix,nickname,category,org,jobTitle,note,key'.split(','),
			strings: 'id,sex,genderIdentity'.split(','),
			dates: 'bday,anniversary,published,updated'.split(','),
			parts: {
				adr: 'streetAddress,locality,postalCode,region,countryName,type'.split(','),
				tel: 'value,carrier,type'.split(','),
				email: 'value,type'.split(','),
				url: 'value,type'.split(','),
				impp: 'value,type'.split(',')
			},
			types: {
				adr: 'home,work'.split(','),
				tel: 'mobile,home,work,personal,faxHome,faxOffice,faxOther,another'.split(','),
				email: 'personal,home,work'.split(','),
				url: 'personal,home,work'.split(','),
				impp: 'personal,home,work'.split(',')
			}
		};
	}
	/**
	 * Unselect current user
	 */
	reset_cur () {
		this.cur_uid = null;
		this.cur_cont = {}		
	}
	/**
	 * Find next free UID
	 * @param  {object} index JS object
	 * @return {number}       UID
	 */
	next_uid (index) {
		let keys = Object.keys(index);
		let c = 0;
		let uid = '';
		do {
			uid = sprintf('%02d',c++);
		} while (keys.indexOf(uid)!=-1 && c<=99);		
		return uid;	// 99 will be overriden	!
	}
	/**
	 * Reset the form
	 */
	refresh () {
		// toggle elements according to user settings
		[...document.querySelectorAll('#activated_fields li')].forEach(li => {
			let fieldname = li.getAttribute('data-value');
			let field = _GUI._DOM.byClassName(fieldname)[0];
			if (li.classList.contains('selected')) {
				if (field.classList.contains('invisible')) {
					field.classList.remove('invisible');
				}
			} else {
				if (!field.classList.contains('invisible')) {
					field.classList.add('invisible');
				}
			}
		});
		// reset static fields (labels and hidden ids)
		'uid,published,updated'.split(',').forEach(field => {
			_GUI._DOM.id('#cont_' + field).innerHTML = '';
		});
		[...document.querySelectorAll('.part_record')].forEach(el => el.remove());
		// Reset submit buttons
		this.part_reset_changed_all();
		// reset form fields
		_GUI._DOM.id('#form1').reset();
		// reset photos
		_GUI._DOM.id('#photos_list').innerHTML = '';
	}
	/**
	 * Edit a contact
	 * @param  {number|null} uid  Contact UID or null if new
	 * @param  {cont} cont Contact object (JS object)
	 */
	edit (uid, cont) {
		// reset datas
		this.refresh();
		// prepare row
		let row = {};
		// fix current contact sequence
		this.cur_id = uid;
		row['cont_uid'] = uid;
		// fix current contact (copy)
		this.cur_cont = cont;
		// name|honorificPrefix|givenName|additionalName|familyName|honorificSuffix|nickname|category|org|jobTitle|note|key
		this.record.arrays.forEach(field => {
			if (hasKey(cont, field, Array)) {
				if (field.match(/^(note|key)$/)!=null) {
					row['cont_' + field] = cont[field].join('---');
				} else {
					row['cont_' + field] = cont[field].join(',');
				}
			}
		});
		// id|sex|genderIdentity
		this.record.strings.forEach(field => {
			if (hasKey(cont, field, String)) {
				row['cont_' + field] = cont[field];
			}
		});
		// bday|anniversary|published|updated
		this.record.dates.forEach(field => {
			if (hasKey(cont, field, String)) {
				/*
				iso: 'YYYY-MM-DD',
				usa: 'MM/DD/YYYY',
				eur: 'DD/MM/YYYY'
				*/
				let v,value = cont[field];
				switch (_Module.const.date_pattern.value) {
					case 'YYYY-MM-DD':
						v = value; // iso format is default
						break;
					case 'MM/DD/YYYY':
						v = sprintf('%s/%s/%s',value.substr(5,2),value.substr(8,2),value.substr(0,4));
						break;
					case 'DD/MM/YYYY':
						v = sprintf('%s/%s/%s',value.substr(8,2),value.substr(5,2),value.substr(0,4));
						break;
				}
				row['cont_' + field] = v;
			}
		});
		// fill contact main entries
		_GUI._HTML.fill(row);
		// build selectors
		'adr_type,email_type,tel_type,url_type,impp_type'.split(',').forEach(field => {
			_GUI._DOM.id('#' + field).innerHTML='';
			let target = field.split('_');
			let hash = {};
			_Module.tbl.contacts.db_select_count(hash,target[0],target[1]);
			let keys = Object.keys(hash);
			if (keys.length) {
				// sort hash by reverse order
				let hash_desc = keys.sort((a, b) => {
					return hash[a] - hash[b]
				}).reverse();
				// take first key (maximum) as default value
				_GUI._DOM.id('#' + field).value = hash_desc[0];
			}
		});
		// add others parts
		Object.keys(this.record.parts).forEach(part => {
			for (let uid in cont[part]) {
				this.part_add(part, uid, cont[part][uid]);
			}
		});
		for (let uid in cont.photo) {
			this.part_add_photo(uid, cont.photo[uid]);
		}
		_GUI._DOM.id('#cont_name').focus();
	}
	/**
	 * Add a new contact
	 */
	add_new () {
		this.reset_cur();
		this.edit(null, {
			uid: '',
			email: {},
			tel: {},
			adr: {},
			url: {},
			impp: {},
			photo: {}
		});
	}
	/**
	 * Update a contact
	 * @param  {number} uid Contact UID
	 */
	update (uid) {
		this.edit(uid, JSON.parse(JSON.stringify(_Module.tbl.contacts.obj[uid].obj)));
	}
	/**
	 * Mark a part as changed
	 * @param  {object} element DOM element
	 */
	part_has_changed (element) {
		element.classList.add('changed');
	}
	/**
	 * Reset all parts as unchanged
	 */
	part_reset_changed_all () {
		[...document.querySelectorAll('tab.editor input, tab.editor textarea')].forEach(el => {
			el.classList.remove('changed');
		});
	}
	/**
	 * Reset a given part as unchanged
	 * @param  {string} part_name Name of the part
	 */
	part_reset_changed (part_name) {
		// remove change decoration
		[...document.querySelectorAll('tab.editor input, tab.editor textarea')].forEach(el => {
			el.classList.remove('changed');
		});
	}
	/**
	 * Add a new part
	 * @param  {string} part_name Name of the part
	 * @param  {number} uid       Contact UID
	 * @param  {object} row       JS object
	 */
	part_add (part_name, uid, row) {
		// create a new li inside a part
		let tbody = _GUI._DOM.id('#' + part_name + '_list');
		let tr = document.createElement('tr');
		tr.id = part_name + '_' + uid;
		tr.className = 'part_record ' + part_name;
		tr.setAttribute('data-part',part_name);
		tbody.insertBefore(tr,tbody.childNodes[0]);
		this.part_update(part_name, uid, row);
	}
	/**
	 * Add a photo using a DataURI
	 * @param  {number} uid     Photo UID
	 * @param  {string} datauri Picture as DataURI
	 */
	part_add_photo (uid, datauri) {
		let img = new Image();
		img.onload = () => {
			let div = document.createElement('div');
			div.id = 'photo_' + uid;
			div.className = 'photo';
			div.innerHTML = '\
			<div class="part_photo">\
				<p><img class="photo" src="' + datauri + '" alt="Photo"/></p>\
				<p>' + img.width + 'x' + img.height + '</p>\
				<p><button type="button" onclick="_Module.core.editor.part_del_photo(\'' + uid + '\');">Delete</button></p>\
			</div>';
			_GUI._DOM.id('#photos_list').appendChild(div);
		}
		img.src = datauri;
	}
	/**
	 * Delete a photo
	 * @param  {number} uid Photo UID
	 */
	part_del_photo (uid) {
		if (this.cur_id.length && this.cur_cont['photo'].hasOwnProperty(uid)) {
			if (_GUI._DOM.id('#photo_' + uid)!=null) {
				let element = _GUI._DOM.id('#photo_' + uid);
				element.parentNode.removeChild(element);
			}
			delete this.cur_cont['photo'][uid];
		}
	}
	/**
	 * Edit an existing part
	 * @param  {string} part_name Type of the part
	 * @param  {number} part_uid  UID of the part
	 */
	part_edit (part_name, part_uid) {
		let id = part_name + '_' + part_uid;
		let row = {};
		switch (part_name) {
			case 'email':
			case 'url':
			case 'impp':
				row[part_name + '_uid'] = _GUI._HTML.getAttribute(id, 'data-uid');
				row[part_name + '_type'] = _GUI._HTML.getAttribute(id, 'data-type');
				row[part_name + '_value'] = _GUI._HTML.getAttribute(id, 'data-value');
				break;
			case 'tel':
				row = {
					'tel_uid': _GUI._HTML.getAttribute(id, 'data-uid'),
					'tel_type': _GUI._HTML.getAttribute(id, 'data-type'),
					'tel_value': _GUI._HTML.getAttribute(id, 'data-value'),
					'tel_carrier': _GUI._HTML.getAttribute(id, 'data-carrier'),
				};
				break;
			case 'adr':
				row = {
					'adr_uid': _GUI._HTML.getAttribute(id, 'data-uid'),
					'adr_type': _GUI._HTML.getAttribute(id, 'data-type'),
					'adr_streetAddress': _GUI._HTML.getAttribute(id, 'data-streetAddress'),
					'adr_postalCode': _GUI._HTML.getAttribute(id, 'data-postalCode'),
					'adr_locality': _GUI._HTML.getAttribute(id, 'data-locality'),
					'adr_region': _GUI._HTML.getAttribute(id, 'data-region'),
					'adr_countryName': _GUI._HTML.getAttribute(id, 'data-countryName'),
				};
				break;
		}
		_GUI._HTML.fill(row);
	}
	/**
	 * Update a part
	 * @param  {string} part_name Part name
	 * @param  {number} uid       Part UID
	 * @param  {object} row       JS object
	 */
	part_update (part_name, uid, row) {
		let output = [], output2 = [], dtype, tr = _GUI._DOM.id('#' + part_name + '_' + uid);
		switch (part_name) {
			case 'email':
			case 'url':
			case 'impp':
				dtype = row.type ? row.type.join(',') : '';
				_GUI._HTML.setAttribute(part_name + '_' + uid, {
					'data-uid': uid,
					'data-type': dtype,
					'data-value': row.value || '',
				});
				if (dtype.length) {
					output.push(_GUI._HTML.escape(dtype));
				}
				if (row.value && row.value.length) {
					output.push(_GUI._HTML.escape(row.value));
				}
				break;
			case 'tel':
				dtype = row.type ? row.type.join(',') : '';
				_GUI._HTML.setAttribute(part_name + '_' + uid, {
					'data-uid': uid,
					'data-type': dtype, // ["home"], ["work"], etc.
					'data-value': row.value || '',
					'data-carrier': row.carrier || '', // experimental extension - not in vCard!
				});
				if (dtype.length || (row.carrier && row.carrier.length)) {
					output.push((dtype.length ? _GUI._HTML.escape(dtype) + ' ' : '') + ((row.carrier && row.carrier.length) ? _GUI._HTML.escape(row.carrier) : '' ));
				}
				if (row.value && row.value.length) {
					output.push(_GUI._HTML.escape(row.value));
				}
				break;
			case 'adr':
				dtype = row.type ? row.type.join(',') : '';
				_GUI._HTML.setAttribute(part_name + '_' + uid, {
					'data-uid': uid,
					'data-type': dtype, // ["home"], ["work"], etc.
					'data-streetAddress': row.streetAddress || '',
					'data-postalCode': row.postalCode || '',
					'data-locality': row.locality || '',
					'data-region': row.region || '',
					'data-countryName': row.countryName || '',
				});
				if (dtype.length) {
					output.push(_GUI._HTML.escape(dtype));
				}
				if (row.streetAddress && row.streetAddress.length) {
					output.push(_GUI._HTML.escape(row.streetAddress));
				}
				output2 = [];
				if (row.postalCode && row.postalCode.length) {
					output2.push(_GUI._HTML.escape(row.postalCode));	
				}
				if (row.locality && row.locality.length) {
					output2.push(_GUI._HTML.escape(row.locality));	
				}
				if (output2.length) {
					output.push(_GUI._HTML.escape(output2.join(' ')));	
				}
				if (row.region && row.region.length) {
					output.push(_GUI._HTML.escape(row.region));
				}
				if (row.countryName && row.countryName.length) {
					output.push(_GUI._HTML.escape(row.countryName));
				}
				break;
		}
		if (output.length) {
			//  onclick="return this.part_delete(\'' + part_name + '\', \'' + uid + '\');"
			tr.innerHTML = '\
					<td>\
						<button class="icon delete" data-part="' + part_name + '" data-uid="' + uid + '" title="Delete record">X</button>\
					</td>\
					<td>\
						<div class="edit" data-part="' + part_name + '" data-uid="' + uid + '">\
						<p>' + this.emphasize(output.join('</p><p>')) + '</p>\
						</div>\
					</td>';			
		}
	}
	/**
	 * HTML emphasize a string
	 * @param  {string} str Source string
	 * @return {string}     Emphasized string
	 */
	emphasize (str) {
		return str.length ? '<b>' + str + '</b>' : '';
	}	
	/**
	 * Reset a part
	 * @param  {string} part_name Part name
	 */
	part_reset (part_name) {
		let row = {};
		switch (part_name) {
			case 'cont':
				// name|honorificPrefix|givenName|additionalName|familyName|honorificSuffix|nickname|category|org|jobTitle|note|key
				this.record.arrays.forEach(field => {
					row['cont_' + field] = '';
				});
				break;
			case 'email':
			case 'impp':
			case 'url':
				row[part_name + '_uid'] = '';
				row[part_name + '_type'] = '';
				row[part_name + '_value'] = '';
				break;
			case 'tel':
				row = {
					'tel_uid': '',
					'tel_type': '',
					'tel_value': '',
					'tel_carrier': '',
				};
				break;
			case 'adr':
				row = {
					'adr_uid': '',
					'adr_type': '',
					'adr_streetAddress': '',
					'adr_postalCode': '',
					'adr_locality': '',
					'adr_countryName': '',
				};
				break;
		}
		_GUI._HTML.fill(row);
		// reset submit button
		this.part_reset_changed(part_name);
	}
	/**
	 * Delete a part
	 * @param  {string} part_name Part name
	 * @param  {number} uid       Part UID
	 * @return {bool}           False
	 */
	part_delete (part_name,uid) {
		let tr = _GUI._DOM.id('#' + part_name + '_' + uid);
		tr.parentNode.removeChild(tr);
		delete this.cur_cont[part_name][uid];
		this.part_reset(part_name);
		return false;
	}
	/**
	 * Submit a part
	 * @param  {string} part_name Part name
	 */
	part_submit (part_name) {
		let fields;
		try {			
			// add/update new datas
			if (part_name=='cont') {
				// name|honorificPrefix|givenName|additionalName|familyName|honorificSuffix|nickname|category|org|jobTitle|note|key
				this.record.arrays.forEach(field => {					
					let col = [];
					if ('note|key'.split('|').indexOf(field) != -1) {
						_GUI._DOM.id('#cont_' + field).value.split(/---/).forEach(value => {
							let v = value.trim();
							if (v.length) {
								col.push(v);
							}
						});
					} else {
						_GUI._DOM.id('#cont_' + field).value.split(',').forEach(value => {
							let v = value.trim();
							if (v.length) {
								col.push(v);
							}
						});
					}
					this.cur_cont[field] = col;
				});
				// id|sex|genderIdentity
				this.record.strings.forEach(field => {
					if (field !== 'id') { // ignore update
						this.cur_cont[field] = _GUI._DOM.id('#cont_' + field).value;
					}
				});
				// bday|anniversary|published|updated
				this.record.dates.forEach(field => {
					if (field === 'bday' || field === 'anniversary') {
						let value = _GUI._DOM.id('#cont_' + field).value;
						if (value.length == 10) {
							try {
								/*
								iso: 'YYYY-MM-DD',
								usa: 'MM/DD/YYYY',
								eur: 'DD/MM/YYYY'
								*/
								let y,m,d;
								switch (_Module.const.date_pattern.key) {
									case 'iso':
										y = value.substr(0,4);
										m = value.substr(5,2);
										d = value.substr(8,2);
										break;
									case 'usa':
										m = value.substr(0,2);
										d = value.substr(3,2);
										y = value.substr(6,4);
										break;
									case 'eur':
										d = value.substr(0,2);
										m = value.substr(3,2);
										y = value.substr(6,4);
										break;
								}
								let s = sprintf('%s-%s-%s',y,m,d);
								let test = new Date(s);
								this.cur_cont[field] = s;
							} catch (err) {
								alert('Invalid date for "' + field +'" !');
							}
						}
					}
				});
				// always force name to be givenName + ' ' + familyName - default behaviour following Firefox OS...
				let name = []
				if (this.cur_cont['givenName'].length || this.cur_cont['familyName'].length) {
					name.push(this.cur_cont['givenName']);
					name.push(this.cur_cont['familyName']);
					this.cur_cont['name'] = [name.join(' ')];
				}
			} else {
				let uid = _GUI._DOM.id('#' + part_name + '_uid').value;
				let row, value;
				switch (part_name) {
					case 'email':
					case 'impp':
					case 'url':
						value = _GUI._DOM.id('#' + part_name + '_value').value.trim();
						if (!this.field_check(part_name, value)) {
							return false;
						}
						row = {
							'type': this.field_clean('type', _GUI._DOM.id('#' + part_name + '_type').value),
							'value': value
						};
						break;
					case 'tel':
						value = _GUI._DOM.id('#tel_value').value.trim();
						if (!this.field_check('tel', value)) {
							return false;
						}
						row = {
							'type': this.field_clean('type', _GUI._DOM.id('#tel_type').value),
							'carrier': _GUI._DOM.id('#tel_carrier').value,
							'value': value
						};
						break;
					case 'adr':
						row = {
							'type': this.field_clean('type', _GUI._DOM.id('#adr_type').value),
							'streetAddress': _GUI._DOM.id('#adr_streetAddress').value.trim(),
							'postalCode': _GUI._DOM.id('#adr_postalCode').value.trim(),
							'locality': _GUI._DOM.id('#adr_locality').value.trim(),
							'countryName': _GUI._DOM.id('#adr_countryName').value.trim()
						};
					break;
				}
				if (!this.cur_cont.hasOwnProperty(part_name)) {
					this.cur_cont[part_name] = {};
				}
				if (uid=='') { // add
					uid = this.next_uid(this.cur_cont[part_name]);
					this.cur_cont[part_name][uid] = row;
					this.part_add(part_name, uid, row);
				} else { //update
					this.cur_cont[part_name][uid] = row;
					this.part_update(part_name, uid, row);
				}
				this.part_reset(part_name);				
			}
			// remove change decoration
			this.part_reset_changed(part_name);
		} catch (e) {
			alert(e);
		}
	}
	/**
	 * Clean a field
	 * @param  {string} key   Field key
	 * @param  {string} value Field value
	 * @return {array}       Array of values cleaned
	 */
	field_clean (key, value) {
		if (key=='type') {
			let types = value.split(',');
			let ok = [];
			for (let i=0;i<types.length;i++) {
				let val = types[i].trim();
				if (val.length)
					ok.push(val);
			}
			return ok;
		}
	}
	/**
	 * Check a field
	 * @param  {string} key   Field key
	 * @param  {string} value Field value
	 * @return {bool}       Success
	 */
	field_check (key, value) {
		if (key=='email') {
			if (value.length==0) {
				alert(sprintf(tr1('Field "%s" is empty !'),'Email'));
				return false;
			}
			let re = new RegExp('[a-zA-Z0-9\\.\\-\\_]+@[a-zA-Z0-9\\.\\-\\_]+\\.[a-zA-Z]{2,100}','');
			if (value.match(re)==null) {
				alert(sprintf(tr1('Wrong format for field "%s" !'),'Email'));
				return false;
			}
			return true;
		}
		else if (key=='impp') {
			if (value.length==0) {
				alert(sprintf(tr1('Field "%s" is empty !'),'IMPP'));
				return false;
			}
			let re = new RegExp('[a-zA-Z0-9\\.\\-\\_]+@[a-zA-Z0-9\\.\\-\\_]+\\.[a-zA-Z]{2,100}','');
			if (value.match(re)==null) {
				alert(sprintf(tr1('Wrong format for field "%s" !'),'IMPP'));
				return false;
			}
			return true;
		}
		else if (key=='tel') {
			if (value.length==0) {
				alert(sprintf(tr1('Field "%s" is empty !'),'Tel'));
				return false;
			}
			let re = new RegExp('[\\#\\+\\-\\*\\(\\)0-9]+','');
			if (value.match(re)==null) {
				alert(sprintf(tr1('Wrong format for field "%s" !'),'Tel'));
				return false;
			}
			return true;
		}
		else if (key=='url') {
			if (value.length==0) {
				alert(sprintf(tr1('Field "%s" is empty !'),'URL'));
				return false;
			}
			let re = new RegExp('https?://[a-z0-9\\.\\_\\-]+','i');
			if (value.match(re)==null) {
				alert(sprintf(tr1('Wrong format for field "%s" !'),'URL'));
				return false;
			}
			return true;
		}
	}
	/**
	 * Add a new picture
	 * @param {object} file DOM file
	 */
	add_pict (file) {
		let pict = new Pict({
			type: 'image/jpeg',
			size: {
				min: 1,
				max: 1024*1024 // 1Mo
			},
			width: {
				min: 32,
				max: 320
			},
			height: {
				min: 32,
				max: 320
			},
			resize: true
		},{
			filename: /^[a-z0-9][a-z0-9\-\.\_]{4,255}$/i,
			extension: /.jpe?g$/i
		});
		if (pict.check(file)) {
			pict.read(file).then(result => {
				let new_uid = this.next_uid(_Contact.obj[this.cur_id]['photo']);
				this.cur_cont['photo'][new_uid] = result;
				this.part_add_photo(new_uid,result);
			}).catch(err => {
				alert(err);
			});
		}
	}
	/**
	 * Translate all types
	 * @param  {number} part_id Part ID
	 * @return {array}         Array of types translated
	 */
	get_options (part_id) {
		let [part_name, field_name] = part_id.split('_');
		// search all available uniq types
		let rows = [];
		// always add defaults types
		if (Object.keys(this.record.types).indexOf(part_name)!=-1) {			
			this.record.types[part_name].forEach(type => {
				rows.push(tr2(type));
			});
		}
		// append others records from _Module.tbl.contacts.obj
		_Module.tbl.contacts.select_distinct_values(rows, part_name, field_name);
		rows.sort();
		return rows;		
	}
}