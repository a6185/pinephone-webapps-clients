// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Contact editor tab
 */

_Tab.edit = {
	/**
	 * Initialization
	 */
	init: () => {
		_GUI._DOM.listen('#button_edit_backward', 'click', _Tab.edit.slot.close);
		_GUI._DOM.listen('#button_edit_rec', 'click', _Tab.edit.slot.rec);
		_GUI._DOM.listen('#contact_edit', 'click', _Tab.edit.slot.handler);
		_GUI._DOM.listen('#import_pict', 'change', _Tab.edit.slot.import_pict);
		// auto detect changes
		let parts = 'cont,adr,email,tel,impp,url'.split(',');
		for (let i=0;i<parts.length;i++) {
			let part = parts[i];
			let elements = document.querySelectorAll('.fieldset_' + part + ' input[type="text"], ' + '.fieldset_' + part + ' textarea');
			for (let j=0;j<elements.length;j++) {
				elements[j].setAttribute('data-part', part);
				/*elements[j].addEventListener("change", function(e) {
					_Module.core.editor.part_has_changed(this.getAttribute('data-part'));
					_Module.core.editor.part_has_changed(this);
				});*/
			}
		}
		// drag'n drop image file
		let _Uploader2 = new Uploader();
		_Uploader2.init('uploader2', (file) => {
			_Module.core.editor.add_pict(file);
		});
	},
	/**
	 * Slots (DOM events handlers)
	 */
	slot: {
		/**
		 * Open the tab and update the contact to work on
		 * @param  {event} e DOM event
		 */
		open: (e) => {
			_Tabs.open('tab_edit');
			_Module.core.editor.update(_Module.core.editor.cur_uid);
		},
		/**
		 * Close the editor and returns to the previous tab
		 * @param  {event} e DOM event
		 * @return {bool|function}   False or GUI lock action
		 */
		close: (e) => {
			let changed = [...document.querySelectorAll('button.changed')];
			if (changed.length && !confirm('Some changes have not been saved - forget them and continue ?')) {
				return false;		
			}
			return _GUI._Lock.action(e, (e) => {
				if (_Module.core.editor.cur_uid !== null) {
					_Tab.resume.contact(_Module.core.editor.cur_uid);
					_Tabs.open('tab_resume');
				} else {
					_Module.core.list.refresh();
					_Tabs.open('tab_main');
				}
			});
		},
		/**
		 * Record a contact
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		rec: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Module.core.editor.part_submit('cont');
				_Signals.submit();
			});
		},
		/**
		 * Main handler
		 * @param  {event} e DOM event
		 * @return {bool}   Success
		 */
		handler: (e) => {
			let el = e.target;
			while (el!=document && !el.hasAttribute('data-part') && !el.hasAttribute('data-target')) {
				el=el.parentNode;
			}
			if (el!=document && (el.hasAttribute('data-part') || el.hasAttribute('data-target'))) {
				if (el.hasAttribute('data-part')) {
					let part_name = el.getAttribute('data-part');
					if (el.classList.contains('part_submit')) {
						_Module.core.editor.part_submit(part_name);
						return true;
					}
					if (el.classList.contains('part_reset')) {
						_Module.core.editor.part_reset(part_name);
						return true;
					}	
					if (el.hasAttribute('data-uid')) {
						let uid = el.getAttribute('data-uid');
						if (el.classList.contains('delete')) {
							_Module.core.editor.part_delete(part_name,uid);
							return true;
						}
						if (el.classList.contains('edit')) {
							_Module.core.editor.part_edit(part_name,uid);
							return true;
						}
					}
				}
				if (el.hasAttribute('data-target')) {
					let id = el.getAttribute('data-target');
					let options = [];
					_Module.core.editor.get_options(id).forEach(option => {
						options.push({
							key: option,
							value: option
						});
					});
					let target = '#' + id;
					let target_object = _GUI._DOM.id(target);
					if (el.classList.contains('selector')) {
						_Tab.selector.slot.open('tab_edit',target_object,{
							options: options
						}, null);
						return true;
					}
				}			
			}
			return true;
		},
		/**
		 * Add new pictures
		 * @param  {event} e DOM event
		 */
		import_pict: (e) => {
			if (e.target.files && e.target.files.length) {
				[...e.target.files].forEach(file => {
					_Module.core.editor.add_pict(file);
				});
				e.target.value ='';
			}
		}
	}
}
