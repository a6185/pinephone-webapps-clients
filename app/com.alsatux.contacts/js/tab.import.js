// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Import tab
 */

_Tab.import = {
	/**
	 * Initialization
	 */
	init: () => {
		_GUI._DOM.listen('#button_import_close', 'click', _Tab.import.slot.close);
		_GUI._DOM.listen('#import_file', 'change', _Tab.import.slot.import);
		// add uploader
		let _Uploader1 = new Uploader();
		_Uploader1.init('uploader1', (file) => {
			_Tab.import.read(file);
		});
	},
	/**
	 * Slots (DOM events handlers)
	 */
	slot: {
		/**
		 * Close the tab and returns to main tab
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		close: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tabs.open('tab_main');
			});
		},
		/**
		 * Import contacts from a backup file
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		import: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tab.import.read(e.target.files[0]);
			});
		}		
	},
	/**
	 * Read the file
	 * @param  {object} file DOM File object
	 */
	read: (file) => {
		let reader = new FileReader();
		reader.onload = (e) => {
			try {
				let success = false, reset = false, override = false;
				let db;
				// no checks on import !
				if (file.name.substr(-4)=='json') {
					db = JSON.parse(e.target.result);
					success = true;
				}
				if (success) {
					alert(tr1('File uploaded') + tr1('!'));
					if (Object.keys(_Contact.obj).length) {
						if (confirm(tr1('Remove existing contacts'),tr1('?'))) {
							reset = true;
						} else {
							if (confirm(tr1('Override existing contacts'),tr1('?'))) {
								override = true;
							}
						}
					}
					_Signals.import(db,reset,override);
					_Tabs.open('tab_main');
				}
			} catch (err) {
				alert(err.message);
			}
		}
		reader.onerror = (e) => {
			alert(tr1('Error while uploading file') + tr1('!'));
		}
		reader.readAsText(file, "UTF-8");		
	}
}
