// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Main
 */

window.onload = () => {	
	_Module = new PP_Module('com.alsatux.contacts');
	_Module.init();
	_Module.const = {
		date_pattern: {
			key: 'iso',
			value: 'YYYY-MM-DD'
		}
	};
	_Module.tbl = {
		'contacts': new Contacts_Tbl()
	};
	_Module.core = {
		'editor': new Contact_Editor(),
		'list': new Contact_List()
	}
	_Module.ipc = (message) => {
		if (message.method == 'open') {
			_Tabs.open('tab_resume');
			_Tab.resume.contact(message.args.uid);
		}
	};
	_Module._Manifest.read().then(result => {
		_Module.settings();
		// add event handlers
		_Tab.edit.init();
		_Tab.export.init();
		_Tab.gotoletter.init();
		_Tab.import.init();
		_Tab.log.init();
		_Tab.main.init();
		_Tab.resume.init();
		_Tab.selector.init();
		_Tab.settings.init();
		_Tab.spinner.init();
		_Tabs.init('tab_main');
		_Module.run(_Signals.start);
	}).catch(err => {
		alert(err);
	});
}

/* for testing purpose
function test () {
	var txt = '';
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function () {
	  if(xmlhttp.status == 200 && xmlhttp.readyState == 4){
	    txt = xmlhttp.responseText;
	    _Contact.read(txt);
	  }
	};
	xmlhttp.open("GET","20200827171819.xml",true);
	xmlhttp.send();
}
*/
