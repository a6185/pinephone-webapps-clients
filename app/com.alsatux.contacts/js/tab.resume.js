// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Resume tab
 */

_Tab.resume = {
	/**
	 * Initialization
	 */
	init: () => {
		_GUI._DOM.listen('#button_resume_backward', 'click', _Tab.resume.slot.close);
		_GUI._DOM.listen('#button_contact_edit', 'click', _Tab.edit.slot.open);
		_GUI._DOM.listen('#button_toggle_favorite', 'click', _Tab.resume.slot.favorite);
		_GUI._DOM.listen('#button_delete_contact', 'click', _Tab.resume.slot.remove);
	},
	/**
	 * Slots (DOM events handlers)
	 */
	slot: {
		/**
		 * Close the tab and returns to main tab
		 * @param  {event} e DOM event
		 * @return {function}   False or GUI lock action
		 */
		close: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Module.core.list.refresh();
				_Tabs.open('tab_main');
			});
		},
		/**
		 * Toogle the favorite attribute
		 * @param  {event} e DOM event
		 * @return {function}   False or GUI lock action
		 */
		favorite: (e) => {
			return _GUI._Lock.action(e, (e) => {
				e.target.classList.toggle('selected');
				_Tab.resume.chg_favorite();
				_Signals.submit();
			});
		},
		/**
		 * Delete the current contact
		 * @param  {event} e DOM event
		 * @return {function}   False or GUI lock action
		 */
		remove: (e) => {
			if (confirm(tr1('Delete this contact') + ' ?')) {
				return _GUI._Lock.action(e, (e) => {
					_Signals.remove();
				});
			}
		},
		/**
		 * Extra actions
		 */
		modem: {
			/**
			 * Start a phone call
			 * @param  {event} e DOM event
			 */
			call: (e) => {
				let number = e.target.getAttribute('data-value');			
				_Signals.modem.call(number);
				//window.open(`https://mobian/app/com.alsatux.phone?number=${number}`,'com.alsatux.phone');
			},
			/**
			 * Start a new SMS
			 * @param  {event} e DOM event
			 */
			sms: (e) => {
				let number = e.target.getAttribute('data-value');			
				_Signals.modem.sms(number);
			}
		}
	},
	/**
	 * Read favorite state from button already toogled
	 * and update the button label
	 */
	chg_favorite: () => {
		if (_Module.core.editor.cur_uid === null) {
			return;
		}
		let cur_uid = _Module.core.editor.cur_uid;
		let but = _GUI._DOM.id('#button_toggle_favorite');
		let cont = _Module.tbl.contacts.obj[cur_uid].obj;
		let favorite = but.classList.contains('selected');
		if (favorite) {
			// add to favorite
			if (cont.hasOwnProperty('category')) {
				if (!cont['category'].includes('favorite')) {
					cont['category'].push('favorite');					
				}
			} else {
				cont['category'] = ['favorite'];				
			}
			but.innerHTML = tr1('Remove from favorites');			
		} else {
			// remove from favorite
			if (cont.hasOwnProperty('category')) {
				let cat = [...cont['category']].filter(item => item !== 'favorite');
				if (cat.length==0) {
					delete cont['category'];
				} else {
					cont['category'] = cat;
				}
			}
			but.innerHTML = tr1('Add to favorites');
		}
	},
	/**
	 * Set the favorite button from the current contact datas
	 */
	set_favorite: () => {
		if (_Module.core.editor.cur_uid === null) {
			return;
		}
		let cur_uid = _Module.core.editor.cur_uid;
		let but = _GUI._DOM.id('#button_toggle_favorite');
		let cont = _Module.tbl.contacts.obj[cur_uid].obj;
		if (cont.hasOwnProperty('category') && cont.category.indexOf('favorite')!=-1) {
			but.classList.add('selected');
			but.innerHTML = tr1('Remove from favorites');
		} else {
			but.classList.remove('selected');
			but.innerHTML = tr1('Add to favorites');
		}
	},
	/**
	 * Add hDOM handlers to phone and SMS buttons
	 */
	upd_handlers: () => {
		[...document.querySelectorAll('button.tel')].forEach(tel => {
			tel.addEventListener('click',_Tab.resume.slot.modem.call);
		});
		[...document.querySelectorAll('button.sms')].forEach(tel => {
			tel.addEventListener('click',_Tab.resume.slot.modem.sms);
		});
	},
	/**
	 * Resume a contact
	 * @param  {number} uid Contact UID
	 */
	contact: (uid) => {
		if (uid === null) {
			return;
		}
		// fix current contact uiduence
		_Module.core.editor.cur_uid = uid;
		_Module.core.editor.cur_cont = _Module.tbl.contacts.obj[uid].obj;
		let cont = _Module.core.editor.cur_cont;
		let out = '';
		let title = cont.givenName + ' ' + cont.familyName;
		title = title.cut_to_chars(16);
		_GUI._DOM.id('#contact_title').innerHTML = _GUI._HTML.escape(title);
		_GUI._DOM.id('#contact_resume').innerHTML = '';
		_GUI._DOM.id('#contact_uid').value = uid;

		if (Object.keys(cont.photo).length) {
			out += '<div class="part_resume">';
			for (let uid2 in cont.photo) {
				out += '<div class="part_resume"><p><img class="photo" src="' + cont['photo'][uid2] + '" alt="Photo"/</p></div>';
			}
			out += '</div>';
		}
		if (Object.keys(cont.tel).length) {
			out += '<div class="part_resume">';		
			for (let uid2 in cont.tel) {
				let row = cont.tel[uid2];
				if ((row.type && row.type.length) || (row.carrier && row.carrier.length)) {
					out += '<p>' + ((row.type && row.type.length) ? _GUI._HTML.escape(row.type.join(',')) + ' ' : '') + ((row.carrier && row.carrier.length) ? _GUI._HTML.escape(row.carrier) : '') + ' :</p>';
				}
				if (row.value && row.value.length) {
					out += '<p><button type="button" class="tel" data-value="' + _GUI._HTML.escape(row.value) + '">&phone; ' + _GUI._HTML.escape(row.value) + '</button> <button type="button" class="sms" data-value="' + _GUI._HTML.escape(row.value) + '">SMS</button></p>';	
				}
				/*if (row.carrier && row.carrier.length) {
					out += '<p>' + _GUI._HTML.escape(row.carrier) + '</p>';	
				}*/
			}
			out += '</div>';
		}
		if (Object.keys(cont.email).length) {
			out += '<div class="part_resume">';		
			for (let uid2 in cont.email) {
				let row = cont.email[uid2];
				if (row.type && row.type.length) {
					out += '<p>' + _GUI._HTML.escape(row.type.join(',')) + ':</p>';
				}
				if (row.value && row.value.length) {
					out += '<p><button type="button" class="email" data-value="' + _GUI._HTML.escape(row.value) + '">&#9993; ' + _GUI._HTML.escape(row.value) + '</button></p>';	
				}
			}
			out += '</div>';
		}
		if (Object.keys(cont.adr).length) {
			out += '<div class="part_resume">';		
			for (let uid2 in cont.adr) {
				let row = cont.adr[uid2];
				if (row.type && row.type.length) {
					out += '<p>' + _GUI._HTML.escape(row.type.join(',')) + ':</p>';
				}
				if (row.streetAddress && row.streetAddress.length) {
					out += '<p>' + _GUI._HTML.escape(row.streetAddress) + '</p>';	
				}
				let row2 = [];
				if (row.postalCode && row.postalCode.length) {
					row2.push(_GUI._HTML.escape(row.postalCode));	
				}
				if (row.locality && row.locality.length) {
					row2.push(_GUI._HTML.escape(row.locality));	
				}
				if (row2.length) {
					out += '<p>' + row2.join(' ') + '</p>';	
				}
				if (row.region && row.region.length) {
					out += '<p>' + _GUI._HTML.escape(row.region) + '</p>';	
				}
				if (row.countryName && row.countryName.length) {
					out += '<p>' + _GUI._HTML.escape(row.countryName) + '</p>';	
				}
			}
			out += '</div>';
		}
		_GUI._DOM.id('#contact_resume').innerHTML += '<div class="tel datarow">' + out + '</div>';		
		_Tab.resume.set_favorite();
		_Tab.resume.upd_handlers();
	}
}
