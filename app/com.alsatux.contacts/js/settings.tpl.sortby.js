// Jean Luc Biellmann - contact@alsatux.com

"use strict";

class Settings_Tpl_Sort_By {
	/**
	 * Constructor
	 * @param  {object} _Setting_Object Setting object
	 */
	constructor (_Setting_Object) {
		this._Setting_Object = _Setting_Object;
		this.span = null;
	}
	/**
	 * Before building the HTML controller
	 */
	pre_HTML () {
	}
	/**
	 * Build the HTML controller
	 */
	to_HTML () {
		_Module._Settings.add_div(`
					<h3><span id="label_sort">Sort contacts by</span> :</h3>
					<div>
						<ul id="sort_by">
							<li data-value="name">name</li>
							<li data-value="givenName">givenName</li>
							<li data-value="familyName">familyName</li>
							<li data-value="org">org</li>
							<li data-value="category">category</li>
						</ul>
					</div>	
		`);
		let fields = this._Setting_Object.obj.cur;
		[...document.querySelectorAll('#sort_by li')].forEach(li => {
			let fieldname = li.getAttribute('data-value').replace(/^tr_/,'');
			if (fields[0]==fieldname) {
				li.classList.add('selected');
			} else {
				li.classList.remove('selected');
			}
		});
	}
	/**
	 * After building the HTML controller
	 */
	post_HTML () {
		//_GUI._DOM.listen('#sort_by', 'change', this.slot.sort_by);
		[...document.querySelectorAll('#sort_by li')].forEach(li => {
			li.addEventListener('click', this.slot_sort_by.bind(this));
		});
	}
	/**
	 * Slot handler to select the sort field
	 * @param  {event} e DOM event
	 * @return {function}   GUI lock action
	 */
	slot_sort_by (e) {
		return _GUI._Lock.action(e, (e) => {
			let value =	e.target.getAttribute('data-value');
			[...document.querySelectorAll('#sort_by li')].forEach(li => {
				if (li.getAttribute('data-value')==value) {
					li.classList.add('selected');	
				} else {
					li.classList.remove('selected');	
				}
			});
			this.rec();
		});
	}
	/**
	 * Record settings
	 */
	rec () {
		let fields = [...document.querySelectorAll('#sort_by li.selected')].map(x => x.getAttribute('data-value').replace(/^tr_/,''));
		this._Setting_Object.callback(fields);
	}
}
