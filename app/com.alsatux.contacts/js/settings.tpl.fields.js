// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Template fields setting
 */
class Settings_Tpl_Fields {
	/**
	 * Constructor
	 * @param  {object} _Setting_Object Setting object
	 */
	constructor (_Setting_Object) {
		this._Setting_Object = _Setting_Object;
		this.span = null;
	}
	/**
	 * Before building the HTML controller
	 */
	pre_HTML () {
	}
	/**
	 * Build the HTML controller
	 */
	to_HTML () {
		_Module._Settings.add_div(`
					<h3 id="label_fields">Visible fields</h3>
					<div id="settings_preselect_fields">
						<span class="rounded" data-value="preselect_all">All</span>
						<span class="rounded" data-value="preselect_usual">Most currents</span>
					</div>
					<div id="activated_fields">
						<p id="activated_part_contact">Contact</p>
						<ul>
							<li data-value="tr_cont_name"/>Name</li>
							<li data-value="tr_cont_honorificPrefix"/>Honorific prefix</li>
							<li data-value="tr_cont_givenName" class="usual"/>Given name</li>
							<li data-value="tr_cont_additionalName"/>Additional name</li>
							<li data-value="tr_cont_familyName" class="usual"/>Family name</li>
							<li data-value="tr_cont_honorificSuffix"/>Honorific suffix</li>
							<li data-value="tr_cont_nickname"/>Nickname</li>
							<li data-value="tr_cont_sex"/>Sex</li>
							<li data-value="tr_cont_genderIdentity"/>Gender identity</li>
							<li data-value="tr_cont_category" class="usual"/>Category</li>
							<li data-value="tr_cont_org" class="usual"/>Org</li>
							<li data-value="tr_cont_jobTitle" class="usual"/>Job title</li>
							<li data-value="tr_cont_bday"/>Birthday</li>
							<li data-value="tr_cont_anniversary"/>Anniversary</li>
							<li data-value="tr_cont_note"/>Note</li>
							<li data-value="tr_cont_key"/>Key</li>
						</ul>
						<p id="activated_part_adr">Addresses</p>
						<ul>
							<li data-value="tr_adr_type" class="usual"/>Type</li>
							<li data-value="tr_adr_streetAddress" class="usual"/>Street address</li>
							<li data-value="tr_adr_postalCode" class="usual"/>Postal code</li>
							<li data-value="tr_adr_locality" class="usual"/>Locality</li>
							<li data-value="tr_adr_region"/>Region</li>
							<li data-value="tr_adr_countryName"/>Country</li>
						</ul>
						<p id="activated_part_email">Emails</p>
						<ul>
							<li data-value="tr_email_type" class="usual"/>Type</li>
							<li data-value="tr_email_value" class="usual"/>Value</li>
						</ul>
						<p id="activated_part_phone">Phones</p>						
						<ul>
							<li data-value="tr_tel_value" class="usual"/>Value</li>
							<li data-value="tr_tel_carrier" class="usual"/>Carrier</li>
						</ul>
						<p id="activated_part_url">URLs</p>
						<ul>
							<li data-value="tr_url_type" class="usual"/>Type</li>
							<li data-value="tr_url_value" class="usual"/>Value</li>
						</ul>
						<p id="activated_part_impp">IMPP</p>
						<ul>
							<li data-value="tr_impp_type" class="usual"/>Type</li>
							<li data-value="tr_impp_value" class="usual"/>Value</li>
						</ul>
					</div>
		`);
		let fields = this._Setting_Object.obj.cur;
		[...document.querySelectorAll('#activated_fields li')].forEach(li => {
			//let tr = [...document.getElementsByClassName(li.getAttribute('data-value'))];
			let fieldname = li.getAttribute('data-value').replace(/^tr_/,'');
			if (fields.indexOf(fieldname)!=-1) {
				li.classList.add('selected');
			} else {
				li.classList.remove('selected');
			}
		});
	}
	/**
	 * After building the HTML controller
	 */
	post_HTML () {
		_GUI._DOM.listen('#settings_preselect_fields', 'click', this.slot_click_preselect_fields.bind(this));
		[...document.querySelectorAll('#activated_fields li')].forEach(li => {
			li.addEventListener('click', this.slot_click_toggle_field.bind(this));
		});
	}
	/**
	 * Slot handler for preselect fields
	 * @param  {event} e DOM event
	 * @return {function}   GUI lock action
	 */
	slot_click_preselect_fields (e) {
		return _GUI._Lock.action(e, (e) => {
			let all = [...document.querySelectorAll('#activated_fields li')];
			let o = e.target;
			if (o.nodeName && o.nodeName=='SPAN') {
				let fields = [];
				switch (o.getAttribute('data-value')) {
					case 'preselect_all':
						all.forEach(li => {
							li.classList.add('selected');
							fields.push(li.getAttribute('data-value').replace(/^tr_/,''));
						});
						break;
					case 'preselect_usual':
						all.forEach(li => {
							if (li.classList.contains('usual')) {
								li.classList.add('selected');
								fields.push(li.getAttribute('data-value').replace(/^tr_/,''));
							} else {
								li.classList.remove('selected');
							}
						});
						break;
				}				
				this._Setting_Object.callback(fields);
			}
		});
	}
	/**
	 * Slot handler for toggling fields
	 * @param  {event} e DOM event
	 * @return {function}   GUI lock action
	 */
	slot_click_toggle_field (e) {
		return _GUI._Lock.action(e, (e) => {
			e.target.classList.toggle('selected');
			this.rec();
		});
	}
	/**
	 * Record settings
	 */
	rec () {
		let fields = [...document.querySelectorAll('#activated_fields li.selected')].map(x => x.getAttribute('data-value').replace(/^tr_/,''));
		this._Setting_Object.callback(fields);
	}
}