// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Main tab
 */

_Tab.main = {
	/**
	 * Initialization
	 */
	init: () => {
		_GUI._DOM.listen('#button_settings_open', 'click', _Tab.settings.slot.open);
		_GUI._DOM.listen('#main_add_contact', 'click', _Tab.main.slot.add_new_contact);
		// no onchange() on input_keywords_filter please (loop) ...
		_GUI._DOM.listen('#kws', 'keyup', _Tab.main.slot.filter_kws);
		_GUI._DOM.listen('#clear_kws', 'click', _Tab.main.slot.clear_kws);
		_GUI._DOM.listen('#contacts', 'click', _Tab.main.slot.edit);
		_GUI._DOM.listen('#button_import_open', 'click', _Tab.import.slot.open);
		_GUI._DOM.listen('#button_export_open', 'click', _Tab.export.slot.open);
	},
	/**
	 * Slots (DOM events handlers)
	 */
	slot: {
		/**
		 * Filter keywords
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		filter_kws: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Module.core.list.filter_kws();
			});
		},
		/**
		 * Clear keywords
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		clear_kws: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Module.core.list.filter_clear();
			});
		},
		/**
		 * Edit the contact
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		edit: (e) => {
			return _GUI._Lock.action(e, (e) => {
				let o = e.target.closest('.contact');
				if (o!=null) {
					_Tabs.open('tab_resume');
					_Tab.resume.contact(o.id.replace('cont_uid_',''));
				}
			});
		},
		/**
		 * Add a new contact (button +)
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		add_new_contact: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tabs.open('tab_edit');
				_Module.core.editor.add_new();
			});
		}
	}
};

