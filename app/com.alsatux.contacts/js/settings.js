// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Module settings
 */

class Settings extends PP_Settings {
	/**
	 * Constructor
	 */
	constructor () {
		super();
		this.add_setting(this.setting_fields_visible);
		this.add_setting(this.setting_fields_sortby);
	}
	/**
	 * Add a setting to choose fields to display
	 */
	setting_fields_visible () {
		/*let values = {};
		[...document.querySelectorAll('#activated_fields li')].forEach(li => {
			let key = li.getAttribute('data-value').replace(/^tr_/,'');
			let val = li.innerHTML;
			values[key] = val;
		});*/
		let values = {
	    "cont_name": "Name",
	    "cont_honorificPrefix": "Honorific prefix",
	    "cont_givenName": "Given name",
	    "cont_additionalName": "Additional name",
	    "cont_familyName": "Family name",
	    "cont_honorificSuffix": "Honorific suffix",
	    "cont_nickname": "Nickname",
	    "cont_sex": "Sex",
	    "cont_genderIdentity": "Gender identity",
	    "cont_category": "Category",
	    "cont_org": "Org",
	    "cont_jobTitle": "Job title",
	    "cont_bday": "Birthday",
	    "cont_anniversary": "Anniversary",
	    "cont_note": "Note",
	    "cont_key": "Key",
	    "adr_type": "Type",
	    "adr_streetAddress": "Street address",
	    "adr_postalCode": "Postal code",
	    "adr_locality": "Locality",
	    "adr_region": "Region",
	    "adr_countryName": "Country",
	    "email_type": "Type",
	    "email_value": "Value",
	    "tel_value": "Value",
	    "tel_carrier": "Carrier",
	    "url_type": "Type",
	    "url_value": "Value",
	    "impp_type": "Type",
	    "impp_value": "Value"
		};
		//let usuals = [...document.querySelectorAll('#activated_fields li.usual')].map(x => x.getAttribute('data-value').replace(/^tr_/,''));
		let usuals = ['cont_givenName', 'cont_familyName', 'cont_category', 'cont_org', 'cont_jobTitle', 'adr_type', 'adr_streetAddress', 'adr_postalCode', 'adr_locality', 'email_type', 'email_value', 'tel_value', 'tel_carrier', 'url_type', 'url_value', 'impp_type', 'impp_value'];
		let _Setting_Object = this.tbl.settings.add({
			cat: 'fields',
			key: 'visible',
			type: 'select',
			nb_options: '1,' + Object.keys(values).length, // min,max
			val: values,
			cur: usuals,
			def: usuals
		});
		_Setting_Object.tpl = new Settings_Tpl_Fields(_Setting_Object);
		_Setting_Object.upd_cur = cur => {
			let data = [];
			let fields = Object.keys(_Setting_Object.obj.val);
			cur.forEach(fieldname => {
				if (fields.indexOf(fieldname)!=-1) {
					data.push(fieldname);
				}
			});
			_Setting_Object.obj.cur = data;
		}
		_Setting_Object.callback = cur => { // from HTML event
			_Setting_Object.upd_cur(cur);
			_Module._Settings.have_changed();
		};
	}
	/**
	 * Add a setting to choose the sort field
	 */
	setting_fields_sortby () {
		let values ={
			"name": "Name",
			"givenName": "Given name",
			"familyName": "Family name",
			"org": "Org",
			"category": "Category"
		};
		/*let values = {};
			[...document.querySelectorAll('#sort_by li')].forEach(li => {
			let key = li.getAttribute('data-value');
			let val = li.innerHTML;
			values[key] = val;
		});*/
		let usuals = ['familyName'];
		let _Setting_Object = this.tbl.settings.add({
			cat: 'fields',
			key: 'sortby',
			type: 'select',
			nb_options: '1,1', // min,max
			val: values,
			cur: usuals,
			def: usuals
		});
		_Setting_Object.tpl = new Settings_Tpl_Sort_By(_Setting_Object);
		_Setting_Object.upd_cur = cur => {
			let data = [];
			let fields = Object.keys(_Setting_Object.obj.val);
			cur.forEach(fieldname => {
				if (fields.indexOf(fieldname)!=-1) {
					data.push(fieldname);
				}
			});
			_Setting_Object.obj.cur = data;
		}
		_Setting_Object.callback = cur => { // from HTML event
			_Setting_Object.upd_cur(cur);
			_Module._Settings.have_changed();
		};
	}
}

