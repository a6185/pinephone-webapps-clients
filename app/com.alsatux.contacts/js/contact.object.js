// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Contact object
 */

class Contact_Object extends PP_Object {
	/**
	 * Constructor
	 */
	constructor () {
		super('contact_object',{
			uid: '',
			email: {},
			tel: {},
			adr: {},
			url: {},
			impp: {},
			photo: {}
		});
	}
}