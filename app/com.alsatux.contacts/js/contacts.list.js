// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Contact list
 */

class Contact_List {
	/**
	 * Constructor
	 */
	constructor () {
		this.settings = {
			sort_by: 'familyName' // default
		};
	}
	/**
	 * Refresh contacts list
	 */
	refresh () {
		let doc = _GUI._DOM.id('#contacts');
		doc.innerHTML = '';		
		for (let uid in _Module.tbl.contacts.obj) {
			let div = document.createElement('div');
			div.id = 'cont_uid_' + uid;
			this.add(div, _Module.tbl.contacts.obj[uid].obj);
			doc.appendChild(div);
		}
		_GUI._DOM.id('#counter').innerHTML = _Module.tbl.contacts.length();
		// sort the list according to current settings
		this.sortBy(this.settings.sort_by);
		// don't forget the filter...
		this.filter_kws();
	}
	/**
	 * Add a contact to the list
	 * @param {object} div DOM div object
	 * @param {object} obj Contact object (JS object)
	 */
	add (div, obj) {
		// name|honorificPrefix|givenName|additionalName|familyName|honorificSuffix|nickname|category|org|jobTitle
		let fields = 'givenName|familyName|org'.split('|');
		let lines = [], html = [];
		// first line
		if (obj.hasOwnProperty('givenName')) {
			if (obj.givenName.length) {
				lines.push('<span class="givenName">' + _GUI._HTML.escape(obj.givenName.join(' ')) + '</span>');
			}
		}
		if (obj.hasOwnProperty('familyName')) {
			if (obj.familyName.length) {
				lines.push('<span class="familyName">' + _GUI._HTML.escape(obj.familyName.join(' ')) + '</span>');
			}
		}
		if (lines.length) {
			html.push('<p>' + lines.join(' ') + '</p>');
		} else {
			html.push('<p><span class="johndoe">?</span></p>');
		}
		// second line
		lines = [];
		if (obj.hasOwnProperty('category')) {
			let cats = [...obj.category].filter(item => item !== "favorite");
			if (cats.length) {
				lines.push('[' + cats.join(',') + '] ');
			}
		}
		if (obj.hasOwnProperty('org')) {
			if (obj.org.length) {
				lines.push(obj.org.join(' '));
			}
		}
		if (lines.length) {
			html.push('<p><span class="secondary">' + _GUI._HTML.escape(lines.join(' ')) + '</span></p>');
		}

		div.classList.add('contact');
		div.classList.add(html.length==2 ? 'dual' : 'single')
		div.innerHTML += html.join("\n");
	}

	/* SORT */

	/**
	 * Sort the list
	 * @param  {string} fieldname Contact field to use
	 */
	sortBy (fieldname) {
		let ul1 = {}, ul2={}, index = [];
		let contacts = _GUI._DOM.id('#contacts');
		this.settings.sort_by = fieldname;
		for (let uid in _Module.tbl.contacts.obj) {
			let cont = _Module.tbl.contacts.obj[uid].obj;
			// to clean letter/index further
			contacts.appendChild(_GUI._DOM.id('#cont_uid_' + uid));
			// sorted fields are all arrays
			let v = cont[fieldname] ? cont[fieldname].join(' ').toUpperCase() : '';
			let c = v.length ? v.substr(0,1).toUpperCase() : '?';
			if (cont.hasOwnProperty('category') && cont.category.includes('favorite')) {
				if (!Object.keys(ul1).includes(c)) {
					ul1[c] = {};
				}
				if (ul1[c].hasOwnProperty(v)) {
					ul1[c][v].push(uid);
				} else {
					ul1[c][v] = [uid];
				}
			} else {
				if (!Object.keys(ul2).includes(c)) {
					ul2[c] = {};
				}
				if (ul2[c].hasOwnProperty(v)) {
					ul2[c][v].push(uid);
				} else {
					ul2[c][v] = [uid];
				}
			}
		}
		[...document.querySelectorAll('#contacts .letter')].forEach(el => el.remove());
		// favorites
		if (Object.keys(ul1).length) {
			let fav_div = document.createElement('div');
			contacts.appendChild(fav_div);
			let h1 = document.createElement('h1');
			h1.setAttribute('id','favorites');
			h1.classList.add('index');
			h1.innerHTML = _GUI._HTML.escape(tr1('Favorites'));
			fav_div.appendChild(h1);
			let fchars = Object.keys(ul1).sort();
			while (fchars.length) {
				let c = fchars.shift();
				let lis = Object.keys(ul1[c]).sort();
				while (lis.length) {
					let key = lis.shift();
					let uids = ul1[c][key];
					for (let j=0;j<uids.length;j++) {
						let uid = uids[j];
						fav_div.appendChild(_GUI._DOM.id('#cont_uid_' + uid));
					}
				}
			}
		}
		// others
		let chars = Object.keys(ul2).sort();
		let c1 = 0;
		let c2 = 0;
		while (chars.length) {
			let c = chars.shift();
			let div = document.createElement('div');
			div.setAttribute('id','letter_' + c1++);
			div.classList.add('letter');
			let h1 = document.createElement('h1');
			h1.setAttribute('id','char_' + c2++);
			h1.classList.add('index');
			h1.innerHTML = _GUI._HTML.escape(c);
			div.appendChild(h1);
			let lis = Object.keys(ul2[c]).sort();
			while (lis.length) {
				let key = lis.shift();
				let uids = ul2[c][key];
				for (let j=0;j<uids.length;j++) {
					let uid = uids[j];
					div.appendChild(_GUI._DOM.id('#cont_uid_' + uid));
				}
			}
			contacts.appendChild(div);
		}
		_Tab.gotoletter.update(Object.keys(ul2).sort());		
	}

	/* FILTER LIST */

	/**
	 * Search records with needle
	 * @param  {object} hash   JS object
	 * @param  {string} needle Key to find
	 * @return {bool}        Success
	 */
	filter_kw (hash, needle) {
		if (hash==null) { // null date value for example
			return false;
		}
		switch (hash.constructor) {
			case String: 
				if (hash.length<100 && hash.search(needle)!=-1) {
					return true;
				}
				break;
			case Date: 
				let d = hash.to_yyyymmdd();
				if (d.search(needle)!=-1) {
					return true;
				}
				break;
			case Array:
				for (let i=0;i<hash.length;i++) {
					if (this.filter_kw(hash[i],needle)) {
						return true;
					}
				}
				break;
			case Object:
				for (let key in hash) {
					if (hash.hasOwnProperty(key)) {
						//console.log(key + ':' + hash[key] + ' - ' + hash[key].constructor);
						if (this.filter_kw(hash[key],needle)) {
							return true;
						}
					}
				}
				break;
		}
		return false;
	}
	/**
	 * Clean keywords from the search HTML field
	 * @param  {string} keywords Keywords
	 * @return {array}          Keywords filtered
	 */
	filter_clean_keywords (keywords) {
		let re = /\s+/;
		let kws = keywords.split(re);
		let kws2 = [];
		for (let i=0;i<kws.length;i++) {
			let kw = kws[i].trim();
			if (kw.length) {
				kws2.push(kw);
			}
		}
		return kws2;
	}
	/**
	 * Filter the contact list searching for keywords
	 */
	filter_kws () {
		let kws = _GUI._DOM.id('#kws').value;
		if (!kws.length) {
			return this.filter_clear();
		}
		kws = this.filter_clean_keywords(kws);
		if (!kws.length) {
			return this.filter_clear();
		}
		let counter = 0;
		for (let uid in _Module.tbl.contacts.obj) {
			let contact = _Module.tbl.contacts.obj[uid].obj;
			let found = false;
			kws.forEach(kw => {
				if (!found) {
					let pattern = '.*' + kw + '.*';
					//console.log(pattern);
					let re = new RegExp(pattern,'i');
					if (this.filter_kw(contact,re)) {
						found = true;
					}
				}
			});
			if (found) {
				_GUI._DOM.show('#cont_uid_' + uid);
				counter++;
			} else {
				_GUI._DOM.hide('#cont_uid_' + uid);
			}
		}
		_GUI._DOM.id('#counter').innerHTML = counter + '/' + _Module.tbl.contacts.length();		
		// show/hide headers h1 (if has childs only...)
		[...document.querySelectorAll('h1.index')].forEach(h1 => {
			// take with the space !
			let not_empty = [...h1.parentNode.querySelectorAll('div:not([style*="display: none"])')].length;
			if (not_empty) {
				_GUI._DOM.show('#' + h1.id);
			} else {
				_GUI._DOM.hide('#' + h1.id);
			}
		});
	}
	/**
	 * Reset the HTML search field
	 */
	filter_clear () {
		_GUI._DOM.id('#kws').value = '';
		[...document.querySelectorAll('h1.index')].forEach(h1 => {
			_GUI._DOM.show('#' + h1.id);
		});			
		for (let uid in _Module.tbl.contacts.obj) {
			_GUI._DOM.show('#cont_uid_' + uid);
		}
		_GUI._DOM.id('#counter').innerHTML = _Module.tbl.contacts.length();
	}
}