// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Calls table
 */

class Calls_Tbl extends PP_Tbl {
	/**
	 * Constructor
	 */
	constructor () {
		super();	
		/* css */
		this.bg = {};
	}
	/**
	 * Open main tab and clear current call
	 */
	reset () {
		_Tabs.open('tab_main');
		_GUI._DOM.id('#calls_active').innerHTML='';
		_GUI._DOM.id('#calls_history').innerHTML='';		
	}
	/**
	 * Add an incoming call fixing random background color
	 * @param {object} call Call object (JS object)
	 */
	add (call) {
		let _Call = new Call_Object();
		_Call.upd(call);
		this.set(call.uid,_Call);
		_Module.tbl.clock.set(call.uid,0);
		_Module.tbl.timer.set(call.uid,null);
		let num = call.number.replace(/[^0-9]/,'');
		if (!this.bg.hasOwnProperty(num)) {
			/* css */
			let n = Math.floor(1 + Math.random()*20);
			if (n<10) {
				n = '0' + n;
			}
			this.bg[num] = 'bg_' + n;
		}
		return _Call;
	}
	/**
	 * Remove a call
	 * @param  {object} call Call object (JS object)
	 */
	del (call) {
		this.unset(call.uid);
	}
	having (key,value) {
		let selection = [];
		for (let uid in this.obj.calls) {
			let _Call = this.obj.calls[uid];
			if (_Call.obj[key]==value) {
				selection.push(_Call);
			}
		}
		return selection;
	}
}

