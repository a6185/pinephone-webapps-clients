// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Module signals
 */

var _Signals = {
	/**
	 * Load calls from server
	 */
	start: () => {
		_Signals.get_sound();
		_Signals.list();
	},
	/**
	 * Get list of calls
	 */
	list: () => {
		_GUI._Log.info(tr1('Searching for the last backup file') + '... ');
		_WS.send({
			route: 'com.alsatux.phone:/calls/list',
			args: {},
			callback: 'com.alsatux.phone:/calls/list'
		});
	},
	/**
	 * Dial a number on the server side
	 * @param  {string} number Number to dial
	 */
	dial: (number) => {
		_WS.send({
			route: 'com.alsatux.phone:/calls/add',
			args: {
				number: number
			}
		});
	},
	/**
	 * Accept an incoming call
	 * @param  {object} call Call object (JS object)
	 */
	accept: (call) => {
		if (call.direction==1) { // 1 = incoming, 2 = outgoing
			_WS.send({
				route: 'com.alsatux.phone:/call/accept',
				args: {
					dbus_path: call.dbus_path
				}
			});
		}
	},
	/**
	 * Deflect an incoming call
	 * @param  {object} call Call object (JS object)
	 * @param  {string} number Call number to deflect
	 */
	deflect: (call, number) => {
		_WS.send({
			route: 'com.alsatux.phone:/call/deflect',
			args: {
				dbus_path: call.dbus_path,
				number: number
			}
		});		
	},
	/**
	 * Join an existing call
	 * @param  {object} call Call object
	 */
	join: (call) => {
		if (call.state_code=='5' && call.multiparty==0) { // HELD
			_WS.send({
				route: 'com.alsatux.phone:/call/multiparty/join',
				args: {
					dbus_path: call.dbus_path
				}
			});
		}
	},
	/**
	 * Leave the current call
	 * @param  {object} call Call object
	 */
	leave: (call) => {
		if ('45'.indexOf(call.state_code)!=-1 && call.multiparty==1) { // ACTIVE OR HELD
			_WS.send({
				route: 'com.alsatux.phone:/call/multiparty/leave',
				args: {
					dbus_path: call.dbus_path
				}
			});
		}
	},
	/**
	 * Hangup the current call
	 * @param  {object} call Call object
	 */
	hangup: (call) => {
		if ('0123456'.indexOf(call.state_code)!=-1) {
			_WS.send({
				route: 'com.alsatux.phone:/call/hangup',
				args: {
					dbus_path: call.dbus_path
				}
			});
		}
	},
	/**
	 * Play a DTMF symbol
	 * @param  {object} call   Call object
	 * @param  {string} symbol DTMF symbol to play
	 */
	play_dtmf: (call, symbol) => {
		_WS.send({
			route: 'com.alsatux.phone:/call/dtmf/outgoing',
			args: {
				dbus_path: call.dbus_path,
				symbol: symbol
			}
		});
	},
	/**
	 * Get callaudio properties
	 */
	get_sound: () => {
		_GUI._Log.info(tr1('Getting sound properties') + '... ');
		_WS.send({
			route: 'pp.core.sound:/callaudio/get',
			args: {
			},
			callback: 'pp.core.sound:/callaudio/refresh'
		});
	},
	/**
	 * Set callaudio properties
	 * @param  {string} target mute/speaker
	 * @param  {number} value  0/1
	 */
	set_sound: (target, value) => {
		_WS.send({
			route: 'pp.core.sound:/callaudio/set/' + target,
			args: {
				value: value
			}
		});
	},
	/**
	 * Search for a contact associated with a number
	 * @param  {string} number Call number
	 */
	search_for_contact: (number) => {
		_WS.send({
			route: 'com.alsatux.contacts:/search/bypart',
			args: {
				part: 'tel',
				key: 'value',
				value: number
			},
			callback: 'com.alsatux.phone:/calls/contact/found'		
		});
	}
};