// Jean Luc Biellmann - contact@alsatux.com

"use strict";

class Echo {
	/**
	 * Constructor
	 */
	constructor () {
		this.counter = 0;
	}
	/**
	 * Sleep
	 * @param  {number} ms Milliseconds to sleep
	 * @return {function}    Promise / resolve
	 */
	sleep (ms) {
		return new Promise(resolve => setTimeout(resolve, ms));
	}
	/**
	 * Dial a number
	 * @param  {string} number Number to dial
	 */
	async dial (number) {
		if (number.length) {
			//this.play('sounds/tonalite.wav');
			let symbol = number.substr(0,1)
			number = number.substr(1);
			this.play_dtmf(symbol);
			await this.sleep(200);
			this.dial(number);
		}
	}
	/**
	 * Play a sound file
	 * @param  {string} src Wav filename to play
	 */
	play (src) {
		(new Audio(src)).play();
	}
	/**
	 * Play a DTMF code
	 * @param  {string} symbol DTMF string
	 */
	play_dtmf (symbol) {
		if (/^[0-9\*\#]$/.test(symbol)) {
			let src = symbol;
			if (symbol=='#') {
				src = 'hash';
			}
			if (symbol=='*') {
				src = 'star';
			}
			this.play('sounds/dtmf/' + src + '.wav');
		}
	}
	/**
	 * Stop sound player
	 */
	stop () {
		_GUI._DOM.id('#player').stop();
	}
	/**
	 * Recall a number
	 * @param  {string} number Phone number to recall
	 */
	async recall (number) {
		this.play('sounds/tonalite.wav');
		await this.dial(number);
		_Signals.dial(number);
	}
	/**
	 * Create a new DOM Audio
	 * @param  {string} src Wav filename
	 * @return {object}     DOM Audio object
	 */
	load (src) {
		return new Audio(src);
	}
	/**
	 * Show a contact
	 * @param  {object} cont Contact object (JS object)
	 */
	show_contact (cont) {
		var row, row2, seq2, out = '';
		if (Object.keys(cont.photo).length) {
			out += '<div class="part_resume">';
			for (seq2 in cont.photo) {
				out += '<div class="part_resume"><p><img class="photo" src="' + cont['photo'][seq2] + '" alt="Photo"/</p></div>';
			}
			out += '</div>';
		}
		if (Object.keys(cont.email).length) {
			out += '<div class="part_resume">';
			for (seq2 in cont.email) {
				row = cont.email[seq2];
				if (row.type && row.type.length) {
					out += '<p>' + _GUI._HTML.escape(row.type.join(',')) + ':</p>';
				}
				if (row.value && row.value.length) {
					out += '<p><button type="button" class="email" data-value="' + _GUI._HTML.escape(row.value) + '">&#9993; ' + _GUI._HTML.escape(row.value) + '</button></p>';
				}
			}
			out += '</div>';
		}
		_GUI._DOM.id('#calls_details').innerHTML += out;
	}
}
