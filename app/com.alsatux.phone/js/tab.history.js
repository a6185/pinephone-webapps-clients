// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * History tab
 */

_Tab.history = {
	/**
	 * Initialization
	 */
	init: () => {
		_GUI._DOM.listen('#button_history_close', 'click', _Tab.history.slot.hide);
		_GUI._DOM.listen('#button_history_close', 'click', _Tab.history.slot.hide);
	},
	/**
	 * Slots (DOM events handlers)
	 */
	slot: {
		/**
		 * Open the tab
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		show: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tabs.open('tab_history');
			});
		},
		/**
		 * Close the tab and returns to main tab
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		hide: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tabs.open('tab_main');
			});
		},
		/**
		 * Open a new navigator tab calling for contacts modules
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		edit: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_GUI._Window.open('com.alsatux.contacts');
				_Module._IPC.trigger('com.alsatux.contacts', {
					method: 'open',
					args: {
						uid: e.target.closest('button').getAttribute('data-uid')
					}
				});
			});			
		},
		/**
		 * Redial an ancient call
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		recall: (e) => {
			return _GUI._Lock.action(e, (e) => {
				let div = e.target.closest('div.call');
				if (div !== null) {
					let uid = div.getAttribute('data-uid');
					_Tab.history.recall(uid);
				}	   	
			});						
		}
	},
	/**
	 * Select a HTML call
	 * @param  {object} call Call object
	 * @return {object}       DOM div associated to the call
	 */
	select: (call) => {
		return document.querySelector(`#tab_history div[data-uid="${call.uid}"]`);
	},
	/**
	 * Add a call to history
	 * @param  {object} _Call Call object
	 */
	add: (_Call) => {
		let div = _Call.to_html('tab_history');
		let call = _Call.obj;
		_GUI._DOM.id('#calls_history').insertBefore(div, _GUI._DOM.id('#calls_history').firstChild);
		let div2 = document.querySelector(`div.call[data-uid="${call.uid}"]`);
		let button = div2.querySelector(`button[data-action="edit"]`);	
		if (button!=null) {
			button.addEventListener('click', _Tab.history.slot.edit);
		}	
		button = div2.querySelector(`button[data-action="recall"]`);	
		if (button!=null) {
			button.addEventListener('click', _Tab.history.slot.recall);
		}
	},
	/**
	 * Sort calls by date
	 */
	sort: () => {
		// reorder calls ascending
		let divs = [...document.querySelectorAll(`#tab_history div.call`)];
		let divs2 = divs.sort((a, b) => {
			let date1 = a.querySelector('.call_actions').getAttribute('data-date');
			let date2 = b.querySelector('.call_actions').getAttribute('data-date');
			return (new Date(date1))-(new Date(date2));
		});
		divs2.forEach(div => {
			_GUI._DOM.id('#calls_history').insertBefore(div, _GUI._DOM.id('#calls_history').firstChild);
		});
	},
	/**
	 * Redial an ancient number
	 * @param  {string} uid Call UID
	 */
	recall: (uid) => {
		let _Call = _Module.tbl.calls.obj.calls[uid];
		let call = _Call.obj;
		_GUI._DOM.id('#tab_history').style.display = 'none';
		_GUI._DOM.id('#number').value = call.number;
		_Signals.dial(call.number);	 
	}
};
