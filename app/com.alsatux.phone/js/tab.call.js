// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Call tab
 */

_Tab.call = {
	counter: 0,
	timerId: null,
	playing: null,
	/**
	 * Initialization
	 */
    init: () => {
		_GUI._DOM.listen('#button_call_close', 'click', _Tab.call.slot.hide);
		_GUI._DOM.listen('#button_call_history', 'click', _Tab.history.slot.show);
		_GUI._DOM.listen('#button_keypad-mute', 'click', _Tab.call.slot.sound);
		_GUI._DOM.listen('#button_keypad-speaker', 'click', _Tab.call.slot.sound);
    },
	/**
	 * Slots (DOM events handlers)
	 */
    slot: {
		/**
		 * Open the tab
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
        show: (e) => {
            return _GUI._Lock.action(e, (e) => {
            	_Tabs.open('tab_call');
            });
        },
		/**
		 * Close the tab and returns to main tab
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
        hide: (e) => {
            return _GUI._Lock.action(e, (e) => {
            	_Tabs.open('tab_main');
            });
        },
        /**
         * Handle buttons actions
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
         */
		action: (e) => {
            return _GUI._Lock.action(e, (e) => {
				let button = e.target.closest('button');
				if (button !== null) {
					let action = button.getAttribute('data-action');
					let div = button.closest('div.call');
					let uid = div.getAttribute('data-uid');
					_Tab.call.action(action, uid, div);
				}
			});
		},
		/**
		 * Sound controllers
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		sound: (e) => {
            return _GUI._Lock.action(e, (e) => {
				var button = e.target.closest('button');
				if (button !== null) {
					button.classList.toggle('selected');
					let target = button.id.replace('button_keypad-','');
					let value = button.classList.contains('selected') ? '1' : '0'
					_Signals.set_sound(target, value);
				}
			});
		}
	},
	/**
	 * Create a new call
	 * @param  {object} _Call Call object
	 */
	add: (_Call) => {
		let div = _Call.to_html('tab_call');
		_GUI._DOM.id('#calls_active').insertBefore(div, _GUI._DOM.id('#calls_active').firstChild);
		_Tab.call.set_state(_Call);
	},
	/**
	 * Stop a call
	 * @param  {object} _Call Call object
	 */
	del: (_Call) => {
		let div = _Tab.call.select(_Call);
		if (div!=null) {
			div.remove();
		}
	},
	/**
	 * Select a HTML call
	 * @param  {object} _Call Call object
	 * @return {object}       DOM div associated to the call
	 */
	select: (_Call) => {
		return document.querySelector(`#tab_call div[data-uid="${_Call.obj.uid}"]`);
	},
	/**
	 * Add a DTMF symbol into HTML dedicated div
	 * @param  {string} symbol DTMF code to add
	 */
	show_dtmf: (symbol) => {
		_GUI._DOM.id('#calls_dtmf').innerHTML += symbol;
	},
	/**
	 * Set micro state
	 * @param {number} uint 0 = off, 1 = on, 255 = unknown
	 */
	set_mic_state (uint) {
		// all other values should be considered the same as 'unknown'
		let button = _GUI._DOM.id('#button_keypad-mute');
		if (uint==0) {
			button.disabled = false;
			button.classList.add('selected');
			return;
		}
		if (uint==1) {
			button.disabled = false;
			button.classList.remove('selected');
			return;
		}
		button.disabled = true;
	},
	/**
	 * Set speaker state
	 * @param {number} uint 0 = off, 1 = on, 255 = unknown
	 */
	set_speaker_state (uint) {
		// all other values should be considered the same as 'unknown'
		let button = _GUI._DOM.id('#button_keypad-speaker');
		if (uint==0) {
			button.disabled = false;
			button.classList.remove('selected');
			return;
		}
		if (uint==1) {
			button.disabled = false;
			button.classList.add('selected');
			return;
		}
		button.disabled = true;
	},
	/**
	 * Stop playing a sound
	 */
	stop_playing: () => {
		if (_Tab.call.playing !== null) {
			_Tab.call.playing.pause();
			_Tab.call.playing = null;
		}		
	},
	/**
	 * Set a call state
	 * @param  {object} _Call Call object
	 */
	set_state: (_Call) => {
		let div = _Tab.call.select(_Call);
		let call = _Call.obj;
		if (div !== null) {
			div.querySelector('.call_state').innerHTML = call.state;
			_Tab.call.stop_playing();
			if (call.state_code == 0) { // STATE UNKNOWN
				_Call.fix_call_buttons(div, 0, 0, 0, 0, 0, 0, 1, 0); // hangup
			}
			if (call.direction == 1) { // incoming
				if (call.state_code == 3) { // RINGING IN
					_Tab.call.playing = _Module.core.echo.load('sounds/incoming.wav');
					_Tab.call.playing.play();
					_Call.fix_call_buttons(div, 1, 1, 0, 0, 0, 0, 1, 0); // accept + deflect + hangup
				}
				if (call.state_code == 6) { // WAITING
					_Tab.call.playing = _Module.core.echo.load('sounds/dtmf/1.wav');
					_Tab.call.playing.play();
					_Call.fix_call_buttons(div, 0, 1, 1, 1, 0, 0, 1, 0); // deflect + hold and accept + hangup and accept + hangup
				}
			} else { // outgoing
				// DIALING or RINGING_OUT
				if ('12'.indexOf(call.state_code)!=-1) {
					_Call.fix_call_buttons(div, 0, 0, 0, 0, 0, 0, 1, 0); // hangup
				}
			}
			if (call.state_code == 4) { // ACTIVE				
				if (call.multiparty == 1) {
					_Call.fix_call_buttons(div, 0, 0, 0, 0, 0, 1, 1, 0); // leave multiparty + hangup
				} else {
					_Call.fix_call_buttons(div, 0, 0, 0, 0, 0, 0, 1, 0); // hangup
				}
				_Tab.call.add_timer(_Call, div);
			}
			if (call.state_code == 5) { // HELD
				if (call.multiparty == 1) {
					_Call.fix_call_buttons(div, 0, 0, 0, 0, 0, 1, 1, 0); // leave multiparty + hangup
				} else {
					_Call.fix_call_buttons(div, 0, 0, 0, 0, 1, 0, 1, 0); // join multiparty + hangup
				}
			}
			if (call.state_code == 7) { // TERMINATED -> send to TabHistory
				_Tab.call.remove_timer(call);
				_Tab.call.del(_Call);
				_Tab.history.add(_Call);
				_GUI._DOM.id('#calls_details').innerHTML = '';
			}
		}
	},
	/**
	 * Add a HTML timer to the current call
	 * @param  {object} _Call Call object
	 * @param  {object} div   DOM div
	 */
	add_timer: (_Call, div) => {
		let call = _Call.obj;
		if (_Module.tbl.timer.get(call.uid) === null) {
			_Module.tbl.clock.set(call.uid,0);
			_Module.tbl.timer.set(call.uid,setInterval(() => {
				let hours, min, sec, remain;
				let timer = _Module.tbl.clock.get(call.uid)+1;
				_Module.tbl.clock.set(call.uid,timer);
				hours = Math.floor(timer/3600);
				if (hours>99) {
					hours = 99;
					min = 59;
					sec = 59;
				} else {
					remain = timer - hours*3600;
					min = Math.floor(remain/60);
					sec = Math.floor(remain - min*60);
				}
				let txt = (hours<10 ? '0' : '') + hours + ':' + (min<10 ? '0' : '') + min + ':' + (sec<10 ? '0' : '') + sec;
				div.querySelector('.call_clock').innerHTML = txt;
			},1000));
		}
	},
	/**
	 * Remove the HTML timer
	 * @param  {object} call Call object
	 */
	remove_timer: (call) => {
		_Module.tbl.clock.set(call.uid,0);
		if (_Module.tbl.timer.get(call.uid) !== null) {
			clearInterval(_Module.tbl.timer.get([call.uid]));
			_Module.tbl.timer.set(call.uid,null);
		}
	},
	/**
	 * Control user action
	 * @param  {string} action Accept/deflect/deflect_confirm/join/leave/hangup
	 * @param  {number} uid    Call UID
	 * @param  {object} div    Call HTML div
	 */
	action: (action, uid, div) => {
		let _Call = _Module.tbl.calls.get(uid);
		if (_Call!=null) {
			let call = _Call.obj;
			switch (action) {
				case 'accept':
					_Signals.accept(call);
					break;
				case 'deflect':
					_GUI._DOM.hide('div.row3');
					_GUI._DOM.show('div.row4');
					break;
				case 'deflect_confirm':
					let number = div.querySelector('div.deflect input').value;
					if (number.length) {
						_Signals.deflect(call, number);
					}
				case 'deflect_cancel':
					_GUI._DOM.hide('div.row4');
					_GUI._DOM.show('div.row3');
					break;
				case 'join':
					_Signals.join(call);
					break;
				case 'leave':
					_Signals.leave(call);
					break;
				case 'hangup':
					_Signals.hangup(call);
					break;
			}
		}
	}
};
