// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Dictionnary use in js code, according to manifest.json
 */

function tr1 (txt) {
	const dict = [
'Please report any bug to','Veuillez signaler tout bug à',	
'Searching for the last backup file','Recherche de la dernière sauvegarde',
'Getting sound properties','Recherche des paramètres sonores',
//	
'!',' !',
// lang.js
'Lang is now','Langue courante',
'Object','L\'objet',
'has no translation','n\'a pas été traduit',
// import.js
'Error while uploading file','Erreur durant le chargement du fichier',
'Add to favorites','Ajouter aux favoris',
'Remove from favorites','Retirer des favoris',
'File uploaded','Fichier téléchargé',
// tab.resume.js
'Delete this contact','Effacer ce contact'
	];
	let offset = Object.keys(_Module._Manifest.obj.lang).indexOf(_Lang.locale);	
	let pos = dict.indexOf(txt);	
	if (pos!=-1) {
		return dict[pos + (offset<0 ? 0 : offset)];	
	} else {
		_Lang.object_not_found(txt);
		return txt;
	}
}
