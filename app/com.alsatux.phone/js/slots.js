// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Module slots
 */

var _Slots = {
	/**
	 * Register available routes
	 */
	init: () => {
		// management
		_Route.add('com.alsatux.phone:/calls/list', _Slots.handler);
		_Route.add('com.alsatux.phone:/call/added', _Slots.handler);
		_Route.add('com.alsatux.phone:/call/deleted', _Slots.handler);
		// active call		
		_Route.add('com.alsatux.phone:/call/state/changed', _Slots.handler); // from StateChanged notification
		_Route.add('com.alsatux.phone:/call/dtmf/incoming', _Slots.handler); // from DtmfReceived notification
		// extras
		_Route.add('com.alsatux.phone:/calls/contact/found', _Slots.handler);
		_Route.add('pp.core.sound:/callaudio/refresh', _Slots.handler);
	},	
	/**
	 * General handler
	 * @param  {object} data Datas from server
	 */
	handler: (data) => {
		if (data.status.code) {
			_Tab.spinner.hide();
		} else {
			let _Call = null;
			if (data.hasOwnProperty('args') && hasKey(data,'callback',String)) {
				if (hasKey(data.args,'properties',Object)) {
					switch (data.callback) {
						case 'pp.core.sound:/callaudio/refresh':
							let properties = data.args.properties;
							if (hasKey(data.args.properties,'SpeakerState',Number)) {
								_Tab.call.set_speaker_state(data.args.properties.SpeakerState);
							}
							if (hasKey(data.args.properties,'MicState',Number)) {
								_Tab.call.set_mic_state(data.args.properties.MicState);
							}
							break;
					}
				}
				if (hasKey(data.args,'call',Object)) {
					let call = data.args.call;
					switch (data.callback) {
						case 'com.alsatux.phone:/call/added': // from DBUS::CallAdded signal (incoming ou outgoing)
							_Call = _Module.tbl.calls.add(call);
							_Tab.call.add(_Call);
							_Tabs.open('tab_call');
							_Signals.search_for_contact(call.number);
							break;
						case 'com.alsatux.phone:/call/deleted':	// from DBUS::CallDeleted
							_Call = _Module.tbl.calls.get(call.uid);
							if (_Call !== null) { // present
								let div = _Tab.call.select(_Call);
								if (div !== null) { // in tabulation call
									let div = _Tab.history.select(_Call.obj);
										_Tab.call.del(_Call);
										if (div === null) { // but not in tabulation history
											_Tab.history.add(_Call);
										}
								}
							}
							break;
						case 'com.alsatux.phone:/call/state/changed':	// from DBUS::StateChanged
							_Call = _Module.tbl.calls.get(call.uid);
							if (_Call !== null) {
								_Call.upd(call);
								_Tab.call.set_state(_Call);							
							}
							break;
						case 'com.alsatux.phone:/call/dtmf/incoming':	// from DBUS::DtmfReceived
							if (data.args.hasOwnProperty('symbol') && data.args.symbol.constructor === String) {
								if ('0123456789ABCD*#'.indexOf(data.args.symbol) != -1) {
									_Call = _Module.tbl.calls.get(call.uid);
									if (_Call !== null) {
										if (_Call.obj.state_code==4) { // ACTIVE
											_Tab.call.show_dtmf(symbol);
										}
									}
								}
							}
							break;
					}
				}
				switch (data.callback) {
					case 'com.alsatux.phone:/calls/list': // on startup OR awake OR after websocket restart	
						_Module.tbl.calls.reset();
						let active_calls = false;
						if (hasKey(data.args,'calls',Object)) {
							for (let uid in data.args.calls) {
								let call = data.args.calls[uid];
								_Call = _Module.tbl.calls.add(call);
								if ('123456'.indexOf(call.state_code)!=-1) { // DIALING, RINGING_OUT, RINGING_IN, ACTIVE, HELD or WAITING
									_Tab.call.add(_Call);
									active_calls = true;
								}
								if ('07'.indexOf(call.state_code)!=-1) { // UNKNOWN or TERMINATED
									_Tab.history.add(_Call);
								}
							}
							_Tab.history.sort();							
							if (active_calls) {
								_Tabs.open('tab_call');
							}
							_Module.loaded();							
						}
						break;
					case 'com.alsatux.phone:/calls/contact/found':
						if (hasKey(data.args,'contacts',Array)) {
							data.args.contacts.forEach(cont => {
								_Module.core.echo.show_contact(cont);
							});
						}
						break;
				}
			}
		}
	}
};	