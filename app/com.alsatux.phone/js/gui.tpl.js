// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * GUI auto template translations
 */

_GUI._Tpl.set([
/* tab main */
'label_online_calls','Online calls','Appels actifs',
'label_calls_active','Active','Actifs',
/* tab history */
'label_history','History','Historique',
/* tab settings */
'label_settings','Settings','Réglages',
'label_setting_user_locale','Language','Langue',
'label_setting_server_websocket','Websocket','Websocket',
'websocket_host','Host ?','Hôte ?',
'websocket_port','Port ?','Port ?',
/* tab log */
'button_log_close','Close log','Fermer le journal'
]);
