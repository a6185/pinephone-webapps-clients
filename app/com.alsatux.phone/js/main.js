// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Main
 */

window.onload = () => {	
	_Module = new PP_Module('com.alsatux.phone');
	_Module.init();
	_Module.core = {
		'echo': new Echo()
	}
	_Module.tbl = {
		'calls': new Calls_Tbl(),
		'clock': new Clock_Tbl(),
		'timer': new Timer_Tbl()
	};
	_Module.ipc = (message) => {
		if (message.method == 'dial') {
			_Signals.dial(message.args.number);
		}
	};
	_Module._Manifest.read().then(result => {
		_Module.settings();
		// add event handlers
		_Tab.call.init();
		_Tab.history.init();
		_Tab.log.init();
		_Tab.main.init();
		_Tab.selector.init();
		_Tab.settings.init();
		_Tabs.init('tab_main');
		_Module.run(_Signals.start);
	}).catch(err => {
		alert(err);
	});
}
