// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Call object
 */

class Call_Object extends PP_Object {
	/**
	 * Constructor
	 */
	constructor () {
		super('call_object',{
			'uid': null,
			'dbus_path': null,
			'timestamp_start': null,
			'duration': null,
			'number': '',
			'direction': 0,
			'multiparty': '',
			'state_code': 0,
			'state': '',
			'reason_code': 0,
			'reason': '',
			'contact': {
				'uid': null,
				'fn': '',
				'ln': ''
			}	
		});
	}
	/**
	 * Update call object
	 * @param  {object} call Call object (JS object)
	 */
	upd (call) {
		this.obj = call;
	}
	/**
	 * Resume call datas depending of the tab open
	 * @param  {string} tab Tab name
	 * @return {string}     HTML code (div)
	 */
	to_html (tab) {
		let div = document.querySelector(`div[data-uid="${this.obj.uid}"]`);
		if (div !== null) {
			div.remove();
		}
		let number = this.obj.number.length ? this.obj.number : 'XXXXXXXXXX';
		let arrow = this.obj.direction==1 ? '&#x2198;' : '&#x2196;'; // 1 = incoming, 2 = outgoing
		div = document.createElement('div');
		div.classList.add('call');
		div.setAttribute('data-uid',this.obj.uid);
		let infos = '';
		let duration = '--:--:--';
		if (this.obj.duration !== null) {
			duration = (new Date(this.obj.duration)).toISOString().slice(11, 19);
		}
		if (tab == 'tab_history') {
			let date = (new Date(this.obj.timestamp_start));
			let locale = _Prefs_Tbl.find_by_cat_key('default','locale').cur;
			let timezone = _Prefs_Tbl.find_by_cat_key('default','timezone').cur;
			let time = date.toLocaleString(locale, {timeZone: timezone});
			infos = `
				<p><span class="call_start">${time}</span></p>`;
		} else { // tab_call
			infos = `
				<p><span class="call_clock">${duration}</span></p>
				<p>State: <span class="call_state">${this.obj.state}</span></p>`;			
		}
		let contact = '';
		if (this.obj.contact !== null) {
			let txt = [];
			if (this.obj.contact.fn.length) {
				txt.push(this.obj.contact.fn);
			}
			if (this.obj.contact.ln.length) {
				txt.push(this.obj.contact.ln);
			}
			contact = _GUI._HTML.escape(txt.join(' '));
		} else {
			contact = _GUI._HTML.escape('');
		}
		let num = this.obj.number.replace(/[^0-9]/,'');
		let bg = _Module.tbl.calls.bg[num];
		let bg2 = tab == 'tab_history' ? bg : 'bg_04';
		let caller = '';
		if (this.obj.contact !== null) {
			caller = `
					<button class="action big ${bg2}" data-action="edit" data-uid="${this.obj.contact.uid}" data-l10n-id="edit">\
						<span class="icon icon_edit_caller"></span>\
					</button>\			
			`;
		} else {
			caller = `
					<button class="action big ${bg2}" data-l10n-id="john_doe">
						<span class="icon icon_john_doe"></span>\
					</button>\
			`;			
		}
		if (tab == 'tab_history') {
			let recall = '';
			if (num.length) {
				recall = `
					<button class="action big ${bg}" data-action="recall" data-l10n-id="recall">\
						<span class="icon icon_recall"></span>\
					</button>\
				`;
			} else {
				recall = `
				`;			
			}
			div.innerHTML = `\
		<div class="call_actions" data-date="${this.obj.timestamp_start}">\
			<div class="row1">\
				<div class="col1">\
${caller}\
				</div>\
				<div class="col2">\			
					<p><span class="contact">${contact}</span></p>\
					<p><span class="number" data-number="${number}">${arrow} ${number}</span></p>\
${infos}\
				</div>\
				<div class="col3">\			
${recall}\
				</div>\			
			</div>\			
			`;
		} else {
			div.innerHTML = `\
		<div class="call_actions" data-date="${this.obj.timestamp_start}">\
			<div class="row2">\
				<p><span class="contact">${contact}</span></p>\
				<p><span class="number" data-number="${number}">${arrow} ${number}</span></p>\
${infos}\
			</div>\			
			<div class="row3 actions">\
				<div class="col31">\
${caller}\
				</div>\
				<div class="col32">\
					<button class="action big bg_06" data-action="accept" data-l10n-id="accept" style="display:none">\
						<span class="icon icon_accept"></span>\
					</button>\
					<button class="action big bg_08" data-action="deflect" data-l10n-id="deflect" style="display:none">\
						<span class="icon icon_deflect"></span>\
					</button>\
					<button class="action big bg_10" data-action="hold_and_accept" data-l10n-id="hold_and_accept" style="display:none">\
						<span class="icon icon_hold_and_accept"></span>\
					</button>\
					<button class="action big bg_12" data-action="hang_and_accept" data-l10n-id="hang_and_accept" style="display:none">\
						<span class="icon icon_hang_and_accept"></span>\
					</button>\
					<button class="action big bg_14" data-action="join" data-l10n-id="join" style="display:none">\
						<span class="icon icon_join"></span>\
					</button>\
					<button class="action big bg_16" data-action="leave" data-l10n-id="leave" style="display:none">\
						<span class="icon icon_leave"></span>\
					</button>\
					<button class="action big bg_18" data-action="hangup" data-l10n-id="hangup" style="display:none">
						<span class="icon icon_hangup"></span>\
					</button>\
				</div>\
			</div>\
		</div>\
		<div class="row4 deflect" style="display:none">\
			<p>
				<input type="text" name="deflect" class="deflect"/> 
				<button class="action big bg_20" data-action="deflect_confirm" data-l10n-id="deflect_confirm">
						<span class="icon icon_deflect_confirm"></span>\
				</button>\ 
				<button class="action big bg_20" data-action="deflect_cancel" data-l10n-id="deflect_cancel">\
						<span class="icon icon_deflect_cancel"></span>\
				</button>\ 
			</p>
		</div>`;
		}
		return div;
	}
	/**
	 * Set/unset buttons according to call state
	 * @param  {array} args Array of call states
	 */
	fix_call_buttons (...args) {
		// args: [div, accept, deflect, hold_and_accept, hang_and_accept, join, leave, hangup, recall]
		let div = args.shift();
		'accept,deflect,hold_and_accept,hang_and_accept,join,leave,hangup,recall'.split(',').forEach(action => {
			let value = args.shift();
			let button = div.querySelector(`[data-action="${action}"]`);
			if (button !== null) {
				button.style.display = value ? 'inline-block' : 'none';
			}
		});
		[...div.querySelectorAll(`div.row3 button.action, div.row4 button.action`)].forEach(button => {
			if (button.style.display=='none') {
				button.removeEventListener('click', _Tab.call.slot.action);
			} else {
				button.addEventListener('click', _Tab.call.slot.action);
			}
		});
	}
}
