// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Main tab
 */

_Tab.main = {
	/**
	 * Initialization
	 */
    init: () => {
		_GUI._DOM.listen('#button_settings_open', 'click', _Tab.settings.slot.open);
		_GUI._DOM.listen('#button_call_open', 'click', _Tab.call.slot.show);
		_GUI._DOM.listen('#button_history_open', 'click', _Tab.history.slot.show);
        _GUI._DOM.listen('#keypad-callbar-call-action', 'click', _Tab.main.slot.call);
		_GUI._DOM.listen('#clear_number', 'click', _Tab.main.slot.clear_number);
		[...document.querySelectorAll('.keypad-key')].forEach(key => {
			key.addEventListener('click',_Tab.main.slot.keypad);
		});
    },
	/**
	 * Slots (DOM events handlers)
	 */
    slot: {
		/**
		 * Open the tab
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
        show: (e) => {
            return _GUI._Lock.action(e, (e) => {
                _GUI._DOM.hide('#tab_settings');
                _GUI._DOM.show('#tab_main');
            });
        },
        /**
         * Dialer controller
         * @param  {event} e DOM event
		 * @return {function}   GUI lock action
         */
		keypad: (e) => {
            return _GUI._Lock.action(e, (e) => {
				let o = e.target.closest('.keypad-key');
				if (o !== null) {
					let symbol = o.getAttribute('data-value');
					_Module.core.echo.play_dtmf(symbol);
					let actives = _Module.tbl.calls.having('state_code',4);
					if (!actives.length) {
						_GUI._DOM.id('#number').value += symbol;
					} else {
						actives.forEach(_Call => {
							_Signals.play_dtmf(_Call.obj, symbol);
						});
					}
				}
            });
		},
		/**
		 * Begin the new call
         * @param  {event} e DOM event
		 * @return {function}   GUI lock action
         */
		call: (e) => {
            return _GUI._Lock.action(e, (e) => {
				let number = _GUI._DOM.id('#number').value;
				if (number.length) {
					_Signals.dial(number);
				}
            });
		},
		/**
		 * Clear the number HTML field
         * @param  {event} e DOM event
		 * @return {function}   GUI lock action
         */
		clear_number: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_GUI._DOM.id('#number').value = '';
			});
		}
	}
};
