// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Module signals
 */

var _Signals = {
	/**
	 * Ask for the SIM state on module startup
	 */
	start: () => {
		_Signals.get_state();
	},
	/**
	 * Retrieve the SIM state
	 */
	get_state: () => {
		_GUI._Log.info(tr1('Searching for the modem\'s state') + '... ');
		_WS.send({
			route: 'com.alsatux.sim:/read',
			args: {},
			callback: 'com.alsatux.sim:/state'
		});
	},	
	/**
	 * Send the PIN code to server side
	 * @param  {string} pin PIN code
	 */
	send_pin: (pin) => {
		_WS.send({
			route: 'com.alsatux.sim:/pin/send',
			args: {
				pin: pin
			}
		});		
	},
	/**
	 * Change PIN code
	 * @param  {string} old_pin Old PIN code
	 * @param  {string} new_pin New PIN code
	 */
	change_pin: (old_pin, new_pin) => {
		_WS.send({
			route: 'com.alsatux.sim:/pin/change',
			args: {
				old_pin: old_pin,
				new_pin: new_pin
			}
		});		
	},
	/**
	 * Enable PIN checking
	 * @param  {string} pin PIN code
	 */
	enable_pin_checking: (pin) => {
		_WS.send({
			route: 'com.alsatux.sim:/pin/checking/enable',
			args: {
				pin: pin
			}
		});		
	},
	/**
	 * Disable PIN checking
	 * @param  {string} pin PIN code
	 */
	disable_pin_checking: (pin) => {
		_WS.send({
			route: 'com.alsatux.sim:/pin/checking/disable',
			args: {
				pin: pin
			}
		});		
	},
	/**
	 * Unlock SIM using PUK code and new PIN
	 * @param  {string} puk     PUK code
	 * @param  {string} new_pin PIN code
	 */
	unlock: (puk, new_pin) => {
		_WS.send({
			route: 'com.alsatux.sim:/unlock',
			args: {
				puk: puk,
				new_pin: new_pin
			}
		});		
	}
};