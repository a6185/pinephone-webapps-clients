// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Dictionnary use in js code, according to manifest.json
 */

function tr1 (txt) {
	const dict = [
'is bad formatted','est mal formaté',
'message viewed','message vu',

'Please report any bug to','Veuillez signaler tout bug à',	
'Searching for the last backup file','Recherche de la dernière sauvegarde',
'Searching for the modem\'s state','Searching for the modem\'s state',
//	
'!',' !',
	]
	let offset = Object.keys(_Module._Manifest.obj.lang).indexOf(_Lang.locale);	
	let pos = dict.indexOf(txt);	
	if (pos!=-1) {
		return dict[pos + (offset<0 ? 0 : offset)];	
	} else {
		_Lang.object_not_found(txt);
		return txt;
	}
}
