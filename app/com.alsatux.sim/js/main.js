// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Main
 */

window.onload = () => {	
	_Module = new PP_Module('com.alsatux.sim');
	_Module.init();
	_Module.core = {
	}
	_Module._Manifest.read().then(result => {
		_Module.settings();
		// add event handlers
		_Tab.log.init();
		_Tab.settings.init();
		_Tabs.init('tab_main');
		_Tab.main.init();
		_Tab.selector.init();
		_Module.run(_Signals.start);
	}).catch(err => {
		alert(err);
	});
}
