// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Main tab
 */

_Tab.main = {
	/**
	 * Initialization
	 */
	init: () => {
		_GUI._DOM.listen('#button_settings_open', 'click', _Tab.settings.slot.open);
		_GUI._DOM.listen('#button_submit', 'click', _Tab.main.slot.submit);
		_GUI._DOM.listen('#actions li', 'click', _Tab.main.slot.change_radio);
		_Tab.main.change_radio(_GUI._DOM.id('#actions li[data-value="send_pin"]'));
	},
	/**
	 * Slots (DOM events handlers)
	 */
	slot: {
		/**
		 * Open the tab
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		show: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tabs.open('#tab_main');
			});
		},
		/**
		 * Choose an action
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		change_radio: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tab.main.change_radio(e.target);
			});
		},
		/**
		 * Submit the current action selected
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		submit: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tab.main.submit();
			});
		}		
	},
	/**
	 * Fill HTML fields using server datas
	 */
	refresh: () => {
		let properties = _Module.get('properties');
		for (let id in properties) {
			switch (id) {
				case 'SimIdentifier':
					_GUI._DOM.id('#value_identifier').innerHTML = _GUI._HTML.escape(properties[id]);
					break;
				case 'Imsi':
					_GUI._DOM.id('#value_imsi').innerHTML = _GUI._HTML.escape(properties[id]);
					break;
				case 'OperatorIdentifier':
					_GUI._DOM.id('#value_operator_id').innerHTML = _GUI._HTML.escape(properties[id]);
					break;
				case 'OperatorName':
					_GUI._DOM.id('#value_operator_name').innerHTML = _GUI._HTML.escape(properties[id]);
					break;
			}
		};
	},
	/**
	 * Select the current action
	 * @param  {object} li DOM LI element
	 */
	change_radio: (li) => {
		[...document.querySelectorAll("#actions li")].forEach(li2 => {
			if (li == li2) {
				li2.classList.add('selected');
			} else {
				li2.classList.remove('selected');
			}
		});
		let action = li.getAttribute('data-value');
		let pin = _GUI._DOM.id('#pin');
		let newpin = _GUI._DOM.id('#newpin');
		let puk = _GUI._DOM.id('#puk');			
		switch (action) {
			case "send_pin":
			case "enable_pin_checking":
			case "disable_pin_checking":
				_GUI._DOM.show('#pin');
				_GUI._DOM.hide('#newpin');
				_GUI._DOM.hide('#puk');
				break;
			case "change_pin":
				_GUI._DOM.show('#pin');
				_GUI._DOM.show('#newpin');
				_GUI._DOM.hide('#puk');
				break;
			case "unlock":
				_GUI._DOM.hide('#pin');
				_GUI._DOM.show('#newpin');
				_GUI._DOM.show('#puk');
				break;
		}
	},
	/**
	 * Submit the current form
	 */
	submit () {
		try {
			let pin = _GUI._DOM.id('#pin').value;
			let new_pin = _GUI._DOM.id('#newpin').value;
			let puk = _GUI._DOM.id('#puk').value;
			let action = _GUI._DOM.id('#actions li.selected').getAttribute('data-value');
			switch (action) {
				case "send_pin":
					_Signals.send_pin(pin);
					break;
				case "change_pin":
					_Signals.change_pin(pin,new_pin);
					break;
				case "unlock":
					_Signals.send_puk(puk, new_pin);
					break;
				case "enable_pin_checking":
					_Signals.enable_pin_checking(pin);
					break;
				case "disable_pin_checking":
					_Signals.disable_pin_checking(pin);
					break;
			}
		} catch (err) {
			alert(err);
		}
	}
};
