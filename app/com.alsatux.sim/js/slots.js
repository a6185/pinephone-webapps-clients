// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Module slots
 */

var _Slots = {
	/**
	 * Register available routes
	 */
	init: () => {
		_Route.add('com.alsatux.sim:/state', _Slots.state);
	},
	/**
	 * Update HTML form using server side changes
	 * @param  {object} data JS obejct
	 */
	state: (data) => {
		if (hasKey(data.args,'properties', Object)) {
			_Module.set('properties',data.args.properties);
			_Tab.main.refresh();
		}
	}
}