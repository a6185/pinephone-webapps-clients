// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * GUI auto template translations
 */

_GUI._Tpl.set([
/* tab main */
'pin','Current pin ?','Code PIN actuel ?',
'newpin','New pin ?','Nouveau code PIN ?',
'puk','Puk ?','Code PUK ?',
'label_operator_name','Operator name','Nom de l\'opérateur',
'label_operator_id','Operator ID','ID opérateur',
'label_imsi','IMSI','IMSI',
'label_sim_identifier','SIM identifier','Identifiant SIM',
'send_pin','Send PIN','Envoyer le code PIN',
'change_pin','Change PIN','Changer le code PIN',
'unlock','Unlock device with PUK + new PIN','Déverouillage PUK avec nouveau PIN',
'enable_pin_checking','Enable PIN checking','Activer le verrouillage PIN',
'disable_pin_checking','Disable PIN code check','Désactiver le verrouillage PIN',
'label_actions','Action ?','Action ?',
'label_button_submit','Send','Envoyer',
/* tab settings */
'label_settings','Settings','Réglages',
'label_setting_user_locale','Language','Langue',
'label_setting_server_websocket','Websocket','Websocket',
'websocket_host','Host ?','Hôte ?',
'websocket_port','Port ?','Port ?',
/* tab log */
'button_log_close','Close log','Fermer le journal'
]);
