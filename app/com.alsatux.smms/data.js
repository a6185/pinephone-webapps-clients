//# mmcli -s 0
//  -----------------------
//  General    | dbus path: /org/freedesktop/ModemManager1/SMS/0
//  -----------------------
//  Content    |    number: +33648991700
//             |      data: 00060401BEAF848C829850356F6F787A46714565755376345350615767676B41008D928918802B33333634383939313730302F545950453D504C4D4E0086818A808F818E0305D6DB83687474703A2F2F3231332E3232382E332E36302F6D6D732E7068703F50356F6F787A46714565755376345350615767676B41008805810303F480
//  -----------------------
//  Properties |  pdu type: deliver
//             |     state: received
//             |   storage: me
//             |      smsc: +33695000659
//             | timestamp: 2020-11-28T12:09:56+01:00
//             
// de https://github.com/heyman/mms-decoder/blob/master/mmsdecoder.php
// et de https://www.kannel.org/pipermail/devel/2007-March/000886.html
// et de https://alt.cellular.tech.narkive.com/olDwt8ui/mms-wap-push-m-notification-ind-pdu-issues
// et de http://read.pudn.com/downloads472/doc/comm/1978360/AT%E6%8C%87%E4%BB%A4/%E6%96%B0%E5%BB%BA%E6%96%87%E6%9C%AC%E6%96%87%E6%A1%A3.txt__.htm
var str="00060401BEAF848C829850356F6F787A46714565755376345350615767676B41008D928918802B33333634383939313730302F545950453D504C4D4E0086818A808F818E0305D6DB83687474703A2F2F3231332E3232382E332E36302F6D6D732E7068703F50356F6F787A46714565755376345350615767676B41008805810303F480";
var str2="
// WSP header
// ----------

// Transaction ID ?
00 

// PDU type (push) ?
06

// header length ?
04 

01 BE 

// WAP PUSH APPLICATION
AF 84 

// MMS Specific stuff
// ------------------

// X-Mms-Message-Type : m-notification-ind / 8C = MESSAGE_TYPE 82 = m-notification-ind
8C 82 

// 98 = TRANSACTION_ID
98 

// P5ooxzFqEeuSv4SPaWggkA // X-Mms-Transaction-Id : P5ooxzFqEeuSv4SPaWggkA
50 35 6F 6F 78 7A 46 71 45 65 75 53 76 34 53 50 61 57 67 67 6B 41 00

// X-Mms-Version: "1.2"
// 8D = MMS_VERSION, 92 = 1.2 mais il faut jouer avec les décalages 9 -> 1 et 2 -> 2...
8D 92 

// 89 = From
89 

// From Length to decimal + 2 (soit ici 24=22+2 sans compter le 80 suivant)
18 

// Address presents sinon on aurait du 81 et sûrement la passerelle SMSC pour cacher le numéro
80 

// +33648991700/TYPE=PLMN 22 chars + 0
2B 33 33 36 34 38 39 39 31 37 30 30 2F 54 59 50 45 3D 50 4C 4D 4E 00 

//
// 86 = DELIVERY_REPORT, 81 = 0
86 81 

// Message-Class: "Personal" 
// 8A = MESSAGE_CLASS, 80 = Personal
8A 80 

// 8F = Priority, 81 = Normal
8F 81 

// MESSAGE_SIZE ?
// 8E = Message size, 03 = un entier long sur 3 octets, 05 D6 DB = 382683o
8E 03 05 D6 DB 

// 83 = CONTENT_LOCATION
83 

// http://213.228.3.60/mms.php? 28 chars
68 74 74 70 3A 2F 2F 32 31 33 2E 32 32 38 2E 33 2E 36 30 2F 6D 6D 73 2E 70 68 70 3F 

// P5ooxzFqEeuSv4SPaWggkA 22 chars + 0
50 35 6F 6F 78 7A 46 71 45 65 75 53 76 34 53 50 61 57 67 67 6B 41 00 

// Expiry : (Value length 05 / DateValue 810303F480)
88 

// Expirity length
05

// 81 = relative format
81 

// 3 octets
03 

// 259200 seconds soit 3 jours
03 F4 80 "; 
// "¾¯P5ooxzFqEeuSv4SPaWggkA+33648991700/TYPE=PLMNÖÛhttp://213.228.3.60/mms.php?P5ooxzFqEeuSv4SPaWggkAô"
/*var myBuffer = [];
//var str = 'Stack Overflow';
var buffer = new Buffer(str, 'utf8');
for (var i = 0; i < buffer.length; i++) {
    myBuffer.push(buffer[i]);
}
console.log(myBuffer);
*/
//console.log(new TextDecoder().decode(data));
//let utf8Encode = new TextEncoder();
//console.log(utf8Encode.encode(str));
//function strToUtf8Bytes(str) {
//  const utf8 = [];
//  for (let ii = 0; ii < str.length; ii++) {
//    let charCode = str.charCodeAt(ii);
//    if (charCode < 0x80) utf8.push(charCode);
//    else if (charCode < 0x800) {
//      utf8.push(0xc0 | (charCode >> 6), 0x80 | (charCode & 0x3f));
//    } else if (charCode < 0xd800 || charCode >= 0xe000) {
//      utf8.push(0xe0 | (charCode >> 12), 0x80 | ((charCode >> 6) & 0x3f), 0x80 | (charCode & 0x3f));
//    } else {
//      ii++;
//      // Surrogate pair:
//      // UTF-16 encodes 0x10000-0x10FFFF by subtracting 0x10000 and
//      // splitting the 20 bits of 0x0-0xFFFFF into two halves
//      charCode = 0x10000 + (((charCode & 0x3ff) << 10) | (str.charCodeAt(ii) & 0x3ff));
//      utf8.push(
//        0xf0 | (charCode >> 18),
//        0x80 | ((charCode >> 12) & 0x3f),
//        0x80 | ((charCode >> 6) & 0x3f),
//        0x80 | (charCode & 0x3f),
//      );
//    }
//  }
//  return utf8;
//}
//console.log(strToUtf8Bytes(str));
//{
//  modem: '0',
//  dbus_path: '/org/freedesktop/ModemManager1/SMS/0',
//  data: Buffer(131) [Uint8Array] [
//      0,   6,   4,   1, 190, 175, 132, 140, 130, 152,  80,  53,
//    111, 111, 120, 122,  70, 113,  69, 101, 117,  83, 118,  52,
//     83,  80,  97,  87, 103, 103, 107,  65,   0, 141, 146, 137,
//     24, 128,  43,  51,  51,  54,  52,  56,  57,  57,  49,  55,
//     48,  48,  47,  84,  89,  80,  69,  61,  80,  76,  77,  78,
//      0, 134, 129, 138, 128, 143, 129, 142,   3,   5, 214, 219,
//    131, 104, 116, 116, 112,  58,  47,  47,  50,  49,  51,  46,
//     50,  50,  56,  46,  51,  46,  54,  48,  47, 109, 109, 115,
//     46, 112, 104, 112,
//    ... 31 more items
//  ],
//  delivery_report_request: 0,
//  class_3gpp: -1,
//  discharge_timestamp: '',
//  number: '+33648991700',
//  smsc: '+33695000659',
//  text: '',
//  timestamp: '2020-11-28T12:09:56+01:00',
//  validity: [ type: 0, value: [ [ { type: 'b', child: [] } ], [ false ] ] ],
//  delivery_state: 256,
//  message_reference: 0,
//  pdu_type: 1,
//  service_category: 0,
//  state: 3,
//  storage: 2,
//  teleservice_id: 0,
//  reason_code: ''
//}
//
//new TextDecoder().decode(new Uint8Array(message.data.data));
//"������P5ooxzFqEeuSv4SPaWggkA����+33648991700/TYPE=PLMN��������ۃhttp://213.228.3.60/mms.php?P5ooxzFqEeuSv4SPaWggkA����"
//﻿
//￼
//​
//let data = new Uint8Array(str);
//console.debug(data);
//console.log(new TextDecoder().decode(data));
//mmcli -s 0 --create-file-with-data=/path/to/the/output/file
//
//String.fromCharCode(...message.data.data)
// "¾¯P5ooxzFqEeuSv4SPaWggkA+33648991700/TYPE=PLMNÖÛhttp://213.228.3.60/mms.php?P5ooxzFqEeuSv4SPaWggkAô"
