// Jean Luc Biellmann - contact@alsatux.com

"use strict";

class SMS_Tbl extends PP_Tbl {
	constructor () {
		super();		
	}
	add (obj) {
		this.set(obj.uid,(new SMS_Object()).upd(obj));
	}
	del (obj) {
		this.unset(obj.uid);
	}
	sort_by_date_desc (number) {
		let m = [];
		for (let uid in this.obj) {
			m.push(this.obj[uid]);
		}
		// sort by date
		m.sort((a,b) => {
			return new Date(a.obj.timestamp)-new Date(b.obj.timestamp);
		});
		return m;		
	}
}