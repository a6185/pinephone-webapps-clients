// Jean Luc Biellmann - contact@alsatux.com

"use strict";

var _XML_Data = {
	debug: false,
	read: (text) => {
		var xml_doc;
		try {
			var str = text.replace('<?xml version="1.0" encoding="UTF-8"?>','');
			var parser = new DOMParser();
			xml_doc = parser.parseFromString(str, 'text/xml');
		} catch (err) {
			return _GUI._Log.error(tr1('Parsing XML failed') + ' : ' + err);
		}
		// read messages
		if (xml_doc.getElementsByTagName('message').length) {
			var value, m;
			var xml_messages = xml_doc.getElementsByTagName('message');
			//_GUI._Log.info('nbmessages=' + xml_messages.length);
			for (var i=0;i<xml_messages.length;i++) {
				try {
					let xml_message = xml_messages[i];
					let message = {
						date: xml_message.getElementsByTagName('date')[0].childNodes[0].nodeValue.nodeValue,
						number: xml_message.getElementsByTagName('number')[0].childNodes[0].nodeValue,
						text: xml_message.getElementsByTagName('text')[0].childNodes[0].nodeValue,
						text: xml_message.getElementsByTagName('viewed')[0].childNodes[0].nodeValue
					} 
					// debug
					if (_XML_Data.debug) {
						for (var key in message) {
							_GUI._Log.info(key + ': ' + message[key]);
						}
					}
					_SmsDB.chk(message);
					if (_XML_Data.debug)
						_GUI._Log.info(tr1('Creating new message') + '...');
					_SmsDB.add(message);
				} catch (err) {
					_GUI._Log.error(tr1('Reading message failed') + ' : ' + err);
					// continue (ignore errors)
				}
			}
		}
		_GUI._Log.info(tr1('Records found') + ': ' + Object.keys(_SmsDB.db).length);
	},
	write: () => {
		var markup, xml_doc, xml_settings, xml_element, xml_textnode;
		var message, xml_messages, xml_node, xml_rrule, xml_cats, xml_cat, xml_filters, xml_filter;
		markup = '<?xml version="1.0" encoding="UTF-8"?><coucou name="default"><messages></messages></coucou>';
		xml_doc = (new DOMParser()).parseFromString(markup, 'application/xml');
		// record messages
		xml_messages = xml_doc.getElementsByTagName('messages')[0];
		for (var seq in _SmsDB.db) {
			message = _SmsDB.db[seq];
			try {
				xml_node = xml_doc.createElement('message');
				xml_messages.appendChild(xml_node);
				// date
				xml_element = xml_doc.createElement('date');
				xml_node.appendChild(xml_element);
				xml_textnode = xml_doc.createTextNode(message.date);
				xml_element.appendChild(xml_textnode);
				// number
				xml_element = xml_doc.createElement('number');
				xml_node.appendChild(xml_element);
				xml_textnode = xml_doc.createTextNode(message.number);
				xml_element.appendChild(xml_textnode);
				// text
				xml_element = xml_doc.createElement('text');
				xml_node.appendChild(xml_element);
				xml_textnode = xml_doc.createTextNode(message.text);
				xml_element.appendChild(xml_textnode);
				// viewed
				xml_element = xml_doc.createElement('viewed');
				xml_node.appendChild(xml_element);
				xml_textnode = xml_doc.createTextNode(message.text);
				xml_element.appendChild(xml_textnode);
			} catch (err) {
				_GUI._Log.error(tr1('Recording message failed') + ' : ' + err);
			}
		}
		return (new XMLSerializer()).serializeToString(xml_doc);
	}
};

