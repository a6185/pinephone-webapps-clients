// Jean Luc Biellmann - contact@alsatux.com

"use strict";

_Tab.message = {
	number: null,
	init: () => {
		_GUI._DOM.listen('#button_message_close', 'click', _Tab.message.slot.hide);
		_GUI._DOM.listen('#messages_list', 'click', _Tab.message.slot.message);
		_GUI._DOM.listen('#button_join', 'click', _Tab.message.slot.join);
		_GUI._DOM.listen('#button_emotes', 'click', _Tab.message.slot.emotes);
		_GUI._DOM.listen('#button_submit', 'click', _Tab.message.slot.send);
		_GUI._DOM.listen('#files', 'change', _Tab.message.slot.join2);
		_GUI._DOM.listen('.contact', 'click', _Tab.message.slot.edit);
	},
	slot: {
		show: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tabs.open('tab_message');
			});
		},
		hide: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tabs.open('tab_main');
			});
		},
		emotes: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tabs.open('tab_emotes');
			});
		},
		join: (e) => {					
			//return _GUI._Lock.action(e, (e) => {
				_GUI._DOM.show('#files');
				_GUI._DOM.id('#files').click();
			//});
		},
		join2: async (e) => {					
			//return _GUI._Lock.action(e, (e) => {
				let maxsize = {
					image: 1.2*1024*1024, // 1.2MB
					pdf: 1.2*1024*1024, // 1.2MB
					txt: 1.2*1024*1024, // 1.2MB
					mp4: 3.5*1024*1024 // 3.5MB
				}
				let files = [..._GUI._DOM.id('#files').files];
				for (let i=0;i<files.length;i++) {
					let file = files[i];
					let opts = {};
					switch (file.type) {
						case 'image/jpeg':
						case 'image/jpg':
						case 'image/png':
							if (file.size>maxsize.image) {
								alert('Image > 1.2 MB !');
								continue;
							} else {
								opts = {
									min: {
										width:1,
										height:1
									},
									max: {
										width:1920,
										height:1080										
									}
								};
							}
							break;
						case 'text/plain':
							if (file.size>maxsize.txt) {
								alert('TXT > 1.2 MB !');
								continue;
							}
							break;
						case 'application/pdf':
							if (file.size>maxsize.pdf) {
								alert('PDF > 1.2 MB !');
								continue;
							}
							break;
						case 'video/mp4':
							if (file.size>maxsize.mp4) {
								alert('MP4 > 3.5 MB !');
								continue;
							}
							break;
					}
					let _Downloader = new Downloader(file, opts);;
					file.data = await _Downloader.readAsDataURL();
					let pos = file.data.indexOf(',');
					// base64 to hex2
					let data = [...atob(file.data.substr(pos+1,))].map(c=> c.charCodeAt(0).toString(16).toUpperCase().padStart(2,0))
					_Signals.send(_Tab.message.number, '', data);
				}
		},
		send: (e) => {
			let text = _GUI._DOM.id('#editor_text').value.trim();
			if (text.length) {
				_Signals.send(_Tab.message.number, text, null);
				_GUI._DOM.id('#editor_text').value = '';
			}
		},
		edit: (e) => {
			return _GUI._Lock.action(e, (e) => {
				let number = _Tab.message.number;
				if (_Module._DB[number] !== undefined) {
					if (_Module._DB[number].contact !== null) {
						_GUI._Window.open('com.alsatux.contacts');
						_Module._IPC.trigger('com.alsatux.contacts', {
							method: 'open',
							args: {
								uid: _Module._DB[number].contact.uid
							}
						});
					}
				}
			});			
		},
	},
	scroll_to_last: () => {
		_GUI._DOM.id('#messages_list2').parentNode.scrollTop = _GUI._DOM.id('#messages_list2').parentNode.scrollHeight;		
	},
	add: (message) => {
		let div = _Tab.message.to_html(message);
		_GUI._DOM.id('#messages_list2').append(div);
		_Tab.message.sort();
		_Tab.message.scroll_to_last();
	},
	sort: () => {
		let divs = [...document.querySelectorAll('div[data-timestamp]')];
		divs.sort((a,b) => {
			return new Date(a.getAttribute('data-timestamp'))-new Date(b.getAttribute('data-timestamp'));
		});
		divs.forEach(div => {
		_GUI._DOM.id('#messages_list2').append(div);
		});
	},
	to_html: (message) => {
		let d = _Message.to_locale_date(message.timestamp);
		let div = document.createElement('div');
		div.setAttribute('data-timestamp',message.timestamp);
		//div.setAttribute('data-number',message.number);
		div.setAttribute('data-uid',message.uid);
		div.classList.add('message2');
		if (message.received) {
			div.classList.add('received');	
		}
		if (message.viewed) {
			div.classList.add('viewed');	
		}
		let text = _GUI._HTML.escape(message.text).nl2br();
		div.innerHTML = `\			
		<p class="text"><span>${text}</span></p>\
		<p class="date"><span>${d.date} ${d.time}</span></p>\
		`;
		return div;
	},
	open: (number) => {
		try {
			let _Phone_Cleaner = new Phone_Cleaner(number);
			number = _Phone_Cleaner.clean();
			_Tab.message.number = number;
			_Message.set_bgcolor_for(number);
			_GUI._DOM.id('#messages_list2').innerHTML = '';
			// add new background color
			_GUI._DOM.id('#messages_icon').classList.add(_Module.bgcolor[number]);
			let contact = _Message.get_contact_fn_ln(number);			
			_GUI._DOM.id('#list_number').innerHTML = _GUI._HTML.escape(contact);
			if (_Module._DB[number] === undefined) {
				_Module._DB[number] = {
					contact: null,
					_SMS_Tbl: new SMS_Tbl()
				};
			}
			Object.values(_Module._DB[number]._SMS_Tbl.obj).forEach(_SMS_Object => {
				let div = _Tab.message.to_html(_SMS_Object.obj);
				_GUI._DOM.id('#messages_list2').appendChild(div);
			});
			_Tab.message.sort();
			_Tab.message.scroll_to_last();
		} catch (err) {
			_GUI._Log.error(tr1('Reading messages failed') + ' : ' + err);
			return false;
		}
		_Tab.message.scroll_to_last();
	}
};
