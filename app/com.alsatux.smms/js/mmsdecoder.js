// Version 0.81 //

// javascript translation : Jean Luc Biellmann - contact@alsatux.com

/**
 * Copyright (C) 2004-2009 Jonatan Heyman
 *
 * This file is part of the PHP application MMS Decoder.
 *
 * MMS Decoder is free software; you can redistribute it and/or
 * modify it under the terms of the Affero General Public License as
 * published by Affero, Inc.; either version 1 of the License, or
 * (at your option) any later version.
 *
 * MMS Decoder is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Affero General Public License for more details.
 *
 * You should have received a copy of the Affero General Public
 * License in the COPYING file that comes with The Affero Project; if
 * not, write to Affero, Inc., 510 Third Street, Suite 225, San
 * Francisco, CA 94107 USA.
 */


const DEBUG = 0;	/* Print parseerrors? Print values while they are parsed? If you enable this,
			   getting the binary encoded confirmation message whensending MMS from mobiles
			   will not work. This is only for development purpose. */


/*---------------------------------------------------*
 * Constants                                         *
 *                                                   *
 * http://wapforum.org/                              *
 * WAP-209-MMSEncapsulation-20020105-a               *
 * Table 8                                           *
 *                                                   *
 * The values are enconded using WSP 7bit encoding.  *
 * Read more about how to decode this here:          *
 * http://www.nowsms.com/discus/messages/12/3287.html*
 *                                                   *
 * Example from the above adress:                    *
 * 7Bit 0D =  0001101                                *
 * 8Bit 0D = 10001101 = 8D                           *
 *---------------------------------------------------*/

const BCC = '81';
const CC = '82';
const CONTENT_LOCATION = '83';
const CONTENT_TYPE = '84';
const DATE = '85';
const DELIVERY_REPORT = '86';
const DELIVERY_TIME = '87';
const EXPIRY = '88';
const FROM = '89';
const MESSAGE_CLASS = '8A';
const MESSAGE_ID = '8B';
const MESSAGE_TYPE = '8C';
const MMS_VERSION = '8D';
const MESSAGE_SIZE = '8E';
const PRIORITY = '8F';
const READ_REPLY = '90';
const REPORT_ALLOWED = '91';
const RESPONSE_STATUS = '92';
const RESPONSE_TEXT = '93';
const SENDER_VISIBILITY = '94';
const STATUS = '95';
const SUBJECT = '96';
const TO = '97';
const TRANSACTION_ID = '98';

/*--------------------------*
 * Array of header contents *
 *--------------------------*/
var mmsMessageTypes = {
	'80': 'm-send-req',
	'81': 'm-send-conf',
	'82': 'm-notification-ind',
	'83': 'm-notifyresp-ind',
	'84': 'm-retrieve-conf',
	'85': 'm-acknowledge-ind',
	'86': 'm-delivery-ind',
	'00': null
};

/*--------------------------*
 * Some other useful arrays *
 *--------------------------*/
var mmsYesNo = {
	'80': 1,
	'81': 0,
	'00': null
};

var mmsPriority = {
	'80': 'Low',
	'81': 'Normal',
	'82': 'High',
	'00': null
};

var mmsMessageClass = {
	'80': 'Personal',
	'81': 'Advertisement',
	'82': 'Informational',
	'83': 'Auto'
};

var mmsContentTypes =  {
	'00': '*/*',
	'01': 'text/*',
	'02': 'text/html',
	'03': 'text/plain',
	'04': 'text/x-hdml',
	'05': 'text/x-ttml',
	'06': 'text/x-vCalendar',
	'07': 'text/x-vCard',
	'08': 'text/vnd.wap.wml',
	'09': 'text/vnd.wap.wmlscript',
	'0A': 'text/vnd.wap.wta-event',
	'0B': 'multipart/*',
	'0C': 'multipart/mixed',
	'0D': 'multipart/form-data',
	'0E': 'multipart/byterantes',
	'0F': 'multipart/alternative',
	'10': 'application/*',
	'11': 'application/java-vm',
	'12': 'application/x-www-form-urlencoded',
	'13': 'application/x-hdmlc',
	'14': 'application/vnd.wap.wmlc',
	'15': 'application/vnd.wap.wmlscriptc',
	'16': 'application/vnd.wap.wta-eventc',
	'17': 'application/vnd.wap.uaprof',
	'18': 'application/vnd.wap.wtls-ca-certificate',
	'19': 'application/vnd.wap.wtls-user-certificate',
	'1A': 'application/x-x509-ca-cert',
	'1B': 'application/x-x509-user-cert',
	'1C': 'image/*',
	'1D': 'image/gif',
	'1E': 'image/jpeg',
	'1F': 'image/tiff',
	'20': 'image/png',
	'21': 'image/vnd.wap.wbmp',
	'22': 'application/vnd.wap.multipart.*',
	'23': 'application/vnd.wap.multipart.mixed',
	'24': 'application/vnd.wap.multipart.form-data',
	'25': 'application/vnd.wap.multipart.byteranges',
	'26': 'application/vnd.wap.multipart.alternative',
	'27': 'application/xml',
	'28': 'text/xml',
	'29': 'application/vnd.wap.wbxml',
	'2A': 'application/x-x968-cross-cert',
	'2B': 'application/x-x968-ca-cert',
	'2C': 'application/x-x968-user-cert',
	'2D': 'text/vnd.wap.si',
	'2E': 'application/vnd.wap.sic',
	'2F': 'text/vnd.wap.sl',
	'30': 'application/vnd.wap.slc',
	'31': 'text/vnd.wap.co',
	'32': 'application/vnd.wap.coc',
	'33': 'application/vnd.wap.multipart.related',
	'34': 'application/vnd.wap.sia',
	'35': 'text/vnd.wap.connectivity-xml',
	'36': 'application/vnd.wap.connectivity-wbxml',
	'37': 'application/pkcs7-mime',
	'38': 'application/vnd.wap.hashed-certificate',
	'39': 'application/vnd.wap.signed-certificate',
	'3A': 'application/vnd.wap.cert-response',
	'3B': 'application/xhtml+xml',
	'3C': 'application/wml+xml',
	'3D': 'text/css',
	'3E': 'application/vnd.wap.mms-message',
	'3F': 'application/vnd.wap.rollover-certificate',
	'40': 'application/vnd.wap.locc+wbxml',
	'41': 'application/vnd.wap.loc+xml',
	'42': 'application/vnd.syncml.dm+wbxml',
	'43': 'application/vnd.syncml.dm+xml',
	'44': 'application/vnd.syncml.notification',
	'45': 'application/vnd.wap.xhtml+xml',
	'46': 'application/vnd.wv.csp.cir',
	'47': 'application/vnd.oma.dd+xml',
	'48': 'application/vnd.oma.drm.message',
	'49': 'application/vnd.oma.drm.content',
	'4A': 'application/vnd.oma.drm.rights+xml',
	'4B': 'application/vnd.oma.drm.rights+wbxml'
};

// character set (mibenum numbers by IANA, ored with '80')
var mmsCharSet = {
	'EA': 'utf-8',
    '83': 'ASCII', // ascii
    '84': 'iso-8859-1',
    '85': 'iso-8859-2',
    '86': 'iso-8859-3',
    '87': 'iso-8859-4'
 };

String.prototype.to_uint = function () {
	let uint = parseInt(this,16);
	if (DEBUG) console.log('to_uint: code=' + this + ' / uint=' + uint);
	return uint;
}

/*-------------------------------*
 * The MMS header decoding class *
 *-------------------------------*/
class MMSDecoder {

	constructor (data) {
		if (data.indexOf('AF84')!=-1) {
			this.data = data.replace(/.*AF84/,''); // AF 84 = WAP PUSH APPLICATION
		} else {
			this.data = data;	// The unparsed MMS data in an array of the ascii numbers
		}
		this.data_original = data;
		this.pos = 0;	// The current parsing position of the data array
		this.PARTS = [];

		// The parsed data will be saved in these variables

		this.mms = {
			BBC: null,
			CC: null,
			CONTENTLOCATION: null,
			CONTENTTYPE: null,
			DATE: null,
			DELIVERYREPORT: null,
			DELIVERYTIME: null,
			EXPIRY: null,
			FROM: null,
			MESSAGECLASS: null,
			MESSAGEID: null,
			MESSAGETYPE: null,
			MMSVERSIONMAJOR: null,
			MMSVERSIONMINOR: null,
			MESSAGESIZE: null,
			PRIORITY: null,
			READREPLY: null,
			REPORTALLOWED: null,
			RESPONSESTATUS: null,
			RESPONSETEXT: null,
			SENDERVISIBILITY: null,
			STATUS: null,
			SUBJECT: null,
			TO: null,
			TRANSACTIONID: null,
			MMSVERSIONRAW: null, 	// used for the m-send-conf (confirmation answer)
			CONTENTTYPE_PARAMS: null	// parameter-values for the MMS content-type
		};
	}

	// This function is called when the data is to be parsed
	parse () {
		try {
			// Reset position
			this.pos = 0;
			// parse the header
			while (this.parseHeader());
			// Header done, fetch parts, but make sure the header was parsed correctly
			if (this.mms.CONTENTTYPE == 'application/vnd.wap.multipart.related' || this.mms.CONTENTTYPE == 'application/vnd.wap.multipart.mixed')
				while (this.parseParts());
			else
				return 0;
			return 1;
		} catch (e) {
			console.log(e);
			return 1;
		}
	}

	cur_byte () {
		if (this.data.length<2) {
			return null;
		}
		return this.data.substr(0,2);
	}

	read_byte () {
		if (this.data.length<2) {
			return null;
		}
		let byte = this.data.substr(0,2);
		this.data = this.data.substr(2);
		this.debug('pos: ' + this.pos + ' / byte = ' + byte + ' / remaining length = ' + this.data.length);
		this.pos+=2;
		return byte;
	}

	chr (code) { // '43' -> '+'
		return String.fromCharCode(code);
	}

	ord (char) { // '+' -> 43
		return char.chatCodeAt(0);
	}

	/*---------------------------------------------------*
	 * This function checks what kind of field is to be  *
	 * parsed at the moment                              *
	 *                                                   *
	 * If true is returned, the class will go on and     *
	 * and continue decode the header. If false, the     *
	 * class will end the header decoding.               *
	 *---------------------------------------------------*/
	parseHeader () {

		this.debug('parseHeader');
		let byte = this.read_byte();

		if (byte === null)
			return 0;

		switch (byte) {
			case BCC:
				this.mms.BBC = this.parseEncodedStringValue();
				this.debug('BCC', this.mms.BBC);
				break;
			case CC:
				this.mms.CC = this.parseEncodedStringValue();
				this.debug('CC', this.mms.CC);
				break;
			case CONTENT_LOCATION:
				this.mms.CONTENTLOCATION = this.parseTextString();
				this.debug('Content-location', this.mms.CONTENTLOCATION);
				break;
			case CONTENT_TYPE:
				if (byte.to_uint() <= 31) { /* Content-general-form */
					len = this.parseValueLength();

					// check if next byte is in range of 32-127. Then we have a Extension-media which is a textstring
					if (byte.to_uint() > 31 && byte.to_uint() < 128)
						this.mms.CONTENTTYPE = this.parseTextString();
					else {
						// we have Well-known-media; which is an integer
						this.mms.CONTENTTYPE = mmsContentTypes[this.parseIntegerValue()];
					}
				} else if (byte.to_uint() < 128) { /* Constrained-media - Extension-media*/
					this.mms.CONTENTTYPE = this.parseTextString();
				} else /* Constrained-media - Short Integer*/
					this.mms.CONTENTTYPE = mmsContentTypes[this.parseShortInteger()];

				let next_byte = this.data.substr(0,2);
				// Ok, now we have parsed the content-type of the message, let's see if there are any parameters
				let noparams = false;
				while (!noparams) {
					switch (next_byte) {
						case '89': // Start, textstring
							this.read_byte();
							this.parseTextString();
							break;
						case '8A': // type, constrained media
							byte = this.read_byte();
							if (byte.to_uint() < 128) { /* Constrained-media - Extension-media*/
								byte = this.read_byte();
								this.parseTextString();
							} else // Constraind-media Short Integer
								this.mms.CONTENTTYPE_PARAMS[type] = this.parseShortInteger();
							break;
						default:
							noparams = 1;
							break;
					}
				}

				this.debug('Content-type', this.mms.CONTENTTYPE);

				// content-type parsed, that means we have reached the end of the header
				return 0;

			case DATE: /* In seconds from 1970-01-01 00:00 GMT */
				this.mms.DATE = this.parseDate();
				this.debug('Date', this.mms.DATE);
				break;
			case DELIVERY_REPORT:		/* Yes | No */
				this.mms.DELIVERYREPORT = mmsYesNo[ this.read_byte() ];
				this.debug('Delivery-report', this.mms.DELIVERYREPORT);
				break;
			case DELIVERY_TIME:
				this.debug('Delivery-time', this.mms.DELIVERYTIME);
				break;
			case EXPIRY:
				this.mms.EXPIRY = this.parseDate();
				this.debug('Expiry', this.mms.EXPIRY);
				break;
			case FROM:
				/**
				 * TODO: make better encoding for this field
				 * The encoding mechanism works like this:
				 *  From-value = ['01' or VALUE-length] ['80' or '81'] [Optional: Encoded-String-Value]
				 * If we have '80' we have an encoded-string-value ('80' is part of that string).
				 * If we have '81' this is a insert-adress-token which means that the MMSC is supposed
				 * to insert it, which means that we can't retrieve the sender.
				 */

				this.mms.FROM = this.parseEncodedStringValue();
				this.debug('From', this.mms.FROM);
				break;
			case MESSAGE_CLASS:
				this.mms.MESSAGECLASS = mmsMessageClass[ this.parseMessageClassValue() ];
				this.debug('Message-class', this.mms.MESSAGECLASS);
				break;
			case MESSAGE_ID:		/* Text string */
				this.mms.MESSAGEID = this.parseTextString();
				this.debug('Message-id', this.mms.MESSAGEID);
				break;
			case MESSAGE_TYPE:
				this.mms.MESSAGETYPE = mmsMessageTypes[ this.read_byte() ];
				this.debug('Message-type', mmsMessageTypes[this.mms.MESSAGETYPE]);
				break;
			case MMS_VERSION:
				/**
				 * The version number (1.0) is encoded as a WSP short integer, which
				 * is a 7 bit value.
				 *
				 * The three most significant bits (001) are used to encode a major
				 * version number in the range 1-7. The four least significant
				 * bits (0000) contain a minor version number in the range 1-14.
				 */
				this.mms.MMSVERSIONRAW = this.read_byte();
				let version = this.mms.MMSVERSIONRAW.to_uint();
				this.mms.MMSVERSIONMAJOR = (version & '0x70') >> 4;
				this.mms.MMSVERSIONMINOR = (version & '0x0F');
				this.debug('MMS-version', this.mms.MMSVERSIONMAJOR + '.' + this.mms.MMSVERSIONMINOR);
				break;
			case MESSAGE_SIZE:		/* Long integer */
				this.mms.MESSAGESIZE = this.parseLongInteger();
				this.debug('Message-size', this.mms.MESSAGESIZE);
				break;
			case PRIORITY:			/* Low | Normal | High */
				this.mms.PRIORITY = mmsPriority[ this.read_byte() ];
				this.debug('Priority', this.mms.PRIORITY);
				break;
			case READ_REPLY:		/* Yes | No */
				this.mms.READREPLY = mmsYesNo[ this.read_byte() ];
				this.debug('Read-reply', this.mms.READREPLY);
				break;
			case REPORT_ALLOWED:		/* Yes | No */
				this.mms.REPORTALLOWED = mmsYesNo[ this.read_byte() ];
				this.debug('Report-allowed', this.mms.REPORTALLOWED);
				break;
			case RESPONSE_STATUS:
				this.mms.RESPONSESTATUS = this.read_byte();
				this.debug('Response-status', this.mms.RESPONSESTATUS);
				break;
			case RESPONSE_TEXT:		/* Encoded string value */
				this.mms.RESPONSETEXT = this.parseEncodedStringValue();
				this.debug('Response-text', this.mms.RESPONSETEXT);
				break;
			case SENDER_VISIBILITY:		/* Hide | show */
				this.mms.SENDERVISIBILITY = mmsYesNo[ this.read_byte() ];
				this.debug('Sender-visibility', this.mms.SENDERVISIBILITY);
				break;
			case STATUS:
				this.mms.STATUS = this.read_byte();
				this.debug('Status', this.mms.STATUS);
				break;
			case SUBJECT:
				this.mms.SUBJECT = this.parseEncodedStringValue();
				this.debug('Subject', this.mms.SUBJECT);
				break;
			case TO:
				this.mms.TO = this.parseEncodedStringValue();
				this.debug('To', this.mms.TO);
				break;
			case TRANSACTION_ID:
				this.mms.TRANSACTIONID = this.parseTextString();
				this.debug('Transaction-id', this.mms.TRANSACTIONID);
				break;
			default:
				if (byte.to_uint() > 127) {
					this.debug('Parse error:', 'Unknown field (' + byte + ') !', this.pos-2);
				} else {
					this.debug('Parse error:', 'Value encountered when expecting field !', this.pos-2);
				}
				break;
		}

		return true;
	}

	parseDate() {
		// Total of bytes to read
		let octetcount = this.read_byte();
		// Error checking
		if (octetcount.to_uint() > 30)
			throw('Parse error: Short-length-octet (' + octetcount + ') > 30 in Long-integer at offset ' + this.pos-2 + '!\n');
		let token = this.read_byte();
		let longint = '';
		let value = this.parseLongInteger();
		if (token == '81') { // relative-token
			this.debug('Date relative: ' + value + ' s.');
			return {
				type: 'seconds',
				value: value
			};
		}
		if (token == '80') { // absolute-token
			this.debug('Date absolute: ' + date('Y-m-d- H:i:s',value) + '.');
			return {
				type: 'date',
				value: value
			};
		}
		throw('Parse error: unknown date at offset ' + this.pos-2 + '!\n');
	}

	/*-------------------------------------------------------------------*
	 * Parse message-class                                               *
	 * message-class-value = Class-identifier | Token-text               *
	 * Class-idetifier = Personal | Advertisement | Informational | Auto *
	 *-------------------------------------------------------------------*/
	parseMessageClassValue() {
		if (this.cur_byte().to_uint() > 127) {
			// the byte is one of these 128=personal, 129=advertisement, 130=informational, 131=auto
			return this.read_byte();
		} else
			return this.parseTextString();
	}

	/*----------------------------------------------------------------*
	 * Parse Text-string                                              *
	 * text-string = [Quote <Octet 127>] text [End-string <Octet 00>] *
	 *----------------------------------------------------------------*/
	parseTextString() {
		let str = '';
		// Remove quote
		if (this.cur_byte() == '7F') {
			this.read_byte();
		}

		while (this.cur_byte() != '00') {
			//str += this.chr(this.read_byte());
			str += String.fromCharCode(parseInt(this.read_byte(),16));
		}
		this.read_byte();
		return str;
	}


	/*------------------------------------------------------------------------*
	 * Parse Encoded-string-value                                             *
	 *                                                                        *
	 * Encoded-string-value = Text-string | Value-length Char-set Text-string *
	 *                                                                        *
	 *------------------------------------------------------------------------*/
	parseEncodedStringValue() {
		if (this.cur_byte().to_uint() <= 31) {
			let len = this.parseValueLength();

			let mibenum = this.read_byte().to_uint();

			let charset = '';
			// handle unknown charsets
			if (Object.keys(mmsCharSet).indexOf(mibenum)!=-1) {
				charset = mmsCharSet[mibenum];
			}

			let raw = this.parseTextString();

			// the only case we can handle currently is utf8 since character encoding support
			// in native PHP is so lousy
			if (charset == 'utf-8')
			    raw = new TextDecoder('utf-8').decode(raw);

			return raw;

			//for (i = 0; i < len-1; i++)
			//	str .= chr( this.data[this.pos++] );
			//return str;
		} else
			return this.parseTextString();
	}


	/*--------------------------------------------------------------------------------*
	 * Parse Value-length                                                             *
	 * Value-length = Short-length<Octet 0-30> | Length-quote<Octet 31> Length<Uint>  *
	 *                                                                                *
	 * A list of content-types of a MMS message can be found here:                    *
	 * http://www.wapforum.org/wina/wsp-content-type.htm                              *
	 *--------------------------------------------------------------------------------*/
	parseValueLength() {
		let byte = this.cur_byte().to_uint();
		if (byte < 31) {
			// it's a short-length
			return this.read_byte();
		} else if (byte == 31) {
			// got the quote, length is an Uint
			this.read_byte();
			return this.parseUint();
		} else {
			// uh, oh... houston, we got a problem
			throw('Parse error: Short-length-octet (' + this.cur_byte() + ') > 31 in Value-length  at offset ' + this.pos + '!\n');
		}
	}


	/*--------------------------------------------------------------------------*
	 * Parse Long-integer                                                       *
	 * Long-integer = Short-length<Octet 0-30> Multi-octet-integer<1*30 Octets> *
	 *--------------------------------------------------------------------------*/
	parseLongInteger() {
		let byte = this.cur_byte();
		// Get the number of octets which the long-integer is stored in
		let octetcount = this.read_byte().to_uint();

		// Error checking
		if (octetcount > 30)
			throw('Parse error: Short-length-octet (' + byte + ') > 30 in Long-integer at offset ' + this.pos-2 + '!\n');

		// Get the long-integer
		let longint = '';
		for (let i = 0; i < octetcount; i++) {
			longint += this.read_byte();
		}

		return parseInt(longint,16);
	}


	/*------------------------------------------------------------------------*
	 * Parse Short-integer                                                    *
	 * Short-integer = OCTET                                                  *
	 * Integers in range 0-127 shall be encoded as a one octet value with the *
	 * most significant bit set to one, and the value in the remaining 7 bits *
	 *------------------------------------------------------------------------*/
	parseShortInteger() {
		return this.read_byte().to_uint() & '0x7F';
	}


	/*-------------------------------------------------------------*
	 * Parse Integer-value                                         *
	 * Integer-value = short-integer | long-integer                *
	 *                                                             *
	 * This function checks the value of the current byte and then *
	 * calls either parseLongInt() or parseShortInt() depending on *
	 * what value the current byte has                             *
	 *-------------------------------------------------------------*/
	parseIntegerValue() {
		if (this.cur_byte().to_uint() < 31) {
			return this.parseLongInteger();
		} else if (this.cur_byte().to_uint() > 127) {
			return this.parseShortInteger();
		} else {
			this.debug('ERROR', 'Not a IntegerValue field', this.pos);
			this.read_byte();
			return 0;
		}
	}


	/*------------------------------------------------------------------*
	 * Parse Unsigned-integer                                           *
	 *                                                                  *
	 * The value is stored in the 7 last bits. If the first bit is set, *
	 * then the value continues into the next byte.                     *
	 *                                                                  *
	 * http://www.nowsms.com/discus/messages/12/522.html                *
	 *------------------------------------------------------------------*/
	parseUint() {
		if (!(this.cur_byte().to_uint() & '0x80')) {
			return this.read_byte().to_uint() & '0x7F';
		}

		let uint = 0;

		while (this.cur_byte().to_uint() & '0x80') {
			// Shift the current value 7 steps
			uint = uint << 7;
			// Remove the first bit of the byte and add it to the current value
			uint |= this.read_byte().to_uint() & '0x7F';
		}

		// Shift the current value 7 steps
		uint = uint << 7;
		// Remove the first bit of the byte and add it to the current value
		uint |= this.read_byte().to_uint() & '0x7F';

		return uint;
	}


	/*---------------------------------------*
	 * Function which outputs debug messages *
	 *---------------------------------------*/
	debug (name, str, pos = -1, errorlevel = 0) {
		if (!DEBUG) {
			return true;
		}

		let out = '';
		if (pos != -1) {
			out += '<b>' + name + ' (' + pos +'):</b> ' + str;
		} else {
			out += '<b>' + name + ':</b> ' + str;
		}

		out += '<br>\n';

		console.log(out);

		if (errorlevel > 0) {
			throw 'Fatal error !';
		}
	}


	/*---------------------------------------------------------------------*
	 * Function called after header has been parsed. This function fetches *
	 * the different parts in the MMS. Returns true until it encounter end *
	 * of data.                                                            *
	 *---------------------------------------------------------------------*/
	parseParts() {
		let i,j,ctype;

		if (this.pos>=this.data.length) {
			return 0;
		}

		// get number of parts
		let count = this.parseUint();

		this.debug('MMS parts', count);

		for (i = 0; i < count; i++) {
			// new part, so clear the old data and header
			let data = '';
			let header = '';

			// get header and data length
			let headerlen = this.parseUint();
			let datalen = this.parseUint();


			/* PARSE CONTENT-TYPE */
			// this is actually the same structure as in the MMS content-type
			// so maybe we should make this in a better way, but for now, I'll
			// just cut n paste

			// right now I just save the position in the MMS data array before I parse
			// the content-type, to be able to roll back after it has been parsed beacause
			// the headerlen includes both the content-type and the header
			// TODO: this is just a fast hack and shoul be done in a more proper way
			let ctypepos = this.pos;

			if (cur_byte().to_uint() <= 31) { /* Content-general-form */
				// the value follows after the current byte and is 'current byte' long
				
				let len = this.parseValueLength();

				// ???? No sens for the following... JLB
				// check if next byte is in range of 32-127. Then we have a Extension-media which is a textstring
				if (cur_byte().to_uint() > 31 && cur_byte().to_uint() < 128)
					ctype = this.parseTextString();
				else {
					// we have Well-known-media; which is an integer
					ctype = mmsContentTypes[this.parseIntegerValue()];
				}
			} else if (cur_byte().to_uint() < 128) { /* Constrained-media - Extension-media*/
				read_byte();
				ctype = this.parseTextString();
			} else /* Constrained-media - Short Integer */
				ctype = mmsContentTypes[this.parseShortInteger()];

			// roll back position so it's just before the content-type again
			this.pos = ctypepos;
			/* END OF CONTENT TYPE */

			// Read header. Actually, we don't do anything with this yet.. just skipping it (note that the content-type is included in the header)
			for (j = 0; j < headerlen; j++)
				header += chr(read_byte());

			// read data
			for (j = 0; j < datalen; j++)
				data += chr(read_byte());

			this.debug('Part (i):headerlen', headerlen);
			this.debug('Part (i):datalen', datalen);
			this.debug('Part (i):content-type', ctype);
			//this.debug('Part (i):data', data); // I've commented this one, to get a cleaner debug

			this.PARTS.push(new MMSPart(headerlen, datalen, ctype, header, data));
		}

		return false;
	}

	/**
	 * Send an OK response to the sender after the MMS has been recieved
	 * See '6.1.2. Send confirmation' in the wap-209-mmsencapsulation specification, on how this is constructed
	 */

	confirm() {
		let str = '';
		str += '8C'; // message-type
		str += '81'; // m-send-conf
		str += '98'; // transaction-id
		str += this.mms.TRANSACTIONID.toString(16);
		str += '00'; // end of string
		str += '8D'; // version
		str += '90'; // 1.0
		str += '92'; // response-status
		str += '80'; // OK
		str += '8b'; // Message-id
		// generate a message id based on the time
		str += Date.now().toString(16)
		str += '00'; // end of string
		// respond with the m-send-conf
		return str;
	}
}


/*---------------------------------------------------------------------*
 * The MMS part class                                                  *
 * An instance of this class contains the one parts of an MMS message. *
 *                                                                     *
 * The multipart type is formed as:                                    *
 * number |part1|part2|....|partN                                      *
 * where part# is formed by headerlen|datalen|contenttype|headers|data *
 *---------------------------------------------------------------------*/
class MMSPart {

	/*----------------------------------*
	 * Constructor, just store the data *
	 *----------------------------------*/
	MMSPart(headerlen, datalen, ctype, header, data) {
		this.hpos = 0;
		this.headerlen = headerlen;
		this.DATALEN = datalen;
		this.CONTENTTYPE = ctype;
		this.DATA = data;
	}

	/*-------------------------------------*
	 * Save the data to a location on disk *
	 *-------------------------------------*/
	save(filename) {
		/*
		fp = fopen(filename, 'wb');
		fwrite(fp, this.DATA);
		fclose(fp);
		*/
	}
}
