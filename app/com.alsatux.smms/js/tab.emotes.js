// Jean Luc Biellmann - contact@alsatux.com

"use strict";

_Tab.emotes = {
	init: () => {
		_GUI._DOM.listen('#button_emotes_close', 'click', _Tab.emotes.slot.hide);
		_GUI._DOM.listen('#emotes_body', 'click', _Tab.emotes.slot.emotes);
	},
	slot: {
		show: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tabs.open('tab_emotes');
			});
		},
		hide: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tabs.open('tab_message');
			});
		},		
		emotes: (e) => {
			if (e.target.nodeName == 'SPAN') {
				_GUI._DOM.id('#editor_text').value += e.target.innerHTML;
			}
		}
	}	
}