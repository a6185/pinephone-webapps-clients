// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Main
 */

window.onload = () => {	
	_Module = new PP_Module('com.alsatux.smms');
	_Module.init();
	_Module.core = {
	}
	/*_Module.tbl = {
		'calls': new Calls_Tbl(),
		'clock': new Clock_Tbl(),
		'timer': new Timer_Tbl()
	};*/
	_Module._DB = {}
	_Module.bgcolor = {}
	_Module.ipc = (message) => {
		if (message.method == 'create') {
			_Tabs.open('tab_message');
			_Tab.message.open(message.args.number);
		}
	};
	_Module._Manifest.read().then(result => {
		_Module.settings();
		// add event handlers
		_Tab.log.init();
		_Tab.main.init();
		_Tab.message.init();
		_Tab.emotes.init();
		_Tab.selector.init();
		_Tab.settings.init();
		_Tabs.init('tab_main');
		_Module.run(_Signals.start);
	}).catch(err => {
		alert(err);
	});
}
