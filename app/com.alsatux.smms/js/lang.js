// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Dictionnary use in js code, according to manifest.json
 */

function tr1 (txt) {
	const dict = [
'is bad formatted','est mal formaté',
'message viewed','message vu',

'Please report any bug to','Veuillez signaler tout bug à',	
'Searching for the last backup file','Recherche de la dernière sauvegarde',
//	
'!',' !',
// eventdb.js
'An message object is missing','Un objet message est manquant',
'The field','Le champ',
'date','date',
'number','numéro',
'text','text',
'viewed','vu',
'is missing','est manquant',
'Wrong message date','Date illisible',
// lang.js
'Lang is now','Langue courante',
'Object','L\'objet',
'has no translation','n\'a pas été traduit',
// import.js
'Error while uploading file','Erreur durant le chargement du fichier',
'File uploaded','Fichier téléchargé',
// xml.data.js
'Parsing XML failed','L\'analyse XML a échoué',
'Creating new message','Création d\'un nouveau message',
'Reading messages failed','La lecture des messages a échoué',
'Records found','Engistrements trouvés',
	]
	let offset = Object.keys(_Module._Manifest.obj.lang).indexOf(_Lang.locale);	
	let pos = dict.indexOf(txt);	
	if (pos!=-1) {
		return dict[pos + (offset<0 ? 0 : offset)];	
	} else {
		_Lang.object_not_found(txt);
		return txt;
	}
}
