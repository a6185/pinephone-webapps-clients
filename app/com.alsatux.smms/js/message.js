// Jean Luc Biellmann - contact@alsatux.com

"use strict";

var _Message = {
	set_bgcolor_for: (number) => {
		if (!_Module.bgcolor.hasOwnProperty(number)) {
			/* css */
			let n = Math.floor(1 + Math.random()*20);
			if (n<10) {
				n = '0' + n;
			}
			_Module.bgcolor[number] = 'bg_' + n;			
		}		
	},
	get_contact_fn_ln: (number) => {
		if (_Module._DB[number] === undefined) {
			return number;
		}
		let contact = _Module._DB[number].contact;
		if (contact === null) {
			return number;	
		}
		let c = [];
		c.push(contact.fn);
		c.push(contact.ln);
		return c.join(' ');
	},
	to_locale_date: (date) => {
		let d = new Date(date);
		let now = new Date();
		let diff = now-d;
		if (diff<86400*1000) {
			return {
				date : '',
				time: d.toTimeString().substr(0,8)
			}
		}
		if (diff<86400*1000*7) {
			return {
				date : d.toLocaleDateString('fr-FR', {weekday: 'short'}),
				time: d.toTimeString().substr(0,8)
			}
		}
		return {
			date: d.toLocaleDateString(_Lang.locale.replace('_','-')),
			time: d.toTimeString().substr(0,8)
		};
	}
}

