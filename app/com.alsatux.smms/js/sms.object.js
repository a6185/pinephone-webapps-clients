// Jean Luc Biellmann - contact@alsatux.com

'use strict';

class SMS_Object extends PP_Object {
	constructor (_Module) {
		super(_Module,'sms_object');
		this.obj = {
			uid: null,
			text: '',
			parts: [],
			timestamp: null,
			received: null
		};
	}
	debug (level, msg) {
		global._Console.debug(level,`<sms_object>${msg}</sms_object>`);
	}
}