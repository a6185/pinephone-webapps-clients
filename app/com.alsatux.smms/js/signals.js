// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Module signals
 */

var _Signals = {
	/**
	 * Retrieve last backup file from server side
	 */
	start: () => {
		_GUI._Log.info(tr1('Searching for the last backup file') + '... ');
		_Signals.refresh();
	},
	/**
	 * Refresh MMS list
	 */
	refresh: () => {
		_WS.send({
			route: 'com.alsatux.smms:/read',
			args: {},
			callback: 'com.alsatux.smms:/list'
		});
	},
	/**
	 * Create a new SMS/MMS
	 * @param  {string} number Contact number
	 * @param  {string} text   Text (if SMS)
	 * @param  {object} data   MMS parts
	 */
	send: (number, text, data) => {
		_WS.send({
			route: 'com.alsatux.smms:/add',
			args: {
				number: number,
				text: text,
				data: data
			}
		});		
	},
	/**
	 * Get all messages for a specific numbert
	 * @param  {string} number Contact number
	 */
	get_all_messages_for: (number) => {
		_WS.send({
			route: 'com.alsatux.smms:/get',
			args: {
				number: number
			},
			callback: 'com.alsatux.smms:/get'
		});				
	}
}