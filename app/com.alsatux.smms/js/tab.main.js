// Jean Luc Biellmann - contact@alsatux.com

"use strict";

_Tab.main = {
	init: () => {
		_GUI._DOM.listen('#button_settings_open', 'click', _Tab.settings.slot.open);
		_GUI._DOM.listen('#button_contacts', 'click', _Tab.main.slot.contacts);
		_GUI._DOM.listen('#button_new', 'click', _Tab.main.slot.new_number);
		_GUI._DOM.listen('#messages_list1', 'click', _Tab.main.slot.open);
		_GUI._DOM.listen('#selector_kws', 'keyup', _Tab.main.slot.filter);
		_GUI._DOM.listen('#selector_clear_kws', 'click', _Tab.main.slot.filter_clear);
	},
	slot: {
		show: (e) => {
			return _GUI._Lock.action(e, (e) => {
				_Tabs.open('tab_main');
			});
		},
		open: (e) => {
			return _GUI._Lock.action(e, (e) => {
				let o = e.target.closest('.message');
				if (o!=null) {
					o.classList.remove('not_viewed');
					o.classList.add('viewed');
					let number = o.getAttribute('data-number');
					_Signals.get_all_messages_for(number);
				}
			});
		},
		contacts: (e) => {
			window.open('https://mobian/app/com.alsatux.contacts','com.alsatux.contacts');
		},
		filter: (e) => {
			return _GUI._Lock.action(e, (e) => {
				let kws = _GUI._DOM.id('#selector_kws').value.trim();
				let divs = [...document.querySelectorAll('#messages_list1 div.message')];
				if (!kws.length) {
					divs.forEach(div => {
						div.style.display = 'block';
					});
				} else {
					let re = new RegExp(kws + '.*','ig');
					divs.forEach(div => {
						let found = false;
						[...div.querySelectorAll('span')].forEach(span => {
							if (re.test(span.innerHTML)) {
								found = true;
							}
						});
						div.style.display = found ? 'block': 'none';
					});
				}
			});			
		},
		filter_clear: (e) => {
			// filter will lock...
			_GUI._DOM.id('#selector_kws').value = '';
			_Tab.main.slot.filter(e);
		},
		new_number: (e) => {
			return _GUI._Lock.action(e, (e) => {
				let number = window.prompt('New number ?','+');
				if (number.length && number.match(/^\+[0-9]+$/)) {
					if (hasKey(_Module._DB,number,Object)) {
						_Signals.get_all_messages_for(number);
					} else {
						_Tabs.open('tab_message');
						_Tab.message.open(number);
					}
				} else {
					alert('Number should be given in the international format ! (starting with +)');
				}
			});			
		}
	},
	upd: () => {		
		_GUI._DOM.id('#messages_list1').innerHTML = '';
		let last = [];
		for (let number in _Module._DB) {
			let contact = _Message.get_contact_fn_ln(number);
			let m = _Module._DB[number]._SMS_Tbl.sort_by_date_desc(number);			
			//let m = _Tab.main.sort_by_date_desc(number);
			last.push({
				number: number,
				contact: contact,
				message: m[0] // last message only
			});
		}
		last.sort((a,b) => {
			return new Date(b.message.obj.timestamp)-new Date(a.message.obj.timestamp);
		});
		last.forEach(message => {
			_Tab.main.resume(message);
		});
	},
	resume: (data) => {
		let text = data.message.obj.text;
		if (text.length) { // sms
			// do not tranlate br in message list !
			data.resume = text.cut_to_words(41);
			_Tab.main.to_html(data); 								
		} else { // mms
			/* TODO */
		}
	},
	to_html: (data) => {
		let message = data.message.obj;
		let d = _Message.to_locale_date(message.timestamp);
		let div = document.createElement('div');
		let contact = _GUI._HTML.escape(data.contact);
		let resume = _GUI._HTML.escape(data.resume);
		div.setAttribute('data-number',data.number);
		div.setAttribute('data-uid',message.uid);
		div.classList.add('message');
		div.classList.add(message.viewed ? 'viewed' : 'not_viewed');
		div.innerHTML = `\
			<div class="col1">\
				<div class="big ${_Module.bgcolor[data.number]}">\
					<span class="icon icon_people"></span>\
				</div>\
			</div>\
			<div class="col2">\
				<p><span class="number bold">${contact}</span><span class="date">${d.date} ${d.time}</span></p>\
				<p><span class="text">${resume}</span></p>\
			</div>\
		`;
		_GUI._DOM.id('#messages_list1').appendChild(div);
	}
};
