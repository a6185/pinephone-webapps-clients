// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Module slots
 */

var _Slots = {
	/**
	 * Register available routes
	 */
	init: () => {
		_Route.add('com.alsatux.smms:/list', _Slots.handler);
		_Route.add('com.alsatux.smms:/get', _Slots.handler);
		_Route.add('com.alsatux.smms:/added', _Slots.handler);
		_Route.add('com.alsatux.smms:/deleted', _Slots.handler);
	},
	/**
	 * Handle actions
	 * @param  {object} data JS object returns by server
	 */
	handler: (data) => {
		if (data.hasOwnProperty('args') && hasKey(data,'callback',String)) {
			switch (data.callback) {
				case 'com.alsatux.smms:/list': // on startup OR awake OR after websocket restart
					if (hasKey(data.args,'messages',Object)) {
						_Slots.list(data.args.messages);
					}
					break;
				case 'com.alsatux.smms:/get':
					if (hasKey(data.args,'number',String) && hasKey(data.args,'messages',Object) && data.args.hasOwnProperty('contact')) { // contact can be null
						_Slots.get(data.args.number, data.args.messages, data.args.contact);
					}
					break;
				case 'com.alsatux.smms:/added':
					if (hasKey(data.args,'number',String) && hasKey(data.args,'message',Object) && data.args.hasOwnProperty('contact')) { // contact can be null
						_Slots.added(data.args.number, data.args.message, data.args.contact);
					}
					if (hasKey(data.args,'messages',Object)) {
						_Slots.list(data.args.messages);
					}
					break;
			}
		}
		_Tab.spinner.hide();
	},

	list: (messages) => {
		for (let number in messages) {
			if (!hasKey(_Module._DB, number, Object)) {
				_Module._DB[number] = {
					contact: null,
					_SMS_Tbl: new SMS_Tbl()
				};
			}
			let resume = messages[number];
			if (resume.hasOwnProperty('contact')) { // can be null !
				_Module._DB[number].contact = resume.contact;
			}
			if (hasKey(resume,'last',Object)) {
				_Module._DB[number]._SMS_Tbl.add(resume.last);
			}
			_Message.set_bgcolor_for(number);
		}
		_Tab.main.upd();
		_Module.loaded();
	},
	added: (number, message, contact) => {
		if (!hasKey(_Module._DB,'number',Object)) {
			_Module._DB[number] = {
				contact: null,
				_SMS_Tbl: new SMS_Tbl()
			};
		}
		_Module._DB[number].contact = contact;		
		_Module._DB[number]._SMS_Tbl.add(message);
		_Tab.main.upd();
		_Tabs.open('tab_message');
		if (_Tab.message.number != number) {
			_Tab.message.open(number);
		} else {
			_Tab.message.add(message);
		}
	},
	get: (number, messages, contact) => {
		_Module._DB[number] = {
			contact: contact,
			_SMS_Tbl: new SMS_Tbl()
		};
		for (let uid in messages) {
			_Module._DB[number]._SMS_Tbl.add(messages[uid]);
		}
		_Tabs.open('tab_message');
		_Tab.message.open(number);
	}
}