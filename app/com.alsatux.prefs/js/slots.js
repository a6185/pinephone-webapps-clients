// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Module slots
 */

var _Slots = {
	/**
	 * Register available routes
	 */
	init: () => {
		// cf. core/autoload/com.alsatux.prefs/prefs.slots.js
		// don't delete this empty function !
		// (always called in main.js)
	}
};
