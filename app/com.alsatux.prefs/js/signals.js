// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Module signals
 */

var _Signals = {
	/**
	 * Load prefs from server
	 */
	start: () => {
		/**
		 * Fix HTML object using current prefs
		 * @param  {[type]} let uid           in _Prefs_Tbl.obj [description]
		 * @return {[type]}     [description]
		 */
		for (let uid in _Prefs_Tbl.obj) {
			let obj = _Prefs_Tbl.obj[uid];
			_Tab.main.set_span(obj);
		}
	},
	/**
	 * Write a pref to server
	 * @param  {string} cat Category
	 * @param  {string} key Key
	 * @param  {string} cur Current/New value
	 */
	rec: (cat, key, cur) => {
		_WS.send({
			route: 'com.alsatux.prefs:/write',
			args: {
				cat: cat,
				key: key,
				cur: [cur]
			}
		});
	}
};