// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * GUI auto template translations
 */

_GUI._Tpl.set([
// tab.main.js
'label_default_locale','Locale ?','Langue ?',
'label_default_timezone','Timezone ?','Fuseau horaire ?',
'label_default_currency','Currency ?','Monnaie ?',
'label_default_phoneprefix','Phone prefix ?','Préfixe téléphonique ?',
'label_default_date_pattern','Date format ?','Format des dates ?',
'label_default_separator_real','Real separator ?','Séparateur des nombres réels ?',
/* tab settings */
'label_settings','Settings','Réglages',
'label_setting_user_locale','Language','Langue',
'label_setting_server_websocket','Websocket','Websocket',
'websocket_host','Host ?','Hôte ?',
'websocket_port','Port ?','Port ?',
/* tab log */
'button_log_close','Close log','Fermer le journal'
]);
