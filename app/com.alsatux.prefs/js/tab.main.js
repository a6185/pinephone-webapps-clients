// Jean Luc Biellmann - contact@alsatux.com

"use strict";

/**
 * Main tab
 */

_Tab.main = {
	/**
	 * Initialization
	 */
	init: () => {
		_GUI._DOM.listen('#button_settings_open', 'click', _Tab.settings.slot.open);
		[...document.querySelectorAll('div.pref')].forEach(div => {
			[...div.querySelectorAll('span')].forEach(span => {				
				span.addEventListener('click', _Tab.main.slot.upd);
			});
		});
	},
	/**
	 * Slots (DOM events handlers)
	 */
	slot: {
		/**
		 * Update a pref
		 * @param  {event} e DOM event
		 * @return {function}   GUI lock action
		 */
		upd: (e) => {
			return _GUI._Lock.action(e, (e) => {
				var o = e.target;
				if (o.nodeName && o.nodeName=='SPAN') {
					var p = o.parentNode;
					var uid = p.getAttribute('data-uid');
					var obj = _Prefs_Tbl.obj[uid];
					var cat = p.getAttribute('data-cat');
					var key = p.getAttribute('data-key');
					// call the selector
					_Tab.selector.slot.open('tab_main', o, null, (keys,values) => {
						// we only record the keys, not the translated values !
						obj.cur = keys;
						_Tab.main.set_span(obj);
						// update datas to server
						_Signals.rec(cat, key, keys[0]);
					});
				}
			});
		},
	},
	/**
	 * Set a HTML pref element
	 * @param  {object} obj Pref object (JS object)
	 */
	set_span: (obj) => {
		let div = document.querySelector('#tab_main div[data-cat="' + obj.cat + '"][data-key="' + obj.key +'"]');
		if (div == null) {
			return console.log(`<settings>Cat "${obj.cat}" with key "${obj.key}" not found ?!</settings>`);
		}
		div.setAttribute('data-uid',obj.uid);	
		let options = _GUI._DOM.id('#' + div.getAttribute('data-options'));
		let span = div.querySelector('span');
		obj.cur.forEach(val => {
			span.setAttribute('data-value',val);
			span.textContent = options.querySelector('span[data-value="' + val + '"]').textContent;
		});
	}
};


