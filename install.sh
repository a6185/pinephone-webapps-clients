#!/bin/bash

# for convenience...
if [[ ! " $(groups mobian) " =~ ' www-data ' ]]; then
	adduser mobian www-data
fi

chown -R mobian:www-data /var/www/html/app
find /var/www/html/app -type d -print0|xargs -0iX chmod 770 X
find /var/www/html/app -type f -print0|xargs -0iX chmod 660 X

