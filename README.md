# Pinephone webapps clients

## Contains

- HTML5/JS webapps clients to place in /var/www/html. 
- Can be used on the pinephone, or on your external desktop/laptop ! (need the pinphone to be connected using a RJ45/ethernet adapter)

## Required

- Pinephone Webapps Server from the same repository (to install first, BEFORE this client side)
- A pinephone with Debian GNU/Linux 12 / Mobian
- Apache2 + PHP + Firefox installed

## Install (with admin privileges)

```
git clone https://gitlab.com/a6185/pinephone-webapps-clients.git
mv pinephone-webapps-clients/* /var/www/html/
sudo -s
./install.sh
```

## Licence please ?

All code is available under GPLv2, instead of a few lines code coming from outside world, when i'm not the author.

## Certificates please !

Pages and websocket use HTTPS only. So you need to import the CA certificate (/var/www/html/app/certs/CA_Alsatux.crt) into your browser on the first attempt, or you will have every time an annoying security warning !). 

The main access URL is https://mobian/app.

## Known bugs with epiphany (2022 - not retested since)

When disconnected, my pinephone doesn't find mobian host anymore with Epiphany browser only. I checked with an Epiphany dev if this is a bug of Epiphany or Mobian, but we still didn't found the cause. So please tell me if you have the same behaviour.

## Questions:

### While not using JQuery ?

Because it's heavy and since ECMAScript6, the old advantages of JQuery are no more. 

### I want to write a new module. Do I need a Settings tab ?

Settings are now integrated as a main default tab, including websocket and language by default, so you just need to create your settings.js with your extras parameters.

## Available webapps: 

- Cactus (calendar)
- Cockatoo (contacts)
- SIM (to unlock SIM card)
- StatsCars (cars management)
- User preferences (locale/timezone/currency/phone prefix/date format/...) - used in all others modules

- Echo (phone) without options:
  - "deflect": interface ok but got the error "Deflect failed for /org/freedesktop/ModemManager1/Call/X - error: DBusError: Cannot run sequence: 'Could not open serial device ttyUSB2: it has been forced close)" - did modem really supports deflect ?
  - "multiparty": not tested
  - using callaudio still not audible on destination side sometimes (this problem of mic and echo levels are definitily not acceptable for a smartphone - and sadly still no DBus interfaces for pulseaudio)

- SMMS (sms and mms management)
  - global prefixes integrated (cf. app/com.alsatux.prefs.user/prefs.user.js on server side) but my problem here is i still don't know exactly how people using international prefixes outside of France, so you have to complete prefix management in app/core/js/phone.cleaner.js for your own country !
  - Then SMS are ok (tested for french users) with hundreds of UTF8 emoticons available !
  - MMS still not working:
  	- sending: got error "Couldn't write SMS part: QMI protocol error (54): 'WmsCauseCode'" with firmware EG25GGBR07A08M2G_01.001.01.001 on pinephone 1.2 (minicom -D /dev/ttyUSB2 then AT+QGMR / dmesg|grep Model). I tried 'AT+QCFG="ims",0' then 'AT+QCFG="ims",1' then 'AT+QCFG="ims",2' but no better result. If you have any idea...
	- MMS reception: not tested: need people to send me MMS, so i could test reception and continue this part ! 

## The future ?

I would like spending more time on this project, but it use a lot of time, and since the begining i'm all alone... So consider a little donation if you want to help me.
And of course, if you want to participate, you sure are welcome !
